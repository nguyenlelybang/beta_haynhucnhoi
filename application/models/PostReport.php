<?php

class PostReport
{
    const _TABLE_REPORT = 'posts_reports';

    public static function initReport($data)
    {
        $rs     = array();
        $fields = array(
            'report_id', 
            'post_id', 
            'user_id', 
            'time', 
            'ip', 
            'reason', 
            'reason_detail'
            );
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }
    public static function insert($data)
    {
        //Init data
        $data = self::initReport($data);
        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE_REPORT, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('PostReport::insert - ' . $ex->getMessage());
            return false;
        }
    }

    public static function delete($reportId)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->query('DELETE FROM ' . self::_TABLE_REPORT . ' WHERE report_id = ' . $reportId);
            if ($rs) {

            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('PostReport::delete - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getList($filters = array(), $offset = 0, $limit = 25, $options = array('order_by' => 'report_id desc'))
    {
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                    ->from(array('r' => self::_TABLE_REPORT), '*')
                    ->joinLeft(array('p' => Post::_TABLE), 'p.post_id = r.post_id', array('story', 'is_actived', 'is_approved', 'phase'))
                    ->joinLeft(array('m' => 'members'), 'm.user_id = p.user_id', array('fullname', 'rank', 'like_count', 'comment_count', 'facebookid'))
                    ->limit($limit, $offset);

            if (isset($filters['post_id'])) {
                if (is_numeric($filters['post_id'])) {
                    $select->where('r.post_id = ?', $filters['post_id']);
                } elseif (is_array($filters['category_id'])) {
                    $select->where('r.post_id IN (?)', $filters['post_id']);
                }
            }

            if (isset($filters['user_id'])) {
                if (is_numeric($filters['user_id'])) {
                    $select->where('r.user_id = ?', $filters['user_id']);
                } elseif (is_array($filters['category_id'])) {
                    $select->where('r.user_id IN (?)', $filters['user_id']);
                }
            }

            if (isset($filters['from_date'])) {
                $select->where('r.time >= ' . $filters['from_date']);
            }

            if (isset($options['order_by'])) {
                $select->order($options['order_by']);
            }

            $data = $storage->fetchAll($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('PostReport::getList - ' . $ex->getMessage());

            return false;
        }
        return $data;
    }

    public static function countTotal($filters = array())
    {
        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                    ->from(array('r' => self::_TABLE_REPORT), 'count(report_id) as total');

            if (isset($filters['post_id'])) {
                if (is_numeric($filters['post_id'])) {
                    $select->where('r.post_id = ?', $filters['post_id']);
                } elseif (is_array($filters['category_id'])) {
                    $select->where('r.post_id IN (?)', $filters['post_id']);
                }
            }

            if (isset($filters['user_id'])) {
                if (is_numeric($filters['user_id'])) {
                    $select->where('r.user_id = ?', $filters['user_id']);
                } elseif (is_array($filters['category_id'])) {
                    $select->where('r.user_id IN (?)', $filters['user_id']);
                }
            }

            if (isset($filters['from_date'])) {
                $select->where('r.time >= ' . $filters['from_date']);
            }

            $data = $storage->fetchRow($select);

            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('PostReport::countTotal - ' . $ex->getMessage());

            return false;
        }
        return $data;
    }
}
