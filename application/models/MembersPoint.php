<?php

class MembersPoint {

    const TABLE_NAME    ='members_point';
    const TYPE_UPLOAD_PICTURES          =   'type_picture';
    const TYPE_UPLOAD_PICTURES_POINTS   =   5;

    const TYPE_UPLOAD_VIDEOS            =   'type_videos';
    const TYPE_UPLOAD_VIDEOS_POINTS     =   3;

    const TYPE_MOVE_TO_HOME             =   'type_home';
    const TYPE_MOVE_TO_HOME_POINTS      =   5;

    const TYPE_MOVE_TO_HOT              =   'type_hot';
    const TYPE_MOVE_TO_HOT_POINTS       =   10;

    const TYPE_CATEGORY         =   'type_category';
    const TYPE_CATEGORY_POINTS  =   1;

    const TYPE_LIKE_50          =   'type_like50';
    const TYPE_LIKE_50_RULE     =   50;
    const TYPE_LIKE_50_POINT    =   5;

    const TYPE_LIKE_100         =   'type_like100';
    const TYPE_LIKE_100_RULE    =   100;
    const TYPE_LIKE_100_POINT   =   10;

    const TYPE_LIKE_300         =   'type_like300';
    const TYPE_LIKE_300_RULE    =   300;
    const TYPE_LIKE_300_POINT   =   45;

    const TYPE_LIKE_500         =   'type_like500';
    const TYPE_LIKE_500_RULE    =   500;
    const TYPE_LIKE_500_POINT   =   75;

    const TYPE_LIKE_1000        =   'type_like1000';
    const TYPE_LIKE_1000_RULE   =   1000;
    const TYPE_LIKE_1000_POINT  =   150;

    const TYPE_LIKE_2000        =   'type_like2000';
    const TYPE_LIKE_2000_RULE   =   2000;
    const TYPE_LIKE_2000_POINT  =   300;

    const TYPE_LIKE_3000        =   'type_like3000';
    const TYPE_LIKE_3000_RULE   =   3000;
    const TYPE_LIKE_3000_POINT  =   450;

    const TYPE_LIKE_5000        =   'type_like5000';
    const TYPE_LIKE_5000_RULE   =   5000;
    const TYPE_LIKE_5000_POINT  =   750;

    const TYPE_SHARE            =   'type_share';
    const TYPE_SHARE_RULE       =   1;
    const TYPE_SHARE_POINT      =   1;

    const TYPE_VIEW            =   'type_view';
    const TYPE_VIEW_RULE       =   1000;
    const TYPE_VIEW_POINT      =   5;

    const TYPE_COMMENT          =   'type_comment';
    const TYPE_COMMENT_RULE     =   10;
    const TYPE_COMMENT_POINT    =   1;

    const TYPE_FOLLOW           =   'type_follow';
    const TYPE_FOLLOW_RULE      =   10;
    const TYPE_FOLLOW_POINT     =   1;

    const MSG_500               =   "Số điểm của bạn từ 0 - 500đ: Số bài đăng mỗi ngày";
    const MSG_999               =   "Số điểm của bạn từ 500 - 999đ: Tối đa 2 bài / ngày, không quá 1 clip / ngày.";
    const MSG_2999              =   "Số điểm của bạn từ 1000 - 2999đ: Tối đa 4 bài / ngày, không quá 2 clip / ngày";
    const MSG_4999              =   "Số điểm của bạn từ 3000 - 4999đ: Tối đa 4 bài / ngày, đăng tự do.";
    const MSG_5000              =   "Số điểm của bạn từ 5000đ: Tối đa 5 bài / ngày";
    const MSG_LIKE              =   "Bạn đã Like.";
    const MSG_POST              =   "Bạn đã post.";
    const MSG_VIEW              =   "Bạn đã xem.";

    public static function initPost($data)
    {
        $fields = array(
            'id',
            'user_id',
            'type_point',
            'points',
            'time_point_datetime',
            'post_id',
            'msg'
        );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function insert($data)
    {
        $data = self::initPost($data);
        if ($data === false) {
            return false;
        }
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::TABLE_NAME, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getMemberPoint($user_id,$type_point,$post_id)
    {
        if (empty($user_id) || empty($type_point) || empty($post_id) ) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::TABLE_NAME;
        $select  = $storage->select()
            ->from($table, '*')
            ->where('user_id = ?', (int)$user_id)
            ->where('type_point = ?', $type_point)
            ->where('post_id = ?', (int)$post_id)
            ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if(!empty($row))
        {
            $data   =   $row;
        }
        return $data;
    }

    public  static function getMessage($type = '')
    {
        $msg  = '';
        switch($type)
        {
            case    'type_picture':
                $msg    ='Hình ảnh của bạn được duyệt +'.self::TYPE_UPLOAD_PICTURES_POINTS.' điểm';
                break;

            case    'type_videos':
                $msg    ='Video của bạn được duyệt +'.self::TYPE_UPLOAD_VIDEOS_POINTS.' điểm';
                break;

            case    'type_home':
                $msg    ='Bài được đưa ra Trang chủ +'.self::TYPE_MOVE_TO_HOME_POINTS.' điểm';
                break;

            case    'type_hot':
                $msg    ='Bài được đưa vào Hay Nhức Nhói +'.self::TYPE_MOVE_TO_HOT_POINTS.' điểm';
                break;

            case    'type_category':
                $msg    ='Bài viêt của bạn chọn vào trong category +'.self::TYPE_CATEGORY_POINTS.' điểm';
                break;

            case    'type_like50':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_50_RULE.' like  +'.self::TYPE_LIKE_50_POINT.' điểm';
                break;

            case    'type_like100':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_100_RULE.' like  +'.self::TYPE_LIKE_100_POINT.' điểm';
                break;

            case    'type_like300':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_300_RULE.' like  +'.self::TYPE_LIKE_300_POINT.' điểm';
                break;

            case    'type_like500':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_500_RULE.' like  +'.self::TYPE_LIKE_500_POINT.' điểm';
                break;

            case    'type_like1000':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_1000_RULE.' like  +'.self::TYPE_LIKE_1000_POINT.' điểm';
                break;

            case    'type_like2000':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_2000_RULE.' like  +'.self::TYPE_LIKE_2000_POINT.' điểm';
                break;


            case    'type_like3000':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_3000_RULE.' like  +'.self::TYPE_LIKE_3000_POINT.' điểm';
                break;


            case    'type_like5000':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_LIKE_5000_RULE.' like  +'.self::TYPE_LIKE_5000_POINT.' điểm';
                break;

            case    'type_share':
                $msg    ='Bài viêt của bạn chia sẻ '.self::TYPE_SHARE_RULE.' lần  +'.self::TYPE_SHARE_POINT.' điểm';
                break;


            case    'type_view':
                $msg    ='Bài viêt của bạn đã xem '.self::TYPE_VIEW_RULE.' lần  +'.self::TYPE_VIEW_POINT.' điểm';
                break;

            case    'type_comment':
                $msg    ='Bài viêt của bạn đạt '.self::TYPE_COMMENT_RULE.'  +'.self::TYPE_COMMENT_POINT.' điểm';
                break;

            case    'type_follow':
                $msg    ='Bạn được '.self::TYPE_FOLLOW_RULE.' hóng  +'.self::TYPE_FOLLOW_POINT.' điểm';
                break;

            case    'type_post':
                $msg    = MembersPoint::MSG_POST;
                break;

            case    'type_view':
                $msg    = MembersPoint::MSG_VIEW;
                break;

            case    'type_like':
                $msg    = MembersPoint::MSG_LIKE;
                break;

        }

        return $msg;
    }
     public static function getInsertLike($post_new = array())
     {
         if(!empty($post_new))
         {
             $arrMemberPoint = array(
                 'user_id'=>(int)$post_new['user_id'],
                 'time_point_datetime'=>new Zend_Db_Expr("NOW()"),
                 'post_id'=>(int)$post_new['post_id']
             );
            $nlike  = $post_new['number_of_like'];
             switch($nlike)
             {
                 case MembersPoint::TYPE_LIKE_50_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_50;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_50_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_50) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_50,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                         break;

                 case MembersPoint::TYPE_LIKE_100_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_100;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_100_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_100) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_100,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                        break;

                 case MembersPoint::TYPE_LIKE_300_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_300;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_300_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_300) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_300,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                        break;

                 case MembersPoint::TYPE_LIKE_500_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_500;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_500_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_500) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_500,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                         break;

                 case MembersPoint::TYPE_LIKE_1000_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_10000;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_1000_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_10000) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_10000,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                         break;

                 case MembersPoint::TYPE_LIKE_2000_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_20000;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_2000_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_20000) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_20000,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                         break;

                 case MembersPoint::TYPE_LIKE_5000_RULE :
                         $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_50000;
                         $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_5000_POINT;
                         $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_50000) ;
                         $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_50000,$post_new['post_id']);
                         if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                         break;
             }
         }
     }
    public static function getWeekMonthYear($type = 'week')
    {
        //$date = date('Y-m-d H:i:s',time()-(7*86400)); // 7 days ago
        //$sql = "SELECT * FROM table WHERE date <='$date' ";
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        if($type =='week')
        {
            $data = array();

//            $table   = self::_TABLE;
//            $select  = $storage->select('SELECT SUM(points) as diem , user_id')
//                ->from($table, '*')
//                ->where('id = ?', $id)
//                ->limit(1, 0);

            $select = $storage->select()
                ->from($table,array('points'=>'SUM(points)', 'user_id'))
                ->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
                ->group('user_id');
            $data = $storage->fetchAll($select);
        }

    }
    public static function getListMonthDayAll($type = 'week')
    {
        $data = array();
        $storage   = My_Zend_Globals::getStorage();
        $notin     = User::getUserMoveHome();
        $table     = MembersPoint::TABLE_NAME;
        $select    = $storage->select()
            ->from($table,array('diem'=>'SUM(points)', 'user_id'))
            //->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
            ->where(' DATE_SUB(CURDATE(),INTERVAL 7 DAY) <= time_point_datetime ')
            ->where(' user_id NOT IN (?)',$notin)
            ->order('points DESC')
            ->limit(20,0)
            ->group('user_id');

        $data = $storage->fetchAll($select);
        if(!empty($data))
        {

        }
    }
    public static function getCron()
    {
        /*
             WHERE DATE_SUB(CURDATE(),INTERVAL 7 DAY) <= time_point_datetime
            $date = date('Y-m-d H:i:s',time()-(7*86400)); // 7 days ago
            $sql = "SELECT * FROM table WHERE date <='$date' ";
        */
        echo "<table cellpadding='2' cellspacing='2' width='100%'>";
        $data_week     = array();
        $data_month    = array();
        $data_all      = array();
        $storage   = My_Zend_Globals::getStorage();
        $table     = MembersPoint::TABLE_NAME;
        $select    = $storage->select()
            ->from($table,array('points'=>'SUM(points)', 'user_id'))
            //->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
            ->where(' DATE_SUB(CURDATE(),INTERVAL 7 DAY) <= time_point_datetime ')
            ->order('points DESC')
            ->group('user_id');
        $data_week = $storage->fetchAll($select);
        if(!empty($data_week))
        {
            foreach($data_week as $key=>$value)
            {
                $arrData  = array('user_id'=>$value['user_id'],'week_point'=>$value['points']);
                User::updateUser($arrData);
                echo "<tr>";
                echo "<td> User : ".$value['user_id'];
                echo "</td>";
                echo "<td>".$value['points'];
                echo "</td>";
                echo "</tr>";


            }
        }
        echo "<tr><td colspan='2'>===================7 DAYS========================</td></tr>";


        $select = $storage->select()
            ->from($table,array('points'=>'SUM(points)', 'user_id'))
           // ->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ')
            ->where(' DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= time_point_datetime ')
            ->order('points DESC')
            ->group('user_id');
        $data_month = $storage->fetchAll($select);
        if(!empty($data_month))
        {
            foreach($data_month as $key=>$value)
            {
                $arrData  = array('user_id'=>$value['user_id'],'month_point'=>$value['points']);
                User::updateUser($arrData);
                echo "<tr>";
                echo "<td> User : ".$value['user_id'];
                echo "</td>";
                echo "<td>".$value['points'];
                echo "</td>";
                echo "</tr>";
            }

        }

        echo "<tr><td colspan='2'>===================1 MONTH========================</td></tr>";

        $select = $storage->select()
            ->from($table,array('points'=>'SUM(points)', 'user_id'))
            ->order('points DESC')
            ->group('user_id');
        $data_all = $storage->fetchAll($select);
        if(!empty($data_all))
        {
            foreach($data_all as $key=>$value)
            {
                $arrData  = array('user_id'=>$value['user_id'],'all_point'=>$value['points']);
                User::updateUser($arrData);
                echo "<tr>";
                echo "<td> User : ".$value['user_id'];
                echo "</td>";
                echo "<td>".$value['points'];
                echo "</td>";
                echo "</tr>";

            }

        }
        echo "<tr><td colspan='2'>===================ALL========================</td></tr>";

        exit;
    }
    public static function getShare($post_new = array())
    {
        if(empty($post_new))
        {
            return false;
        }

        $arrMemberPoint = array(
            'user_id'=>(int)$post_new['user_id'],
            'time_point_datetime'=>new Zend_Db_Expr("NOW()"),
            'post_id'=>(int)$post_new['post_id']
        );

        /*Post*/
        if( $post_new['is_actived'] ==1 && $post_new['is_approved'] ==1)
        {

            if($post_new['is_photo']==0)
            {
                $arrMemberPoint['type_point']   = MembersPoint::TYPE_UPLOAD_VIDEOS;
                $arrMemberPoint['points']       = MembersPoint::TYPE_UPLOAD_VIDEOS_POINTS;
                $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_UPLOAD_VIDEOS) ;
                $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_UPLOAD_VIDEOS,$post_new['post_id']);

                if(empty($checktype))
                {
                    MembersPoint::insert($arrMemberPoint);
                }
            }

            if($post_new['is_photo']==1)
            {

                $arrMemberPoint['type_point']   = MembersPoint::TYPE_UPLOAD_PICTURES;
                $arrMemberPoint['points']       = MembersPoint::TYPE_UPLOAD_PICTURES_POINTS;
                $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_UPLOAD_PICTURES) ;
                $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_UPLOAD_PICTURES,$post_new['post_id']);
                if(empty($checktype))
                {
                    MembersPoint::insert($arrMemberPoint);
                }
            }

        }

        /*Move to Home Page*/
        if($post_new['phase']==Post::_PHASE_HOMEPAGE)
        {
            $arrMemberPoint['type_point']   = MembersPoint::TYPE_MOVE_TO_HOME;
            $arrMemberPoint['points']       = MembersPoint::TYPE_MOVE_TO_HOME_POINTS;
            $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_MOVE_TO_HOME) ;
            $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_MOVE_TO_HOME,$post_new['post_id']);
            if(empty($checktype))
            {
                MembersPoint::insert($arrMemberPoint);
            }
        }


        /*Move to Hot*/
        if($post_new['phase']==Post::_PHASE_HOT)
        {
            $arrMemberPoint['type_point']   = MembersPoint::TYPE_MOVE_TO_HOT;
            $arrMemberPoint['points']       = MembersPoint::TYPE_MOVE_TO_HOT_POINTS;
            $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_MOVE_TO_HOT) ;
            $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_MOVE_TO_HOT,$post_new['post_id']);
            if(empty($checktype))
            {
                MembersPoint::insert($arrMemberPoint);
            }
        }


        /*Move to Category*/
        $category_id = isset($post_new['category_id']) ? $post_new['category_id'] : 0;
        if($category_id > 0 )
        {
            $arrMemberPoint['type_point']   = MembersPoint::TYPE_CATEGORY;
            $arrMemberPoint['points']       = MembersPoint::TYPE_CATEGORY_POINTS;
            $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_CATEGORY) ;
            $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_CATEGORY,$post_new['post_id']);
            if(empty($checktype))
            {
                MembersPoint::insert($arrMemberPoint);
            }
        }

        /*View*/
        $nview  = $post_new['number_of_view'];
        if($nview >= 1000)
        {
            if($nview%1000==0)
            {
                $arrMemberPoint['type_point']   = MembersPoint::TYPE_VIEW;
                $arrMemberPoint['points']       = MembersPoint::TYPE_VIEW_POINT;
                $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_VIEW) ;
                MembersPoint::insert($arrMemberPoint);
            }
        }

        /*Like*/
        switch($post_new['number_of_like'])
        {
            case MembersPoint::TYPE_LIKE_50_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_50;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_50_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_50) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_50,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                    break;

            case MembersPoint::TYPE_LIKE_100_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_100;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_100_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_100) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_100,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                    break;

            case  MembersPoint::TYPE_LIKE_300_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_300;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_300_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_300) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_300,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                    break;

            case MembersPoint::TYPE_LIKE_500_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_500;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_500_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_500) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_500,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                     break;

            case MembersPoint::TYPE_LIKE_1000_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_1000;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_1000_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_1000) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_1000,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                    break;

            case MembersPoint::TYPE_LIKE_2000_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_2000;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_2000_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_2000) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_2000,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                    break;

            case MembersPoint::TYPE_LIKE_5000_RULE :
                    $arrMemberPoint['type_point']   = MembersPoint::TYPE_LIKE_5000;
                    $arrMemberPoint['points']       = MembersPoint::TYPE_LIKE_5000_POINT;
                    $arrMemberPoint['msg']          = MembersPoint::getMessage(MembersPoint::TYPE_LIKE_5000) ;
                    $checktype  = MembersPoint::getMemberPoint($post_new['user_id'],MembersPoint::TYPE_LIKE_5000,$post_new['post_id']);
                    if(empty($checktype)){  MembersPoint::insert($arrMemberPoint);}
                break;
        }
    }


}