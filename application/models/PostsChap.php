<?php

class PostsChap
{

    const _TABLE = 'posts_chap';
    const _cache_key_post_chaps = 'chaps_post_%s';
	const _cache_key_chap_id = 'chap_%s';

    public static function initPost($data)
    {
        $fields = array(
            'id',
            'post_id',
            'youtube_id',
            'url',
            'chap_name',
            'chap_title',
            'chap_description',
            'weight',
            'is_active'
        );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function update($data)
    {
        try {
            $data = self::initPost($data);
            if ($data === false || !isset($data['id'])) {
                return false;
            }
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->update(self::_TABLE, $data, 'id =' . $data['id']);
	        if ($rs) {
		        $caching = My_Zend_Globals::getCaching();
		        $cacheKey = sprintf(self::_cache_key_chap_id, $data['id']);
		        $caching->delete($cacheKey);
		        $cacheKey = sprintf(self::_cache_key_post_chaps, $data['post_id']);
		        $caching->delete($cacheKey);
	        }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
            return false;
        }
    }

    public static function insert($data)
    {
        //Init data
        $data = self::initPost($data);
        if ($data === false) {
            return false;
        }
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE, $data);
            if ($rs) {
	            $caching = My_Zend_Globals::getCaching();
	            $cacheKey = sprintf(self::_cache_key_post_chaps, $data['post_id']);
	            $caching->delete($cacheKey);
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }

    public static function delete($id, $post_id = null)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $chap = self::getChapByID($id, false);
            if (empty($chap['youtube_id']) && empty($chap['youtube_key'])) {
                $rs = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE id = ' . $id);
            } else {
                $rs = $storage->query('UPDATE ' . self::_TABLE . ' SET is_active = 0 WHERE id = ' . $id);
            }
	        if ($rs && intval($post_id) > 0) {
		        $caching = My_Zend_Globals::getCaching();
		        $cacheKey = sprintf(self::_cache_key_chap_id, $id);
		        $caching->delete($cacheKey);
		        $cacheKey = sprintf(self::_cache_key_post_chaps, $post_id);
		        $caching->delete($cacheKey);
	        }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::delete - ' . $ex->getMessage());

            return false;
        }
    }

    public static function deletePost($id)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->query('UPDATE ' . self::_TABLE . ' SET is_active = 0 WHERE post_id = ' . $id);
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::delete - ' . $ex->getMessage());

            return false;
        }
    }

    public static function getChapByID($id, $useCache = true)
    {
        if (empty($id)) {
            return false;
        }
        $data = array();
        if ($useCache) {
            $caching = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key_chap_id, $id);
            $data = $caching->read($cacheKey);
        }

        if (empty($data)) {
            $storage = My_Zend_Globals::getStorage();
            $table = self::_TABLE;
            $select = $storage->select()
                ->from($table, '*')
                ->where('id = ?', $id)
                ->limit(1, 0);
            $row = $storage->fetchRow($select);
            if (!empty($row)) {
                $data = $row;
                if ($useCache) {
                    $caching->write($cacheKey, $data, 900);
                }
            }
        }

        return $data;
    }

    public static function getChapsByPostID($post_id, $useCache = true)
    {
        if (empty($post_id)) {
            return false;
        }
        $data = array();

	    if ($useCache) {
		    $caching = My_Zend_Globals::getCaching();
		    $cacheKey = sprintf(self::_cache_key_post_chaps, $post_id);
		    $data = $caching->read($cacheKey);
	    }

	    if (empty($data)) {
	        $storage = My_Zend_Globals::getStorage();
	        $table = self::_TABLE;
	        $select = $storage->select()
	            ->from($table, '*')
	            ->where('post_id = ?', $post_id)->order(' weight ASC');
	        $row = $storage->fetchAll($select);
	        if (!empty($row)) {
	            $data = $row;
		        if ($useCache) {
			        $caching->write($cacheKey, $data, 900);
		        }
	        }
	    }
        return $data;

    }

    public static function getPostChapRalativeLast($id)
    {
        if (empty($id)) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table = self::_TABLE;
        $select = $storage->select()
            ->from($table, '*')
            ->where('post_id = ?', $id)->order(' id DESC')
            ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if (!empty($row)) {
            $data = $row;
        }
        return $data;
    }
}
