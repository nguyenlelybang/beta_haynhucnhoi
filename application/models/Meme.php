<?php

class Meme {

    const _TABLE = 'meme';

    public static function initData($data) 
    {
        $fields = array(
            'ID', 'name', 
            'description', 
            'count', 
            'date', 
            'isActive'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }
    public static function insert($data) 
    {
        //Init data
        $data = self::initData($data);
        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {echo $ex->getMessage(); exit;
            My_Zend_Logger::log('Meme::insert - ' . $ex->getMessage());

            return false;
        }
    }
    public static function getDetail($id) 
    {
        if (empty($id)) {
            return false;
        }
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table = self::_TABLE;
            $select = $storage->select()
                    ->from($table, '*')
                    ->where('ID = ?', $id)
                    ->limit(1, 0);
            $data = $storage->fetchRow($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('Meme::getDetail - ' . $ex->getMessage());
            return $data;
        }
        return $data;
    }
    public static function delete($id) 
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE ID = ' . $id);
            if ($rs) {

            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Meme::delete - ' . $ex->getMessage());
            return false;
        }
    }
    public static function getList($filters = array(), $offset = 0, $limit = 25, $options = array('order_by' => 'ID desc')) 
    {
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::_TABLE;
            $select = $storage->select()
                    ->from(array('f' => $table), '*')
                    ->limit($limit, $offset);
            if (isset($filters['is_active'])) {
                $select->where('isActive = ?', $filters['is_active']);
            }
            if (isset($options['order_by'])) {
                $select->order($options['order_by']);
            }
            $data = $storage->fetchAll($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('Meme::getList - ' . $ex->getMessage());
            return false;
        }

        return $data;
    }

    public static function countTotal($filters = array()) 
    {
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table = self::_TABLE;
            $select = $storage->select()
                    ->from($table, 'count(ID) as total');
            if (isset($filters['is_active'])) {
                $select->where('isActive = ?', $filters['is_active']);
            }
            $data = $storage->fetchRow($select);
            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('Meme::countTotal - ' . $ex->getMessage());
            return false;
        }
        return $data;
    }

    public static function memeUrl($meme, $absolute = false)
    {
        if (empty($meme) || !is_array($meme)) {
            return '';
        }

        $url = '/meme-upload/' . $meme['ID'];
        if ($absolute)
            $url = BASE_URL . $url;
        return $url;
    }

    static function createSystemFolder($path) 
    {
        try {   
                $path = trim($path, '/');
                if (empty($path)) {
                    return false;
                }
                $arr  = explode('/', $path);
                $path = UPLOAD_PATH;
                foreach ($arr as $folder) 
                {
                    $path = $path .'/'. $folder;
                    if (!is_dir($path)) {
                        $oldmask = umask(0);
                        mkdir($path, 0777, true);
                        umask($oldmask);
                    }
                }
            return true;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Meme::createSystemFolder - Cannot create folder: ' . $ex->getMessage());
        }
        return false;
    }

    static function saveMeme($userId, $rawData)
    {
        $rawData = trim($rawData);
        if (empty($rawData)) {
            My_Zend_Logger::log('Meme::saveMeme - Invalid data');
            return false;
        }

        $fileName = date('Y') . $userId . time() .'.jpg';
        $folders = 'meme/' . date('Y') .'/'. $userId;
        if (!self::createSystemFolder($folders)){
            My_Zend_Logger::log('Meme::saveMeme - Can not create upload folders');
            return false;
        }

        $uploadTo = UPLOAD_PATH . '/' . $folders . "/" . $fileName;
        $rawData = imagecreatefromstring(base64_decode($rawData));

        if (!imagejpeg($rawData, $uploadTo ,100)) {
            My_Zend_Logger::log('Meme::saveMeme - Can not write file into server - file path: '. $uploadTo);
            return false;
        }

        $imageSize = getimagesize($uploadTo);
        if (!$imageSize) {
            My_Zend_Logger::log('Meme::saveMeme - Image is not valid. Can not get image size');
            return false;
        }

        $watermark = STATIC_PATH .'/images/memewatermark.png';
        $upload = new Upload();
        $upload->watermark($uploadTo, $uploadTo, $watermark, phMagickGravity::SouthEast);
        $config = My_Zend_Globals::getConfiguration();
        return '/'. $folders .'/'. $fileName;
    }
}
