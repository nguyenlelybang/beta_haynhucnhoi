<?php

class Upload {

    protected $max_filesize_upload;
    protected $min_width;
    protected $min_height;
    protected $upload_path;
    protected $original_folder;
    protected $thumb_folder;
    protected $media_folder;
    protected $extenion;
    protected $file_error_log_path;
    protected $log_path;
    protected $userName;
    protected $publicDomain;
    protected $allowExtensions;

    protected $error_message = array(
        0 => 'Upload Successful',
        1 => 'Tên file không hợp lệ',
        2 => 'Kích thước ảnh quá nhỏ hoặc quá lớn',
        3 => 'Ảnh quá dài hoặc quá ốm',
        4 => 'Kiểu file không hợp lệ',
        5 => 'Upload file thất bại',
        6 => 'Upload file thất bại'
    );

    function __construct() {
        require_once LIBS_PATH . '/My/phmagick/phmagick.php';

        $config = My_Zend_Globals::getConfiguration();
        $this->max_filesize_upload = $config->photo->upload->max_file_size * 1024 * 1024;
        $this->min_width           = $config->photo->upload->min_width;
        $this->min_height          = $config->photo->upload->min_height;
        $this->upload_path         = $config->photo->upload->dir;
        $this->media_folder        = $config->photo->upload->upload_media_folder;
        $this->log_path            = $config->photo->upload->log->path;
        $this->publicDomain        = $config->photo->domain;
        $this->allowExtensions     = explode(',', $config->photo->upload->extension_allow);
        $this->extenion = '.jpg';
    }

    function upload($upload, $destFolder = array(), $params=array(), $atomicResize = true)
    {
        $info = array();
        if ($upload && is_array($upload['tmp_name']))
        {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $fileData = array(
                    'tmp_name' => $upload['tmp_name'][$index],
                    'error' => $upload['error'][$index],
                    'name' => isset($_SERVER['HTTP_X_FILE_NAME']) ? $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'][$index],
                    'size' => isset($_SERVER['HTTP_X_FILE_SIZE']) ? $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'][$index],
                    'type' => isset($_SERVER['HTTP_X_FILE_TYPE']) ? $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'][$index]
                );

                $rs = $this->handleFileUpload($fileData, $destFolder);
                $info[] = $rs;
                // push to gearman
                if ($atomicResize) {
                    $worker = new Worker();
                    $worker->resize($rs);
                } else {
                    $jobClient = My_Zend_Globals::getJobClient();
                    $jobClient->doBackgroundTask($rs);
                }
            }
        } elseif ($upload || isset($_SERVER['HTTP_X_FILE_NAME']))
        {

            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:
            $rs = $this->handleFileUpload($upload, $destFolder);
            $info[] = $rs;
            $this->doResize($rs);
        }
        return $info;
    }

    public function uploadToStaticHost($filePath)
    {
        return false;
        try {
            if (!file_exists($this->upload_path . $filePath)) {
                return false;
            }

            $config = My_Zend_Globals::getConfiguration();
            $destFile = $config->ftp->static01->rootpath . $filePath;
            require_once LIBS_PATH .'/My/Zend/Ftp.php';

            $ftp = new Ftp;
            $ftp->connect($config->ftp->static01->host);
            $ftp->login($config->ftp->static01->username, $config->ftp->static01->password);
            // check folder exist
            $arrPath = explode('/', $filePath);
            unset($arrPath[count($arrPath) - 1]);
            $folderPath = implode('/', $arrPath);

            if (!$ftp->isDir($config->ftp->static01->rootpath . $folderPath)) {
                $ftp->mkDirRecursive($config->ftp->static01->rootpath . $folderPath);
            }

            $rs = $ftp->put($destFile, $this->upload_path . $filePath, FTP_BINARY);

            if ($rs) {
                return 'http://'. $config->ftp->static01->domain . $filePath;
            }
            return false;
        } catch (Exception $ex) {
            My_Zend_Logger::log("Upload::uploadToStaticHost - ". $ex->getMessage());
        }
    }

    /**
     * Handle process upload file to server
     * @param array $fileData
     * @param array $rootFolder
     * @return multitype:number |multitype:number unknown |multitype:string unknown multitype:
     */
    function handleFileUpload($fileData, $destFolders = array())
    {
        //var_dump($fileData);die;
        if (!$fileData) {
            return array('error_code' => 1);
        }

        try {
            // neu upload thanh cong
            if ($fileData['error'] === UPLOAD_ERR_OK) {
                $fileSize = $fileData['size'];

                $fileError = $fileData['error'];
                $fileName = $fileData['name'];
                $fileType = $fileData['type'];
                $arrExt = explode('.', trim($fileName));
                $ext = strtolower($arrExt[count($arrExt) - 1]);
                $this->extenion = '.'. $ext;
                array_pop($arrExt);
                $photoName = implode(' ', $arrExt);

                if ($fileName == '')
                    return array('error_code' => 1, 'name' => $fileName, 'size' => $fileSize);

                $imageSize = getimagesize($fileData['tmp_name']);

                if (!$imageSize)
                    return array('error_code' => 4, 'name' => $fileName, 'size' => $fileSize);

                if ($fileSize > $this->max_filesize_upload)
                    return array('error_code' => 2, 'name' => $fileName, 'size' => $fileSize);

                list($width, $height, $type, $attr) = $imageSize;

                if ($width < $this->min_width && $height < $this->min_height)
                    return array('error_code' => 3, 'name' => $fileName, 'size' => $fileSize);

                $systemName = time() . rand();
                $systemName = md5($systemName);

                $folders = array();

                // init dest folders
                if (is_array($destFolders) && !empty($destFolders)) {
                    $destFolders = array_values($destFolders);

                    foreach ($destFolders as $key => $folder) {
                        $folders[$key] = $folder;
                    }
                } else {
                    $folders[0] = date('Y');
                    $folders[1] = strtolower($systemName[0]);
                    $folders[2] = strtolower($systemName[1]);
                }

                // check folder exist
                if (!$this->checkSystemFolder($folders))
                    return array('error_code' => 5, 'name' => $fileName, 'size' => $fileSize);

                $folder = '';

                foreach ($folders as $tmp) {
                    $folder .= $tmp . '/';
                }

                $folder = rtrim($folder, '/');

                $uploadTo = $this->upload_path . "/" . $folder . "/" . $systemName . $this->extenion;
                // upload to original file
                if (move_uploaded_file($fileData['tmp_name'], $uploadTo)) {
                    $shareImage = '';
                    if ($height > 300) {
                        // generate share image
                        $desc = UPLOAD_PATH.'/share/'. date('Y') .'/';
                        if (!is_dir($desc)) {
                            $oldmask = umask(0);
                            mkdir($desc, 0777, true);
                            umask($oldmask);
                        }

                        $fileName = 'share-'. md5($systemName) .'-'. time();
                        $desc .= ($fileName . $this->extenion);

                        $phMagick = new phMagick($uploadTo, $desc);
                        if ($width > 300) {
                            self::uploadImage(600, 315, $uploadTo, $desc);
                        } else {
                            $rs = $phMagick->mycrop($width, $width, 0, 0, phMagickGravity::None, 70);
                        }

                        //$ftpFile = $this->uploadToStaticHost($desc);
                        $ftpFile = '';
                        if (!empty($ftpFile)) {
                            $shareImage = $ftpFile;
                        } else {
                            $shareImage = '/share/'. date('Y') . '/' . $fileName . $this->extenion;
                        }
                    }

                    return array("name" => $photoName,
                        //"url" => $this->publicDomain . '/' . $folder . '/' . $systemName . $this->extenion,
                        "url" =>  '/' . $folder . '/' . $systemName . $this->extenion,
                        "path"  => $folder,
                        "sys_name" => $systemName,
                        "ext" => $this->extenion,
                        "w" => $width,
                        "h" => $height,
                        "size" => $fileSize,
                        "share_image" => $shareImage,
                        "is_animated" => $this->isAnimated($uploadTo)

                    );
                } else {
                    return array('error_code' => 8, 'name' => $fileName, 'size' => $fileSize);
                }
            }

            return array('error_code' => 7);
        } catch (Exception $ex) {
            return array('error_code' => -7);
        }
    }

    private function checkSystemFolder($folderName) {
        try {
            $rs = false;

            if (is_array($folderName)) {
                $path = $this->upload_path;
                foreach ($folderName as $folder) {
                    $path .= '/' . $folder;
                    if (!is_dir($path)) {
                        $oldmask = umask(0);
                        mkdir($path, 0777, true);
                        umask($oldmask);
                    }
                }

                $rs = true;
            } elseif (!is_dir($this->upload_path . '/' . $folderName)) {
                $oldmask = umask(0);
                $rs = mkdir($this->upload_path . '/' . $folderName, 0777, true);
                umask($oldmask);
            } else {
                $rs = true;
            }

            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log($ex->getMessage());
            return false;
        }
    }

    private function writeErrorLog($message = '') {
        //$writer = new Zend_Log_Writer_Stream($this->file_error_log_path.$file);
        //$logger = new Zend_Log($writer);
        //$logger->err($message);

        unset($writer, $logger);
    }

    function createSystemFolder() {
        try {
            $year = date('Y', time());

            // create current year folder
            if (!is_dir($this->upload_path . '/' . $year)) {
                $oldmask = umask(0);
                mkdir($this->upload_path . '/' . $year, 0777, true);
                umask($oldmask);
            }

            if (!is_dir($this->upload_path . '/' . $year . '/' . $username)) {
                $oldmask = umask(0);
                mkdir($this->upload_path . '/' . $year . '/' . $username, 0777, true);
                umask($oldmask);
            }
        } catch (Exception $ex) {
            $this->writeErrorLog('Cannot create folder: ' . $ex->getMessage());
        }

        return false;
    }

    function handleError($message, $exit = false, $code = 500) {
        switch ($code) {
            case 500:
                header("HTTP/1.1 500 Internal Server Error");
                break;
            case 501:
                header("HTTP/1.1 501 Not Implemented");
                break;
            case 502:
                header("HTTP/1.1 502 Bad Gateway");
                break;
            case 503:
                header("HTTP/1.1 503 Service Unavailable");
                break;
        }
        echo $message;
        if ($exit)
            exit;
    }

    function resize($source, $width = 1024, $height = 1024, $exactDimentions = false, $background = '') {
        $phMagick = new phMagick($source, $source);
        return $phMagick->resize($width, $height, $exactDimentions, $background);
    }

    function crop($source, $destFile, $width = 1024, $height = 1024) {
        $phMagick = new phMagick($source, $destFile);
        return $phMagick->mycrop($width, $height);

        $iMagik = new Imagick();

        list($w, $h) = getimagesize($source);

        if ($w > $h) {
            $tmpWidth = $height;
        }

        $iMagik->readImage($source);
        $iMagik->resizeimage($tmpWidth, $tmpWidth, Imagick::FILTER_LANCZOS, 1, true);
        $iMagik->writeImage($source);
        $iMagik->cropimage($width, $height, 0, 0);
        $iMagik->writeImage($destFile);

        $iMagik->destroy();

        return true;
    }

    function rotate($source, $angle) {
        $phMagick = new phMagick($source, $source);
        $rs = $phMagick->rotate($angle);
        return $rs;
    }

    function watermark($source, $destination, $watermark, $position, $opacity=100) {
        $gravity = "None";
        switch (strtolower($position)) {
            case "center":
                $gravity = "Center";
                break;
            case "east":
                $gravity = "East";
                break;
            case "forget":
                $gravity = "Forget";
                break;
            case "northeast":
                $gravity = "NorthEast";
                break;
            case "north":
                $gravity = "North";
                break;
            case "northwest":
                $gravity = "NorthWest";
                break;
            case "southeast":
                $gravity = "SouthEast";
                break;
            case "south":
                $gravity = "South";
                break;
            case "southwest":
                $gravity = "SouthWest";
                break;
            case "west":
                $gravity = "West";
                break;
            default:
                break;
        }
        $phMagick = new phMagick($source, $destination);
        //var_dump($watermark);die;
        $rs = $phMagick->compose($watermark, $gravity, $opacity);
        // ini_set('xdebug.var_display_max_depth', -1);
        // ini_set('xdebug.var_display_max_children', -1);
        // ini_set('xdebug.var_display_max_data', -1);
        //echo '<pre>';print_r($rs);die;
        return $rs;
    }

    function watermark1($source, $destination, $watermark) {
        list($w, $h) = getimagesize($source);
        list($ww, $hw) = getimagesize($watermark);
        if ($w < $ww) {
            $this->crop($watermark, $watermark, $w);
        }
        $phMagick = new phMagick($source, $destination);
        $rs = $phMagick->montage($watermark);
        return $rs;
    }

    public function getImageDim($with = 0, $height = 0, $size = 720) {
        $w = $with;
        $h = $height;
        $max_edge = max($with, $height);
        if ($max_edge > $size && $max_edge > 0) {
            $rate = $size / $max_edge;
            $w = ceil($with * $rate);
            $h = ceil($height * $rate);
        }
        return array('w' => $w, 'h' => $h);
    }

    public function isAnimated($source) {
        return (bool)preg_match('#(\x00\x21\xF9\x04.{4}\x00\x2C.*){2,}#s', file_get_contents($source));
    }

    public function getErrorMessage($code) {
        return $this->error_message[$code];
    }

    function uploadFile() {
        $fileData = isset($_FILES['Filedata']) ? $_FILES['Filedata'] : null;
        $signKey = isset($_POST['signkey']) ? $_POST['signkey'] : '';

        // validate
        if (!$this->verifySignKey($signKey)) {
            /* return array(
              'error_code' => 6,
              ); */
        }

        $rs = array();

        if ($fileData || isset($_SERVER['HTTP_X_FILE_NAME'])) {

            if ($fileData['error'] === UPLOAD_ERR_OK) {
                $fileSize = $fileData['size'];
                $fileError = $fileData['error'];
                $fileName = $fileData['name'];
                $fileType = $fileData['type'];
                $arrExt = explode('.', trim($fileName));
                $ext = strtolower($arrExt[count($arrExt) - 1]);
                array_pop($arrExt);
                $photoName = implode(' ', $arrExt);

                if ($fileName == '')
                    return array('error_code' => 1, 'name' => $fileName, 'size' => $fileSize);

                $systemName = time() . rand();
                $systemName = md5($systemName);

                // get first character of name
                $folders[0] = $this->media_folder;
                $folders[1] = 'files';
                $folders[2] = date('Y');

                // check folder exist
                if (!$this->checkSystemFolder($folders))
                    return array('error_code' => 5, 'name' => $fileName, 'size' => $fileSize);

                $folder = '';

                foreach ($folders as $tmp) {
                    $folder .= $tmp . '/';
                }

                $folder = rtrim($folder, '/');

                $uploadTo = $this->upload_path . '/' . $folder . "/" . $systemName . '.' . $ext;

                // upload to original file
                if (move_uploaded_file($fileData['tmp_name'], $uploadTo)) {
                    return array("name" => $photoName,
                        "url" => $this->publicDomain . '/' . $folder . '/' . $systemName . '.' . $ext
                    );
                } else {
                    return array('error_code' => 8, 'name' => $fileName, 'size' => $fileSize);
                }
            }
        }

        return $rs;
    }

    private function get_redirect_url($url) {
        $redirect_url = null;

        $url_parts = @parse_url($url);
        if (!$url_parts)
            return false;
        if (!isset($url_parts['host']))
            return false; //can't process relative URLs
        if (!isset($url_parts['path']))
            $url_parts['path'] = '/';

        $sock = fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int) $url_parts['port'] : 80), $errno, $errstr, 30);
        if (!$sock)
            return false;

        $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?' . $url_parts['query'] : '') . " HTTP/1.1\r\n";
        $request .= 'Host: ' . $url_parts['host'] . "\r\n";
        $request .= "Connection: Close\r\n\r\n";
        fwrite($sock, $request);
        $response = '';
        while (!feof($sock))
            $response .= fread($sock, 8192);
        fclose($sock);

        if (preg_match('/^Location: (.+?)$/m', $response, $matches)) {
            if (substr($matches[1], 0, 1) == "/")
                return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);
            else
                return trim($matches[1]);
        } else {
            return $url;
        }
    }

    public function download($url, $fileName, $path, $width = 0, $height = 0) {
        $url = trim($url);

        if (empty($url)) {
            return false;
        }

        // check folder exist
        if (!$this->checkSystemFolder($path)) {
            return false;
        }

        try {
            $ext = '.jpg';
            $link = $this->upload_path . "/" . $path . "/" . $fileName . $ext;

            $ch = curl_init($url);
            $fp = fopen($link, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, false);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);

            curl_exec($ch);
            curl_close($ch);
            fclose($fp);

            $imageSize = getimagesize($link);

            // if this file is not image
            if (!$imageSize) {
                unlink($fileName);
                return false;
            }

            $url = $this->publicDomain . '/' . $path . '/' . $fileName . $ext;

            if ($width > 0 || $height > 0) {
                // resize
                $this->resize($link, $width, $height);
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('Upload::download - '. $ex->getMessage());
        }

        return $url;
    }

    public function doResize($data) {
        try {
            $config = My_Zend_Globals::getConfiguration();

            $source = $config->photo->upload->dir . '/' . $data['path'] . '/' . $data['sys_name'] . $data['ext'];
            $watermark = STATIC_PATH .'/images/hnn-watermark.png';


            if (is_file($source)) {
                $thumbnail = $config->photo->thumbnail->thumb;
                $thumbnail = explode(',', $thumbnail);

                foreach ($thumbnail as $size) {
                    //var_dump($size);die;
                    list($type, $size, $method, $hasWatermark) = explode('|', $size);
                    if (strpos($size, 'x') !== false) {
                        list($width, $height) = explode('x', $size);
                        $width = intval($width);
                        $height = intval($height);
                    }
                    else {
                        $width = intval($size);
                        $height = 0;
                    }

                    $fileName = $data['sys_name'] .'-'. $type;

                    $newName = $config->photo->upload->dir . '/' . $data['path'] . '/' . $fileName . $data['ext'];

                    $phMagick = new phMagick($source, $newName);

                    if ($width > $data['w']) {
                        if ($height > 0) {
                            $height = ($data['h'] * $width) / $data['w'];
                        }
                        $width = $data['w'];
                    }

                    if ($data['is_animated'] && $type == 'thumb1') {
                        $rs = $phMagick->$method($width, $height, false, '', 0)->getLog();
                    } else {
                        $rs = $phMagick->$method($width, $height)->getLog();
                    }

                    if (isset($rs[0]['return']) && $rs[0]['return'] === 0) {
                        //var_dump($watermark);die;
                        if ($hasWatermark && !$data['is_animated']) {
                            //$phMagick = new phMagick($newName, $newName);
                            //$phMagick->watermark($watermark, phMagickGravity::East);
                            $rs = $this->watermark($newName, $newName, $watermark, phMagickGravity::SouthWest, 100);
                        }
                    } else {
                        My_Zend_Logger::log('Imagick - '. json_encode($rs));
                    }

                    if ($data['is_animated']) {
                        $newName = $config->photo->upload->dir . '/' . $data['path'] . '/' . $fileName . '.jpg';
                        $phMagick = new phMagick($source, $newName);
                        $rs = $phMagick->resize($width, $height)->getLog();

                        if ($rs[0]['return'] !== 0) {
                            My_Zend_Logger::log('Imagick - '. json_encode($rs));
                        }
                    }
                }
                //$phMagick = new phMagick($source, $source);
                //$phMagick->resize($config->photo->upload->size, $config->photo->upload->size);
                // remove source
                //unlink($source); //source goc no xoa file da up len sau khi tao thumb va chen watrmark: PhatNN
                return true;
            }

            return false;
        } catch (Exception $ex) {
            // log
            return false;
        }
    }

    public function thumbnailJob($data, $source, $thumbConfig) {
        $config = My_Zend_Globals::getConfiguration();
        $upload_to = $config->photo->upload->dir . '/' . $data['path'];

        $thumbnail = explode(',', $thumbConfig->thumb);

        foreach ($thumbnail as $size) {
            if (strpos($size, 'x') !== false) {
                list($width, $height) = explode('x', $size);
                $width = intval($width);
                $height = intval($height);
                $fileName = $data['sys_name'] . '_' . $width . 'x' . $height;
            }
            else {
                $width = intval($size);
                $height = 0;
                $fileName = $data['sys_name'] . '_' . $width;
            }

            $newName = $config->photo->upload->dir . '/' . $data['path'] . '/' . $fileName . $data['ext'];
            $phMagick = new phMagick($source, $newName);
            $phMagick->resize($width, $height);
        }

        // resize source
        $phMagick = new phMagick($source, $source);
        $phMagick->resize($config->photo->upload->size, $config->photo->upload->size);
    }

    function saveRawData($rawData, $path) {

        if (trim($rawData) != '') {
            $upload_status = 0;
            $ext = 'jpg';
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $hour = date('G');
            $minutes = date('i');
            $this->createSystemFolder($year, $month, $day, $hour, $minutes);
            $linkfile = "$year/$month/$day/$hour/$minutes";
            $time = time();
            $rand = rand();
            $systemname = $time . $rand . ".jpg";
            $temp_name = $time . $rand . "_temp.jpg";
            $upload_to = $this->upload_path . '/' . $this->uploadFolder . "/" . $linkfile . "/" . $temp_name;
            $f = fopen($upload_to, 'w');
            if (fwrite($f, $rawData) !== FALSE) {
                $upload_status = 1;
            } else {
                $return_data['error_code'] = 5;
            }
            fclose($f);
            $image_size = getimagesize($upload_to);
            if ($image_size) {
                $file_ratio_invalid = false;
                list($o_width, $o_height, $o_type, $o_attr) = $image_size;
                if ($o_width > $o_height * 8 || $o_height > $o_width * 2 || ( $o_height < 50 && $o_width < 50 )) {
                    $file_ratio_invalid = true;
                    if ($o_height < 50 && $o_width < 50) {
                        $return_data['error_code'] = 2;
                    } else {
                        $return_data['error_code'] = 3;
                    }
                }
                if ($upload_status == 1) {
                    //Server Upload
                    $server = new Business_Photo_Server();
                    $defaultServer = $server->getServerDefault(SERVER_NUM);
                    $serverId = $defaultServer['serverid'];
                    $image_dim = $this->getImageDim($o_width, $o_height, 1024);
                    $uploaded_info = array("name" => time(),
                        "url" => $linkfile . "/" . $systemname,
                        "temp_url" => $linkfile . "/" . $temp_name,
                        "serverid" => $serverId,
                        'w' => $image_dim['w'],
                        'h' => $image_dim['h'],
                        "time" => $time,
                        "rand" => $rand
                    );
                    $return_data['data'] = $uploaded_info;
                }
            } else {
                $return_data['error_code'] = 4;
            }
            return $return_data;
        }
    }

    public static function getUploadLimit($point)
    {
        $data = array();

        if ($point >=0 && $point <= 499)
        {
            $totalPerDay = 5;
            $videoPerDay = 0;
            $photoPerDay = 5;
            $nextLevel = 500;
        }
        elseif ($point >= 500 & $point < 999)
        {
            $totalPerDay = 6;
            $videoPerDay = 1;
            $photoPerDay = 5;
            $nextLevel = 1000;
        }
        elseif ($point >= 1000 & $point < 2999)
        {
            $totalPerDay = 8;
            $videoPerDay = 2;
            $photoPerDay = 6;
            $nextLevel = 3000;
        }
        elseif ($point >= 3000 & $point < 4999)
        {
            $totalPerDay = 10;
            $videoPerDay = 3;
            $photoPerDay = 7;
            $nextLevel = 5000;
        }
        else
        {
            $totalPerDay = 20;
            $videoPerDay = 10;
            $photoPerDay = 10;
            $nextLevel = null;
        }

        $data['total_limit'] = $totalPerDay;
        $data['photo_limit'] = $photoPerDay;
        $data['video_limit'] = $videoPerDay;
        $data['next_level'] = $nextLevel;

        return $data;
    }

    function uploadImage($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch($mime) {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;

        if($width_new > $width){
            $h_point = (($height - $height_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }
        else {
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);
        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
        return true;
    }
}

?>