<?php

class Api
{

    const _TABLE = 'posts_advertisment';

    public static function initPost($data)
    {
        $fields = array(
            'id',
            'title',
            'url',
            'images',
            'pos',
            'weight'
        );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function getListPost($pos = '')
    {
        $data = array();
        if (empty($pos)) {
            return $data;
        }

        $storage = My_Zend_Globals::getStorage();
        $table = self::_TABLE;
        $select = $storage->select()
            ->from($table, '*')
            ->where('pos = ?', $pos);
        $rows = $storage->fetchAll($select);
        if (!empty($rows)) {
            $data = $rows;
        }
        return $data;
    }

    public static function getList($limit = 20, $offset = 0, $where = array())
    {
        $data = array();
        try {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = 'vechai_' . $limit . isset($where['pos']) ? $where['pos'] : 1;
            $data = $cache->read($cacheKey);
            if (empty($data))
            {
                $db = My_Zend_Globals::getStorage();
                $select = $db->select()
                    ->from(array('p' => self::_TABLE))
                    ->limit($limit, $offset);
                if (isset($where['pos'])) {
                    if (is_numeric($where['pos'])) {
                        $select->where('p.pos = ?', $where['pos']);
                    } elseif (is_array($where['pos'])) {
                        $select->where('p.pos IN (?)', $where['pos']);
                    }
                }
                $data = $db->fetchAll($select);
                if (!empty($data)) {
                    $cache->write($cacheKey, $data, 3603); //TODO: Caching_time
                }
            }
            return $data;
        } catch (Exception $ex) {
            return $data;
        }
    }


}
