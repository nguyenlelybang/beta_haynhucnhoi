<?php
class User {

    public static $user = null;

    const _TABLE_USER           = 'members';
    const _TABLE_USER_VIEW      = 'members_view';
    const _SOCIAL_TYPE_FACEBOOK = 'facebook';
    const _SOCIAL_TYPE_GOOGLE   = 'google';
    const _TOP_POST_BY_WEEK     = 'post_week';
    const _TOP_POST_BY_MONTH    = 'post_month';
    const _TOP_POST_MEMBERS     = 'top_post';
    const _TOP_LIKED_BY_WEEK    = 'liked_week';
    const _TOP_LIKED_BY_MONTH   = 'liked_month';
    const _TOP_LIKED_MEMBERS    = 'top_liked';
    const _TOP_RANK_MEMBERS     = 'top_rank';
    const _TOP_FOLLOWED_MEMBERS = 'top_followed';
    const _TOP_POINTS_WEEK_MEMBERS = 'week_point';
    const _TOP_POINTS_MONTH_MEMBERS = 'month_point';
    const _TOP_POINTS_ALL_MEMBER = 'all_point';
    const _MAX_RANK  = 50000;
    const _cache_key = 'user_v2_%s';

    public static function initUser($data) 
    {
        $tmp = array();
        $fields = array(
            'user_id', 
            'email', 
            'username', 
            'password', 
            'salt', 
            'fullname', 
            'gender', 
            'birthday', 
            'description',
            'country', 
            'posts', 
            'yourviewed', 
            'profileviews', 
            'youviewed', 
            'addtime', 
            'lastlogin', 
            'verified',
            'status', 
            'profilepicture', 
            'remember_me_key', 
            'remember_me_time', 
            'ip', 
            'lip', 
            'location', 
            'filter',
            'website', 
            'news', 
            'theme', 
            'points', 
            'facebookid', 
            'like_count', 
            'comment_count', 
            'rank',
            'follow_number', 
            'followed_number', 
            'approved_new_post', 
            'new_post_to_home',
            'post_count_week', 
            'post_count_month', 
            'like_count_week', 
            'like_count_month',
            'week_point',
            'month_point',
            'all_point',
            'like_point'
        );
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $tmp[$field] = $data[$field];
            }
        }
        $rs = $tmp;
        return $rs;
    }

    public static function insertUser($data) 
    {
        $data = self::initUser($data);
        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE_USER, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function updateUser($data, $updateCache = true)
    {
        $data = self::initUser($data);

        if ($data === false) {
            return false;
        }
        $storage = My_Zend_Globals::getStorage();
        try
        {
            $rs = $storage->update(self::_TABLE_USER, $data, 'user_id = ' . $data['user_id']);
        }
        catch(Exception $ex)
        {

        }
        if (isset($rs) && !empty($rs) && $updateCache == true)
        {
            $userId   = $data['user_id'];
            $cache    = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key, $userId);
            $user     = self::getUser($userId);
            $user     = array_merge($user, $data);
            $cache->write($cacheKey, $user, 3620); //TODO: Caching_time
        }
        return $rs;
    }

    public static function increaseUserPostViewed($userId, $number = 1) 
    {
        $user = self::getUser($userId);
        $user['yourviewed'] += $number;
        if ($user['yourviewed'] % 5 == 0) {
            self::updateUser($user);
        } else {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key, $userId);
            $cache->write($cacheKey, $user, 3625); //TODO: Caching_time
        }
        return true;
    }

    public static function getUser($userId, $useCache = true)
    {
        $cache = My_Zend_Globals::getCaching();
        $cacheKey = sprintf(self::_cache_key, $userId);
        $data = array();
        // get cache instance
        if ($useCache)
        {
            $data = $cache->read($cacheKey);
        }

        if (empty($data)) 
        {
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                ->from(self::_TABLE_USER, '*')
                ->where('user_id = ?', $userId)
                ->limit(1, 0);
            $data = $storage->fetchRow($select);
            if (!empty($data) && $useCache) {
                $cache->write($cacheKey, $data, 3627);  //TODO: Caching_time
            }
        }

        //If empty
        if (empty($data)) {
            $data = array();
        } else {
            if (!empty($data['filter'])) {
                $data['filter'] = @unserialize($data['filter']); 
            }

            $data['fullname'] = htmlspecialchars($data['fullname']);
        }

        self::$user[$userId] = $data;
        return $data;
    }

    public static function getUserByEmail($email) 
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select  = $storage->select()
                ->from(self::_TABLE_USER, '*')
                ->where('email = ?', $email)
                ->limit(1, 0);
            $data = $storage->fetchRow($select);

            //If empty
            if (empty($data)) {
                $data = array();
            }

            return $data;
        } catch (Exception $ex) {
            return array();
        }
    }

    public static function checkUsernameExist($userId, $username) 
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select  = $storage->select()
                ->from(self::_TABLE_USER, 'user_id')
                ->where('user_id = ?', $userId)
                ->where('username = ?', $username)
                ->limit(1, 0);
            $data = $storage->fetchRow($select);

            //If empty
            if (empty($data)) {
                $data = array();
            }
            return $data;
        } catch (Exception $ex) {
            return array();
        }
    }

    public static function getUserViewPost($userId)
    {
        if(empty($userId))
        {
            return false;
        }
        $data  = array();
        try {
            $storage = My_Zend_Globals::getStorage();
            $select  = $storage->select()
                ->from(array('v'=>self::_TABLE_USER_VIEW,array('post_id')))
                ->where('user_id = ?', (int)$userId);
            $data = $storage->fetchAll($select);
            return $data;
        } catch (Exception $ex) {
            return $data;
        }
    }

    public static function insertView($data = array())
    {
        if(empty($data))
        {
            return false;
        }
        try {
            $storage = My_Zend_Globals::getStorage();
            $select  = $storage->insert(self::_TABLE_USER_VIEW,$data);
            $id  = $storage->lastInsertId();
            return $id;
        } catch (Exception $ex) {
            return $data;
        }
    }

    public static function getPostView($user_id,$post_id) {

        if(empty($user_id) || empty($post_id))
        {
            return false;
        }
        $data  = array();
        try {
            $storage = My_Zend_Globals::getStorage();
            $select  = $storage->select()
                ->from(self::_TABLE_USER_VIEW,'*')
                ->where('user_id = ?',$user_id)
                ->where('post_id = ?',$post_id)->limit(1, 0);
            $data = $storage->fetchRow($select);
            return $data;
        } catch (Exception $ex) {
            return $data;
        }
    }


    public static function getUserBySocialId($socialId) {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                ->from(self::_TABLE_USER, '*')
                ->where('facebookid = ?', $socialId)
                ->limit(1, 0);
            $data = $storage->fetchRow($select);

            //If empty
            if (empty($data)) {
                $data = array();
            }

            return $data;
        } catch (Exception $ex) {
            return array();
        }
    }

    public static function hashPassword($password, $salt) {
        if (empty($password)) {
            return FALSE;
        }
        return md5(md5($password) . $salt);
        //return sha1($password . $salt);
    }

    public static function hashPasswordDb($userId, $password) 
    {
        if (empty($userId) || empty($password)) {
            return false;
        }

        $storage = My_Zend_Globals::getStorage();
        $select  = $storage->select()
            ->from(self::_TABLE_USER, 'salt')
            ->where('user_id = ?', $userId)
            ->limit(1, 0);
        $data = $storage->fetchRow($select);
        if (count($data) !== 1) {
            return false;
        }
        return self::hashPassword($password, $data['salt']);
    }

    public static function createUserSalt($length = 10) {
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= chr(rand(33, 126));
        }
        return $salt;
    }

    public static function login($email, $password) 
    {
        $email = trim($email);
        if (empty($email) || empty($password)) {
            return false;
        }

        try {

            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                ->from(self::_TABLE_USER, '*')
                ->where('email = ?', $email)
                ->where('status = 1')
                ->limit(1, 0);
            $user = $storage->fetchRow($select);
            if (!empty($user)) 
            {
                $password = self::hashPasswordDb($user['user_id'], $password);
                if ($user['password'] === $password) {
                    //self::createRememberMe($user['user_id']);
                    return self::setlogin($user['user_id'], $password);
                }
            }

            return false;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function setlogin($userId, $email) 
    {
        $auth = Zend_Auth::getInstance();

        Zend_Session::rememberMe(30 * 24 * 3600);
        $auth->setStorage(new Zend_Auth_Storage_Session('Default'));
        
        $identity = new stdClass();
        $identity->user_id = $userId;
        //$identity->email = $email;

        $auth->getStorage()->write($identity);
        $_SESSION['userid'] = $userId;
        // update last login time
        $data = array(
            'user_id'   => $userId,
            'lastlogin' => time()
        );
        self::updateUser($data);

        // set cookie clear cache page
        setcookie("lcache", 1, time()+30*24*3600, '/');
        setcookie("_hu", 1, time()+30*24*3600, '/');
        return true;
    }

    public static function createRememberMe($userid) 
    {
        // Generate a new remember me key
        $newKey = md5(uniqid(rand(), true));
        $user = self::getUser($userid, true);
        $data = array(
            'user_id'   => $user['user_id'],
            'remember_me_key' => $newKey
        );
        self::updateUser($data);
        // Set a new cookie
        $cookieContent = $user['user_id'] . '/' . md5($user['salt'] . $newKey);
        setcookie('remember_me', $cookieContent, time() + 86400 * 30, '/');
        return true;
    }

    public static function deleteRememberMe()
    {
        $data = array(
            'user_id'   => LOGIN_UID,
            'remember_me_key' => ''
        );
        self::updateUser($data);
        setcookie('remember_me', '', time() - 3600);
        setcookie('lcache', 0, time() - 3600, '/');
        setcookie("_hu", 0, time() - 3600, '/');
        unset($_COOKIE['lcache']);
    }

    public static function getRememberMe() 
    {
        if (isset($_SESSION['userid']) && $_SESSION['userid'] > 0)
        {
            return $_SESSION['userid'];
        }
        else
        {
            $cookie_content = $_COOKIE['remember_me'];
            if ($cookie_content)
            {
                $params = explode('/', $cookie_content);
                $userid = $params[0];
                $key = $params[1];
                $user = self::getUser($userid, true);

                if ($key == md5($user['salt'] . $user['remember_me_key']))
                {
                    $_SESSION['userid'] = $userid;
                    return $userid;
                }
                else return false;
            }
            return false;
        }
    }

    public static function downloadFacebookAvatar($userId, $userName, $facebookId) 
    {
        $facebookId = trim($facebookId);
        if (empty($facebookId)) {
            return false;
        }

        $config   = My_Zend_Globals::getConfiguration();
        $fileName = $userName .'_'. $userId;
        $url      = 'https://graph.facebook.com/'. $facebookId .'/picture?type=large';
        $path     = 'avatar';
        $upload   = new Upload();
        $url = $upload->download($url, $fileName, $path, $config->photo->user->avatar->width, $config->photo->user->avatar->height);
        return $url;
    }

    public static function userUrl($user, $absolute = false) {
        if (empty($user) || !is_array($user)) {
            return '';
        }

        $url = '/user/' . $user['user_id'];
        if ($absolute)
            $url = BASE_URL . $url;
        return $url;
    }

    public static function getUserMoveHome()
    {
        $storage = My_Zend_Globals::getStorage();
        $select = $storage->select()
            ->from(array('u' => self::_TABLE_USER), 'user_id')
            ->where('approved_new_post = 1')
            ->orWhere('new_post_to_home = 1');
        $data = $storage->fetchAll($select);
        $arrData  = array();
        if(!empty($data))
        {
            foreach($data as $key=>$value)
            {
               $arrData[] =  $value['user_id'];
            }
        }
        return $arrData;
    }

    public static function getList($filters = array(), $offset = 0, $limit = 25, $options = array('order_by' => 'user_id desc')) {
        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                ->from(array('u' => self::_TABLE_USER), '*')
                ->limit($limit, $offset);

            if (isset($filters['not_user_id'])) {

                if (is_numeric($filters['not_user_id'])) {
                    $select->where('u.user_id <> ?', $filters['not_user_id']);
                } elseif (is_array($filters['not_user_id'])) {
                    $select->where('u.user_id NOT IN (?)', $filters['not_user_id']);
                }
            }

            if (isset($filters['status']) && !empty($filters['status'])) {
                $select->where('status = ?', $filters['status']);
            }

            if (isset($filters['user_id']) && !empty($filters['user_id'])){
                $select->where('user_id = ?', $filters['user_id']);
            }

            if (isset($filters['is_verified']) && !empty($filters['is_verified'])){
                $select->where('is_verified = ?', $filters['is_verified']);
            }

            if (isset($filters['email']) && !empty($filters['email'])) {
                $select->where('email LIKE ?', '%' . trim($filters['email']) . '%');
            }

            if (isset($filters['created_from']) && !empty($filters['created_from'])){
                $select->where('addtime >= ?', $filters['created_from']);
            }

            if (isset($filters['created_to']) && !empty($filters['created_to'])){
                $select->where('addtime <= ?', $filters['created_to']);
            }

            if (isset($options['order_by'])) {
                $select->order($options['order_by']);
            }

            $data = $storage->fetchAll($select);
            if (!empty($data)) {
                foreach ($data as &$user) {
                    $user['fullname'] = htmlspecialchars($user['fullname']);
                }
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('User::getList - ' . $ex->getMessage());
            return false;
        }
        return $data;
    }

    public static function getUsersForPoint()
    {
        $db = My_Zend_Globals::getStorage();
        try
        {
            $select = $db->select()
                ->from(User::_TABLE_USER, array('user_id', 'like_count_week', 'like_count_month', 'like_point'))
                ->where('posts > 0')
                ->where('status = 1');
            $data = $db->fetchAll($select);
        }
        catch (Exception $ex)
        {
            My_Zend_Logger::log('User::getUsersForPoint - ' . $ex->getMessage());
            return false;
        }
        return $data;
    }

    public static function countTotal($filters=array()) 
    {
        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                ->from(self::_TABLE_USER, 'count(user_id) as total');

            if (isset($filters['status'])) {
                $select->where('status = ?', $filters['status']);
            }

            if (isset($filters['is_verified'])) {
                $select->where('is_verified = ?', $filters['is_verified']);
            }

            if (isset($filters['email']) && !empty($filters['email'])) {
                $select->where('email LIKE ?', '%' . trim($filters['email']) . '%');
            }

            if (isset($filters['created_from'])) {
                $select->where('addtime >= ?', $filters['created_from']);
            }

            if (isset($filters['created_to'])) {
                $select->where('addtime <= ?', $filters['created_to']);
            }

            $data = $storage->fetchRow($select);
            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('User::countTotal - ' . $ex->getMessage());
            return false;
        }

        return $data;
    }
    public static function getUsersPoints($type,$limit = 5)
    {
        $list       = array();
        $not_user_id = User::getUserMoveHome();

        $filters    = array('status'=>1,'not_user_id'=>$not_user_id) ;
        switch($type)
        {
            case "week":
                $options = array('order_by' => 'like_count_week DESC');
                break;

            case "month":
                $options = array('order_by' => 'like_count_month DESC');
                break;

            case "all":
                $options = array('order_by' => 'like_point DESC');
                break;
        }
        try{

            $cache    = My_Zend_Globals::getCaching();
            $cacheKey = 'getPointUser'. md5(serialize($options).$type.$limit);
            $list     = $cache->read($cacheKey);
            if (empty($list)) {
                $list = self::getList($filters, 0, $limit, $options);
                if (!empty($list)) {
                    $cache->write($cacheKey, $list, 3640); //TODO: Caching_time
                }
            }
            return $list;

        }catch (Exception $ex)
        {
            My_Zend_Logger::log('User::getPointUser - '. $ex->getMessage());
            return $list;
        }
    }
    public static function getTopUser($type, $limit=10, $userId = 0) 
    {
        switch ($type) 
        {
            case self::_TOP_POST_BY_MONTH:
                $options = array('order_by' => 'post_count_month DESC');
                break;
            case self::_TOP_POST_BY_WEEK:
                $options = array('order_by' => 'post_count_week DESC');
                break;
            case self::_TOP_LIKED_BY_MONTH:
                $options = array('order_by' => 'like_count_month DESC');
                break;
            case self::_TOP_LIKED_BY_WEEK:
                $options = array('order_by' => 'like_count_week DESC');
                break;
            case self::_TOP_LIKED_MEMBERS:
                $options = array('order_by' => 'like_count DESC');
                break;
            case self::_TOP_FOLLOWED_MEMBERS:
                $options = array('order_by' => 'followed_number DESC');
                break;
            case self::_TOP_RANK_MEMBERS:
                $options = array('order_by' => 'rank ASC');
                break;
            case self::_TOP_POINTS_WEEK_MEMBERS:
                $options = array('order_by' => 'like_count_week DESC');
                break;
            case self::_TOP_POINTS_MONTH_MEMBERS:
                $options = array('order_by' => 'like_count_month DESC');
                break;
            case self::_TOP_POINTS_ALL_MEMBERS:
                $options = array('order_by' => 'like_point DESC');
                break;
            default:
                $options = array('order_by' => 'posts DESC');
                break;
        }

        try {
                // get cache instance
                $cache    = My_Zend_Globals::getCaching();
                $cacheKey = 'getTopUser'. md5(serialize($options).$type.$limit);
                $list     = $cache->read($cacheKey);

                if (empty($list)) {
                    if(in_array($type,array(self::_TOP_POINTS_WEEK_MEMBERS,self::_TOP_POINTS_MONTH_MEMBERS,self::_TOP_POINTS_ALL_MEMBERS)))
                    {
                        $not_user_id = User::getUserMoveHome();
                        $list = self::getList(array('status' => 1,'not_user_id'=>$not_user_id), 0, $limit, $options);
                    }else
                    {
                        $list = self::getList(array('status' => 1), 0, $limit, $options);
                    }

                    if (!empty($list)) {
                        $cache->write($cacheKey, $list, 3647); //TODO: Caching_time
                    }
                }

                $followedData = array();
                if (!empty($list) && $userId > 0) {
                    $followedId = array();
                    foreach ($list as $user) {
                        $followedId[] = $user['user_id'];
                    }

                    $filter = array(
                        'follower_id' => $userId,
                        'followed_id' => $followedId
                    );

                    $followedData = Follow::getList($filter, 0, $limit, array());
                }

                if (!empty($followedData)) 
                {
                    foreach ($list as &$user) {
                        $isFollowed = false;
                        foreach ($followedData as $item) {
                            if ($item['followed_id'] == $user['user_id']) {
                                $isFollowed = true;
                                //break;
                            }
                        }
                        $user['is_followed'] = $isFollowed;
                    }
                }
            return $list;
        } catch (Exception $ex) {
            My_Zend_Logger::log('User::getTopUser - '. $ex->getMessage());
            return array();
        }
    }

    /**
     * get list multi user by array user id
     * @param array $arrPostId
     * @return array
     */
    public static function getListMultiUser($arrUserId)
    {
        if (empty($arrUserId) || !is_array($arrUserId)) {
            return array();
        }

        $arrUserId = array_unique($arrUserId);
        $cache     = My_Zend_Globals::getCaching();
        $arrKeys   = array();
        $result    = array();
        foreach($arrUserId as $userId) {
            $arrKeys[$userId] = sprintf(self::_cache_key, $userId);
        }
        // recheck data
        $result = $cache->readMulti($arrKeys);

        $data   = array();
        foreach ($arrKeys as $userId => $key) {
            if (!isset($result[$key])) {
                $post = self::getUser($userId);
                if (!empty($post)) {
                    $data[$userId] = $post;
                }
            } else {
                $data[$userId] = $result[$key];
            }
        }
        return $data;
    }
    public static function getUserFacebook($accessToken)
    {
        require_once LIBS_PATH . '/My/facebook/src/facebook.php';
            $facebook = new Facebook(array(
              'appId'  => FACEBOOK_APPID,
              'secret' => FACEBOOK_SECKEY,
            ));
        
        $user_profile = array();
        $facebook->setAccessToken($accessToken);
        $user = $facebook->getUser();
        $checkAccessToken = $facebook->getAccessToken();
        if ($user) 
        {
              try 
              {
                if($checkAccessToken !='')
                {
                 $user_profile = $facebook->api('/me?fields=email,name,id,birthday');
                 return $user_profile;
             }else
             {
               $facebook->setAccessToken($accessToken);
                $user = $facebook->getUser();
                return $user_profile = $facebook->api('/me?fields=email,name,id,birthday');
             }   
          } catch (FacebookApiException $e) {
             $user_profile = $facebook->api('/me?fields=email,name,id,birthday');
            return $user_profile;
          }
        }else
        {
            $facebook->setAccessToken($accessToken);
            $user = $facebook->getUser();
            $user_profile = $facebook->api('/me?fields=email,name,id,birthday');
            return $user_profile;    
        }
        
    }

    public static function countPostToday($userID)
    {
        $result = array();

        try {
            //Get db instance
            $db = My_Zend_Globals::getStorage();
            $table   = Post::_TABLE;
            $select  = $db->select()
                ->from($table)
                ->where('user_id = ?', $userID)
				->where('DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%Y-%m-%d") = CURDATE()');

            $postList = $db->fetchAll($select);

            $totalPost = 0;
            $videoPost = 0;
            $photoPost = 0;

            if (!empty($postList) && is_array($postList))
            {
                foreach ($postList as $post)
                {
					$totalPost += 1;
					if ($post['is_photo'] == 0)
						$videoPost += 1;
					else $photoPost =+ 1;
                }
            }

			$result['total'] = $totalPost;
			$result['video'] = $videoPost;
			$result['photo'] = $photoPost;

			return $result;


		} catch (Exception $ex) {
			My_Zend_Logger::log('User::countPostToday - ' . $ex->getMessage());

			return false;
		}
    }


}
