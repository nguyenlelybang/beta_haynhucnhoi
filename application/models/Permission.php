<?php

class Permission {
    // Admin
    const ADMIN_VIEW_LIST = 100;
    const ADMIN_MANAGE    = 101;
    // Article
    const ARTICLE_ADD          = 200;
    const ARTICLE_EDIT         = 201;
    const ARTICLE_DELETE       = 203;
    const ARTICLE_APPROVE      = 204;
    const ARTICLE_MOVE_TO_HOME = 205;
    const ARTICLE_MANAGE_TAG   = 206;
    // category
    const CATEGORY_ADD    = 101;
    const CATEGORY_EDIT   = 102;
    const CATEGORY_DELETE = 103;
    const CATEGORY_VIEW   = 104;
    // User
    const USER_VIEW   = 300;
    const USER_ADD    = 301;
    const USER_EDIT   = 302;
    const USER_DELETE = 303;
    // Tag
    const TAG_VIEW   = 400;
    const TAG_ADD    = 401;
    const TAG_EDIT   = 402;
    const TAG_DELETE = 403;
    // user
    const USER_LOCK   = 151;
    const USER_UNLOCK = 152;
    public static $details = array(
        'Category' => array(
            self::CATEGORY_VIEW =>  'Xem danh sách / thông tin',
            self::CATEGORY_ADD => 'Thêm',
            self::CATEGORY_EDIT => 'Sửa',
            self::CATEGORY_DELETE => 'Xóa',
        )
    );
    public static $message = "Ban khong co quyen thuc hien lenh nay";
    private static $_list, $_nameList;
    public static function getPermissionList() 
    {
        if (self::$_list == null) {
            $list = array();
            foreach (self::$details as $parent => $sub) {
                $list = array_merge($list, array_keys($sub));
            }
            self::$_list = $list;
        }
        return self::$_list;
    }

    public static function getPermissionNameList()
    {
        if (self::$_nameList == null) {
            $list = array();
            foreach (self::$details as $parent => $sub) {
                foreach ($sub as $id => $name)
                {
                    $list[$id] = $parent . ' - ' . $name;
                }

            }
            self::$_nameList = $list;
        }

        return self::$_nameList;
    }

    public static function getPermissionName($permissionId)
    {
        $nameList = self::getPermissionNameList();
        return $nameList[$permissionId];
    }

    public static function validate($permissions) 
    {
        $list = self::getPermissionList();
        if (is_array($permissions))
        {
            $result = array_diff($permissions, array_diff($permissions, $list));
        }
        else
        {
            $result = in_array($permissions, $list)?$permissions:false;
        }
        return $result;
    }

}