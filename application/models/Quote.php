<?php

class Quote {

    const _TABLE = 'quote';

    public static function initPost($data)
    {
        $fields = array(
            'id',
            'title'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function insert($data)
    {
        $data = self::initPost($data);
        if ($data === false) {
            return false;
        }

        try{

            $storage    = My_Zend_Globals::getStorage();
            $rs         = $storage->insert(self::_TABLE, $data);
            if($rs)
            {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }
    public static function delete($id)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE id = ' . $id);
            return $rs;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }

    public static function update($data)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        try {
                $data = self::initPost($data);
                if ($data === false || !isset($data['id'])) {
                    return false;
                }
                $storage = My_Zend_Globals::getStorage();
                $rs      = $storage->update(self::_TABLE, $data, 'id =' . $data['id']);
                return $rs;
            } catch (Exception $ex)
            {
                My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
                return false;
            }
    }

    public static function getListShow()
    {
        $data = array();
        try {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = 'quote_1';
            $data = $cache->read($cacheKey);
            if (empty($data))
            {
                $db = My_Zend_Globals::getStorage();
                $select = $db->select()
                    ->from(array('p' => self::_TABLE));
                $data = $db->fetchAll($select);
                if (!empty($data)) {
                    $cache->write($cacheKey, $data, 3610); //TODO: Caching_time
                }
            }
            return $data;
        } catch (Exception $ex) {
            return $data;
        }
    }

    public static function getList($limit = 20, $offset = 0, $where = array())
    {
        $data = array();
        try {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = 'vechai_' . $limit . isset($where['pos']) ? $where['pos'] : 1;
            $data = $cache->read($cacheKey);
            if (empty($data))
            {
                $db = My_Zend_Globals::getStorage();
                $select = $db->select()
                    ->from(array('p' => self::_TABLE))
                    ->limit($limit, $offset);
                if (isset($where['pos'])) {
                    if (is_numeric($where['pos'])) {
                        $select->where('p.pos = ?', $where['pos']);
                    } elseif (is_array($where['pos'])) {
                        $select->where('p.pos IN (?)', $where['pos']);
                    }
                }
                $data = $db->fetchAll($select);
                if (!empty($data)) {
                    $cache->write($cacheKey, $data, 3613); //TODO: Caching_time
                }
            }
            return $data;
        } catch (Exception $ex) {
            return $data;
        }
    }
    public static function edit($id)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        if (empty($id)) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
                ->from($table, '*')
                ->where('id = ?', $id)
                ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if(!empty($row))
        {
            $data =$row;
        }
        return $data;
    }

    public static function getTotal($where = array())
    {
        $data = array();
        try {
                $db     =   My_Zend_Globals::getStorage();
                $select =   $db->select()->from(array(self::_TABLE,'p'),'count(id) as total');
                if(isset($where['id']))
                {
                    if(is_numeric($where['id']))
                    {
                        $select->where('p.id = ?', $where['id']);
                    }elseif(is_array($where['id']))
                    {
                        $select->where('p.id IN (?)', $where['id']);
                    }
                }
                $data = $db->fetchRow($select);
                $data = isset($data['total']) ? $data['total'] : 0;
            return $data;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
            return $data;
        }
    }
}
