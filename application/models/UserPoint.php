<?php

class UserPoint {

	protected $userID;
	const _TABLE_TOP_POINT = 'top_point';

	public function __construct($userID = null) {
		if ($userID) {
			$this->userID = $userID;
		}
	}

	/**
	 * @param string|null $dateRange Specify the date rage for calculating point. Available options are 'week', 'month' or 'year' if set to NULL. Defauts: NULL
	 */
	public function calculateAllUserPoint()
	{
		$error = false;
		$userPointTable = array();

		if (date('d', time()) == '01' && intval(date('G', time())) < 2)
		{
			$prevMonth = date('m', strtotime('last month'));
			echo "Saving top point of previous month... \n";
			$this->saveTopPoint('m', $prevMonth);
		}

		if (date('N', time()) == 1 && intval(date('H', time())) < 2)
		{
			$prevWeek = date('W', strtotime('last week'));
			echo "Saving top point of previous week... \n";
			$this->saveTopPoint('w', $prevWeek);
		}

		echo "Calculating user point...\n";

		$userList = User::getUsersForPoint();
		$postFilter = array(
			'is_actived' => 1,
			'is_approved' => 1
		);
		if (!empty($userList) && is_array($userList))
		{
			set_time_limit(0);
			foreach ($userList as $user)
			{
				$postFilter['user_id'] = $user['user_id'];
				$userPointTable[$user['user_id']]['ID'] = $user['user_id'];
				$postList = Post::getPostsForCalculatingPoint($user['user_id']);
				$totalPoint = 0;
				$weekPoint = 0;
				$monthPoint = 0;
//				$userLike = 0;
//				$userComment = 0;
//				$userView = 0;
				if (!empty($postList) && is_array($postList))
				{
					foreach ($postList as $post) {

						$point = 0;

						$point += $post['number_of_like'];

						/* Co che cu */

						/*
						if ($post['is_photo']) $point += 5;
						else $point += 3;

						if ($post['phase'] == 1) $point += 5;
						if ($post['phase'] == 2) $point += 15; //Bao gom ca diem cua Trang chu & Nhuc Nhoi, Nhuc nhoi 10 diem;

						if (!empty($post['category_id']) && intval($post['category_id']) > 0) $point += 1;

						if ($post['number_of_like'] > 0)
							$point += $this->convertLikeToPoint($post['number_of_like']);

						if ($post['number_of_view'] > 0)
							$point += floor($post['number_of_view'] / 500);

						if ($post['number_of_comment'] > 0)
							$point += floor($post['number_of_comment'] / 5); */

						//$userLike += $post['number_of_like'];
						//$userComment += $post['number_of_comment'];
						//$userView += $post['number_of_view'];

						if (date('W', $post['approved_at']) == date('W', time()) && date('Y', $post['approved_at']) == date('Y', time())) {
							$weekPoint += $point;
						}

						if (date('m', $post['approved_at']) == date('m', time()) && date('Y', $post['approved_at']) == date('Y', time())) {
							$monthPoint += $point;
						}

						$totalPoint += $point;
					}
				}
				$dataToUpdate = array(
					'user_id' => $user['user_id'],
					'like_point' => $totalPoint,
					'like_count_week' => $weekPoint,
					'like_count_month' => $monthPoint
				);
				User::updateUser($dataToUpdate, false);
			}
		}
		else
		{
			$error = 'Empty user list';
		}

		if ($error) {
			echo "Error: " . $error . "\n";
		}
		else {
			echo "User point successfully updated \n";
		}
	}

	public static function updateUserPostCount()
	{
		$db = My_Zend_Globals::getStorage();
		$query = 'UPDATE '. User::_TABLE_USER . ' SET posts = (SELECT COUNT(post_id) AS posts FROM ' . Post::_TABLE . ' WHERE '  . Post::_TABLE .'.user_id = ' . User::_TABLE_USER. '.user_id)';
		$update = $db->query($query);
		$db->closeConnection();
		if ($update) {
			echo "Updating user post count successfully \n";
		}
		else {
			echo "Cannot update user post count \n";
		}

	}

	public static function updateUserRank()
	{
		$members = User::_TABLE_USER;
		$excludedIDs = array_unique(array_merge(explode(',', EXCLUDED_USER_ID_POINT), User::getUserMoveHome()));
		$db = My_Zend_Globals::getStorage();
		$query1 = 'SET @curRank = 0';
		$query2 = 'UPDATE ' . $members . ' JOIN (SELECT m.user_id, @curRank := @curRank + 1 AS rank
          FROM ' . $members . ' m
          JOIN (SELECT @curRank := 0) r
          WHERE m.user_id NOT IN ( ' . implode(',',$excludedIDs) . ') AND m.status = 1
          ORDER BY m.like_point DESC)
          ranks ON (ranks.user_id = ' . $members . '.user_id) SET ' . $members .'.rank = ranks.rank;';
		$db->update($members, array('rank' => 0));
		$db->query($query1);
		if ($db->query($query2))
		{
			echo "Updating user rank successfully \n";
		}
		else
		{
			echo "Cannot update user rank \n";
		}
		$db->closeConnection();
	}

	protected function convertLikeToPoint($number)
	{
		$point = 0;

		if ($number >= 50) $point += 5;
		if ($number >= 100) $point += 15;
		if ($number >= 300) $point += 45;
		if ($number >= 500) $point += 75;
		if ($number >= 1000) $point += 150;
		if ($number >= 2000) $point += 300;
		if ($number >= 3000) $point += 450;
		if ($number >= 5000) $point += 750;

		return $point;
	}

	public static function getTopPoint($dateRange, $limit, $useCache = true)
	{
		$data = array();

		if ($dateRange == 'month')
			$pointField = 'like_count_month';
		elseif ($dateRange == 'week')
			$pointField = 'like_count_week';
		else $pointField = 'like_point';

		if ($limit && is_numeric($limit) && $limit > 0) {
			$queryLimit = $limit;
		}
		else $queryLimit = 30;

		if ($useCache) {
			$cacheKey = 'v2_top_point_' . $dateRange . '_' . $queryLimit;
			$caching = My_Zend_Globals::getCaching();
			$data = $caching->read($cacheKey);
		}

		if (empty($data)) {
			try {
				$db = My_Zend_Globals::getStorage();

				$excludedIDs = array_unique(array_merge(explode(',', EXCLUDED_USER_ID_POINT), User::getUserMoveHome()));

				$select = $db->select()
					->from(User::_TABLE_USER)
					->where('user_id NOT IN (?)', $excludedIDs)
					->where('status = 1')
					->order(array("$pointField DESC"))
					->limit($queryLimit);

				$data = $db->fetchAll($select);

				if (is_array($data)) {
					foreach ($data as &$user) {
						$user['point'] = $user[$pointField];
					}
				}

				if ($useCache) {
					$caching->write($cacheKey, $data, 60 * 60);
				}
			}
			catch (Exception $ex) {
				My_Zend_Logger::log('UserPoint::GetTopPoint - ' . $ex->getMessage());
			}
		}
		return $data;
	}

	public static function getPointByUserID($userID)
	{
		$data = array();
		if (!empty($userID) && is_numeric($userID))
		{
			$db = My_Zend_Globals::getStorage();
			$select = $db->select()
				->from(User::_TABLE_USER, array('user_id', 'week_point', 'month_point', 'all_point'))
				->where(array('user_id' => $userID));
			$data = $db->fetchRow($select);
		}
		return $data;
	}

	/**
	 * @param string $type Type of point to save: w (weekly) or m (monthly)
	 */
	public static function saveTopPoint($type = 'w', $time)
	{
		$db = My_Zend_Globals::getStorage();
		$table = 'top_point';
		if ($type == 'm') {
			$timeCode = self::genTimeCode('m', $time);
			$field = 'month_point';
		} else {
			$timeCode = self::genTimeCode('w', $time);
			$field = 'week_point';
		}

		$excludedIDs = array_unique(array_merge(explode(',', EXCLUDED_USER_ID_POINT), User::getUserMoveHome()));

		$select = $db->select()
			->from(User::_TABLE_USER, array('user_id', $field))
			->where('user_id NOT IN (?)', $excludedIDs)
			->where('status = 1')
			->order($field . ' DESC')
			->limit(50);

		$topPoint = $db->fetchAll($select);

		if (!empty($topPoint) && is_array($topPoint)) {
			$db = My_Zend_Globals::getStorage();

			foreach ($topPoint as $user) {
				$data = array(
					'user_id' => $user['user_id'],
					'timecode' => $timeCode,
					'point' => $user[$field]
				);
				$insert = $db->insert($table, $data);
				if (!$insert) {
					echo "Error updating top " . $type . " point for user id = " . $user['user_id'];
					exit;
				}
			}
			echo "Successfully updated " . $type . " point\n";
			$db->closeConnection();
		}
		else
		{
			echo "Error: Empty top ' . $type . ' point\n";
		}
	}

	public static function getWeekMonthTop($timecode)
	{
		$db = My_Zend_Globals::getStorage();
		$pointList = array();

		$select = $db->select()
			->from(array('t' => self::_TABLE_TOP_POINT))
			->join(array('m' => User::_TABLE_USER), 't.user_id = m.user_id')
			->where('timecode = ?', $timecode)
			->order('point DESC');
		$members = $db->fetchAll($select);

		if ($members) {
			foreach ($members as $member) {
				$m['user_id'] = $member['user_id'];
				$m['point'] = $member['point'];
				$m['fullname'] = $member['fullname'];
				$pointList[] = $m;
			}

			return $pointList;

		} else {
			return false;
		}
	}

	public static function genTimeCode($type = 'w', $time)
	{
		if ($type == 'w') {
			return 'w' . $time . date('Y', time());
		} else {
			return 'm' . $time . date('Y', time());
		}

	}
}