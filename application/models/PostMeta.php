<?php

class PostMeta
{
	const _TABLE = 'posts_meta';
	const _META_CACHE_KEY = 'post_meta_%s_%s';

	/**
	 * @param $postID
	 * @param $metaKey
	 * @return array|bool
	 * @throws Exception
	 */

	public static function get($postID, $metaKey = null, $useCache = true)
	{
		$data = array();

		if ($useCache) {
			$cache = My_Zend_Globals::getCaching();
			$cacheKey = sprintf(self::_META_CACHE_KEY, $postID, $metaKey);
			$data = $cache->read($cacheKey);
		}

		if (empty($data)) {

			$db = My_Zend_Globals::getStorage();

			$select = $db->select()
				->from(self::_TABLE)
				->where('post_id = ?', $postID);

			if ($metaKey != null) {
				$select->where('meta_key = ?', $metaKey);
			}

			if($result = $db->fetchAll($select)) {

				if ($metaKey != null) {
					$data = $result[0]['meta_value'];
				} else {
					foreach ($result as $meta) {
						$data[$meta['meta_key']] = $meta['meta_value'];
					}
				}

				if ($useCache) {
					$cache->write($cacheKey, $data, 900);
				}
			} else {
				return false;
			}
		}

		return $data;
	}

	public static function update($postID, $metaKey, $metaValue)
	{
		$db = My_Zend_Globals::getStorage();

		$check = self::check($postID, $metaKey);

		if ($check == 0) {
			if ($metaValue) {
				$data = array(
					'post_id' => $postID,
					'meta_key' => $metaKey,
					'meta_value' => $metaValue
				);
				$db->insert(self::_TABLE, $data);
			}
		} else {
			$cache = My_Zend_Globals::getCaching();
			$cacheKey = sprintf(self::_META_CACHE_KEY, $postID, $metaKey);
			if (empty($metaValue) || $metaValue == '' || $metaValue === 0) {
				self::delete($postID, $metaKey);
			} else {
				$data = array(
					'meta_value' => $metaValue
				);
				$update = $db->update(self::_TABLE, $data, array(
					'post_id = ?' => $postID,
					'meta_key = ?' => $metaKey
				));
				if ((isset($update) && $update)) {
					$cache->delete($cacheKey);
				}
			}
		}
	}

	public static function delete($postID, $metaKey)
	{
		$db = My_Zend_Globals::getStorage();

		$delete = $db->delete(self::_TABLE, array(
			'post_id = ?' => $postID,
			'meta_key = ?' => $metaKey
		));

		if ($delete) {
			$cache = My_Zend_Globals::getCaching();
			$cacheKey = sprintf(self::_META_CACHE_KEY, $postID, $metaKey);
			$cache->delete($cacheKey);
			return true;
		}
		else return false;
	}

	public static function check($postID, $metaKey)
	{
		$db = My_Zend_Globals::getStorage();
		$select = $db->select()
			->from(self::_TABLE)
			->where('post_id = ?', $postID)
			->where('meta_key = ?', $metaKey);
		$count = count($db->fetchAll($select));

		return $count;
	}

}