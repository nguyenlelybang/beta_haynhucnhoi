<?php

class Category {

    const table_category          = 'categories';
    const _cache_key_detail       = 'category_%s';
    const _cache_key_list         = 'category_list_%s';
	const _CATEGORY_LIST_HOME_MENU = 'category_menu_home';
    const _cache_key_alias        = 'category_alias_%s';
    private static $_category     = null;
    private static $_categoryTree = null;

    public static function initCategory($data)
    {
        $fields = array(
            'category_id',
            'category_name',
            'description',
            'alias',
            'page_title',
            'meta_keyword',
            'meta_description',
            'parent_id',
            'is_hidden',
            'ordering',
            'is_active_home',
            'is_grid',
            'is_hot',
            'category_customize',
            'is_url'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        if (isset($rs['alias'])) {
            $rs['alias'] = My_Zend_Globals::aliasCreator($rs['alias']);
        }else {
            $rs['alias'] = My_Zend_Globals::aliasCreator($rs['category_name']);
        }
        return $rs;
    }

    public static function insertCategory($data)
    {
        $data = self::initCategory($data);
        if ($data === false) {
            return false;
        }

        try {
            $storage = My_Zend_Globals::getStorage();
            $table   = self::table_category;
            $rs      = $storage->insert($table, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Category::insertCategory - ' . $ex->getMessage());
            return false;
        }
    }

	public static function updateCategory($data)
	{
		try {
			$data = self::initCategory($data);
			if ($data === false || ! isset($data['category_id'])) {
				return false;
			}
			$storage = My_Zend_Globals::getStorage();
			$table   = self::table_category;
			$rs      = $storage->update($table, $data, 'category_id = ' . $data['category_id']);
            if ($rs) {
                $cache = My_Zend_Globals::getCaching();
                $cache->delete(sprintf(self::_cache_key_detail, $data['category_id']));
                $cache->delete(sprintf(self::_cache_key_alias, str_replace('-', '_', $data['alias'])));
            }
			return $rs;
		} catch (Exception $ex) {
			My_Zend_Logger::log('Category::updateCategory - ' . $ex->getMessage());
			return false;
		}
	}

    public static function deleteCategory($categoryId)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::table_category;
            $rs      = $storage->query('DELETE FROM ' . $table . ' WHERE category_id = ' . $categoryId);
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Category::deleteCategory - ' . $ex->getMessage());
            return false;
        }
    }
    public static function selectSinglePath($categoryId)
    {
        if (empty($categoryId)) {
            return false;
        }

        $sub  = self::selectCategory($categoryId);
        $main = self::selectCategory($sub['parent_id']);
        $data = array();

        if (!empty($main)) {
            $data[] = $main;
        }

        $data[] = $sub;

        return $data;
    }

    public static function selectCategory($categoryId, $useCache = true)
    {
        if (empty($categoryId)) {
            return false;
        }

        if (isset(self::$_category[$categoryId])) {
            return self::$_category[$categoryId];
        }

        $data = array();

        if ($useCache) {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key_detail, $categoryId);
            $data = $cache->read($cacheKey);
        }

        if (empty($data)) {
            try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::table_category;
                $select = $storage->select()
                    ->from($table, '*')
                    ->where('category_id = ?', $categoryId)
                    ->limit(1, 0);
                $data = $storage->fetchRow($select);
            } catch (Exception $ex) {
                My_Zend_Logger::log('Category::selectCategory - ' . $ex->getMessage());
                return $data;
            }
            self::$_category[$categoryId] = $data;
            if ($useCache) {
                $cache->write($cacheKey, $data, 3600);
            }
        }
        return $data;
    }

    public static function selectCategoryByAlias($categoryAlias, $useCache = true)
    {
        if (empty($categoryAlias)) {
            return false;
        }

        if (isset(self::$_category[$categoryAlias])) {
            return self::$_category[$categoryAlias];
        }
        $category = array();

        if ($useCache) {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key_alias, str_replace('-', '_', $categoryAlias));
            $category = $cache->read($cacheKey);
        }

        if (empty($category)) {
            try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $select  = $storage->select()
                    ->from(self::table_category, '*')
                    ->where('alias = ?', $categoryAlias)
                    ->limit(1, 0);
                $category = $storage->fetchRow($select);
            } catch (Exception $ex) {
                My_Zend_Logger::log('Category::selectCategoryByAlias - ' . $ex->getMessage());
                return $category;
            }
            if ($useCache) {
                $cache->write($cacheKey, $category, 3600);
            }
        }

        self::$_category[$categoryAlias] = $category;
        return $category;
    }

    public static function selectCategoryList($filters = array(), $useCache = true, $cacheKey = false)
    {
        $data = array();

        try {
            // get cache instance
            if ($useCache) {
                $cache    = My_Zend_Globals::getCaching();
	            if (!is_string($cacheKey)) {
		            $cacheKey = sprintf(self::_cache_key_list, md5(serialize($filters)));
	            }
                $data = $cache->read($cacheKey);
            }

            if (empty($data))
            {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::table_category;
                $select = $storage->select()
                        ->from($table, '*')
                        ->order('ordering');

                if (isset($filters['parent_id'])) {
                    $select->where('parent_id = ?', $filters['parent_id']);
                }

                if (isset($filters['show_menu'])) {
                    $select->where('show_menu = ?', $filters['show_menu']);
                }

                if (isset($filters['is_hidden'])) {
                    $select->where('is_hidden = ?', $filters['is_hidden']);
                }

                if (isset($filters['exclude_empty']) && $filters['exclude_empty'] == true) {
                    $select->where('category_name <> ""');
                }

                if (isset($filters['category_ids']) && ($filters['category_ids'])) {
                    $select->where('category_id IN (?)', $filters['category_ids']);
                }

                if (isset($filters['category_id']) && ($filters['category_id'])) {
                    $select->where('category_id = (?)', $filters['category_id']);
                }

                if (isset($filters['offset']) && isset($filters['limit'])) {
                    $select->limit($filters['limit'], $filters['offset']);
                }

                $data = $storage->fetchAll($select);

                if (!empty($data) && $useCache) {
                    $cache->write($cacheKey, $data, 3601); //TODO: Caching_time
                }
            }

        } catch (Exception $ex) {
            My_Zend_Logger::log('Category::selectCategoryList - ' . $ex->getMessage());
            return false;
        }
        return $data;
    }

    public  static function getListCategoryNoActiveHome()
    {
        $data = array();
        try
        {
            $cache  = My_Zend_Globals::getCaching();
            $data   = $cache->read('List_Category_Active_Home');
            if(empty($data))
            {
                $db =   My_Zend_Globals::getStorage();
                $tbl =   self::table_category;
                $rs =   $db->select()
                        ->from($tbl)
                        ->where('is_active_home = ?',0)
                        ->order('category_id DESC');
                $data   =   $db->fetchAll($rs);
                $cache->write('List_Category_Active_Home', $data, 3607); //TODO: Caching_time
            }

        }catch(Exception $ex) {
            My_Zend_Logger::log('List Category Active Home'. $ex->getMessage());
        }
        return  $data;
    }
    public static function selectChildCategoryList($categoryId)
    {
        if (empty($categoryId)) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::table_category;
            $select = $storage->select()
                    ->from($table, '*')
                    ->where('parent_id = ?', $categoryId)
                    ->order('ordering asc');
            $data = $storage->fetchAll($select);
            return $data;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Category::selectChildCategoryList - ' . $ex->getMessage());
            return array();
        }
    }

    public static function selectChildCategory($categoryId)
    {
        $arrCategory = self::selectChildCategoryList($categoryId);
        return $arrCategory;
    }

    public static function selectCategoryTree()
    {
        if (!is_null(self::$_categoryTree))
            return self::$_categoryTree;

        $tree       = array();
        $parentList = self::selectCategoryList(array('parent_id' => 0));
        $i          = 0;
        if ($parentList)
        {
            foreach ($parentList as $parent)
            {
                $parent['sub'] = self::selectChildCategoryList($parent['category_id']);
                if (count($parent['sub']) > 1) {
                    //$parent['sub'] = array_slice($parent['sub'], 1);
                }
                $tree[] = $parent;
            }
        }
        return $tree;
    }

    public static function selectMenuCategoryTree()
    {
        if (!is_null(self::$_categoryTree))
            return self::$_categoryTree;

        $tree       = array();
        $parentList = self::selectCategoryList(
            array(
                'parent_id' => 0,
                'is_hidden' => 0,
                'show_menu' => 1
                ));
        $i          = 0;

        if ($parentList) {
            foreach ($parentList as $parent) {
                $parent['sub'] = self::selectChildCategoryList($parent['category_id']);

                if (count($parent['sub']) > 1) {
                    //$parent['sub'] = array_slice($parent['sub'], 1);
                }

                $tree[] = $parent;
            }
        }
        return $tree;
    }

    public static function categoryUrl($category, $absolute = false)
    {
        if (empty($category) || !is_array($category)) {
            return '';
        }
        $url = '/chu-de/' . $category['alias'] .'/';
        if ($absolute) {
            $url = BASE_URL . $url;
        }
        return $url;
    }

}
