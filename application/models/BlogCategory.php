<?php
class BlogCategory {
    const _TABLE = 'blog_categories';

    public static function initCategory($data)
    {
        $fields = array(
            'category_id',
            'category_name',
            'category_alias'
        );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        if (isset($rs['category_alias'])) {
            $rs['category_alias'] = My_Zend_Globals::aliasCreator($rs['category_alias']);
        }else {
            $rs['category_alias'] = My_Zend_Globals::aliasCreator($rs['category_name']);
        }
        return $rs;
    }

    public static function insert($data) {
        $data = self::initCategory($data);
        if($data === false)
            return false;

        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->insert(self::_TABLE,$data);
            return $rs;

        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogCategory::get - ' . $ex->getMessage());
            return $data;
        }
    }

    public static function getAll() {
        $data = array();
        try {
            if(empty($data)) {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from(array('c' => $table));
                $data = $storage->fetchAll($select);
            }
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogCategory::get - ' . $ex->getMessage());
            return $data;
        }
        return $data;
    }

    public static function get($filters = array()) {

        $data = array();

        try {
            if(empty($data)) {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from($table, '*')
                    ->limit(1, 0);
                if (isset($filters['category_alias'])) {
                    $select->where('category_alias = ?', $filters['category_alias']);
                }
                if (isset($filters['category_id'])) {
                    $select->where('category_id = ?', $filters['category_id']);
                }
                $data = $storage->fetchRow($select);
            }
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogCategory::get - ' . $ex->getMessage());
            return $data;
        }
        return $data;
    }

    public static function delete($categoryID) {
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->delete(self::_TABLE,'category_id = ' . $categoryID);
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('BlogCategory::delete - ' . $ex->getMessage());
            return false;
        }
    }

    public static function update($data) {
        $data = self::initCategory($data);
        if($data === false)
            return false;

        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->update(self::_TABLE, $data, 'category_id =' . $data['category_id']);
            return $rs;

        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogCategory::get - ' . $ex->getMessage());
            return $data;
        }
    }
}