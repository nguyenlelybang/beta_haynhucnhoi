<?php
class BlogPost {

    const _TABLE = 'blog_posts';
    const _cache_key = 'blog_post_%s';
    const _cache_key_list = 'blog_posts_%s';

    public static function initBlogPost($data)
    {
        $fields = array(
            'post_id',
            'user_id',
            'category_id',
            'post_status',
            'post_title',
            'post_image',
            'post_created',
            'post_summary',
            'post_content',
            'post_views'
        );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function insert($data) {
        $data = self::initBlogPost($data);
        if($data === false)
            return false;
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->insert(self::_TABLE,$data);
            return $rs;

        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogPost::insert - ' . $ex->getMessage());
            return $data;
        }
    }

    public static function update($data, $updateCache = true) {

        try {
            $data = self::initBlogPost($data);
            if($data === false || !isset($data['post_id'])) {
                return false;
            }
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->update(self::_TABLE, $data, 'post_id =' . $data['post_id']);
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogPost::update - ' . $ex->getMessage());
            return false;
        }
        if ($rs && $updateCache)
        {
            $postID     = $data['post_id'];
            $cache      = My_Zend_Globals::getCaching();
            $cacheKey   = sprintf(self::_cache_key, $postID);
            $postDetail = self::get($postID, false);
            $postDetail = array_merge($postDetail, $data);
            $cache->write($cacheKey, $postDetail, 900);
        }
        return $rs;
    }

    public static function delete($postID) {
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->delete(self::_TABLE,'post_id = ' . $postID);
            $cache    = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key, $postID);
            $cache->delete($cacheKey);
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('BlogPost::delete - ' . $ex->getMessage());
            return false;
        }
    }

    public static function get($postID, $useCache = false) {
        if(empty($postID))
            return false;
        $data = array();

        try {
            if ($useCache) {
                $cache = My_Zend_Globals::getCaching();
                $cacheKey = sprintf(self::_cache_key, $postID);
                $data = $cache->read($cacheKey);
            }
            if(empty($data)) {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from($table, '*')
                    ->where('post_id = ?', $postID)
                    ->limit(1, 0);
                $data = $storage->fetchRow($select);
                if (!empty($data) && $useCache) {
                    $storage->closeConnection();
                    $cache->write($cacheKey, $data, 900);
                }
            }
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogPost::get - ' . $ex->getMessage());
            return $data;
        }
        return $data;
    }

    public static function getList($filters = array(), $useCache = false) {
        $data = array();
        if ($useCache) {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key_list, md5(serialize($filters)));
            $data = $cache->read($cacheKey);
        }

        if (empty($data)) {
            try {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from(array('p' => $table))
                    ->order(array('post_created DESC'));
                if (isset($filters['offset']) && isset($filters['limit'])) {
                    $select->limit($filters['limit'], $filters['offset']);
                }
                if (isset($filters['category_id'])) {
                    $select->where('p.category_id = ?', $filters['category_id']);
                }
                if (isset($filters['post_status'])) {
                    $select->where('p.post_status > ?', $filters['post_status']);
                }
                if (isset($filters['orderByID'])) {
                    $select->order($filters['orderByID']);
                }
                $data = $storage->fetchAll($select);
            } catch(Exception $ex) {
                My_Zend_Logger::log('BlogPost::getList - ' . $ex->getMessage());
                return $data;
            }
            if ($useCache) {
                $cache->write($cacheKey, $data, 3600);
            }
        }
        return $data;
    }

    public static function getListOrderByID() {
        $data = array();
        if (empty($data)) {
            try {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from(array('p' => $table))
                    ->order('post_id DESC');
                $data = $storage->fetchAll($select);
            } catch(Exception $ex) {
                My_Zend_Logger::log('BlogPost::getList - ' . $ex->getMessage());
                return $data;
            }
        }
        return $data;
    }

    public static function postUrl($post)
    {
        if (empty($post) || !is_array($post)) {
            return '';
        }
        $url = '/blog/' . $post['post_id'];
        $url .= '/' . My_Zend_Globals::convertToSEO($post['post_title']) . '.hnn';
        return $url;
    }

    public static function categoryUrl($category)
    {
        if (empty($category) || !is_array($category)) {
            return '';
        }
        $url = BASE_URL . '/blog/chu-de/' . $category['category_alias'];
        return $url;
    }

    public static function getNextPost($postID, $useCache = false) {

        $cacheKey = 'next_blog_post_of_' . $postID;
        if(empty($postID))
            return false;

        $data = array();
        if ($useCache) {
            $caching = My_Zend_Globals::getCaching();
            $data = $caching->read($cacheKey);
        }
        try {
            if(empty($data)) {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from($table, '*')
                    ->where('post_id > ?', $postID)
                    ->order('post_id ASC')
                    ->limit(1, 0);
                $data = $storage->fetchRow($select);
            }
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogPost::getNextPost - ' . $ex->getMessage());
            return $data;
        }
        if ($data && $useCache) {
            $caching->write($cacheKey, $data, 3607); //TODO: Caching_time
        }
        return $data;
    }

    public static function getPrevPost($postID, $useCache = false) {

        $cacheKey = 'prev_blog_post_of_' . $postID;
        if(empty($postID))
            return false;
        $data = array();

        if ($useCache) {
            $caching = My_Zend_Globals::getCaching();
            $data = $caching->read($cacheKey);
        }
        try {
            if(empty($data)) {
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from($table, '*')
                    ->where('post_id < ?', $postID)
                    ->order('post_id DESC')
                    ->limit(1, 0);
                $data = $storage->fetchRow($select);
            }
        } catch(Exception $ex) {
            My_Zend_Logger::log('BlogPost::getNextPost - ' . $ex->getMessage());
            return $data;
        }
        if ($data && $useCache) {
            $caching->write($cacheKey, $data, 3607); //TODO: Caching_time
        }
        return $data;
    }
}