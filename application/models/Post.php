<?php

class Post {

    const _TABLE                       = 'posts';
    const _TABLE_POST_TAGS             = 'posts_tags';
    const _NUMBER_VIEWS_TO_HOMEPAGE    = 500;
    const _NUMBER_LIKE_TO_HOMEPAGE     = 5;
    const _NUMBER_SHARE_TO_HOMEPAGE    = 5;
    const _NUMBER_COMMENT_TO_HOME      = 3;
    const _NUMBER_COMMENT_TO_HOT       = 20;
    const _NUMBER_VIEWS_TO_HOT         = 1000;
    const _NUMBER_LIKE_TO_HOT          = 300;
    const _NUMBER_SHARE_TO_HOT         = 20;
    const _PHASE_HOT                   = 2;
    const _PHASE_HOMEPAGE              = 1;
    const _PHASE_NEW_POST              = 0;
    const _NUMBER_OF_LIKE_NOTIFICATION = 50;
    const _NUMBER_OF_VOTE_TO_HOMEPAGE  = 5;
    const _cache_key                   = 'post3_%s';
    const _cache_key_active_list       = 'laids_';

    private static $_post = null;

    public static function initPost($data)
    {
        $fields = array(
            'post_id', 
            'story',
            'description',
            'user_id', 
            'source', 
            'category_id', 
            'nsfw', 
            'url', 
            'tags', 
            'phase',
            'is_photo',
            'is_meme',
            'is_gif',
            'picture', 
            'picture_share',
            'picture_upload',
            'youtube_key',
            'vmo_key', 
            'zingtv_key',
            'mecloud_key',
            'nct_mp3',
            'nct_video',
            'is_hidden',
            'is_approved', 
            'pip', 
            'pip2',
            'is_actived', 
            'is_featured', 
            'created_at', 
            'updated_at', 
            'approved_at', 
            'number_of_view',
            'number_of_comment', 
            'number_of_share', 
            'number_of_like', 
            'last_viewed',
            'is_grid',
            'favclicks'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function initPostTag($data) 
    {
        if (!isset($data['post_id']) || !isset($data['tag_id'])) {
            return false;
        }
        $fields = array('post_id', 'tag_id');
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }

        return $rs;
    }
    public static function insert($data, $updateUserPostCount = true)
    {
        //Init data
        $data = self::initPost($data);

        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
                if ($updateUserPostCount) {
                    $user = User::getUser($data['user_id']);
                    if (!empty($user)) {
                        $profile = array('user_id' => $user['user_id'], 'posts' => $user['posts'] + 1);
                        User::updateUser($profile);
                    }
                }
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());

            return false;
        }
    }

    public static function update($data, $updateCache = true)
    {
        try
        {
            $data = self::initPost($data);
            if ($data === false || !isset($data['post_id'])) {
                return false;
            }
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->update(self::_TABLE, $data, 'post_id =' . $data['post_id']);
        }
        catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
            return false;
        }
        if ($rs && $updateCache)
        {
            $postId     = $data['post_id'];
            $cache      = My_Zend_Globals::getCaching();
            $cacheKey   = sprintf(self::_cache_key, $postId);
            $postDetail = self::getPost($postId, false);
            $postDetail = array_merge($postDetail, $data);
            $cache->write($cacheKey, $postDetail, 900);
        }
        return $rs;
    }

    public static function increaseView($postId, $number = 1) {
        $post = self::getPost($postId);
        $post['number_of_view'] += $number;
        if ($post['number_of_view'] % 5 == 0) {
            $post['last_viewed'] = time();
            $data = array(
                'post_id' => $postId,
                'last_viewed' => time(),
                'number_of_view' => $post['number_of_view']
            );
            self::update($data);
        } else {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key, $postId);
            $cache->write($cacheKey, $post, 900);
        }

        return true;
    }

    public static function delete($postId)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->query('UPDATE ' . self::_TABLE . ' SET is_actived = 0 WHERE post_id = ' . $postId);
            $cache    = My_Zend_Globals::getCaching();
            $cacheKey = sprintf(self::_cache_key, $postId);
            $cache->delete($cacheKey);
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::delete - ' . $ex->getMessage());

            return false;
        }
    }

    public static function getPostOnStaticMenu()
    {
        $posts = array();
        try {
            $static_menu = Setting::getSetting('static_menu');

            if (!empty($static_menu)) {
                $ids = explode(',', $static_menu['setting_value']);
                $posts = self::getListMultiPost($ids);
                foreach($posts as $key => $p) {
                    $posts[$key]['link'] = self::postUrl($p, true, true);
            }
        }
        } catch(Exception $e) {

        }

        return $posts;
    }

    public static function getPost($postId, $useCache = true)
    {
        if (empty($postId)) {
            return false;
        }

        if (isset(self::$_post[$postId])) {
            return self::$_post[$postId];
        }

        $data = array();

        try {
	        if ($useCache)
	        {
		        $cache = My_Zend_Globals::getCaching();
		        $cacheKey = sprintf(self::_cache_key, $postId);
		        $data = $cache->read($cacheKey);
	        }

            if (empty($data)) {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                        ->from($table, '*')
                        ->where('post_id = ?', $postId)
                        ->limit(1, 0);
                $data = $storage->fetchRow($select);
                if (!empty($data) && $useCache) {
	                $storage->closeConnection();
                    $cache->write($cacheKey, $data, 900);
                }
            }
            if (!empty($data['picture'])) {
                $data['picture'] = str_replace('bachaybochet.com', 'haynhucnhoi.tv', $data['picture']);
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getPost - ' . $ex->getMessage());
            return $data;
        }

        self::$_post[$postId] = $data;
        return $data;
    }
    
    public static function getTotalView($user_id)
    {
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
        ->from($table, array("totalview"=>"SUM(number_of_view)"))
        ->where("user_id = ?", $user_id);
       $num = $storage->fetchRow($select);
       return $num["totalview"];
    }
    
    public static function getTotalPost($user_id)
    {
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
        ->from($table, array("totalpost"=>"COUNT(*)"))
        ->where("user_id = ?", $user_id);
       $num = $storage->fetchRow($select);
       return $num["totalpost"];
    }
    
    public static function getListMultiPost($arrPostId)
    {
        if (empty($arrPostId) || !is_array($arrPostId)) {
            return array();
        }

        $arrPostId = array_unique($arrPostId);
        $cache     = My_Zend_Globals::getCaching();
        $arrKeys   = array();
        $result    = array();

        foreach($arrPostId as $postId) {
            $arrKeys[$postId] = sprintf(self::_cache_key, $postId);
        }

        $result = $cache->readMulti($arrKeys);
        $data = array();
        foreach ($arrKeys as $postId => $key) {
            if (!isset($result[$key])) {
                $post = self::getPost($postId);
                if (!empty($post)) {
                    $data[$postId] = $post;
                }
            } else {
                $data[$postId] = $result[$key];
            }
        }
        return $data;
    }

    public static function getListActivePostID($phase, $getVideo = 0, $random = false, $offset = 0, $limit = 25)
    {
        try {
            $cache = My_Zend_Globals::getCaching();
            $cacheKey = self::_cache_key_active_list . $phase . '_'. $getVideo;
            $data = $cache->read($cacheKey);
            if (!empty($data)) {
                if ($random) {
                    shuffle($data);
                }
                $data = array_slice($data, $offset, $limit);
                return $data;
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('post::getListActivePostID - '. $ex->getMessage());
        }

        return array();
    }
    public static function getTopList($limit = 7,$op = 0)
    {
        $data =  array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select('*')
        ->from(array('p' => $table))->order('RAND()')->where('is_photo = ?',$op)
        ->limit($limit, 0);
       $data = $storage->fetchAll($select);
        $arrayPostId = array();
        if(!empty($data))
        {
            foreach ($data as $post) {
                if (isset($post['post_id'])) {
                    $arrayPostId[] = $post['post_id'];
                } else {
                    $arrayPostId[] = intval($post);
                }
            }
        }
        $data = self::getListMultiPost($arrayPostId);
        return $data;
    }

    public static function getList($filters = array(), $offset = 0, $limit = 25, $options = array('order_by' => 'post_id desc', 'get_views' => false), $useCache = true, $cacheTimeout = 900, $origin = false, $cacheKey = false)
    {

        $data = array();
        $cache = My_Zend_Globals::getCaching();
        try {
            if ($offset > 0) {
                $cacheTimeout = 300;
            }

            /*

            if ((count($filters) == 3 || count($filters) == 4) && !empty($filters['is_actived']) && !empty($filters['is_approved']) && isset($filters['phase']) && !isset($filters['exclude_id'])) {
                $random = (isset($options['order_by']) && $options['order_by'] == 'rand()') ? true : false;
                $getVideo = (isset($filters['is_photo']) && $filters['is_photo'] == 0) ? true : false;
                $data = self::getListActivePostID($filters['phase'], $getVideo, $random, $offset, $limit);
            } */

            if (empty($data) && $useCache) {
                if (empty($cacheKey)) {
                    $cacheKey = 'getListv3_'. md5(serialize($filters) . $offset . $limit);
                }
                $data = $cache->read($cacheKey);

            }


            if (empty($data)) {

                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                    ->from(array('p' => $table), 'post_id')
                    //->join(array('m' => 'members'), 'p.user_id = m.user_id', array('user_id', 'fullname', 'facebookid'))
                    ->limit($limit, $offset);
                if (isset($filters['category_id'])) {
                    if (is_numeric($filters['category_id'])) {
                        $select->where('p.category_id = ?', $filters['category_id']);
                    } elseif (is_array($filters['category_id'])) {
                        $select->where('p.category_id IN (?)', $filters['category_id']);
                    }
                }
                if (isset($filters['no_view'])) {
                    if (is_numeric($filters['no_view'])) {
                        $select->where('p.post_id <> ?', $filters['no_view']);
                    } else {
                        $arrExp = explode(',', $filters['no_view']);
                        if ( ! empty($arrExp)) {
                            $select->where('p.post_id NOT IN (?)', $arrExp);
                        }
                    }
                }
                if (isset($filters['category_not_id'])) {
                    if (is_numeric($filters['category_not_id'])) {
                        $select->where('p.category_id <> ?', $filters['category_not_id']);
                    } elseif (is_array($filters['category_not_id'])) {
                        $select->where('p.category_id NOT IN (?)', $filters['category_not_id']);
                    }
                }
                if (isset($filters['post_id']) && ! empty($filters['post_id'])) {
                    if (is_array($filters['post_id'])) {
                        $select->where('p.post_id IN (?)', $filters['post_id']);
                    } else {
                        $select->where('p.post_id = ?', intval($filters['post_id']));
                    }
                }
                if (isset($filters['user_id'])) {
                    if (is_array($filters['user_id'])) {
                        $select->where('p.user_id IN (?)', $filters['user_id']);
                    } else {
                        $select->where('p.user_id = ?', intval($filters['user_id']));
                    }
                }
                if (isset($filters['from_post_id'])) {
                    $select->where('p.post_id < ?', $filters['from_post_id']);
                }
                if (isset($filters['no_post_id'])) {
                    if (is_array($filters['no_post_id'])) {
                        $select->where('p.post_id NOT IN (?)', $filters['no_post_id']);
                    } else {
                        $select->where('p.post_id <> ?', intval($filters['no_post_id']));
                    }
                }
                if (isset($filters['larger_post_id'])) {
                    $select->where('p.post_id > ?', $filters['larger_post_id']);
                }
                if (isset($filters['is_photo'])) {
                    $select->where('p.is_photo = ?', $filters['is_photo']);
                }
                if (isset($filters['is_meme'])) {
                    $select->where('p.is_meme = ?', $filters['is_meme']);
                }
                if (isset($filters['is_actived']) && $filters['is_actived'] != -1) {
                    $select->where('p.is_actived = ?', $filters['is_actived']);
                }
                if (isset($filters['is_approved']) && $filters['is_approved'] != -1) {
                    $select->where('p.is_approved = ?', $filters['is_approved']);
                }
                if (isset($filters['post_phase']) && $filters['post_phase'] != -1) {
                    $select->where('p.favclicks >= ?', Post::_NUMBER_OF_VOTE_TO_HOMEPAGE);
                }
                if (isset($filters['phase']) && $filters['phase'] != -1) {

                    if (!is_array($filters['phase']))
                    {
                        if ($filters['phase'] == Post::_PHASE_NEW_POST) {
                            $select->where('p.phase = ?', $filters['phase']);
                        } else {
                            $select->where('p.phase >= ?', $filters['phase']);
                        }
                    } else {
                        $select->where('p.phase IN (?)', $filters['phase']);
                    }

                }
                if (isset($filters['story']) && $filters['story'] != '') {
                    $select->where('p.story LIKE ?', '%' . $filters['story'] . '%');
                }
                if (isset($filters['exclude_id'])) {
                    $excludedList = $filters['exclude_id'];
                    $select->where('p.post_id NOT IN (?)', $excludedList);
                }
                if (isset($filters['from_date'])) {
                    if (isset($filters['phase']) && $filters['phase'] == Post::_PHASE_NEW_POST) {
                        $select->where('p.approved_at >= ' . $filters['from_date']);
                    } else {
                        $select->where('p.updated_at >= ' . $filters['from_date']);
                    }
                }
                if (isset($filters['to_date'])) {
                    if (isset($filters['phase']) && $filters['phase'] == Post::_PHASE_NEW_POST) {
                        $select->where('p.approved_at <= ' . $filters['to_date']);
                    } else {
                        $select->where('p.updated_at <= ' . $filters['to_date']);
                    }
                }
                if (isset($filters['number_of_view'])) {
                    $select->where('p.number_of_view > ? ', $filters['number_of_view']);
                }
                if (isset($options['order_by']) && $options['order_by'] != 'rand()') {
                    $select->order($options['order_by']);
                } elseif (isset($options['order_by']) && $options['order_by'] == 'rand()') {
                    $select->order('RAND()');
                } else {
                    $select->order('p.updated_at desc');
                }
                $data = $storage->fetchAll($select);

                if ( ! empty($data) && ($useCache)) {
                    $cache->write($cacheKey, $data, $cacheTimeout);
                }
            }

            // fix for old version if video don't have picture url
            if (!empty($data)) {
                $arrayPostId = array();
                foreach ($data as $post) {
                    if (isset($post['post_id'])) {
                        $arrayPostId[] = $post['post_id'];
                    } else {
                        $arrayPostId[] = intval($post);
                    }
                }
                $data = self::getListMultiPost($arrayPostId);
                // get user info
                $arrayUserId = array();
                foreach ($data as $post) {
                    $arrayUserId[] = $post['user_id'];
                }
                $users = User::getListMultiUser($arrayUserId);

                foreach ($data as &$post) {
                    if (isset($users[$post['user_id']])) {
                        $user = $users[$post['user_id']];
                        $post['fullname'] = $user['fullname'];
                        $post['rank'] = $user['rank'];
                        $post['like_count'] = $user['like_count'];
                        $post['comment_count'] = $user['comment_count'];
                        $post['facebookid'] = $user['facebookid'];
                    }
                }

                foreach ($data as &$post) {
                    if (!$post['is_photo'] && !$post['is_gif'] && empty($post['picture'])) {
                        if ($post['youtube_key'] != '') {
                            $youtubeInfo = My_Zend_Media::retrieveYouTubeInfo($post['youtube_key']);
                            if (!empty($youtubeInfo)) {
                                $post['picture'] = isset($youtubeInfo['thumb']['maxres']) ? $youtubeInfo['thumb']['maxres'] : $youtubeInfo['thumb']['high'];
                                // update data
                                $updateData = array(
                                    'post_id' => $post['post_id'],
                                    'picture' => $post['picture']
                                );
                                self::update($updateData);
                            }
                        }
                    } else {
                        $post['picture'] = str_replace('bachaybochet.com', 'haynhucnhoi.tv', $post['picture']);
                    }
                }
            }

        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getList - ' . $ex->getMessage());
            return false;
        }

        return $data;
    }

    public static function getSimilarPost($postID, $title, $useCache = true, $limit = 20)
    {
        $cache      = My_Zend_Globals::getCaching();
        $cacheKey   = 'similarpost_'. md5($postID);
        if ($useCache)
        {
            $data = $cache->read($cacheKey);
        }
        if (empty($data))
        {
            $data = array();
            try {
                $storage = My_Zend_Globals::getStorage();
                $title = $storage->quote($title);
                $select = $storage->query('SELECT *, MATCH(story) AGAINST("' . $title . '") AS score FROM ' . self::_TABLE . ' WHERE MATCH(story) AGAINST("' . $title . '") AND post_id NOT IN (' . $postID . ') AND is_approved = "1" AND is_actived = "1" ORDER BY score DESC LIMIT ' . $limit);
                $data = $select->fetchAll();
                if ($useCache)
                {
                    $cache->write($cacheKey, $data, 3605); //TODO: Caching_time
                }
            }
            catch (Exception $ex) {
                echo $ex->getMessage();exit;
            }
        }
        return $data;

    }

    public static function countTotal($filters = array())
    {
        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::_TABLE;
            $select  = $storage->select()
                    ->from(array('p' => $table), 'count(post_id) as total');

            if (isset($filters['category_id'])) {
                if (is_numeric($filters['category_id'])) {
                    $select->where('p.category_id = ?', $filters['category_id']);
                } elseif (is_array($filters['category_id'])) {
                    $select->where('p.category_id IN (?)', $filters['category_id']);
                }
            }

            if (isset($filters['category_not_id'])) {
                if (is_numeric($filters['category_not_id'])) {
                    $select->where('p.category_id <> ?', $filters['category_not_id']);
                } elseif (is_array($filters['category_not_id'])) {
                    $select->where('p.category_id NOT IN (?)', $filters['category_not_id']);
                }
            }

            if (isset($filters['post_id']) && !empty($filters['post_id'])) {
                if (is_array($filters['post_id'])) {
                    $select->where('p.post_id IN (?)', $filters['post_id']);
                } else {
                    $select->where('p.post_id = ?', intval($filters['post_id']));
                }
            }

            if (isset($filters['user_id'])) {
                if (is_array($filters['user_id'])) {
                    $select->where('p.user_id IN (?)', $filters['user_id']);
                } else {
                    $select->where('p.user_id = ?', intval($filters['user_id']));
                }
            }

            if (isset($filters['from_post_id'])) {
                $select->where('p.post_id < ?', $filters['from_post_id']);
            }

            if (isset($filters['no_post_id'])) {
                if (is_array($filters['no_post_id'])) {
                    $select->where('p.post_id NOT IN (?)', $filters['no_post_id']);
                } else {
                    $select->where('p.post_id <> ?', intval($filters['no_post_id']));
                }
            }

            if (isset($filters['larger_post_id'])) {
                $select->where('p.post_id > ?', $filters['larger_post_id']);
            }

            if (isset($filters['is_photo'])) {
                $select->where('p.is_photo = ?', $filters['is_photo']);
            }

            if (isset($filters['is_actived']) && $filters['is_actived'] != -1) {
                $select->where('p.is_actived = ?', $filters['is_actived']);
            }

            if (isset($filters['is_approved']) && $filters['is_approved'] != -1) {
                $select->where('p.is_approved = ?', $filters['is_approved']);
            }

            if (isset($filters['phase']) && $filters['phase'] != -1) {
                if ($filters['phase'] == 0) {
                    $select->where('p.phase = ?', $filters['phase']);
                } else {
                    $select->where('p.phase >= ?', $filters['phase']);
                }
            }

            if (isset($filters['story']) && $filters['story'] != '') {
                $select->where('p.story LIKE ?', '%' . $filters['story'] . '%');
            }

            if (isset($filters['from_date'])) {
                $select->where('p.created_at >= ' . $filters['from_date']);
            }

            if (isset($filters['user_post_day'])) {
                $select->where('p.user_id = ' . (int)LOGIN_UID);
                $select->where(" FROM_UNIXTIME(p.created_at, '%Y-%m-%d') = '".date('Y-m-d')."' ");
            }

            if (isset($filters['user_post_day_video'])) {
                $select->where('p.user_id = ' . (int)LOGIN_UID);
                $select->where("FROM_UNIXTIME(p.created_at, '%Y-%m-%d') = '".date('Y-m-d')."' ");
                $select->where("is_photo = 0 ");
            }

            $data = $storage->fetchRow($select);
            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getList - ' . $ex->getMessage());

            return false;
        }

        return $data;
    }

    public static function getNextPost($postId, $params=array(), $useCache = true)
    {
        $cacheKey = 'next_post_of_' . $postId;

        if (empty($postId)) {
            return array();
        }

        $data = array();

        if ($useCache)
        {
            $caching = My_Zend_Globals::getCaching();
            $data = $caching->read($cacheKey);
        }

        if (empty($data))
        {
            try {
                $storage = My_Zend_Globals::getStorage();
                $select = $storage->select()
                    ->from(self::_TABLE, '*')
                    ->where('is_actived = 1')
                    ->where('is_approved = 1')
                    ->where('post_id > ?', $postId)
                    ->order('post_id ASC')
                    ->limit(1, 0);
                $data = $storage->fetchRow($select);
            } catch (Exception $ex) {
                My_Zend_Logger::log('Post::getNextPost - ' . $ex->getMessage());
                return $data;
            }
            if ($data && $useCache)
            {
                $caching->write($cacheKey, $data, 3607); //TODO: Caching_time
            }
        }
        return $data;
    }

    public static function getPrevPost($postId, $params=array())
    {
        if (empty($postId)) {
            return array();
        }

        $data = array();
        try {
                $storage = My_Zend_Globals::getStorage();
                $select = $storage->select()
                        ->from(self::_TABLE, '*')
                        ->where('is_actived = 1')
                        ->where('is_approved = 1')
                        ->where('post_id < ?', $postId)
                        ->order('post_id DESC')
                        ->limit(1, 0);
                $data = $storage->fetchRow($select);

        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getNextPost - ' . $ex->getMessage());
            return $data;
        }

        return $data;
    }

    public static function postUrl($post, $absolute = false, $friendlyURL = true)
    {
        if (empty($post) || !is_array($post)) {
            return '';
        }
        $url = '/photo/' . $post['post_id'];

		if ($friendlyURL)
		{
			$url .= '/' . My_Zend_Globals::convertToSEO($post['story']) . '.hnn';
		}
        if ($absolute)
            $url = BASE_URL . $url;
        return $url;
    }

    public static function getNsfwThumb()
    {
        $id = array(1,2,3);
        shuffle($id);
        return 'nsfw' . $id[1] . '.png';
    }

    public static function setViewedPostCookie($postId)
    {
        $value = isset($_COOKIE['vp']) ? $_COOKIE['vp'] : '';
        if (!empty($value)) {
            $value = My_Zend_Globals::decrypt($value);
            $value = explode(':', $value);
            if (!in_array($postId, $value)) {
                array_push($value, $postId);

                if (count($value) > 10) {
                    array_shift($value);
                }
            }
            $value = implode(':', $value);
        }
        else {
            $value = $postId;
        }
        $value = My_Zend_Globals::encrypt($value);
        setcookie('vp', $value, time() + 24*3600, '/');
    }

    public static function getViewedPostByCookie() 
    {
        $value = isset($_COOKIE['vp']) ? $_COOKIE['vp'] : '';
        if (!empty($value)) {
            $value = My_Zend_Globals::decrypt($value);
            $value = explode(':', $value);
            return $value;
        }
        return null;
    }

    public static function getFacebookUrlStats($url) 
    {
        $apiUrl = 'http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls='. $url;
        $returnData  = array();
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $apiUrl);
            curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            if (!empty($data))
            {
                $data       = Zend_Json::decode($data);
                $data       = $data[0];
                $returnData = array(
                    'share_count'   => $data['share_count'],
                    'like_count'    => $data['like_count'],
                    'total_count'   => $data['total_count'],
                    'comment_count' => $data['commentsbox_count']
                );
            }
             return $returnData;
        }
        catch(Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public static function generateShareImage($picture)
    {
        if (empty($picture)) {
            return '';
        }
        $dir    = UPLOAD_PATH.'/share/'. date('Y') .'/';
        $desc   = '/share/'. date('Y') .'/';
        $rtUrl  = '/share/'. date('Y') .'/';
        if (!is_dir($dir)) {
            $oldmask = umask(0);
            $rs = mkdir($dir, 0777, true);
            umask($oldmask);
        }

        $extension = explode('.', $picture);
        $extension = end($extension);
        // download file
        $desc .= 'share-'. md5($picture) .'.'. $extension;
        $rtUrl .= 'share-'. md5($picture) .'.'. $extension;

        if (!My_Zend_Globals::download($picture, UPLOAD_PATH . $desc)) {
            return '';
        }

        // resize to 480x360
        $upload = new Upload();
        $upload->resize(UPLOAD_PATH . $desc, 480, 360);
        // add play icon
        $icon = STATIC_PATH .'/images/play-64.png';
        require_once LIBS_PATH . '/My/phmagick/phmagick.php';
        $phMagick = new phMagick(UPLOAD_PATH . $desc, UPLOAD_PATH . $desc);
        $phMagick->compose($icon, phMagickGravity::Center, 100);

        return $rtUrl;
    }

    /**
     * Insert tag of article
     * @param array $data
     * @return boolean
     */
    public static function insertPostTag($data) {
        //Init data
        $data = self::initPostTag($data);
        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $rs = $storage->insert(self::_TABLE_POST_TAGS, $data);
                if ($rs) {
                    $cache = My_Zend_Globals::getCaching();
                    $cache->delete('getPostTags_'. $data['post_id']);
                    $cache->delete('getPostIdByTagId_'. $data['tag_id'] .'_0_20');
                    $cache->delete('countPostIdByTagId_'. $data['tag_id']);
                }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::insertPostTag - ' . $ex->getMessage());

            return false;
        }
    }

    /**
     * Select list tags of post
     * @param int $postId
     */
    public static function getPostTags($postId) 
    {
        $data = array();

        try {
                $cache    = My_Zend_Globals::getCaching();
                $cacheKey = 'getPostTags_'. $postId;
                $data     = $cache->read($cacheKey);

                if ($data === false) 
                {
                    //Get db instance
                    $storage = My_Zend_Globals::getStorage();
                    $select  = $storage->select()
                            ->from(self::_TABLE_POST_TAGS, 'tag_id')
                            ->where('post_id = ?', $postId);
                    $data = $storage->fetchAll($select);
                    if (!empty($data)) {
                        $tmp = array();
                        foreach ($data as $item) {
                            $tmp[] = $item['tag_id'];
                        }

                        $data = $tmp;
                        unset($tmp);
                    } else {
                        $data = array();
                    }
                    $cache->write($cacheKey, $data, 30*24*3601); //TODO: Caching_time
                }
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getPostTags - ' . $ex->getMessage());

            return false;
        }

        return $data;
    }

    /**
     * get list article id by tag id
     * @param int $tagId
     * @return boolean|multitype:unknown
     */
    public static function getPostIdByTagId($tagId, $offset = 0, $limit = 30) {
        $data = array();

        try {
                if ($offset == 0) {
                    $cache    = My_Zend_Globals::getCaching();
                    $cacheKey = 'getPostIdByTagId_'. $tagId .'_'. $offset .'_'. $limit;
                    $data     = $cache->read($cacheKey);
                }

                if ($offset > 0 || $data === false) 
                {
                    //Get db instance
                    $storage = My_Zend_Globals::getStorage();
                    $select = $storage->select()->distinct()
                            ->from(self::_TABLE_POST_TAGS, 'post_id')
                            ->order('post_id desc')
                            ->limit($limit, $offset);

                    if (is_array($tagId)) {
                        $select->where('tag_id IN (?)', $tagId);
                    } else {
                        $select->where('tag_id = ?', intval($tagId));
                    }

                    if (isset($options['exclude_post_id']) && is_array($options['exclude_post_id'])) {
                        $select->where('post_id NOT IN (?)', $options['exclude_post_id']);
                    }

                    $data = $storage->fetchAll($select);
                    if (!empty($data)) {
                        $tmp = array();
                        foreach ($data as $item) {
                            $tmp[] = $item['post_id'];
                        }
                        $data = $tmp;
                        unset($tmp);
                    } else {
                        $data = array();
                    }

                    if ($offset == 0) {
                        $cache->write($cacheKey, $data, 30*24*3602); //TODO: Caching_time
                    }
                }

        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getPostIdByTagId - ' . $ex->getMessage());

            return false;
        }

        return $data;
    }

    public static function countPostIdByTagId($tagId, $options = array()) {
        $data = array();

        try {
                $cache = My_Zend_Globals::getCaching();
                $cacheKey = 'countPostIdByTagId_'. $tagId;
                $data = $cache->read($cacheKey);

                if ($data === false) {
                    //Get db instance
                    $storage = My_Zend_Globals::getStorage();
                    $select = $storage->select()
                            ->from(self::_TABLE_POST_TAGS, 'count(post_id) as total');

                    if (is_array($tagId)) {
                        $select->where('tag_id IN (?)', $tagId);
                    } else {
                        $select->where('tag_id = ?', intval($tagId));
                    }

                    if (isset($options['exclude_post_id']) && is_array($options['exclude_post_id'])) {
                        $select->where('post_id NOT IN (?)', $options['exclude_post_id']);
                    }

                    $data = $storage->fetchRow($select);
                    $data = $data['total'];
                    $cache->write($cacheKey, $data, 30*24*3603); //TODO: Caching_time
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::countPostIdByTagId - ' . $ex->getMessage());

            return false;
        }

        return $data;
    }

    /**
     * Delete post tag
     * @param int $postId
     * @param int $tagId
     */
    public static function deletePostTag($postId, $tagId = 0, $oldTagId = 0) {
        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                //Delete data
                if ($tagId > 0) {
                    $rs = $storage->query('DELETE FROM ' . self::_TABLE_POST_TAGS . ' WHERE post_id = ' . intval($postId) . ' AND tag_id=' . intval($tagId));
                } else {
                    $rs = $storage->query('DELETE FROM ' . self::_TABLE_POST_TAGS . ' WHERE post_id = ' . intval($postId));
                }

                if ($rs) {
                    $cache = My_Zend_Globals::getCaching();
                    $cache->delete('getPostTags_'. $postId);
                    if ($tagId > 0) {
                        $cache->delete('getPostIdByTagId_'. $tagId .'_0_20');
                        $cache->delete('countPostIdByTagId_'. $tagId);
                    }
                    if (!empty($oldTagId)) 
                    {
                        if (is_array($oldTagId)) 
                        {
                            foreach($oldTagId as $tagId) 
                            {
                                $cache->delete('getPostIdByTagId_'. $tagId .'_0_20');
                                $cache->delete('countPostIdByTagId_'. $tagId);
                            }
                        } else {
                            $cache->delete('getPostIdByTagId_'. $oldTagId .'_0_20');
                            $cache->delete('countPostIdByTagId_'. $oldTagId);
                        }
                    }
                }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::deletePostTag - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getMaxPostId()
    {
        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $select  = $storage->select()
                        ->from($table, 'MAX(post_id) as max')
                        ->limit(1, 0);
                $data = $storage->fetchRow($select);
                if (!empty($data)) {
                    return $data['max'];
                }
        } catch (Exception $ex) {
            My_Zend_Logger::log("Post::getMaxPostId - " . $ex->getMessage());
        }
        return 0;
    }

    public static function getTotalCommentFacbook($url  = '')
    {
        $filecontent = file_get_contents('https://graph.facebook.com/?ids=' . $url);
        $json  = json_decode($filecontent);
        $count = isset($json->$url->comments) ? $json->$url->comments : 0;
        return $count;
    }


	/**
	 * Move a post to phase
	 * @param int|array $post ID of post or array of post detail
	 * @param int $phase Phase number
	 */

	public static function movePostToPhase($post, $phase)
	{
		if (is_numeric($post))
		{
			$post = self::getPost($post);
		}
		if (is_array($post) && !empty($post['post_id']))
		{
			$data = array(
				'post_id'    => $post['post_id'],
				'phase'      => $phase,
				'updated_at' => time()
			);
			if (self::update($data))
			{
                /*
				if ($phase == 1) $notificationType = Notification::_NOTIFICATION_TYPE_MOVE_TO_HOME;
				if ($phase == 2) $notificationType = Notification::_NOTIFICATION_TYPE_MOVE_TO_HAY;
				if (!empty($notificationType))
				{
					$notificationData = array(
						'userID'      => $post['user_id'],
						'type'        => $notificationType,
						'time'        => date('Y-m-d H:i:s'),
						'isActive'    => 1,
						'status'      => 'active',
						'followingID' => 0,
						'itemID'      => $post['post_id'],
						'message'     => Notification::getMessages($notificationType, $post)
					);
					Notification::insert($notificationData);
				}*/
			}
		}
	}

    public static function getPostsForCalculatingPoint($userID)
    {
        $db = My_Zend_Globals::getStorage();

        $select = $db->select()
            ->from(self::_TABLE)
            ->where('is_actived = 1')
            ->where('is_approved = 1')
            ->where('phase > 0')
            ->where('user_id = ?', $userID)
            ->where('approved_at > ?', 1448816400);

        $data = $db->fetchAll($select);

        if (!empty($data) && is_array($data))
        {
            return $data;
        }
        else return false;
    }

}
