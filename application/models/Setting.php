<?php

class Setting {
    const _TABLE = 'settings';

    public static function initPost($data)
    {
        $fields = array(
            'setting_key',
            'setting_value',
        );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function update($data)
    {
        try {
            $data = self::initPost($data);
            if ($data === false || !isset($data['setting_key'])) {
                return false;
            }
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->update(self::_TABLE, $data, 'setting_key = \'' . $data['setting_key'] . '\'');
            return $rs;
        } catch (Exception $ex)
        {
            My_Zend_Logger::log('Setting::update - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getSetting($setting_key)
    {
        if (empty($setting_key)) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
            ->from($table, '*')
            ->where('setting_key = ?', $setting_key)
            ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if(!empty($row))
        {
            $data =$row;
        }
        return $data;
    }
}