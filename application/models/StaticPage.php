<?php

class StaticPage {

    const _TABLE = 'static_pages';

    public static function initData($data) {
        $fields = array(
            'id', 
            'page_name', 
            'file_name', 
            'uri', 
            'post_id', 
            'ttl', 
            'last_updated', 
            'status', 
            'created_at', 
            'updated_at'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function insert($data) {
        //Init data
        $data = self::initData($data);

        if ($data === false) {
            return false;
        }

        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table = self::_TABLE;
                $rs = $storage->insert($table, $data);
                if ($rs) {
                    $rs = $storage->lastInsertId();
                }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::insert - ' . $ex->getMessage());

            return false;
        }
    }

    public static function update($data) 
    {
        try {
                $data = self::initData($data);

                if ($data === false || !isset($data['id'])) {
                    return false;
                }
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $rs      = $storage->update($table, $data, 'id =' . $data['id']);
                return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::update - ' . $ex->getMessage());
            return false;
        }
    }

    public static function delete($id) 
    {
        try {
            //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $rs      = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE id = ' . $id);
                return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::delete - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getList($filters = array()) 
    {
        $data = array();

        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $select  = $storage->select()
                        ->from(self::_TABLE, '*')
                        ->limit(10000, 0);
                if (!empty($filters['status'])) {
                    $select->where('status = ?',  $filters['status']);
                }
            $data = $storage->fetchAll($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::getList - ' . $ex->getMessage());
            return array();
        }

        return $data;
    }

    public static function getPage($id) 
    {
        if (empty($id)) {
            return false;
        }

        $data = array();

        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $select  = $storage->select()
                        ->from(self::_TABLE, '*')
                        ->where('id = ?', $id)
                        ->limit(1, 0);
                $data = $storage->fetchRow($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::getPage - ' . $ex->getMessage());
            return $data;
        }

        return $data;
    }

    public static function getPageByPostId($postId) 
    {
        if (empty($postId)) {
            return false;
        }

        $data = array();
        try {
            //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $select = $storage->select()
                        ->from(self::_TABLE, '*')
                        ->where('post_id = ?', $postId)
                        ->limit(1, 0);
                $data = $storage->fetchRow($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('StaticPage::getPageByPostId - ' . $ex->getMessage());
            return $data;
        }
        return $data;
    }

    public static function postDetailFileName($postId)
    {
        return 'post-'. $postId;
    }
}
