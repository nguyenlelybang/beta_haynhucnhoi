<?php
class PostLog extends Zend_Db_Table
{
	const TABLE = 'posts_log';

	public static function create($params)
	{
		$defaults = array(
			'type' => '',
			'post_id' => '',
			'user_id' => null,
			'ip' => new Zend_Db_Expr('INET_ATON (\'' . My_Zend_Globals::getAltIp() . '\')'),
			'user_agent' => My_Zend_Globals::getUserAgent(),
			'location' => '',
			'platform' => ''
		);

		$data = array_merge($defaults, $params);
		try
		{
			$db = My_Zend_Globals::getStorage();
			$insert = $db->insert(self::TABLE, $data);
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
			exit;
		}

		if ($insert) {
			return true;
		} else {
			return false;
		}
	}

	public static function get($filter)
	{
		if (empty($filter) || !is_array($filter)) {
			return false;
		}

		$db = My_Zend_Globals::getStorage();
		$select = $db->select()
			->from(self::TABLE, array('type', 'post_id', 'user_id', 'time', 'ip' => 'INET_NTOA (ip)', 'type', 'location', 'user_agent', 'platform'));

		if (!empty($filter['type']) && is_string($filter['type'])) {
			$select->where('type = ?', $filter['type']);
		}

		if (!empty($filter['user_id']) && intval($filter['user_id']) > 0) {
			$select->where('user_id = ?', $filter['user_id']);
		}

		if (!empty($filter['post_id']) && intval($filter['post_id']) > 0) {
			$select->where('post_id = ?', $filter['post_id']);
		}

		if (!empty($filter['ip']) && inet_pton($filter['ip'])) {
			$select->where('ip = ?', new Zend_Db_Expr('INET_ATON (\'' . $filter['ip'] . '\')'));
		}

		if (!empty($filter['location']) && is_string($filter['location'])) {
			$select->where('location = ?', $filter['location']);
		}

		if (!empty($filter['platform']) && is_string($filter['platform'])) {
			$select->where('platform = ?', $filter['platform']);
		}

		if (!empty($filter['limit']) && intval(isset($filter['limit']))) {
			$select->limit($filter['limit']);
		}

		if (!empty($filter['order_by'])  && (is_string($filter['order_by']))) {
			$select->order($filter['order_by']);
			} else {
			$select->order('time DESC');
		}

		$data = $db->fetchAll($select);

		if ($data) {
			return $data;
		} else {
			return false;
		}
	}
}