<?php
class Like {

	const _TABLE = 'posts_like';
	const _SECRET_KEY = 'mWgAgecFsst9r$*x';
	const _USER_LIKE_LIST_KEY = 'post_like_user_%s';
	const _POST_LIKE_LIST_KEY = 'post_like_post_%s';

	public static function initLike($data){

		$fields = array(
			'like_id',
			'post_id',
			'user_id',
			'user_ip',
			'like_time',
			'location'
		);

		$rs = array();
		foreach ($fields as $field) {
			if (isset($data[$field])) {
				$rs[$field] = $data[$field];
			}
		}
		return $rs;
	}

	public static function createLike($data) {

		if (empty($data) || !is_array($data)) {
			return false;
		}

		$like = self::initLike($data);

		$like['user_ip'] = My_Zend_Globals::getAltIp();

		try {

			$storage = My_Zend_Globals::getStorage();
			$rs = $storage->insert(self::_TABLE, $like);
			if ($rs) {
				// Cache Handling
				$cache_key1 = sprintf(self::_USER_LIKE_LIST_KEY, $like['user_id']);
				$cache_key2 = sprintf(self::_POST_LIKE_LIST_KEY, $like['post_id']);
				My_Zend_Globals::deleteDataFromCache($cache_key1);
				My_Zend_Globals::deleteDataFromCache($cache_key2);
				return true;
			}

		} catch (Zend_Exception $e) {

		}

		return false;

	}

	public static function deleteLike($data) {

		if (empty($data) || !is_array($data)) {
			return false;
		}

		try {
			$storage = My_Zend_Globals::getStorage();
			$rs = $storage->delete(self::_TABLE, array('post_id = (?)' => $data['post_id'], 'user_id = (?)' => $data['user_id']));
			if ($rs) {
				// Cache Handling
				$cache_key1 = sprintf(self::_USER_LIKE_LIST_KEY, $data['user_id']);
				$cache_key2 = sprintf(self::_POST_LIKE_LIST_KEY, $data['post_id']);
				My_Zend_Globals::deleteDataFromCache($cache_key1);
				My_Zend_Globals::deleteDataFromCache($cache_key2);
				return true;
			}

		} catch (Zend_Exception $e) {

		}

		return false;

	}

	public static function getUserLikeList($user_id, $useCache = true) {

		if (!is_numeric($user_id) || intval($user_id) <= 0) {
			return false;
		};

		$likeList = array();
		$cacheKey = sprintf(self::_USER_LIKE_LIST_KEY, $user_id);
		if ($useCache) {
			$likeList = My_Zend_Globals::getDataFromCache($cacheKey);
		}

		if (empty($likeList)) {
			$storage = My_Zend_Globals::getStorage();
			$select = $storage->select()
				->from(self::_TABLE, array('post_id', 'user_id', 'like_time', 'location'))
				->where('user_id = (?)', $user_id);
			$likeList = $storage->fetchAssoc($select);
			if (!empty($likeList)) {
				My_Zend_Globals::putDataToCache($cacheKey, $likeList, 900);
			}
		}

		return $likeList;

	}

	public static function getPostLikeList($post_id, $useCache = true) {

		if (!is_numeric($post_id) || intval($post_id) <= 0) {
			return false;
		};

		$likeList = array();
		$cacheKey = sprintf(self::_POST_LIKE_LIST_KEY, $post_id);
		if ($useCache) {
			 $likeList = My_Zend_Globals::getDataFromCache($cacheKey);
		}

		if (empty($likeList)) {
			$storage = My_Zend_Globals::getStorage();
			$select = $storage->select()
				->from(array('l' => self::_TABLE), array('user_id', 'user_ip', 'like_time', 'location'))
				->join(array('u' => User::_TABLE_USER), 'l.user_id = u.user_id', array('name' => 'fullname'))
				->where('post_id = (?)', $post_id);
			$likeList = $storage->fetchAssoc($select);
			if (!empty($likeList)) {
				My_Zend_Globals::putDataToCache($cacheKey, $likeList, 900);
			}
		}

		return $likeList;

	}

	public static function doesUserLikeThis($user_id, $post_id) {

		$userLikeList = self::getUserLikeList($user_id);

		if (isset($userLikeList) && isset($userLikeList[$post_id]) && is_array($userLikeList[$post_id])) {
			return true;
		}
		return false;
	}

	public static function processLike($user_id, $post_id) {

		$data['user_id'] = $user_id;
		$data['post_id'] = $post_id;

		if (!self::doesUserLikeThis($user_id, $post_id)) {
			$isSuccessful = self::createLike($data);
			$action = 'like';
		} else {
			$isSuccessful = self::deleteLike($data);
			$action = 'unlike';
		}
		if ($isSuccessful) {
			self::recountLike($post_id);
			return $action;
		}
		return false;
	}

	public static function getToken($user_id, $post_id, $time) {
		return md5($user_id . self::_SECRET_KEY . $post_id . $time);
	}

	public static function isTokenMatched($user_id, $post_id, $time, $token) {
		if (!isset($token) || empty($token)) {
			return false;
		}

		$validToken = self::getToken($user_id, $post_id, $time);

		if ($validToken != $token) {
			return false;
		}
		return true;
	}

	public static function parseLikeButtonTooltip($postLikeList) {

		if (!is_array($postLikeList)) {
			return false;
		}

		if (empty($postLikeList)) {
			return 'Hãy là người đầu bình chọn bài này!';
		}

		if (isset($postLikeList[LOGIN_UID])) {
			unset($postLikeList[LOGIN_UID]);
			$postLikeList = array_values($postLikeList);
			if (count($postLikeList) == 0) {
				return 'Bạn đã bình chọn bài này';
			} elseif (count($postLikeList) == 1) {
				return 'Bạn và ' . $postLikeList[0]['name'] . ' đã bình chọn bài này';
			} else {
				return 'Bạn, ' . $postLikeList[0]['name'] . ' và ' . (count($postLikeList) - 1) . ' người khác bình chọn bài này';
			}

		} else {

			$postLikeList = array_values($postLikeList);
			if (count($postLikeList) == 1) {
				return $postLikeList[0]['name'] . ' đã bình chọn bài này';
			} elseif (count($postLikeList) == 2) {
				return $postLikeList[0]['name'] . ' và ' . $postLikeList[1]['name'] . ' đã bình chọn bài này';
			} else {
				return $postLikeList[0]['name'] . ', ' . $postLikeList[1]['name'] . ' và ' . (count($postLikeList) - 2) . ' người khác bình chọn bài này';
			}
		}
	}

	public static function recountLike($post_id) {
		$numberOfLike = count(self::getPostLikeList($post_id));
		$post['post_id'] = $post_id;
		$post['favclicks'] = $numberOfLike;
		Post::update($post);
	}
}
?>