<?php

class Follow {

    const _TABLE         = 'following';
    const _TABLE_LOG     = 'following_log';
    const _MAX_FOLLOWING = 100;

    public static function initData($data) 
    {
        $fields = array(
            'following_id', 
            'follower_id', 
            'followed_id', 
            'created_time'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function initDataLog($data) 
    {
        $fields = array(
            'action_id', 
            'action', 
            'follower_id', 
            'followed_id', 
            'time'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }
    public static function insert($data) 
    {
        //Init data
        $data = self::initData($data);

        if ($data === false) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->insert(self::_TABLE, $data);
            if ($rs) {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        } catch (Exception $ex) {echo $ex->getMessage(); exit;
            My_Zend_Logger::log('Follow::insert - ' . $ex->getMessage());

            return false;
        }
    }

    public static function delete($id) 
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs      = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE following_id = ' . $id);

            if ($rs) {

            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Follow::delete - ' . $ex->getMessage());

            return false;
        }
    }
    
    public static function getDetailByUserIdAndFollowId($userId, $followedId) 
    {
        if (empty($userId) || empty($followedId)) {
            return false;
        }

        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::_TABLE;
            $select  = $storage->select()
                    ->from($table, '*')
                    ->where('follower_id = ?', $userId)
                    ->where('followed_id = ?', $followedId)
                    ->limit(1, 0);
            $data = $storage->fetchRow($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('Post::getPost - ' . $ex->getMessage());
            return $data;
        }

        return $data;
    }

    public static function getList($filters = array(), $offset = 0, $limit = 25, $options = array('get_user_info' => true, 'order_by' => 'created_time desc')) 
    {
        $data = array();

        try {
                //Get db instance
                $storage = My_Zend_Globals::getStorage();
                $table   = self::_TABLE;
                $arrUser = array(
                    'user_id', 
                    'fullname', 
                    'rank', 
                    'posts', 
                    'like_count', 
                    'comment_count', 
                    'facebookid', 
                    'follow_number', 
                    'followed_number'
                    );
                $select = $storage->select()
                        ->from(array('f' => $table), '*')
                        ->limit($limit, $offset);

                if (isset($filters['follower_id'])) 
                {
                    if (isset($options['get_user_info']) && $options['get_user_info']) {
                        $select->joinLeft(array('m' => 'members'), 'm.user_id = f.followed_id', $arrUser);
                    }
                    $select->where('follower_id = ?', $filters['follower_id']);
                }

                if (isset($filters['followed_id'])) 
                {
                    if (isset($options['get_user_info']) && $options['get_user_info']) {
                        $select->joinLeft(array('m1' => 'members'), 'm1.user_id = f.follower_id',$arrUser);
                    }
                    
                    if (is_array($filters['followed_id'])) {
                        $select->where('followed_id IN (?)', $filters['followed_id']);
                    }
                    else {
                        $select->where('followed_id = ?', $filters['followed_id']);
                    }
                }

                if (isset($options['order_by'])) {
                    $select->order($options['order_by']);
                }
                $data = $storage->fetchAll($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('Follow::getList - ' . $ex->getMessage());
            return false;
        }

        return $data;
    }

    public static function countTotal($filters = array()) 
    {
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $table   = self::_TABLE;
            $select  = $storage->select()
                    ->from($table, 'count(post_id) as total');

            if ($filters['follower_id']) {
                $select->where('follower_id = ?', $filters['follower_id']);
            }

            if ($filters['followed_id']) {
                $select->where('followed_id = ?', $filters['followed_id']);
            }
            $data = $storage->fetchRow($select);
            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('Follow::countTotal - ' . $ex->getMessage());

            return false;
        }
        return $data;
    }
}
