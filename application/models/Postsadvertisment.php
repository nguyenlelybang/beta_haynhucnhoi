<?php

class Postsadvertisment {

    const _TABLE = 'posts_advertisment';

    public static function initPost($data)
    {
        $fields = array(
            'id',
            'title',
            'url',
            'images',
            'pos',
            'weight'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function insert($data)
    {
        $data = self::initPost($data);
        if ($data === false) {
            return false;
        }

        try{

            $storage    = My_Zend_Globals::getStorage();
            $rs         = $storage->insert(self::_TABLE, $data);
            if($rs)
            {
                $rs = $storage->lastInsertId();
            }
            return $rs;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }
    public static function delete($id)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        try {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->query('DELETE FROM ' . self::_TABLE . ' WHERE id = ' . $id);
            return $rs;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::insert - ' . $ex->getMessage());
            return false;
        }
    }

    public static function update($data)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        try {
                $data = self::initPost($data);
                if ($data === false || !isset($data['id'])) {
                    return false;
                }
                $storage = My_Zend_Globals::getStorage();
                $rs      = $storage->update(self::_TABLE, $data, 'id =' . $data['id']);
                return $rs;
            } catch (Exception $ex)
            {
                My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
                return false;
            }
    }

    public static function getListPost($pos = '')
    {
        $data = array();
        if(empty($pos))
        {
            return $data;
        }

        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
            ->from($table, '*')
            ->where('pos = ?', $pos);
        $rows = $storage->fetchAll($select);
        if(!empty($rows))
        {
            $data = $rows;
        }
        return $data;
    }
    public static function getList($limit = 20,$offset  = 0,$where = array())
    {
        $data   = array();
        try {
            $db = My_Zend_Globals::getStorage();
            $select = $db->select()
                ->from(array('p'=>self::_TABLE))
                ->limit($limit, $offset);
            if(isset($where['pos']))
            {
                if(is_numeric($where['pos']))
                {
                    $select->where('p.pos = ?', $where['pos']);
                }elseif(is_array($where['pos']))
                {
                    $select->where('p.pos IN (?)', $where['pos']);
                }
            }
            $rows = $db->fetchAll($select);
            if(!empty($rows))
            {
                $data = $rows;
            }
            return $data;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('error list'.$ex->getMessage());
            return $data;
        }
    }
    public static function edit($id)
    {
        if (!Admin::isLogined()) {
            return false;
        }
        if (empty($id)) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
                ->from($table, '*')
                ->where('id = ?', $id)
                ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if(!empty($row))
        {
            $data =$row;
        }
        return $data;
    }

    public static function getTotal($where = array())
    {
        $data = array();
        try {
                $db     =   My_Zend_Globals::getStorage();
                $select =   $db->select()->from(array(self::_TABLE,'p'),'count(id) as total');
                if(isset($where['pos']))
                {
                    if(is_numeric($where['pos']))
                    {
                        $select->where('p.pos = ?', $where['pos']);
                    }elseif(is_array($where['pos']))
                    {
                        $select->where('p.pos IN (?)', $where['pos']);
                    }
                }
                $data = $db->fetchRow($select);
                $data = isset($data['total']) ? $data['total'] : 0;
            return $data;
        }catch (Exception $ex)
        {
            My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
            return $data;
        }
    }
}
