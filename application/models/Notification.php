<?php

class Notification {

    const _TABLE_NOTIFICATIONS               = 'notifications';
    const _TABLE_NOTIFICATION_DETAIL         = 'notification_detail';
    const _TABLE_NOTIFICATION_STATS          = 'notification_stats';
    const _NOTIFICATION_STATUS_UNREAD        = 'unread';
    const _NOTIFICATION_STATUS_READ          = 'read';
    const _NOTIFICATION_TYPE_FOLLOWING       = 'following';
    const _NOTIFICATION_TYPE_LIKE_REACHED    = 'like_reached';
    const _NOTIFICATION_TYPE_WELCOME         = 'welcome_message';
    const _NOTIFICATION_TYPE_PROFILE_COMMENT = 'profile_comment';
    const _NOTIFICATION_TYPE_MOVE_TO_HAY     = 'move_to_hay';
    const _NOTIFICATION_TYPE_MOVE_TO_HOME    = 'move_to_home';

    public static function initData($data) 
    {
        $fields = array(
            'ID', 
            'type', 
            'time', 
            'itemID', 
            'followingID', 
            'message', 
            'time', 
            'isActive', 
            'status', 
            'like', 
            'isPublic'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function initUserNotification($data) 
    {
        $fields = array(
            'notification_id', 
            'user_id', 
            'created_at'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }

    public static function initDataNotificationStats($data) 
    {
        $fields = array(
            'user_id', 
            'unread_notification', 
            'unread_notification_follow', 
            'unread_notification_message'
            );
        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }
    public static function insert($data) 
    {
        //Init data
        $notification = self::initData($data);
        if ($notification === false) {
            return false;
        }
        $notificationId = 0;
        try 
        {
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->insert(self::_TABLE_NOTIFICATION_DETAIL, $notification);
            if ($rs) {
                $notificationId = $storage->lastInsertId();
                // insert list
                $array = array(
                    'notification_id'   => $notificationId,
                    'user_id'           => $data['userID'],
                    'created_at'        => date('Y-m-d H:i:s')
                );
                self::insertUserNotification($array);
                // update stats
                self::updateStats($data['userID'], $data['type']);
            }
            // Return
            return $notificationId;
        } catch (Exception $ex) {echo $ex->getMessage(); exit;
            My_Zend_Logger::log('Notification::insert - ' . $ex->getMessage());

            return false;
        }
    }

    public static function insertUserNotification($data) 
    {
        //Init data
        $data = self::initUserNotification($data);

        if ($data === false) {
            return false;
        }
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            // Insert data
            $rs = $storage->insert(self::_TABLE_NOTIFICATIONS, $data);
            return $rs;
        } catch (Exception $ex) {echo $ex->getMessage(); exit;
            My_Zend_Logger::log('Notification::insertUserNotification - ' . $ex->getMessage());
            return false;
        }
    }

    public static function getUserStats($userId)
    {
        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                    ->from(self::_TABLE_NOTIFICATION_STATS, '*')
                    ->where('user_id = ?', $userId)
                    ->limit(1, 0);
            $data = $storage->fetchRow($select);
            //If empty
            if (empty($data)) {
                $data = array();
            }
        }
        catch (Exception $ex) {
            My_Zend_Logger::log('Notification::getUserStats - ' . $ex->getMessage());
        }
        return $data;
    }

    public static function insertStats($data) 
    {
        $data = self::initDataNotificationStats($data);
        if ($data === false || !isset($data['user_id'])) {
            return false;
        }
        
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $rs = $storage->insert(self::_TABLE_NOTIFICATION_STATS, $data);
            return $rs;
        } catch (Exception $ex) {echo $ex->getMessage(); exit;
            My_Zend_Logger::log('Notification::insertStats - ' . $ex->getMessage());
            return false;
        }
    }

    /**
     * Update stats
     * @param array $data
     */
    public static function updateStats($userId, $type) 
    {
        try {
            // check data inserted
            $stats = self::getUserStats($userId);
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            if (empty($stats)) {
                if ($type == self::_NOTIFICATION_TYPE_FOLLOWING) {
                    $data = array(
                        'user_id'   => $userId,
                        'unread_notification_follow' => 1
                    );
                }
                else {
                    $data = array(
                        'user_id'   => $userId,
                        'unread_notification' => 1
                    );
                }

                $rs = self::insertStats($data);
            }
            else {
                if ($type == self::_NOTIFICATION_TYPE_FOLLOWING) {
                    $data = array(
                        'user_id'   => $userId,
                        'unread_notification_follow' => ($stats['unread_notification_follow'] + 1)
                    );
                }
                else {
                    $data = array(
                        'user_id'   => $userId,
                        'unread_notification' => ($stats['unread_notification'] + 1)
                    );
                }
                //Update data
                $rs = $storage->update(self::_TABLE_NOTIFICATION_STATS, $data, 'user_id=' . $data['user_id']);
            }
            return $rs;
        } catch (Exception $ex) {
            My_Zend_Logger::log('Notification::updateStats - ' . $ex->getMessage());

            return false;
        }
    }

    public static function resetStats($userId)
    {
        try {
            $userStats = self::getUserStats($userId);

            if (!empty($userStats) && ($userStats['unread_notification'] || $userStats['unread_notification_follow'] || $userStats['unread_notification_message'])) {
                $storage = My_Zend_Globals::getStorage();

                $data = array(
                    'user_id'   => $userId,
                    'unread_notification' => 0,
                    'unread_notification_follow' => 0,
                    'unread_notification_message' => 0
                );
                return $storage->update(self::_TABLE_NOTIFICATION_STATS, $data, 'user_id=' . $userId);
            }
        } catch (Exception $ex) {
            My_Zend_Logger::log('Notification::updateStats - ' . $ex->getMessage());
        }

        return false;
    }

    public static function getNotification($params) {
        if (empty($params)) {
            return false;
        }

        $data = array();
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            //Query data from database
            $select = $storage->select()
                    ->from(self::_TABLE_NOTIFICATION_DETAIL, '*')
                    ->limit(1, 0);

            if (isset($params['type'])) {
                $select->where('type = ?', $params['type']);
            }

            if (isset($params['itemID'])) {
                $select->where('itemID = ?', $params['itemID']);
            }

            if (isset($params['userID'])) {
                $select->where('userID = ?', $params['userID']);
            }

            if (isset($params['followingID'])) {
                $select->where('followingID = ?', $params['followingID']);
            }

            if (isset($params['like'])) {
                $select->where('like = ?', $params['like']);
            }

            $data = $storage->fetchRow($select);
        } catch (Exception $ex) {
            My_Zend_Logger::log('Notification::getNotification - ' . $ex->getMessage());

            return $data;
        }

        return $data;
    }

    public static function getListNotification($userId, $type = '', $offset = 0, $limit = 10)
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();

            $select = $storage->select()
                    ->from(array('n' => self::_TABLE_NOTIFICATIONS), 'created_at')
                    ->joinLeft(array('d' => self::_TABLE_NOTIFICATION_DETAIL), 'n.notification_id =  d.ID', '*')
                    ->joinLeft(array('m' => User::_TABLE_USER), 'm.user_id = d.followingID', array('user_id', 'fullname', 'rank', 'like_count', 'comment_count', 'facebookid'))
                    ->joinLeft(array('p' => Post::_TABLE), 'p.post_id = d.itemID', array('post_id', 'story', 'number_of_like'))
                    ->where('n.user_id = ?', $userId)
                    ->order(array('d.ID DESC'))
                    ->limit($limit, $offset);
            //echo $select->__toString();
            if (!empty($type)) {
                if (is_array($type)) {
                    $select->where('d.type IN (?)', $type);
                }
                else {
                    $select->where('d.type = (?)', $type);
                }
            }
            
            $notifications = $storage->fetchAll($select);
            if (!empty($notifications)) {
                foreach ($notifications as &$notification) {
                    if ($notification['message'] == '') {
                        $notification['message'] = Notification::getMessages($notification['type'], $notification);
                    }
                    $notification['created_at_formated'] = date('d-m-Y H:i:s', strtotime($notification['created_at']));
                }
            }
        }
        catch (Exception $ex) {
            My_Zend_Logger::log('Notification::getListNotification - ' . $ex->getMessage());
        }

        return $notifications;
    }

    public static function countTotal($userId) {
        $data = array();

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                    ->from(self::_TABLE_NOTIFICATIONS, 'count(notification_id) as total');
            $data = $storage->fetchRow($select);
            $data = $data['total'];
        } catch (Exception $ex) {
            My_Zend_Logger::log('Notification::countTotal - ' . $ex->getMessage());
            return false;
        }
        return $data;
    }

    public static function getMessages($type, $data)
    {
        $msg = '';
        switch ($type)
        {
            case self::_NOTIFICATION_TYPE_WELCOME:
                $msg = 'Chào mừng bạn đã đến với <b>Hay Nhức Nhói</b>. Bây giờ bạn đã có thể bắt đầu <a href="'. BASE_URL .'/submit" target="_blank">Đăng bài</a> để chia sẻ hay Hóng dạo những thành viên khác. Ngoài ra cũng đừng quên <a href="'. BASE_URL .'/user/settings" target="_blank">Cập nhật thông tin cá nhân</a> để mọi người biết về bạn nhiều hơn nhé. Chúc bạn có những giây phút vui vẻ tại <b>Hay Nhức Nhói</b>.';
                break;

            case self::_NOTIFICATION_TYPE_FOLLOWING:
                $msg = sprintf('&nbsp;<strong>%s</strong> đã bắt đầu Hóng các bài đăng của bạn.', $data['fullname']);
                break;

            case self::_NOTIFICATION_TYPE_LIKE_REACHED:
                $msg = sprintf('Bài đăng <strong>%s</strong> của bạn đã đạt <strong>%s</strong> Like', $data['story'], $data['number_of_like']);
                break;

            case self::_NOTIFICATION_TYPE_MOVE_TO_HOME:
                $msg = sprintf('Bài đăng <strong>%s</strong> của bạn đã đủ Nhức Nhói ra <b>Trang chủ</b>.', $data['story']);
                break;

            case Notification::_NOTIFICATION_TYPE_MOVE_TO_HAY:
                $msg = sprintf('Bài đăng <strong>%s</strong> của bạn đã được vào <b>Nhức Nhói</b>. Xin chúc mừng!', $data['story']);
                break;

            case Notification::_NOTIFICATION_TYPE_PROFILE_COMMENT:
                $msg = sprintf('<strong>%s</strong> đã gửi cho bạn một tin nhắn.', $data['fullname']);
                break;

            case   MembersPoint::TYPE_UPLOAD_PICTURES:
                $msg    ='Hình ảnh của bạn được duyệt +'.MembersPoint::TYPE_UPLOAD_PICTURES_POINTS.' điểm';
                break;

            case    MembersPoint::TYPE_UPLOAD_VIDEOS:
                $msg    ='Video của bạn được duyệt +'.MembersPoint::TYPE_UPLOAD_VIDEOS_POINTS.' điểm';
                break;

            case    MembersPoint::TYPE_MOVE_TO_HOME:
                $msg    ='Bài viêt của bạn được đưa ra ngoài trang chủ +'.MembersPoint::TYPE_MOVE_TO_HOME_POINTS.' điểm';
                break;

            case    MembersPoint::TYPE_MOVE_TO_HOT:
                $msg    ='Bài viêt của bạn được đưa ra hay nhức nhói +'.MembersPoint::TYPE_MOVE_TO_HOT_POINTS.' điểm';
                break;

            case    MembersPoint::TYPE_CATEGORY:
                $msg    ='Bài viêt của bạn chọn vào trong category +'.MembersPoint::TYPE_CATEGORY_POINTS.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_50:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_50_RULE.' like  +'.MembersPoint::TYPE_LIKE_50_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_100:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_100_RULE.' like  +'.MembersPoint::TYPE_LIKE_100_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_300:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_300_RULE.' like  +'.MembersPoint::TYPE_LIKE_300_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_500:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_500_RULE.' like  +'.MembersPoint::TYPE_LIKE_500_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_1000:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_1000_RULE.' like  +'.MembersPoint::TYPE_LIKE_1000_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_LIKE_2000:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_2000_RULE.' like  +'.MembersPoint::TYPE_LIKE_2000_POINT.' điểm';
                break;


            case    MembersPoint::TYPE_LIKE_3000:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_3000_RULE.' like  +'.MembersPoint::TYPE_LIKE_3000_POINT.' điểm';
                break;


            case    MembersPoint::TYPE_LIKE_5000:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_LIKE_5000_RULE.' like  +'.MembersPoint::TYPE_LIKE_5000_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_SHARE:
                $msg    ='Bài viêt của bạn chia sẻ '.MembersPoint::TYPE_SHARE_RULE.' lần  +'.MembersPoint::TYPE_SHARE_POINT.' điểm';
                break;


            case    MembersPoint::TYPE_VIEW:
                $msg    ='Bài viêt của bạn đã xem '.MembersPoint::TYPE_VIEW_RULE.' lần  +'.MembersPoint::TYPE_VIEW_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_COMMENT:
                $msg    ='Bài viêt của bạn đạt '.MembersPoint::TYPE_COMMENT_RULE.'  +'.MembersPoint::TYPE_COMMENT_POINT.' điểm';
                break;

            case    MembersPoint::TYPE_FOLLOW:
                $msg    ='Bạn được '.MembersPoint::TYPE_FOLLOW_RULE.' hóng  +'.MembersPoint::TYPE_FOLLOW_POINT.' điểm';
                break;
        }
        return $msg;
    }
}
