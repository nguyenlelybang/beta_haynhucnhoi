<?php

class Admin {

    public static $user = null;

    const _TABLE_USER = 'admin';

    public static function initUser($data) 
    {
        $fields = array(
            'user_id', 
            'user_name', 
            'salt', 
            'fullname', 
            'email', 
            'password', 
            'role_id', 
            'is_locked', 
            'created_date', 
            'updated_date', 
            'last_login_date'
            );

        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $tmp[$field] = $data[$field];
            }
        }
        $rs = $tmp;
        return $rs;
    }

    public static function hashPassword($password, $salt) {
        if (empty($password)) {
            return FALSE;
        }
        return md5(md5($password) . $salt);
    }

    public static function hashPasswordDb($userId, $password) 
    {
        if (empty($userId) || empty($password)) {
            return false;
        }
        //Get db instance
        $storage = My_Zend_Globals::getStorage();
        $select = $storage->select()
                ->from(self::_TABLE_USER, 'salt')
                ->where('user_id = ?', $userId)
                ->limit(1, 0);
        $data = $storage->fetchRow($select);
        if (count($data) !== 1) {
            return false;
        }
        return self::hashPassword($password, $data['salt']);
    }

    public static function createUserSalt($length = 10) 
    {
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= chr(rand(33, 126));
        }
        return $salt;
    }

    public static function login($userName, $password) 
    {
        if (empty($userName) || empty($password)) {
            return false;
        }

        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            //Query
            $select = $storage->select()
                    ->from(self::_TABLE_USER, '*')
                    ->where('user_name = ?', $userName)
                    ->where('is_locked = 0')
                    ->limit(1, 0);
            $user = $storage->fetchRow($select);
            if (!empty($user)) {
                $password = self::hashPasswordDb($user['user_id'], $password);
                if ($user['password'] === $password) {
                    $auth = Zend_Auth::getInstance();
                    $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN1111'));
                    //Zend_Session::rememberMe(7 * 86400);
                    $identity = new stdClass();
                    $identity->user_id = $user['user_id'];
                    $identity->user_name = $user['user_name'];
                    $identity->role = $user['role_id'];
                    $auth->getStorage()->write($identity);
                    return TRUE;
                }
            }
            return FALSE;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function insertUser($data) 
    {
        //Init data
        $data = self::initUser($data);

        if ($data === false) {
            return false;
        }
        $storage = My_Zend_Globals::getStorage();
        $rs = $storage->insert(self::_TABLE_USER, $data);
        return $rs;
    }

    public static function updateUser($data) 
    {
        $data = self::initUser($data);
        if ($data === false) {
            return false;
        }
        //Get db instance
        $storage = My_Zend_Globals::getStorage();
        $rs = $storage->update(self::_TABLE_USER, $data, 'user_id=' . $data['user_id']);
        return $rs;
    }

    public static function getUser($userId) 
    {
        if (isset(self::$user[$userId]))
            return self::$user[$userId];

        $storage = My_Zend_Globals::getStorage();
        $select = $storage->select()
                ->from(self::_TABLE_USER, '*')
                ->where('user_id = ?', $userId)
                ->limit(1, 0);
        $data = $storage->fetchRow($select);
        if (empty($data)) {
            $data = array();
        }
        self::$user[$userId] = $data;
        return $data;
    }

    public static function getListAdmin($offset = 0, $limit = 30) 
    {
        $storage = My_Zend_Globals::getStorage();

        $table = self::_TABLE_USER;
        $select = $storage->select()
                ->from($table, '*')
                ->order('fullname ASC')
                ->limit($limit, $offset);
        $listAdmin = $storage->fetchAll($select);
        return $listAdmin;
    }

    public static function getTotalListAdmin() 
    {
        $storage = My_Zend_Globals::getStorage();
        $table = self::_TABLE_USER;
        //Query data from database
        $select = $storage->select()
                ->from($table, 'count(user_id) as total');
        $data = $storage->fetchRow($select);
        return $data['total'];
    }

    public static function getListAdminByGroup($roleIds = 'all') 
    {
        try {
            //Get db instance
            $storage = My_Zend_Globals::getStorage();
            $select = $storage->select()
                    ->from(self::_TABLE_USER, '*')
                    ->order('role_id ASC');
            if ($roleIds != 'all') 
            {
                if (is_array($roleIds)) {
                    $select->where('role_id IN (?)', $roleIds);
                } else {
                    $select->where('role_id = ?', $roleIds);
                }
            }

            $select->where('is_locked = 0');
            $data = $storage->fetchAll($select);
            if (empty($data)) {
                $data = array();
            }

            foreach ($data as &$user) {
                $userDetail = self::selectUser($user['user_id']);
                $user['user_name'] = (isset($userDetail['user_name']) ? $userDetail['user_name'] : '');
            }

            return $data;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function isLogined()
    {
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN1111'));

        if ($auth->hasIdentity()) {
            return true;
        }
        return false;
    }
}
