<?php

class Seo {

    const _TABLE = 'seo';

    public static function initPost($data)
    {
        $fields = array(
            'id', 
            'title', 
            'metatitle', 
            'keyword', 
            'description'
            );

        $rs = array();
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                $rs[$field] = $data[$field];
            }
        }
        return $rs;
    }
    
    public static function update($data)
    { 
        try {
                $data = self::initPost($data);
                if ($data === false || !isset($data['id'])) {
                    return false;
                }
                $storage = My_Zend_Globals::getStorage();
                $rs      = $storage->update(self::_TABLE, $data, 'id =' . $data['id']);
                return $rs;
            } catch (Exception $ex)
            {
                My_Zend_Logger::log('Post::update - ' . $ex->getMessage());
                return false;
            }
    }
    
    public static function getSeo($id)
    {
        if (empty($id)) {
            return false;
        }
        $data = array();
        $storage = My_Zend_Globals::getStorage();
        $table   = self::_TABLE;
        $select  = $storage->select()
                ->from($table, '*')
                ->where('id = ?', $id)
                ->limit(1, 0);
        $row = $storage->fetchRow($select);
        if(!empty($row))
        {
            $data =$row;
        }
        return $data;
    }
}
