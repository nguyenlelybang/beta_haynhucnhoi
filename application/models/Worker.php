<?php

require_once APPLICATION_PATH . '/models/Upload.php';

class Worker {

    protected $_logAdapter = null;
    protected $_logFail    = null;
    protected $_serverid;

    function __construct() {
        require_once LIBS_PATH . '/My/phmagick/phmagick.php';
    }

    private function loadLogAdapter() 
    {
        try {
                $config  = My_Zend_Globals::getConfiguration();
                $success = sprintf($config->gearman->resize->log, 'success');
                $writer  = new Zend_Log_Writer_Stream($success);
                $logger  = new Zend_Log($writer);
                $this->_logAdapter = $logger;
                $fail   = sprintf($config->gearman->resize->log, 'fail');
                $writer = new Zend_Log_Writer_Stream($fail);
                $logger = new Zend_Log($writer);
                $this->_logFail = $logger;

        } catch (Exception $e) {
            echo('Error: Load log failed!!');
            die($e->getMessage());
        }
    }

    public function run() 
    {
        try {

            $config = Zend_Registry::get("configuration");
            $worker = new GearmanWorker();
            $worker->addServer($config->gearman->resize->host, $config->gearman->resize->port);
            if ($this->_serverid > 0) {
                $worker->addFunction($config->gearman->resize->job . '_' . $this->_serverid, array($this, "resize"));
                while (true) {
                    $worker->work();
                }
            } else {
                $this->_logAdapter->info('Server id notfound ' . $this->_serverid);
            }
        } catch (Exception $ex) {
            $this->_logAdapter->info("Uncaught exception: " . $ex->getMessage(), LOG_TYPE_ERROR);
        }
    }

    public function resize($data) 
    {
        try {
                $config = My_Zend_Globals::getConfiguration();
                $source = $config->photo->upload->dir . '/' . $data['path'] . '/' . $data['sys_name'] . $data['ext'];
                if (is_file($source)) 
                {
                    $thumbConfig = $config->photo->thumbnail;
                    $this->thumbnailJob($data, $source, $thumbConfig);
                    //echo "\nINFO: Resized ".$source."::";
                    //$this->_logAdapter->info("\nINFO: Resized ".$source.":".$debug_msg.":: Feed ".$rs_feed);

                    return "success";
                }
            return "fail";
        } catch (Exception $ex) {
            // log			
            return false;
        }
    }

    public function thumbnailJob($data, $source, $thumbConfig) 
    {
        $config    = My_Zend_Globals::getConfiguration();
        $upload_to = $config->photo->upload->dir . '/' . $data['path'];
        $thumbnail = explode(',', $thumbConfig->thumb);
        foreach ($thumbnail as $size) {
            list($width, $height) = explode('x', $size);
            $width    = intval($width);
            $height   = intval($height);
            $newName  = $config->photo->upload->dir . '/' . $data['path'] . '/' . $data['sys_name'] . '_' . $width . 'x' . $height . $data['ext'];
            $phMagick = new phMagick($source, $newName);
            //$phMagick->resize($width, $height, false, 'white');
            $phMagick->mycrop($width, $height);
        }
        // resize source        
        $phMagick = new phMagick($source, $source);
        $phMagick->resize($config->photo->upload->size, $config->photo->upload->size);
    }

}
