<?php
global $_REASONS;
$_REASONS = array (
1 => array ('id' => 1, 'text' => 'Bài có nội dung đồi trụy'),
2 => array ('id' => 2, 'text' => 'Bài có nội dung phản động'),
3 => array ('id' => 3, 'text' => 'Bài liên quan đến chính trị'),
4 => array ('id' => 4, 'text' => 'Bài liên quan đến tôn giáo'),
5 => array ('id' => 5, 'text' => 'Bài mang tính đả kích cá nhân, vùng miền'),
6 => array ('id' => 6, 'text' => 'Bài có nội dung phản cảm'),
7 => array ('id' => 7, 'text' => 'Bài này vi phạm bản quyền'),
8 => array ('id' => 8, 'text' => 'Lý do khác (ghi rõ bên dưới)')
);