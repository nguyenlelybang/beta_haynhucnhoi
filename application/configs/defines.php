<?php
define('URL_HAYNHUCNHOI', 'haynhucnhoi.tv');
define('URL_DEV', 'test.haynhucnhoi.local');
define('URL_PHUONG', 'haynhucnhoi.dev');
define('URL_NAM', 'haynhucnhoi-namnt.local');
define('URL_KHANH', 'hnn.me');

switch ($_SERVER['HTTP_HOST']) {
    case URL_HAYNHUCNHOI:
        $_SERVER['Site_ENV'] = 'production';
        break;
    case URL_PHUONG:
        $_SERVER['Site_ENV'] = 'development-phuong';
        break;
    case URL_NAM:
        $_SERVER['Site_ENV'] = 'development-nam';
        break;
    case URL_DEV:
        $_SERVER['Site_ENV'] = 'staging';
        break;
    case URL_KHANH:
        $_SERVER['Site_ENV'] = 'development-khanh';
        break;
}

define('ENVIRONMENT', 'env');
define('APPLICATION_ENVIRONMENT', isset($_SERVER['Site_ENV']) ? $_SERVER['Site_ENV'] : 'development');

if (isset($_SERVER['HTTP_HOST'])) {
    define('BASE_URL', 'http://' . $_SERVER['HTTP_HOST']);
} elseif (isset($_SERVER['SERVER_NAME'])) {
    define('BASE_URL', 'http://' . $_SERVER['SERVER_NAME']);
} elseif (APPLICATION_ENVIRONMENT == "development-phuong") {
    define('BASE_URL', URL_PHUONG);
} elseif (APPLICATION_ENVIRONMENT == "development-nam") {
    define('BASE_URL', URL_NAM);
} elseif (APPLICATION_ENVIRONMENT == "development-khanh") {
    define('BASE_URL', URL_KHANH);
}

define('ADM_FOLDER', 'admhnn01');
define('ADM_REBORN_FOLDER', 'adm_reborn');
define('CONTENT_URL', BASE_URL . '/content');
define('LOGIN_URL', BASE_URL . '/user/login');
define('LOGIN_ADM_URL', BASE_URL . '/' . ADM_FOLDER . '/user/login');
define('LOGIN_ADM_REBORN_URL', BASE_URL . '/' . ADM_REBORN_FOLDER . '/user/login');
define('LOCKED_URL', BASE_URL . '/user_locked.html');
define('LOCKED_ADM_URL', BASE_URL . '/user_adm_locked.html');
define('CONFIGS_PATH', 'configs_path');
define('LOGGER_DUMP', 'logger_dump');
define('APPLICATION_CONFIGURATION', 'appConfig');
define('ADMIN_LOG_CONFIGURATION', 'adminlogConfig');
define('PROFILER_CACHE', 'PROFILER_CACHE');
define('RECORD_PER_PAGE', 30);
define('PAGE_SIZE', 8);
define('SYSTEM_USER', 'system_user');

defined('CACHE_ENABLE') || define('CACHE_ENABLE', false);
defined('GENERAL_CACHE_TIMEOUT') || define('GENERAL_CACHE_TIMEOUT', 3600);
defined('GENERAL_CACHE_DIR') || define('GENERAL_CACHE_DIR', APPLICATION_PATH.'/storage/cache');
defined('DB_CACHE_TIMEOUT') || define('DB_CACHE_TIMEOUT', 86400); // seconds

// Facebook Info
if (APPLICATION_ENVIRONMENT == 'production') {
    define('FACEBOOK_APPID', 'YOUR_ID'); //TODO
    define('FACEBOOK_SECKEY', 'YOUR_KEY');//TODO
    define('DOMAIN', 'haynhucnhoi.tv');
} elseif (APPLICATION_ENVIRONMENT == 'staging') {
    define('FACEBOOK_APPID', 'YOUR_ID');//TODO
    define('FACEBOOK_SECKEY', 'YOUR_KEY');//TODO
    define('DOMAIN', 'test.haynhucnhoi.local');
} elseif (APPLICATION_ENVIRONMENT == "development-nam") {
    define('FACEBOOK_APPID', 'YOUR_ID');//TODO
    define('FACEBOOK_SECKEY', 'YOUR_KEY');//TODO
    define('DOMAIN', 'haynhucnhoi-namnt.local');
} elseif (APPLICATION_ENVIRONMENT == "development-phuong") {
    define('FACEBOOK_APPID', 'YOUR_ID');//TODO
    define('FACEBOOK_SECKEY', 'YOUR_KEY');//TODO
    define('DOMAIN', 'haynhucnhoi.dev');
} elseif (APPLICATION_ENVIRONMENT == "development-khanh") {
    define('FACEBOOK_APPID', 'YOUR_ID');//TODO
    define('FACEBOOK_SECKEY', 'YOUR_KEY');//TODO
    define('DOMAIN', 'hnn.me');
}
define('FACEBOOK_FANPAGE_URL', 'http://www.facebook.com/haynhucnhoi');
define('GOOGLEPLUS_FANPAGE_URL', 'https://plus.google.com/110129889455394911570');
define('YOUTUBE_API_KEY', 'YOUR_KEY');//TODO
define('SECRET_KEY', 'YOUR_KEY');//TODO
define('TWITTER_PAGE_URL', '');
define('EXCLUDED_USER_ID_POINT', '1,2,4,7,14,22,25,38,295,1977,2082,2105,2121,2667,2817,3151,3379,3463,4437,36676,36677,5838,13737,36681,36732,38258,38375,38432,38433,38899,38896,38900,5847,53723,25326,59575,60051,60072,59949,60470,60456,60516,60515,59577,60505,60528,60599,62559,62560,62561,62561,60507,60946,50609,60369,60943,1727,60602');
define('MECLOUD_APPID', 'YOUR_ID');//TODO
define('MECLOUD_SECRET_KEY', 'YOUR_KEY');//TODO