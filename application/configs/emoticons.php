<?php

$emoticonsList = array(
	':v' => 'pacman.png',
	':V' => 'pacman.png',
	':3' => 'curly.png',
	':))' => 'laugh.gif',
	':)' => 'smile.gif',
	':((' => 'cry.gif',
	':(' => 'sad.gif',
	'=))' => 'rofl.gif',
	'<3' => 'heart.png',
	':x' => 'love.gif',
	':X' => 'love.gif',
	';))' => 'hehe.gif',
);

?>