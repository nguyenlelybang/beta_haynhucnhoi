<?php
define('URL_HAYNHUCNHOI', 'haynhucnhoi.tv');
define('URL_BACHAYBOCHET', 'bachaybochet.com');
define('URL_DEV', 'test.haynhucnhoi.local');
define('URL_PHUONG', 'haynhucnhoi.dev');
define('URL_NAM', 'haynhucnhoi-namnt.local');

global $argv;
$env = $argv[1];

if (isset($env))
{
	if (!in_array($env, array('development', 'staging', 'development-nam', 'development-phuong','development-khanh')))
	{
		$env = 'production';
	}
}
else {
	echo 'No environment set. Exiting...';
	exit;
}

define('ENVIRONMENT', 'env');
define('APPLICATION_ENVIRONMENT', $env);

define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../../data'));
define('LOGGERS_PATH', realpath(DOCUMENT_ROOT . '/../../logs'));
define('PAGES_PATH', realpath(DOCUMENT_ROOT . '/../static_pages'));
define('ADM_FOLDER', 'admhnn01');
define('CONFIGS_PATH', 'configs_path');
define('LOGGER_DUMP', 'logger_dump');
define('APPLICATION_CONFIGURATION', 'appConfig');
define('ADMIN_LOG_CONFIGURATION', 'adminlogConfig');
define('PROFILER_CACHE', 'PROFILER_CACHE');
define('RECORD_PER_PAGE', 30);
define('PAGE_SIZE', 8);
define('SYSTEM_USER', 'system_user');
define('EXCLUDED_USER_ID_POINT', '1,2,3,4,6,14,22,25,38,295,407,477,1349,1977,2082,2105,2111,2121,2252,2667,2817,3151,3463,4437,5331,5337,5466,5620');