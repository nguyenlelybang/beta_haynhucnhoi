<?php

class PagesController extends Zend_Controller_Action {
	public function init()
	{

	}

	public function tosAction()
	{

	}

	public function rulesAction()
	{

	}

	public function aboutAction()
	{

	}

	public function faqAction()
	{

	}

	public function contactAction()
	{

	}

	public function privacypolicyAction()
	{

	}

	public function trianAction()
	{
		$layout = Zend_Layout::getMvcInstance();
		$image = $layout->_general['server']['img']['path'] . '/trian_thumbnail.jpg?v=2';
		$this->view->image = $image;

		My_Zend_Globals::setTitle('Chương trình tri ân thành viên - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:title','Chương trình tri ân thành viên - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:description', 'Nhằm cảm ơn sự đóng góp của các thành viên nhiệt thành, HayNhucNhoi.tv tổ chức chương trình tri ân hàng tháng. Click để xem thêm và tham gia.');
		My_Zend_Globals::setProperty('og:image', $image);
		My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
	}

	public function triAnThang9Action()
	{
		$layout = Zend_Layout::getMvcInstance();
		$image = $layout->_general['server']['img']['path'] . '/top50thang9.png?v=1';
		$this->view->image = $image;

		My_Zend_Globals::setTitle('Kết quả Tri ân tháng 9 - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:title','Kết quả Tri ân tháng 9 - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:description', 'Kết quả chương trình Tri ân thành viên tháng 9 - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:image', $image);
		My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');

		$this->renderScript('pages/ketquatrian.phtml');
	}

	public function cctalkAction()
	{
		My_Zend_Globals::setTitle('Giao lưu online cùng hot girl trên Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:title','Xem trực tuyến TalkTV - Hay Nhức Nhói');
		My_Zend_Globals::setProperty('og:description', 'Xem trực tuyến gái xinh hát hay, nhảy đẹp tại Hay Nhức Nhói');
		My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
	}
}
