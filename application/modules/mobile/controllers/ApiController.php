<?php

class ApiController extends Zend_Controller_Action
{
    const LIMIT               = 6;
    const LIMIT_POST_IFRAME_2 = 6;

    public function init()
    {
        $this->view->pageType = 'detail';
        //$this->_helper->layout->disableLayout();
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('layout_vechai');
    }

    public function indexAction()
    {
        $this->_redirect(BASE_URL);
    }

    public function vechaiAction()
    {

        $id = (int) $this->_getParam('id', 0);
        if ( ! in_array($id, array(1, 2))) {
            $this->_redirect(BASE_URL);
        }
        if ( ! empty($id) && is_numeric($id)) {
            if ($id == 1) {
                $posts = Post::getList(
                    array('phase' => Post::_PHASE_HOT,
                          'is_actived' => 1,
                          'is_approved' => 1,
                          'is_photo' => 0,
                          'from_date' => time() - 86400 * 7),
                    0, 50, array('order_by' => 'post_id desc'), true, 1800, 'vechai_' . $id, 'vechai_iframe_' . $id);
                shuffle($posts);
                $posts             = array_slice($posts, 0, self::LIMIT);
                $this->view->posts = $posts;
            } else {
                $categoryList = Category::selectCategoryList();
                shuffle($categoryList);
                $category_id = $categoryList[0]['category_id'];
                $posts  = Post::getList(
                    array('category_id' => $category_id,
                          'is_actived' => 1,
                          'is_approved' => 1,
                          'is_photo' => 0,
                          'from_date' => time() - 86400 * 7), 0, 50,
                    array('order_by' => 'post_id desc'), true, 1800, false, 'vechai_iframe_category_' . $category_id);
                shuffle($posts);
                $posts = array_slice($posts, 0, self::LIMIT_POST_IFRAME_2);
                $this->view->posts = $posts;
                $this->view->category = $categoryList[0];
            }
        }
        $this->view->pos = $id;
    }

}
