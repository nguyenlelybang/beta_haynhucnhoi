<?php

class BlogController extends Zend_Controller_Action {

    const _LIMIT      = 5;
    const _BLOG_APPROVED = 0;
    public function init() {
        $this->view->dataMeta =  Seo::getSeo(array('id'=>1));
        $this->view->lsCatogory = Category::selectCategoryList();
    }

    public function indexAction() {
        $this->view->pageType = 'index';
        $pageType = $this->_getParam('pageType', 'index');
        $page = $this->_getParam('page', 1);

        $offset = ($page - 1) * self::_LIMIT;
        $count = count(BlogPost::getList(array('post_status'=>self::_BLOG_APPROVED)));
        $paging = new My_Helper_Paging();
        if($count > 5) {
            $paging = $paging->blogPaging($pageType,$count,$page,$limit=self::_LIMIT,$page_size=8,$style='page_item');
            $this->view->paging = $paging;
        }
        $posts = BlogPost::getList(array('limit'=>self::_LIMIT, 'offset'=>$offset, 'post_status'=>self::_BLOG_APPROVED));

        $this->view->posts = $posts;
        $this->view->page = $page;

        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle('Blog Hay Nhức Nhói');
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', $this->view->dataMeta['title']);
        My_Zend_Globals::setProperty('og:description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    public function blogDetailAction() {

        $this->view->pageType = 'blogDetail';
        $postID = $this->_getParam('post_id', 0);
        $postDetail = BlogPost::get($postID);
        if (empty($postDetail)) {
            $this->_forward('page-not-found');
            return;
        }
        $search     = array('&amp;quot;','&amp;amp;quot;');
        $replace    = array('"','"');
        $postDetail['post_title'] = htmlspecialchars($postDetail['post_title']);
        $postDetail['post_title'] = str_replace($search,$replace,$postDetail['post_title']);

        $this->view->post = $postDetail;

        $categoryName = BlogCategory::get(array('category_id'=>$postDetail['category_id']));
        $this->view->categoryName = $categoryName;

        $blogAuthor = Admin::getUser($postDetail['user_id']);
        $this->view->blogAuthor = $blogAuthor;

        $filters = array(
            'is_actived'    => 1,
            'status'        => 1,
            'phase'         => Post::_PHASE_HOMEPAGE,
            'is_photo'      => 0
        );
        $posts = Post::getList($filters, 0, 100, array('order_by' => 'updated_at DESC'));
        if ($posts) {
            unset($posts[$postID]);
        }
        $rand_five  = array_rand($posts, 5);
        $arrResult  = array();
        foreach($rand_five as $key =>$value){
            $arrResult[$value] = $posts[$value];
        }

        $nextPost = BlogPost::getNextPost($postID);
        $prevPost = BlogPost::getPrevPost($postID);

        $this->view->nextPost = $nextPost;
        $this->view->prevPost = $prevPost;
        $this->view->recommendPosts = $arrResult;
        $this->view->bodyClass = 'detail';
        $this->view->pageType = 'blogDetail';
        $this->view->ChapPost          = PostsChap::getChapsByPostID($postID);
        $this->view->ChapVideo         = PostsChap::getChapByID($this->view->video);

        My_Zend_Globals::setTitle($postDetail['post_title'] . ' - Blog Hay Nhức Nhói');
    }

    public function blogCategoryAction() {

        $this->view->pageType = 'chu-de';
        $pageType = $this->_getParam('pageType', 'chu-de');
        $categoryAlias = $this->_getParam('alias', '');
        $page = $this->_getParam('page', 1);
        $this->view->page = $page;

        $offset = ($page - 1) * self::_LIMIT;
        $alias = BlogCategory::get(array('category_alias'=>$categoryAlias));
        $categoryID = $alias['category_id'];
        $count = count(BlogPost::getList(array('category_id'=>$categoryID, 'post_status'=>self::_BLOG_APPROVED)));
        $paging = new My_Helper_Paging();
        $urlPage = $pageType . '/' . $categoryAlias;
        if($count > 5) {
            $paging = $paging->blogPaging($urlPage,$count,$page,$limit=self::_LIMIT,$page_size=8,$style='page_item');
            $this->view->paging = $paging;
        }

        $posts = BlogPost::getList(array('limit'=>self::_LIMIT, 'offset'=>$offset, 'post_status'=>self::_BLOG_APPROVED, 'category_id'=>$categoryID));
        $this->view->posts = $posts;

        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle($alias['category_name'] . ' - Blog Hay Nhức Nhói');
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', $this->view->dataMeta['title']);
        My_Zend_Globals::setProperty('og:description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }
}