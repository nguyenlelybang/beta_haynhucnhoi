<?php

class IndexController extends Zend_Controller_Action {
    const _LIMIT = 15;
    const _VIEW_TO_HOT = 1000;

    public function init()
    {
//        My_Zend_Cache::cacheHTML_Start();
        $this->view->dataMeta =  Seo::getSeo(array('id'=>1));
        $this->view->lsCatogory = Category::selectCategoryList();

    }
    
    public function indexAction()
    {
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;
        $filters = array(
            'is_actived'    => 1,
            'is_approved'   => 1,
            'phase'         => Post::_PHASE_HOMEPAGE
        );
        
        $posts = Post::getList($filters, $offset, self::_LIMIT, array('order_by' => 'updated_at desc'), true, 900, 'home');
        $this->view->page = $page;
        $this->view->nextPage = BASE_URL .'/index/'. ($page + 1);
        $this->view->posts = $posts;
        $this->view->pageType = 'index';

        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle($this->view->dataMeta['title']);
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', $this->view->dataMeta['title']);
        My_Zend_Globals::setProperty('og:description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    public function newAction()
    {
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;

        // get list homepage
        $filters = array(
            'is_actived'    => 1,
            'is_approved'    => 1,
            'phase'         => Post::_PHASE_NEW_POST
        );

        if (LOGIN_UID > 0) {
            $userId = LOGIN_UID;
            $userInfo = User::getUser($userId);
            if (!empty($userInfo) && !empty($userInfo['filter']) && ($userInfo['filter']['photo'] || $userInfo['filter']['music'] || $userInfo['filter']['clip'])) {
                if ($userInfo['filter']['photo'] && !$userInfo['filter']['clip']) {
                    $filters['is_photo'] = 1;
                } else if ($userInfo['filter']['clip'] && !$userInfo['filter']['photo']) {
                    $filters['is_photo'] = 0;
                }
            }
        }

        $posts = Post::getList($filters, $offset, self::_LIMIT, array('order_by' => 'approved_at desc'));
       
        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->pageType = 'index';
        $this->view->nextPage = BASE_URL .'/new/'. ($page + 1);
         $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle($this->view->dataMeta['title']);
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
    }

    public function followAction()
    {
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;
        $userId = LOGIN_UID;
        $posts = array();

        if ($userId > 0) {
            // get list user id followed
            $followedList = Follow::getList(array('follower_id' => $userId), 0, Follow::_MAX_FOLLOWING, array('get_user_info' => false));
            $followedIds = array();

            if (!empty($followedList)) {
                foreach ($followedList as $value) {
                    $followedIds[] = $value['followed_id'];
                }
            }

            if (!empty($followedIds)) {
                if (LOGIN_UID > 0) {
                    $userId = LOGIN_UID;
                    $userInfo = User::getUser($userId);
                    if (!empty($userInfo) && !empty($userInfo['filter']) && ($userInfo['filter']['photo'] || $userInfo['filter']['music'] || $userInfo['filter']['clip'])) {
                        if ($userInfo['filter']['photo'] && !$userInfo['filter']['clip']) {
                            $filters['is_photo'] = 1;
                        } else if ($userInfo['filter']['clip'] && !$userInfo['filter']['photo']) {
                            $filters['is_photo'] = 0;
                        }
                    }
                }
                $posts = Post::getList(array('status' => 1, 'user_id' => $followedIds), $offset, self::_LIMIT, array('order_by' => 'approved_at desc'));
            }
        }
        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->pageType = 'index';
        $this->view->nextPage = BASE_URL .'/follow/'. ($page + 1);
        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', 'Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', 'Cười xả láng mỗi ngày tại Hay Nhức Nhói với những hình ảnh hài hước, vui nhộn, ảnh động vui, clip hay, clip độc và clip cực hot');
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    public function hotAction()
    {
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;

        // get list homepage
        $filters = array(
            'is_actived'    => 1,
            'status'        => 1,
            'number_of_view' => self::_VIEW_TO_HOT
        );

        if (LOGIN_UID > 0) {
            $userId = LOGIN_UID;
            $userInfo = User::getUser($userId);
            if (!empty($userInfo) && !empty($userInfo['filter']) && ($userInfo['filter']['photo'] || $userInfo['filter']['music'] || $userInfo['filter']['clip'])) {
                if ($userInfo['filter']['photo'] && !$userInfo['filter']['clip']) {
                    $filters['is_photo'] = 1;
                } else if ($userInfo['filter']['clip'] && !$userInfo['filter']['photo']) {
                    $filters['is_photo'] = 0;
                }
            }
        }

        $posts = Post::getList($filters, $offset, self::_LIMIT, array('order_by' => 'approved_at desc'));

        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->pageType = 'index';
        $this->view->nextPage = BASE_URL .'/hot/'. ($page + 1);
        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle($this->view->dataMeta['title']);
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
    }

    public function cliphotAction()
    {
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;

        // get list homepage
        $filters = array(
            'is_actived'    => 1,
            'status'        => 1,
            'is_photo'      => 0,
            'phase'         => Post::_PHASE_HOT
        );

        $posts = Post::getList($filters, $offset, self::_LIMIT, array('order_by' => 'approved_at desc'));

        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->nextPage = BASE_URL .'/clip-hai/'. ($page + 1);
        $this->view->pageType = 'index';

        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', 'Clip hài, clip HOT vui nhộn tại Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', 'Cười xả láng mỗi ngày tại Hay Nhức Nhói với những hình ảnh hài hước, vui nhộn, ảnh động vui, clip hay, clip độc và clip cực hot');
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] .'/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    public function tagAction()
    {
        $alias = $this->_getParam('tag_alias', '');
        $tagId = $this->_getParam('tag_id', 0);
        $page = $this->_getParam('page', 1);

        $limit = 20;
        $offset = ($page - 1) * $limit;

        if (empty($alias) || empty($tagId)) {
            $this->_forward('page-not-found');
            return;
        }

        $tag = Tag::getTag($tagId);
        if (empty($tag)) {
            $this->_forward('page-not-found');
            return;
        }

        // get articles
        $total = Post::countPostIdByTagId($tag['id']);
        $totalPage = ceil($total / $limit);
        $posts = array();
        if ($total > 0) {
            $postIds = Post::getPostIdByTagId($tag['id'], $offset, $limit);

            if (!empty($postIds)) {
                $posts = Post::getList(array('post_id' => $postIds, 'is_actived' => 1, 'status' => 1), $offset, $limit);
            }
        }

        $nextPage = '';
        if ($totalPage > $page) {
            $nextPage = Tag::tagUrl($tag) .'?page='. ($page + 1);
        }

        $this->view->tag = $tag;
        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->pageType = 'index';
        $this->view->nextPage = $nextPage;
    }
    public function categoryAction()
    {

        $alias  = $this->_getParam('alias', '');
        $page   = $this->_getParam('page', 1);

        if (empty($alias)) {
            $this->_forward('page-not-found');
            return;
        }

        $category = Category::selectCategoryByAlias($alias);
        $grid = self::_LIMIT;

        if (empty($category)) {
            $this->_forward('page-not-found');
            return;
        }
        $filters = array(
            'category_id' => $category['category_id'],
            'is_actived'  => 1,
            'is_approved' => 1
        );
        $offset = ($page - 1) * $grid;
        $posts   = Post::getList($filters, $offset, $grid);
        $this->view->category = $category;
        $this->view->page     = $page;
        $this->view->posts    = $posts;
        $this->view->alias    = $alias;
        $this->view->nextPage = BASE_URL . '/chu-de/' . $category['alias'] . '/' . ($page + 1);
        $this->view->pageType = 'index';
        My_Zend_Globals::setTitle( $this->view->category['page_title']);
        My_Zend_Globals::setMeta('keywords',$this->view->category['meta_keyword']);
        My_Zend_Globals::setMeta('description',$this->view->category['meta_description']);
    }
    public function detailAction()
    {
        $postId = $this->_getParam('post_id', 0);
        $url_string = $this->_getParam('url_string','');
        $postId = intval($postId);
        $this->view->video = $this->_getParam('video', null);
        $post = Post::getPost($postId);
        if (empty($post)) {
            $this->_forward('page-not-found');
            return;
        }
        if (empty($url_string) || str_replace('.hnn', '', $url_string) != My_Zend_Globals::convertToSEO($post['story']))
        {
            $this->getHelper('Redirector')->setCode(301);
            $this->_redirect(Post::postUrl($post, true, true));
        }
        $search     = array('&amp;quot;','&amp;amp;quot;', 'amp;');
        $replace    = array('"','"');
        $post['story'] = htmlspecialchars($post['story']);
        $post['story'] = str_replace($search,$replace,$post['story']);

        //$post['story'] = htmlspecialchars($post['story']);

        $profile = User::getUser($post['user_id']);
        //$recommendPosts = Post::getList(array('is_actived' => 1, 'is_approved' => 1, 'no_post_id' => $viewedPostId), 0, 5);
        $nextPost = Post::getNextPost($postId);
        $prevPost = Post::getPrevPost($postId);
        
        $filters = array(
            'is_actived'    => 1,
            'status'        => 1,
            'phase'         => Post::_PHASE_HOMEPAGE,
            'is_photo'      => 0
        );
        $posts = Post::getList($filters, 0, 100, array('order_by' => 'updated_at DESC'));
        if ($posts) {
            unset($posts[$postId]);
        }
        $rand_five  = array_rand($posts, 10);
        $arrResult  = array();
        foreach($rand_five as $key =>$value){
            $arrResult[$value] = $posts[$value];
        }

        if (LOGIN_UID != $post['user_id'] && $post['is_approved'] == 1) {
            $data = array('post_id'=>$post['post_id'],'number_of_view'=>$post['number_of_view'] + 1);
            Post::update($data);
        }
        $canApprovePost = false;
        $canMovePostToHome = false;
        if (Admin::isLogined()) {
            $canApprovePost = Role::isAllowed(Permission::ARTICLE_APPROVE);
            $canMovePostToHome = Role::isAllowed(Permission::ARTICLE_MOVE_TO_HOME);
        }

        $pictureShare = My_Zend_Globals::getThumbImage($post['picture'], 'thumb2');
        if (isset($post['picture_share']) && !empty($post['picture_share'])) {
            $pictureShare = $post['picture_share'];
        }
        //new video
        $newVideos = Post::getList(array(
            'is_actived' => 1,
            'is_approved' => 1,
            'phase' => Post::_PHASE_NEW_POST,
            'is_photo' => 0,
            'from_date' => time() - 86400*7), 0, 40, array('order_by' => 'post_id DESC'), true, 3600, 'clipnew_detail', 'clipnew_detail');

        if ($newVideos) {
            if (isset($newVideos[$postId])) unset($newVideos[$postId]);
            shuffle($newVideos);
            $slicedNewVideos = array_slice($newVideos, 0, 6);
        }
        // set viewed cookie
        //Post::setViewedPostCookie($postId);
        $this->view->post = $post;
        $this->view->profile = $profile;
        $this->view->nextPost = $nextPost;
        $this->view->prevPost = $prevPost;
        $this->view->recommendPosts = $arrResult;
        $this->view->bodyClass = 'detail';
        $this->view->pageType = 'detail';
        $this->view->ChapPost          = PostsChap::getChapsByPostID($postId);
        $this->view->ChapVideo         = PostsChap::getChapByID($this->view->video);
        if (!empty($slicedNewVideos)) $this->view->newVideos = $slicedNewVideos;

        My_Zend_Globals::setTitle($post['story'] . ' - Hay Nhức Nhói');
    }
}
