<?php
class AjaxController extends Zend_Controller_Action {
    public function init() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
    }

    public function getMediaInfoAction() {
        $url = $this->_getParam('url', '');
        $url = trim($url);

        if ($url == '') {
            $this->returnJson();
        }

        $mediaType = My_Zend_Media::getMediaType($url);

        if ($mediaType == '') {
            $this->returnJson();
        }

        switch ($mediaType) {
            case My_Zend_Media::_MEDIA_TYPE_YOUTUBE:
                $youtubeInfo = My_Zend_Media::retrieveYouTubeInfo($url);
                if (empty($youtubeInfo)) {
                    $this->returnJson();
                }

                $data = array(
                    'title' => $youtubeInfo['title'],
                    'thumbnail' => $youtubeInfo['thumb']['default'],
                    'uploader'  => 'Youtube'
                );
                $this->returnJson($data);
                break;
            case My_Zend_Media::_MEDIA_TYPE_VIMEO:
                $vimeoInfo = My_Zend_Media::retrieveVimeoInfo($url);
                if (empty($vimeoInfo)) {
                    $this->returnJson();
                }

                $data = array(
                    'title' => $vimeoInfo['title'],
                    'thumbnail' => $vimeoInfo['thumb']['default'],
                    'uploader'  => $vimeoInfo['uploader']
                );
                $this->returnJson($data);
                break;
            case My_Zend_Media::_MEDIA_TYPE_ZINGTV:
                $videoInfo = My_Zend_Media::retrieveZingTVInfo($url);
                if (empty($videoInfo)) {
                    $this->returnJson();
                }

                $data = array(
                    'title' => $videoInfo['title'],
                    'thumbnail' => $videoInfo['thumb'],
                    'uploader'  => $videoInfo['uploader']
                );
                $this->returnJson($data);
                break;
            default:
                break;
        }

        $this->returnJson();
    }

	public function updatestatsAction() {

		$url = $this->_getParam('url', 0);

		if ($url) {
			$regEx = '/.+\/photo\/(\d+)/';
			preg_match($regEx, $url, $matches);
			$postId = (int) $matches[1];
		} else {
			$postId = $this->_getParam('id', 0);
		}

		if ($postId == 0) {
			exit;
		}

		$post = Post::getPost($postId, false);

		if (empty($post)) {
			exit;
		}

		$stats = Post::getFacebookUrlStats(Post::postUrl($post, true, false));

		if (empty($stats)) {
			exit;
		}

		if ($post['number_of_like'] != $stats['like_count'] || $post['number_of_comment'] != $stats['comment_count'] || $post['number_of_share'] != $stats['share_count']) {

			$data = array(
				'post_id'           => $postId,
				'number_of_like'    => $stats['like_count'],
				'number_of_comment' => $stats['comment_count'],
				'number_of_share'   => $stats['share_count'],
				'last_viewed'       => time()
			);

//			if ($post['phase'] < Post::_PHASE_HOMEPAGE && ($stats['like_count'] >= Post::_NUMBER_LIKE_TO_HOMEPAGE && $stats['comment_count'] >= Post::_NUMBER_COMMENT_TO_HOME && $stats['share_count'] >= Post::_NUMBER_SHARE_TO_HOMEPAGE)) {
//				$data['phase'] = Post::_PHASE_HOMEPAGE;
//			}

			if ($post['phase'] < Post::_PHASE_HOT && ($stats['like_count'] >= Post::_NUMBER_LIKE_TO_HOT && $stats['share_count'] >= Post::_NUMBER_SHARE_TO_HOT)) {
				if (($post['is_photo'] == 1 && $stats['comment_count'] >= 10) || ($post['is_photo'] == 0 && $stats['comment_count'] >= Post::_NUMBER_COMMENT_TO_HOT)) {
					$data['phase'] = Post::_PHASE_HOT;
				}
			}

			if (Post::update($data)) echo "Updated successfully";
			else echo "Updating failed";
		}

		exit;
	}

    public function logAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {

            $params = $this->getRequest()->getParams();

            if ($params['url']) {
                $regEx = '/.+\/photo\/(\d+)/';
                preg_match($regEx, $params['url'], $matches);
                $postId = (int) $matches[1];
            }

            if (!empty($postId) && $postId > 0) {
                $params['post_id'] = $postId;

                unset($params['url']);
                unset($params['controller']);
                unset($params['module']);
                unset($params['action']);

                $createLog = PostLog::create($params);

                if ($createLog) {
                    echo "Log OK";
                } else {
                    echo "Log Failed";
                }
            } else {
                echo "Invalid Post ID";
            }
        }
        exit;
    }

    public function ajxupuAction() {
        $userId = $this->_getParam('id', 0);

        $userId = intval($userId);
        if ($userId == 0) {
            exit;
        }

        // get user info
        $user = User::getUser($userId);
        if (empty($user)) {
            exit;
        }

        if (LOGIN_UID == $userId) {
            exit;
        }

        // get facebook stats
        $stats = Post::getFacebookUrlStats(User::userUrl($user, true));

        if (empty($stats)) {
            exit;
        }

        if ( $user['comment_count'] != $stats['comment_count'] ) {

            // update stats
            $data = array(
                    'user_id'           => $userId,
                    //'like_count'    => $stats['like_count'],
                    'comment_count' => $stats['comment_count'],
                    'profileviews'    => $user['profileviews'] + 1
            );

            User::updateUser($data);
        }
        else {
            $data = array(
                    'user_id'           => $userId,
                    'profileviews'      => $user['profileviews'] + 1
            );

            User::updateUser($data);
        }

        exit;
    }

    public function followAction() {
        $followedId = $this->_getParam('followed', 0);
        $followedId = intval($followedId);
        $userId = LOGIN_UID;

        if ($followedId == 0 || $followedId == $userId) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Invalid data'));
        }

        if ($userId == 0) {
            $this->returnJson(array('error_code' => 2, 'error_message' => 'User has not logined yet'));
        }

        // get user info
        $userInfo = User::getUser($userId);

        // get followed user info
        $followedUserInfo = User::getUser($followedId);

        if (empty($userInfo) || empty($followedUserInfo)) {
            $this->returnJson(array('error_code' => 3, 'error_message' => 'Invalid data'));
        }

        // check user follow or not
        $check = Follow::getDetailByUserIdAndFollowId($userId, $followedId);

        if ($check) {
            $result = Follow::delete($check['following_id']);

            if ($result) {
                if ($userInfo['followed_number'] > 0) {
                    // update total user follow
                    $data = array('user_id' => $followedId, 'followed_number' => $followedUserInfo['followed_number'] - 1);
                    User::updateUser($data);
                }

                if ($userInfo['follow_number'] > 0) {
                    // update total user follow
                    $data = array('user_id' => $userId, 'follow_number' => $userInfo['follow_number'] - 1);
                    User::updateUser($data);
                }
            }
        }
        else {
            // insert data
            $data = array(
                'follower_id'   => $userId,
                'followed_id'   => $followedId,
                'created_time'  => date('Y-m-d H:i:s')
            );

            $result = Follow::insert($data);

            if ($result) {
                // update total user follow
                $data = array('user_id' => $followedId, 'followed_number' => $followedUserInfo['followed_number'] + 1);
                User::updateUser($data);

                // update total follow
                $data = array('user_id' => $userId, 'follow_number' => $userInfo['follow_number'] + 1);
                User::updateUser($data);

                // insert notification
                $data = array(
                    'userID'   => $followedId,
                    'type'      => Notification::_NOTIFICATION_TYPE_FOLLOWING,
                    'time'      => date('Y-m-d H:i:s'),
                    'isActive'  => 1,
                    'status'    => 'active',
                    'followingID'    => $userId,
                    'message'   => ''
                );

                Notification::insert($data);
            }
        }

        if ($result) {
            $this->returnJson(array('error_code' => 0, 'error_message' => 'Follow sucessful'));
        }

        $this->returnJson(array('error_code' => -1, 'error_message' => 'System is busy'));
    }

    public function getnotificationsAction()
    {
        $type = $this->_getParam('type', '');
        $userId = LOGIN_UID;

        if ($userId == 0) {
            exit();
        }

        $filterType = '';

        if ($type == 'follow') {
            $filterType = Notification::_NOTIFICATION_TYPE_FOLLOWING;
        }
        elseif ($type == 'message') {
            $filterType = Notification::_NOTIFICATION_TYPE_WELCOME;
        }
        elseif ($type == 'other') {
            $filterType = array(
                            Notification::_NOTIFICATION_TYPE_LIKE_REACHED,
                            Notification::_NOTIFICATION_TYPE_MOVE_TO_HAY,
                            Notification::_NOTIFICATION_TYPE_MOVE_TO_HOME,
                            Notification::_NOTIFICATION_TYPE_PROFILE_COMMENT
            );
        }

        $notifications = Notification::getListNotification($userId, $filterType);

        // reset stats
        Notification::resetStats($userId, $filterType);

        $this->view->notifications = $notifications;
    }

    public function reportAction()
    {
        include_once APPLICATION_PATH . '/configs/reasons.php';
        $reasonId = $this->_getParam('reason', 0);
        $postId = intval($this->_getParam('itemID', 0));
        $reasonDetail = My_Zend_Globals::strip_word_html(trim($this->_getParam('reasonDetail', '')), '');

        if (!isset($_REASONS[$reasonId])) {
            echo "Failed"; exit();
        }

        if (LOGIN_UID == 0) {
            echo "Failed"; exit();
        }

        $post = Post::getPost($postId);

        if (empty($post)) {
            echo "Failed"; exit();
        }

        $data = array(
            'post_id'       => $postId,
            'user_id'       => LOGIN_UID,
            'time'          => time(),
            'ip'            => My_Zend_Globals::getAltIp(),
            'reason'        => $reasonId,
            'reason_detail' => $reasonDetail
        );

        if (PostReport::insert($data)) {
            echo "OK"; exit();
        }

        echo "Failed";
        exit;
    }

    public function approvePostAction()
    {
        $postId = $this->_getParam('post_id', 0);
        if (!Admin::isLogined()) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Admin is not logged in yet.'));
        }
        if (!Admin::isLogined()) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Data is invalid'));
        }
        if (!Role::isAllowed(Permission::ARTICLE_APPROVE)) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Bạn không có quyền duyệt bài'));
        }
        $data = array(
            'post_id' => $postId,
            'is_approved' => 1,
            'is_actived' => 1,
            'approved_at' => time()
        );

        if (Post::update($data)) {
            $this->returnJson(array('error_code' => 0, 'error_message' => 'Bài viết đã được duyệt thành công'));
        } else {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Hệ thống đang bận.'));
        }
    }

    public function movePostToHomeAction()
    {
        $postId = $this->_getParam('post_id', 0);
        if (!Admin::isLogined()) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Admin is not logged in yet.'));
        }
        if (!Admin::isLogined()) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Data is invalid'));
        }
        if (!Role::isAllowed(Permission::ARTICLE_MOVE_TO_HOME)) {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Bạn không có quyền đưa bài ra trang chủ'));
        }
        $data = array(
            'post_id' => $postId,
            'is_approved' => 1,
            'is_actived' => 1,
            'phase'     => Post::_PHASE_HOMEPAGE,
            'approved_at' => time()
        );

        if (Post::update($data)) {
            $this->returnJson(array('error_code' => 0, 'error_message' => 'Bài viết đã được hiển thị trang chủ'));
        } else {
            $this->returnJson(array('error_code' => 1, 'error_message' => 'Hệ thống đang bận.'));
        }
    }

    public function getTopMemberAction()
    {
        $type = $this->_getParam('type', '');
        if ($type == '') {
            exit();
        }

        $arrTypes = array();

        if ($type == 'week') {
            $this->view->topUsers = User::getTopUser(User::_TOP_LIKED_BY_WEEK, 10);
        } else if ($type == 'month') {
            $this->view->topUsers = User::getTopUser(User::_TOP_LIKED_BY_MONTH, 10);
        } else if ($type == 'all') {
            $this->view->topUsers = User::getTopUser(User::_TOP_LIKED_MEMBERS, 10);
        }

        $this->view->type = $type;
        $this->renderScript('partials/box_top_member.phtml');
    }

    private function returnJson($data=array()) {
        echo Zend_Json::encode($data);
        exit();
    }
}
