<?php

class RssController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
	
	public function indexAction()
	{
		$this->getHelper('layout')->disableLayout();
        $params   = $this->_getAllParams();
        $alias    = isset($params['alias']) ? trim($params['alias']) : '';
        $limit    = 25;
        $filters  = array('status' => 1);
        $articles = Article::getList($filters, 0, $limit);
        $feedData = array(
				'title' 		=> 'CamNangLamMe.vn',
				'description' 	=> $category['meta_description'],
				'link'			=> Category::categoryUrl($category, true),
				'charset'		=>'utf8',
				'generator'		=> 'CamNangLamMe.vn',
				'docs'			=> 'http://www.camnanglamme.vn'
		);

		if(!empty($articles))
		{
			$entries = array();
			foreach($articles as $article)
			{
                $description = '<a href="'. Article::articleUrl($article, true) .'"><img src="'. My_Zend_Globals::getThumbImage($article['picture'], 'thumb') .'" align="left" /></a>'. $article['description'];
                $entries[]   = array(
						'title'			=> $article['title'],
						'description'	=> $description,
						'link'			=> Article::articleUrl($article, true),
						'lastUpdate'	=> $article['updated_date'],
						'guid'			=> $article['article_id']
				);
			}
			$feedData['entries'] = $entries;
		}
		
		$feed = Zend_Feed::importArray($feedData, 'rss');
		header('Content-type: text/xml');
		echo $feed->send(); 
	}
	
    public function categoryAction()
    {
        $this->getHelper('layout')->disableLayout();
        $params        = $this->_getAllParams();
        $categoryId    = isset($params['category_id']) ? intval($params['category_id']) : 0;
        $categoryAlias = isset($params['category_alias']) ? trim($params['category_alias']) : '';
        $limit         = 25;
        $category      = Category::selectCategory($categoryId);
        if(empty($category))
        {
        	exit;
        }

        $categories = array($categoryId);
        // get children
        $children = Category::selectChildCategory($categoryId);
        if(!empty($children))
        {
        	foreach($children as $cat)
        	{
        		$categories[] = $cat['category_id'];
        	}
        }
        
        $filters = array(
        		'category_id'	=> $categories,
        		'status'		=> 1
        );
        
        $articles = Article::getList($filters, 0, $limit);
        $feedData = array(
			            'title' 		=> $category['category_name'] .' tại CamNangLamMe.vn',
			            'description' 	=> $category['meta_description'],
			            'link'			=> Category::categoryUrl($category, true),
			            'charset'		=>'utf8',
        				'generator'		=> 'CamNangLamMe.vn',
        				'docs'			=> 'http://www.camnanglamme.vn'
        );

        if(!empty($articles))
        {
        	$entries = array();
        	foreach($articles as $article)
        	{
        		$description = '<a href="'. Article::articleUrl($article, true) .'"><img src="'. My_Zend_Globals::getThumbImage($article['picture'], 'thumb') .'" align="left" /></a>'. $article['description'];	
        		$entries[] = array(
        				'title'			=> $article['title'],
        				'description'	=> $description,
        				'link'			=> Article::articleUrl($article, true),        				
        				'lastUpdate'	=> $article['updated_date'],
        				'guid'			=> $article['article_id']        				
        		);
        	}
        	
        	$feedData['entries'] = $entries;
        }        
        
        $feed = Zend_Feed::importArray($feedData, 'rss');
        header('Content-type: text/xml');
        echo $feed->send();
    }
    
    private function export($data, $type='rss')
    {
    	if($type == 'json')
    	{
    		
    	}
    	else
    	{
    		
    	}    		
    }
}