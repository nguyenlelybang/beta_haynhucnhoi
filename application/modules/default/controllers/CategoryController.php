<?php

class CategoryController extends Zend_Controller_Action {

	public function init()
	{
	}

	public function videosAction()
	{
		$this->_forward('category', 'category', 'default', array('category_alias' => 'videos'));
		return;
	}

	public function categoryAction()
	{
		$categoryAlias = $this->_getParam('category_alias', '');
		$page          = $this->_getParam('page', 1);
		$categoryAlias = trim($categoryAlias);
		$page          = intval($page);
		$limit         = 20;
		if ($categoryAlias == 'mang-thang-3-thang-dau') {
			$this->_redirect(BASE_URL . '/cam-nang/mang-thai-3-thang-dau', array('code' => 301));
			exit;
		}

		$category = Category::selectCategoryByAlias($categoryAlias);
		if (empty($category)) {
			$this->_forward('page-not-found', 'error');
			return;
		}

		$categoryId = $category['category_id'];
		$offset     = ($page - 1) * $limit;
		$path       = Category::selectSinglePath($categoryId);
		$categories = array($categoryId);
		$children   = Category::selectChildCategory($categoryId);
		if (!empty($children)) {
			foreach ($children as $cat) {
				$categories[] = $cat['category_id'];
			}
		}
		// get articles
		//if($category['page_content'] == '')
		{
			$filters  = array(
				'category_id' => $categories,
				'status' => 1
			);
			$total    = Article::countTotal($filters);
			$articles = array();
			if ($total > 0) {
				$articles = Article::getList($filters, $offset, $limit);

				foreach ($articles as &$article) {
					$article['description'] = My_Zend_Globals::cutString($article['description'], 0, 240);
				}
			}

			$this->view->addHelperPath('My/Helper/', 'My_Helper');
			$this->view->paging   = $this->view->paging(Category::categoryUrl($category), array(), $total, $page, $limit, PAGE_SIZE, "");
			$this->view->articles = $articles;
			$this->view->children = $children;
		}

		// get page navigation
		//$navigationGroups = CategoryNavigation::getGroupList(array('category_id' => $path[0]['category_id'], 'status' => 1));
		$navigationGroups = array();
		if (!empty($navigationGroups)) {
			$groupIds = array();
			foreach ($navigationGroups as $group) {
				$groupIds[] = $group['group_id'];
			}

			// get links
			$links = CategoryNavigation::getNavigationList(array('group_id' => $groupIds));
			if (!empty($links)) {
				foreach ($navigationGroups as &$group) {
					foreach ($links as $key => $link) {
						if ($group['group_id'] == $link['group_id']) {
							if (isset($group['links'])) {
								$group['links'][] = $link;
							} else {
								$group['links'] = array($link);
							}

							unset($links[$key]);
						}
					}
				}
			}
		}

		$topArticles                  = Article::getList(array('category_id' => $categories, 'status' => 1), 0, 10, array('order_by' => 'views desc'));
		$this->view->navigationGroups = $navigationGroups;
		$this->view->navigationPath   = $path;
		$this->view->category         = $category;
		$this->view->page             = $page;
		$this->view->topArticles      = $topArticles;
		$this->view->canonical        = Category::categoryUrl($category, true);
		$this->view->menuCategoryId   = $category['parent_id'] == 0 ? $category['category_id'] : $category['parent_id'];
		$this->view->rss              = array('title' => $category['category_name'] . ' tại CamNangLamMe.vn', 'link' => BASE_URL . '/rss/' . $category['category_id'] . '-' . $category['category_alias'] . '.rss');

		My_Zend_Globals::setTitle($category['page_title'] . ' | Cẩm nang làm mẹ');
		My_Zend_Globals::setMeta('keywords', $category['meta_keyword']);
		My_Zend_Globals::setMeta('description', $category['meta_description']);
	}
}
