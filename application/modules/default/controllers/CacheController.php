<?php

class CacheController extends Zend_Controller_Action {
    public function init()
    {
        parent::init();
    }
    public function indexAction(){

    }
    public function cleanallAction(){
        My_Zend_Cache::CleanAll();
        My_Zend_Cache::cacheHTML_Cancel();
        $this->_redirect(BASE_URL);
    }
    public function cleanallhtmlAction(){
        My_Zend_Cache::cacheHTML_CleanAll();
        My_Zend_Cache::cacheHTML_Cancel();
        $this->_redirect(BASE_URL);
    }

}