<?php

class MemberController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->postOnStaticMenu = Post::getPostOnStaticMenu();
    }

    public function indexAction()
    {
        $type = $this->_getParam('type', 'profile');
        $memberId = intval($this->_getParam('member_id', 0));
        $isFollowed = false;
        $userId = LOGIN_UID;
        if ($memberId == 0) {
            $this->_forward('page-not-found');
            return;
        }

        $profile = User::getUser($memberId);
        if (empty($profile) || !$profile['status']) {
            $this->_forward('page-not-found');
            return;
        }
        // check user followed or not
        if ($userId > 0 && $userId != $memberId) {
            if (Follow::getDetailByUserIdAndFollowId($userId, $memberId)) {
                $isFollowed = true;
            }
        }

        switch ($type) {
            case 'posts':
                $this->_forward('posts');
                break;

            case 'following':
                $this->_forward('following');
                break;

            case 'follower':
                $this->_forward('follower');
                break;

            default :
                $this->_forward('profile');
                break;
        }

        $this->view->isFollowed = $isFollowed;
        $this->view->profile = $profile;
        My_Zend_Globals::setTitle($profile['fullname'] . ' - HayNhucNhoi.tv');
    }

    public function profileAction()
    {
        $this->view->type = 'profile';
        $profile = $this->view->profile;
        My_Zend_Globals::setTitle($profile['fullname'] . ' - HayNhucNhoi.tv');
        My_Zend_Globals::setMeta('description', 'Trang cá nhân của ' . $profile['fullname'] . ' trên Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:title', $profile['fullname'] . ' - HayNhucNhoi.tv');
        My_Zend_Globals::setProperty('og:description', 'Trang cá nhân của ' . $profile['fullname'] . ' trên Hay Nhức Nhói');
    }

    public function postsAction()
    {
        $pageType = $this->_getParam('type');
        $page = intval($this->_getParam('page', 1));
        $limit = 20;
        $offset = ($page - 1) * $limit;
        $profile = $this->view->profile;

        $count = Post::countTotal(array('user_id' => $profile['user_id'], 'is_actived' => 1));
        $paging = new My_Helper_Paging();
        if ($count > 20) {
            $paging = $paging->blogPaging($pageType, $count, $page, $limit = 20, $page_size = 8, $style = 'page_item profile-post-paging', array('user_id' => $profile['user_id']));
            $this->view->paging = $paging;
        }
        $posts = Post::getList(array('user_id' => $profile['user_id'], 'is_actived' => 1), $offset, $limit, array('order_by' => 'post_id desc', 'get_views' => false), true, 300);

        $this->view->posts = $posts;
        $this->view->page = $page;
        $this->view->type = $pageType;

        My_Zend_Globals::setTitle('Bài đăng của ' . $profile['fullname'] . ' - HayNhucNhoi.tv');
        My_Zend_Globals::setMeta('description', 'Những bài đã đăng của ' . $profile['fullname'] . ' trên Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:title', 'Bài đăng của ' . $profile['fullname'] . ' - HayNhucNhoi.tv');
        My_Zend_Globals::setProperty('og:description', 'Những bài đã đăng của ' . $profile['fullname'] . ' trên Hay Nhức Nhói');
    }

    public function followingAction()
    {

        $page = intval($this->_getParam('page', 1));
        $limit = 20;
        $offset = ($page - 1) * $limit;
        $profile = $this->view->profile;
        $list = Follow::getList(array('follower_id' => $profile['user_id']), $offset, $limit);
        $arrMSG = array('errors' => 0, 'msg' => '', 'showhide' => 0);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            if ($this->getRequest()->isPost()) {

                if (LOGIN_UID > 0) {

                    $op = $this->_request->getParam('op');
                    $followed = (int)$this->_request->getParam('followed', 0);
                    $id = (int)$this->_request->getParam('id', 0);
                    $user = User::getUser(LOGIN_UID);
                    $user_follow = User::getUser($followed);
                    $check_follow = Follow::getDetailByUserIdAndFollowId(LOGIN_UID, $followed);

                    if ($check_follow) {

                        $arrMSG['errors'] = 0;
                        $arrMSG['msg'] = 'done';
                        $following_id = isset($check_follow['following_id']) ? $check_follow['following_id'] : 0;
                        if (LOGIN_UID != $profile['user_id']) {
                            $arrMSG['showhide'] = 1;
                            Follow::delete($following_id);
                        } else {
                            $arrMSG['showhide'] = 0;
                            Follow::delete($id);
                        }

                        $follow_number = isset($user['follow_number']) ? $user['follow_number'] : 0;
                        $followed_number = isset($user_follow['followed_number']) ? $user_follow['followed_number'] : 0;
                        if ($follow_number > 0) {
                            $follow_number = $user['follow_number'] - 1;
                        }

                        if ($followed_number > 0) {
                            $followed_number = $user_follow['followed_number'] - 1;
                        }

                        $data = array('user_id' => LOGIN_UID, 'follow_number' => $follow_number);
                        $data_follow = array('user_id' => $followed, 'followed_number' => $followed_number);
                        User::updateUser($data);
                        User::updateUser($data_follow);
                        $msg = $this->_helper->json($arrMSG, true);

                    } else {
                        if (LOGIN_UID == $followed) {
                            $arrMSG['errors'] = 1;
                            $arrMSG['msg'] = 'not user';
                            $msg = $this->_helper->json($arrMSG, true);

                        } else if (!empty($user_follow)) {
                            $data = array(
                                'follower_id' => LOGIN_UID,
                                'followed_id' => $followed,
                                'created_time' => date('Y-m-d H:i:s'));

                            $result = Follow::insert($data);
                            if ($result) {
                                $arrUser = array('user_id' => LOGIN_UID, 'follow_number' => $user['follow_number'] + 1);
                                $arrUserFollow = array('user_id' => $followed, 'followed_number' => $user_follow['followed_number'] + 1);
                                User::updateUser($arrUser);
                                User::updateUser($arrUserFollow);
                                $arrNotice = array(
                                    'userID' => $followed,
                                    'type' => Notification::_NOTIFICATION_TYPE_FOLLOWING,
                                    'time' => date('Y-m-d H:i:s'),
                                    'isActive' => 1,
                                    'status' => 'active',
                                    'followingID' => LOGIN_UID,
                                    'message' => ''
                                );
                                Notification::insert($arrNotice);
                                $arrMSG['errors'] = 0;
                                $arrMSG['msg'] = 'done';
                                $msg = $this->_helper->json($arrMSG, true);

                            }

                        } else {
                            $arrMSG['errors'] = 1;
                            $arrMSG['msg'] = 'not user';
                            $msg = $this->_helper->json($arrMSG, true);

                        }

                    } // end else

                }
            }
            echo $msg;
        } // end ajax


        if (LOGIN_UID > 0) {
            foreach ($list as $key => $value) {

                if (LOGIN_UID == $profile['user_id']) {
                    $list[$key]['status_follow'] = 1; // chinh user dang xem cua minh

                } else if ($value['user_id'] == LOGIN_UID) {
                    $list[$key]['status_follow'] = 2; // chinh user dang xem tai user khac

                } else {
                    if (Follow::getDetailByUserIdAndFollowId(LOGIN_UID, $value['user_id'])) {
                        $list[$key]['status_follow'] = 3; // da hong

                    } else {
                        $list[$key]['status_follow'] = 4; // chua hong
                    }

                }
            }
        }

        $this->view->currURL = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->list = $list;
        $this->view->type = 'following';
    }

    public function followerAction()
    {
        $page = intval($this->_getParam('page', 1));
        $limit = 20;
        $offset = ($page - 1) * $limit;
        $profile = $this->view->profile;
        $list = Follow::getList(array('followed_id' => $profile['user_id']), $offset, $limit);
        $arrMSG = array('errors' => 0, 'msg' => '', 'showhide' => 0);

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            if ($this->getRequest()->isPost()) {

                if (LOGIN_UID > 0) {

                    $op = $this->_request->getParam('op');
                    $followed = (int)$this->_request->getParam('followed', 0);
                    $id = (int)$this->_request->getParam('id', 0);
                    $user = User::getUser(LOGIN_UID);
                    $user_follow = User::getUser($followed);
                    $check_follow = Follow::getDetailByUserIdAndFollowId(LOGIN_UID, $followed);

                    if ($check_follow) {

                        $arrMSG['errors'] = 0;
                        $arrMSG['msg'] = 'done';
                        $following_id = isset($check_follow['following_id']) ? $check_follow['following_id'] : 0;
                        $arrMSG['showhide'] = 1;
                        Follow::delete($following_id);

                        $follow_number = isset($user['follow_number']) ? $user['follow_number'] : 0;
                        $followed_number = isset($user_follow['followed_number']) ? $user_follow['followed_number'] : 0;
                        if ($follow_number > 0) {
                            $follow_number = $user['follow_number'] - 1;
                        }

                        if ($followed_number > 0) {
                            $followed_number = $user_follow['followed_number'] - 1;
                        }

                        $data = array('user_id' => LOGIN_UID, 'follow_number' => $follow_number);
                        $data_follow = array('user_id' => $followed, 'followed_number' => $followed_number);
                        User::updateUser($data);
                        User::updateUser($data_follow);
                        $msg = $this->_helper->json($arrMSG, true);

                    } else {
                        if (LOGIN_UID == $followed) {
                            $arrMSG['errors'] = 1;
                            $arrMSG['msg'] = 'not user';
                            $msg = $this->_helper->json($arrMSG, true);

                        } else if (!empty($user_follow)) {
                            $data = array(
                                'follower_id' => LOGIN_UID,
                                'followed_id' => $followed,
                                'created_time' => date('Y-m-d H:i:s'));

                            $result = Follow::insert($data);
                            if ($result) {
                                $arrUser = array('user_id' => LOGIN_UID, 'follow_number' => $user['follow_number'] + 1);
                                $arrUserFollow = array('user_id' => $followed, 'followed_number' => $user_follow['followed_number'] + 1);
                                User::updateUser($arrUser);
                                User::updateUser($arrUserFollow);
                                $arrNotice = array(
                                    'userID' => $followed,
                                    'type' => Notification::_NOTIFICATION_TYPE_FOLLOWING,
                                    'time' => date('Y-m-d H:i:s'),
                                    'isActive' => 1,
                                    'status' => 'active',
                                    'followingID' => LOGIN_UID,
                                    'message' => ''
                                );
                                Notification::insert($arrNotice);
                                $arrMSG['errors'] = 0;
                                $arrMSG['msg'] = 'done';
                                $msg = $this->_helper->json($arrMSG, true);

                            }

                        } else {
                            $arrMSG['errors'] = 1;
                            $arrMSG['msg'] = 'not user';
                            $msg = $this->_helper->json($arrMSG, true);

                        }

                    } // end else

                }
            }
            echo $msg;
        } // end ajax

        if (LOGIN_UID > 0) {
            foreach ($list as $key => $value) {

                if ($value['user_id'] == LOGIN_UID) {
                    $list[$key]['status_follow'] = 2; // chinh user dang xem tai user khac

                } else {
                    if (Follow::getDetailByUserIdAndFollowId(LOGIN_UID, $value['user_id'])) {
                        $list[$key]['status_follow'] = 3; // da hong

                    } else {
                        $list[$key]['status_follow'] = 4; // chua hong
                    }

                }
            }
        }

        $this->view->currURL = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        $this->view->list = $list;
        $this->view->type = 'follower';
    }
}
