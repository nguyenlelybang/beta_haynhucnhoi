<?php

class BlogController extends Zend_Controller_Action {

    const _LIMIT      = 5;
    const _BLOG_APPROVED = 0;
	public function init() {

        $pinnedPostIDs = "";
        if (!empty($pinnedPostIDs)) {
            $pinnedPosts = Post::getListMultiPost(explode(',', $pinnedPostIDs));
            $currentTime = time();
            foreach ($pinnedPosts as &$pin) {
                $pin['is_pinned'] = true;
                $pin['number_of_view'] += (($currentTime - 1445747907) / 60);
            }
        }
        $filterNewPosts = array(
            'is_actived' => 1,
            'is_approved' => 1,
            'phase' => Post::_PHASE_NEW_POST,
            'from_date' => time() - 86400 * 2
        );

        $newPosts = Post::getList($filterNewPosts, 0, 60, array('order_by' => 'post_id DESC'), true, 900, false, 'new_posts_to_vote');

        if ( ! empty($newPosts) && is_array($newPosts)) {
            shuffle($newPosts);
            if (!empty($pinnedPosts)) {
                $newPosts = array_slice($newPosts, 0, 5 - count($pinnedPosts));
            } else {
                $newPosts = array_slice($newPosts, 0, 5);
            }
            foreach ($newPosts as $nPost) {
                $pinnedPosts[] = $nPost;
            }
        }

        if (isset($pinnedPosts)) {
            $pinnedPosts          = array_values($pinnedPosts);
            $this->view->newPosts = $pinnedPosts;
        }
        $this->view->lsCatogory = Category::selectCategoryList(array('exclude_empty' => false, 'is_hidden = 0'), true, Category::_CATEGORY_LIST_HOME_MENU);
        $this->view->isNavBar   = 1;
        $this->view->topUserPoint = UserPoint::getTopPoint('month', 10);
        $this->view->pageType   = 'index';
        $this->view->dataMeta   = Seo::getSeo(array('id' => 1));
    }

	public function indexAction() {
		$this->view->pageType = 'index';
        $pageType = $this->_getParam('pageType', 'index');
        $page = $this->_getParam('page', 1);

        $offset = ($page - 1) * self::_LIMIT;
        $count = count(BlogPost::getList(array('post_status'=>self::_BLOG_APPROVED)));
        $paging = new My_Helper_Paging();
        if($count > 5) {
            $paging = $paging->blogPaging($pageType,$count,$page,$limit=self::_LIMIT,$page_size=8,$style='page_item');
            $this->view->paging = $paging;
        }
        $posts = BlogPost::getList(array('limit'=>self::_LIMIT, 'offset'=>$offset, 'post_status'=>self::_BLOG_APPROVED));

        $this->view->posts = $posts;
        $this->view->page = $page;
        $breadcrumb      = new My_Helper_Breadcrumb('blog');
        $breadcrumb      = $breadcrumb->getBreadcrumb();
        $this->view->breadcrumb      = $breadcrumb;

        My_Zend_Globals::setTitle('Blog Hay Nhức Nhói');
    }

	public function blogDetailAction() {

		$this->view->pageType = 'blogDetail';
        $postID = $this->_getParam('post_id', 0);
        $postDetail = BlogPost::get($postID);
        $this->view->post = $postDetail;

        $categoryName = BlogCategory::get(array('category_id'=>$postDetail['category_id']));
        $this->view->categoryName = $categoryName;

        $blogAuthor = Admin::getUser($postDetail['user_id']);
        $this->view->blogAuthor = $blogAuthor;

        $breadcrumb = new My_Helper_Breadcrumb('blogDetail', $postDetail['post_id']);
        $breadcrumb = $breadcrumb->getBreadcrumb();
        $this->view->breadcrumb = $breadcrumb;

        $nextPost = BlogPost::getNextPost($postID);
        $prevPost = BlogPost::getPrevPost($postID);
        $this->view->nextPost = $nextPost;
        $this->view->prevPost = $prevPost;

        My_Zend_Globals::setTitle($postDetail['post_title'] . ' - Blog Hay Nhức Nhói');
        My_Zend_Globals::setMeta('description', $postDetail['post_summary']);
        My_Zend_Globals::setProperty('og:title', $postDetail['post_title'] . ' - Blog Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', $postDetail['post_summary']);
        My_Zend_Globals::setProperty('og:image', My_Zend_Globals::getThumbImage($postDetail['post_image']));
        My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
        My_Zend_Globals::setProperty('article:publisher', 'https://www.facebook.com/haynhucnhoi');
	}

    public function blogCategoryAction() {

        $this->view->pageType = 'chu-de';
        $pageType = $this->_getParam('pageType', 'chu-de');
        $categoryAlias = $this->_getParam('alias', '');
        $page = $this->_getParam('page', 1);
        $this->view->page = $page;

        $offset = ($page - 1) * self::_LIMIT;
        $alias = BlogCategory::get(array('category_alias'=>$categoryAlias));
        $categoryID = $alias['category_id'];
        $count = count(BlogPost::getList(array('category_id'=>$categoryID, 'post_status'=>self::_BLOG_APPROVED)));
        $paging = new My_Helper_Paging();
        $urlPage = $pageType . '/' . $categoryAlias;
        if($count > 5) {
            $paging = $paging->blogPaging($urlPage,$count,$page,$limit=self::_LIMIT,$page_size=8,$style='page_item');
            $this->view->paging = $paging;
        }

        $posts = BlogPost::getList(array('limit'=>self::_LIMIT, 'offset'=>$offset, 'post_status'=>self::_BLOG_APPROVED, 'category_id'=>$categoryID));
        $this->view->posts = $posts;

        $breadcrumb      = new My_Helper_Breadcrumb('blogCategory');
        $breadcrumb      = $breadcrumb->getBreadcrumb();
        $this->view->breadcrumb      = $breadcrumb;

        My_Zend_Globals::setTitle($alias['category_name'] . ' - Blog Hay Nhức Nhói');
    }
}