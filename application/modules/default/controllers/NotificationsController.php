<?php

class NotificationsController extends Zend_Controller_Action {
    const _LIMIT = 20;

    public function init() {

        $this->_redirect(BASE_URL);
        exit;

        if  (LOGIN_UID == 0) {
            return $this->_redirect(BASE_URL . '/user/login?url='.BASE_URL.'/'.$this->getRequest()->getControllerName());
        }
    }

    public function indexAction() 
    {
        $this->_redirect(BASE_URL);

        $page   = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;
        $notifications = Notification::getListNotification(LOGIN_UID, null, $offset, self::_LIMIT);
        $total         = Notification::countTotal(LOGIN_UID);
        $this->view->totalPage     = ceil($total / self::_LIMIT);
        $this->view->page          = $page;
        $this->view->notifications = $notifications;

        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $this->view->isAjax        = $isAjax;
        if ($isAjax) 
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_helper->layout()->disableLayout();
            $this->renderScript('notifications/followmore.phtml');
        }

    
        
    }
    public function follownoticeAction()
    {
        $page   = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;

        $isAjax = $this->getRequest()->isXmlHttpRequest();
        if ($isAjax) {
            $this->_helper->layout()->disableLayout();
             
        }

        $notifications = Notification::getListNotification(LOGIN_UID, null, $offset, self::_LIMIT);
        $total         = Notification::countTotal(LOGIN_UID);
        $this->view->totalPage     = ceil($total / self::_LIMIT);
        $this->view->page          = $page;
        $this->view->notifications = $notifications;
        $this->view->isAjax        = $isAjax;
    }
}
