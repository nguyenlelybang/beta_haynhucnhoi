<?php

class PostController extends Zend_Controller_Action
{
    const _VIEW_TO_HOT = 5000;

	public function init()
	{
		
		$this->view->pageType = 'detail';
		$this->view->lsCatogory = Category::selectCategoryList();
		$this->view->isNavBar   = 1;
		$this->view->postOnStaticMenu = Post::getPostOnStaticMenu();
	}

	public function postDetailAction()
	{
		$postId = $this->_getParam('post_id', 0);
        $url_string = $this->_getParam('url_string','');
		$video = $this->_getParam('video', null);

		$postId = intval($postId);
		$post   = Post::getPost($postId);

		if ($post['is_actived'] == 0 || empty($post)) {
			$this->_response->setHttpResponseCode(404);
			$this->_redirect('error/pagenotfound');
		}
		$chaps = PostsChap::getChapsByPostID($postId);
		$chapDetail = PostsChap::getChapByID($video);
		$this->view->approved = isset($post['is_approved']) ? $post['is_approved'] : 0;

		$postCategory = Category::selectCategory($post['category_id']);

		if(isset($_SERVER['HTTP_USER_AGENT']) && strstr($_SERVER['HTTP_USER_AGENT'],'facebookexternalhit')) {
			$isFacebookBot = true;
		}
		else {
			$isFacebookBot = false;
		}
		if (empty($url_string) || str_replace('.hnn', '', $url_string) != My_Zend_Globals::convertToSEO($post['story'])) {
			if ($isFacebookBot == false) {
				$this->getHelper('Redirector')->setCode(301);

				$postUrl = Post::postUrl($post, true, true);
				if ($video) {
					$postUrl .= '?video=' . $video;
				}
				$this->_redirect($postUrl);
			}
		}

		if(LOGIN_UID > 0 ) {
			$check_view  = User::getPostView(LOGIN_UID,$postId);
			if(empty($check_view))
			{
				$data_view  = array(
					'user_id'=>LOGIN_UID,
					'post_id'=>(int)$postId,
					'view_time'=>time(),
					'view_ip'=>$this->getRequest()->getServer('REMOTE_ADDR')
				);
				User::insertView($data_view);
			}
		}

		/*$user_id = $post['user_id'];*/
		/*$userinfo = User::getUser($user_id);*/
		/*$yourviewed = isset($userinfo['yourviewed']) ? $userinfo['yourviewed'] : 0;
		User::updateUser(array('user_id'=>$user_id,'yourviewed'=>$yourviewed + 1));*/

		$profile = User::getUser($post['user_id']);

		// Moi Keng Xa Beng

		if (APPLICATION_ENVIRONMENT == 'production') {
			$from_date = time() - 86400*3;
		} else {
			$from_date = null;
		}

		$newPosts = Post::getList(
			array(
				'is_actived' => 1,
				'is_approved' => 1,
				'phase' => Post::_PHASE_NEW_POST,
				'from_date' => $from_date),
			0, 100, array('order_by' => 'post_id DESC'), true, 900 , 'newposts_v2', 'newposts_detail');
		if (!empty($newPosts)) {
			unset($newPosts[$postId]);
			$newPosts1 = array(); $newPosts2 = array();
			foreach ($newPosts as $newPost) {
				if ($newPost['post_id'] % 2 == 0) {
					$newPosts1[] = $newPost;
				} else {
					$newPosts2[] = $newPost;
				}
			}
			shuffle($newPosts1);
			shuffle($newPosts2);
			$slicedNewPosts1 = array_slice($newPosts1, 0, 4);
			$slicedNewPosts2 = array_slice($newPosts2, 0, 9);
		}


		// Co the ban chua them xem
		/*
		$homepagePosts  = Post::getList(array('is_actived' => 1, 'is_approved' => 1, 'phase' => Post::_PHASE_HOMEPAGE, 'exclude_id' => $postId, 'from_date' => time() - 86400*5), 0, 60, array('order_by' => 'rand()'), false);
		shuffle($homepagePosts);
		$slicedHomepagePost = array_slice($homepagePosts, 0, 6);*/

		//Clip hot
		if (APPLICATION_ENVIRONMENT == 'production') {
			$topPostFromDate = time() - 86400*7;
		} else {
			$topPostFromDate = null;
		}
		$topPosts = Post::getList(array(
			'is_actived' => 1,
			'is_approved' => 1,
			'number_of_view' => self::_VIEW_TO_HOT,
			'is_photo' => 0,
			'from_date' => $topPostFromDate), 0, 40, array('order_by' => 'post_id DESC'), true, 3600, 'cliphot_detail', 'cliphot_detail');

		if ($topPosts) {
			if (isset($topPosts[$postId])) unset($topPosts[$postId]);
			shuffle($topPosts);
			$slicedTopPosts = array_slice($topPosts, 0, 5);
		}

		$newVideos = Post::getList(array(
			'is_actived' => 1,
			'is_approved' => 1,
			'phase' => Post::_PHASE_NEW_POST,
			'is_photo' => 0,
			'from_date' => time() - 86400*7), 0, 40, array('order_by' => 'post_id DESC'), true, 3600, 'clipnew_detail', 'clipnew_detail');

        if ($newVideos) {
			if (isset($newVideos[$postId])) unset($newVideos[$postId]);
			shuffle($newVideos);
			$slicedNewVideos = array_slice($newVideos, 0, 6);
            $suggestPost = $newVideos[6];
        }

		$nextPost = Post::getNextPost($postId);
		$prevPost = Post::getPrevPost($postId);

		// Bai viet lien quan
		$postTitle = $post['story'];
		$similarPosts = Post::getSimilarPost($postId, $postTitle);
		shuffle($similarPosts);
		$slicedSimilarPosts = array_slice($similarPosts, 0, 4);

		/*
		$tagIds   = Post::getPostTags($postId);
		if (!empty($tagIds)) {
			$tags = Tag::getList(array('tag_id' => $tagIds));
		} else {
			$tags = array();
		}*/

		$canApprovePost    = false;
		$canMovePostToHome = false;
		$canEditPost       = false;
		$canDeletePost     = false;
		if (Admin::isLogined()) {
			$canApprovePost    = true;
			$canMovePostToHome = false;
			$canEditPost       = true;
			$canDeletePost     = true;
		}

		$config = My_Zend_Globals::getConfiguration();
		$layout = Zend_Layout::getMvcInstance();

		if ($post['nsfw'])
			$pictureShare = $layout->_general['server']['img']['path'].'/'.Post::getNsfwThumb();
		elseif ($post['is_photo'])
					$pictureShare = $config->photo->domain.$post['picture'];
		else
			$pictureShare = My_Zend_Globals::getThumbImageVideo($post['picture']);

		if (!empty($post['picture_share']) || $post['picture_share'] != '')
			$pictureShare = $config->photo->domain . $post['picture_share'];

		if (LOGIN_UID != $post['user_id'] && $post['is_approved']) {
			$data = array(
				'post_id' => $post['post_id'],
				'number_of_view' => $post['number_of_view'] + 1
			);
			Post::update($data);
		};

		$timeToken = time();
		// Get breadcrumb

		$breadcrumb = new My_Helper_Breadcrumb('detail', $post['post_id']);
		$breadcrumb = $breadcrumb->getBreadcrumb();

		$post['story'] = htmlspecialchars_decode($post['story']);

		$doesUserLikeThisPost = Like::doesUserLikeThis(LOGIN_UID, $post['post_id']);
		$listOfUserLikeThisPost = Like::getPostLikeList($post['post_id']);
		$likeButtonTooltip = Like::parseLikeButtonTooltip($listOfUserLikeThisPost);
		// set viewed cookie
		//Post::setViewedPostCookie($postId);
		$this->view->post              = $post;
		$this->view->breadcrumb        = $breadcrumb;
		$this->view->profile           = $profile;
		$this->view->nextPost          = $nextPost;
		$this->view->prevPost          = $prevPost;
		$this->view->userLiked         = $doesUserLikeThisPost;
		$this->view->likeButtonTooltip = $likeButtonTooltip;
		if (!empty($slicedNewPosts1)) $this->view->newPosts1 = $slicedNewPosts1;
		if (!empty($slicedNewPosts2)) $this->view->newPosts2 = $slicedNewPosts2;
		if (!empty($slicedTopPosts)) $this->view->topPosts = $slicedTopPosts;
		if (!empty($slicedNewVideos)) $this->view->newVideos = $slicedNewVideos;
		if (!empty($suggestPost)) $this->view->suggestPost = $suggestPost;
		$this->view->similarPosts      = $slicedSimilarPosts;
		$this->view->canApprovePost    = $canApprovePost;
		$this->view->canMovePostToHome = $canMovePostToHome;
		$this->view->canEditPost       = $canEditPost;
		$this->view->canDeletePost     = $canDeletePost;
		$this->view->pictureShare      = $pictureShare;
		$this->view->ChapPost          = $chaps;
		$this->view->video = $video;
		$this->view->ChapVideo         = $chapDetail;
		$this->view->breadcrumb        = $breadcrumb;
		$this->view->tokenTime         = $timeToken;
		$this->view->now = time();
		if ($post['is_photo'] == 0) {
			$this->view->forceYouTube  = PostMeta::get($postId, 'force_youtube');
		}
		if ($chapDetail) {
			if ($chapDetail['chap_title']) {
				$title = $chapDetail['chap_title'];
			} else {
				$title = '[' . $chapDetail['chap_name'] . '] ' . $post['story'];
			}
		} else {
			$title = preg_replace('/:([a-z0-9A-Z_-]+):/', '', $post['story']);
		}
		if ($post['description']) {
			$description = $post['description'];
		} else {
			$description = 'Không những Hay mà còn Nhức Nhói. Sinh ra ở Mỹ, ầm ĩ ở Việt Nam. Cộng đồng chia sẻ nội dung hay, vui, nhức nhói thứ hai Việt Nam, sau Haivl.';
		}

		My_Zend_Globals::setTitle($title . ' - Hay Nhức Nhói');
		My_Zend_Globals::setMeta('description', $description);
		My_Zend_Globals::setProperty('og:url', Post::postUrl($post, true, false));
		My_Zend_Globals::setProperty('og:title', $title . ' - HayNhucNhoi.tv');
		My_Zend_Globals::setProperty('og:description', $description);
        My_Zend_Globals::setProperty('og:image', $pictureShare);
        My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
		My_Zend_Globals::setProperty('article:publisher', 'https://www.facebook.com/haynhucnhoi');
		$this->view->HeadMeta($title, 'headline', 'itemprop');
		$this->view->HeadMeta($description, 'description', 'itemprop');
		$this->view->HeadMeta($pictureShare, 'image', 'itemprop');

		if ($post['is_photo'] == 0) {
			My_Zend_Globals::setProperty('og:type', 'video.other');
		}
		else {
			My_Zend_Globals::setProperty('og:type', 'article');
		}
	}
}