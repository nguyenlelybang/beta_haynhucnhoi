<?php

class ErrorController extends Zend_Controller_Action
{
    function init()
    {
        //
    }

    /**
     * Error handle action
     */
    public function errorAction()
    {
        $error = $this->_getParam('error_handler');

        switch (get_class($error->exception)) {
            case 'PageNotFoundException':
                if (APPLICATION_ENVIRONMENT == 'production') {
                    $this->getResponse()->setHttpResponseCode(404);
                    $this->_forward('page-not-found');
                } else {
                    print_r($exception = $error->exception);
                    exit;
                }
                break;

            default:
                //put some default handling logic here
                if (APPLICATION_ENVIRONMENT == 'production') {
                    $this->_forward('page-not-found');
                } else {
                    print_r($exception = $error->exception);
                    exit;
                }

                break;

        }

    }

    public function pagenotfoundAction()
    {
        header("refresh:5;url=/");
        $this->_response->setHttpResponseCode(404);
        $this->render('page-not-found');
    }

    public function accessAction()
    {
        $this->render('access');
    }
}