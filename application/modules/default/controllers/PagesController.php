<?php

class PagesController extends Zend_Controller_Action {
    public function init()
    {
        $this->view->postOnStaticMenu = Post::getPostOnStaticMenu();
    }

    public function tosAction()
    {

    }

    public function rulesAction()
    {

    }

    public function aboutAction()
    {

    }

    public function faqAction()
    {

    }

    public function contactAction()
    {

    }

    public function privacypolicyAction()
    {

    }

    public function trianAction()
    {
        $layout = Zend_Layout::getMvcInstance();
        $image = $layout->_general['server']['img']['path'] . '/trian_thumbnail.jpg?v=2';
        $this->view->image = $image;

        My_Zend_Globals::setTitle('Chương trình tri ân thành viên - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:title','Chương trình tri ân thành viên - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', 'Nhằm cảm ơn sự đóng góp của các thành viên nhiệt thành, HayNhucNhoi.tv tổ chức chương trình tri ân hàng tháng. Click để xem thêm và tham gia.');
        My_Zend_Globals::setProperty('og:image', $image);
        My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
    }

    public function triAnThang10Action()
    {
        $layout = Zend_Layout::getMvcInstance();
        $image = $layout->_general['server']['img']['path'] . '/event/top50thang10.png?v=2';
        $this->view->image = $image;

        My_Zend_Globals::setTitle('Kết quả Tri ân tháng 10 - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:title','Kết quả Tri ân tháng 10 - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', 'Kết quả chương trình Tri ân thành viên tháng 10 - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:image', $image);
        My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');

        $this->renderScript('pages/ketquatrian.phtml');
    }

    public function cctalkAction()
    {
        $layout = Zend_Layout::getMvcInstance();
        $room = $this->_getParam('room', 88);
        $image = $layout->_general['server']['img']['path'] . '/event/cctalk-thumb.jpg?v=2';
        $this->view->room = $room;

        My_Zend_Globals::setTitle('Giao lưu online cùng hot girl trên Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:title','Xem trực tuyến TalkTV - Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', 'Xem trực tuyến gái xinh hát hay, nhảy đẹp tại Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:image',$image);
        My_Zend_Globals::setProperty('article:author', 'https://www.facebook.com/haynhucnhoi');
    }
}
