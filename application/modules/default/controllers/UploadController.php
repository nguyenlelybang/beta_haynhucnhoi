<?php

class UploadController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->dataMeta = Seo::getSeo(array('id' => 1));
    }

    public function indexAction()
    {
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/upload');
        }
        $userInfo = User::getUser(LOGIN_UID, false);

        if (empty($userInfo)) {
            $this->_redirect(BASE_URL);
        }

        $status =  isset($userInfo['status']) ? $userInfo['status'] : 0;

        if ($status == 0)
        {
            $this->_redirect(BASE_URL.'/error/access');
        }

        if (isset($userInfo['like_point']) && $userInfo['all_point'] > $userInfo['like_point'])
            $userPoint = $userInfo['all_point'];
        else
            $userPoint = $userInfo['like_point'];

        $point = !empty($userPoint) ? $userPoint : 0;
        $userPostCountToday  = User::countPostToday($userInfo['user_id']);
        $limit = Upload::getUploadLimit($point);

        if ($this->getRequest()->isPost())
        {
            if (!in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT)))
            {
                if ($userPostCountToday['total'] >= $limit['total_limit'])
				{
                    $this->_helper->flashMessenger->addMessage('Bạn đã đăng đủ số lượng bài đươc cho phép hôm nay.');
                    $this->_redirect(BASE_URL . '/upload');
					exit;
                }
            }

            $data = $this->_request->getPost();

            $file = isset($_FILES['image']) ? $_FILES['image'] : null;
            $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($data['title']), ''));
            $isSelfMade = isset($data['isSelfMade']) ? (int)$data['isSelfMade'] : 0;
            $isNSFW = isset($data['isNSFW']) ? (int)$data['isNSFW'] : 0;
            $data['source'] = isset($data['source']) ? $data['source'] : '';
            $source = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($data['source']), ''));

            if (empty($title)) {
                $this->_helper->flashMessenger->addMessage('Bạn chưa nhập tiêu đề');
                $this->_redirect(BASE_URL . '/upload');
            }

            if (!My_Zend_Globals::is_image($_FILES['image']['tmp_name'])) {

                $this->_helper->flashMessenger->addMessage('Tập tin này bị lỗi không thể tải lên được.Vui lòng chọn tập tin khác!');
                $this->_redirect(BASE_URL . '/upload');
            }

            $maxsize = round($_FILES["image"]["size"] / 1024 / 1024);
            if ($maxsize > 4) {
                $this->_helper->flashMessenger->addMessage('Hay Nhức Nhói hiện tại chỉ hỗ trợ đăng ảnh 4MB. Vui lòng chọn lại.');
                $this->_redirect(BASE_URL . '/upload');
            }

            $upload = new Upload();
            $destFolder = array('photo', date('Y'), date('m'));
            $rsUpload = $upload->upload($file, $destFolder);
            $rsUpload = $rsUpload[0];
            if (!isset($rsUpload['url']) || empty($rsUpload['url'])) {
                $this->_helper->flashMessenger->addMessage('Hệ thống đang bận. Bạn vui lòng thử lại.');
                $this->_redirect(BASE_URL . '/upload');
            }

            if ($isSelfMade) {
                $source = 'Tự làm';
            }

            $isGif = $rsUpload['is_animated'] ? 1 : 0;
            $insertData = array(
                'user_id' => LOGIN_UID,
                'story' => $title,
                'source' => $source,
                'picture' => $rsUpload['url'],
                'picture_share' => $rsUpload['share_image'],
                'created_at' => time(),
                'updated_at' => time(),
                'is_approved' => 0,
                'is_photo' => 1,
                'is_gif' => $isGif,
                'is_actived' => 1,
                'tags' => '',
                'nsfw' => $isNSFW,
				'category_id' => 0,
                'pip' => My_Zend_Globals::getAltIp()
            );

            if ($userInfo['approved_new_post'])
            {
                $insertData['is_approved'] = 1;
                $insertData['approved_at'] = time();
                $insertData['category_id'] = isset($data['category_id']) ? (int)$data['category_id'] : 0;
            }

            if ($userInfo['new_post_to_home']) {
                $insertData['phase'] = Post::_PHASE_HOMEPAGE;
            }
            $postId = Post::insert($insertData);

            if ($postId > 0) {
                $params['tag_id']  = isset($params['tag_id']) ? $params['tag_id'] : array();
                if(!empty($params['tag_id']))
                {
                    $tagIds = $data['tag_id'];
                    foreach ($tagIds as $tagId) {
                        Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                    }
                }

                // redirect to photo
                $this->_redirect(Post::postUrl(Post::getPost($postId)));
            } else {
                $this->_helper->flashMessenger->addMessage('Hệ thống đang bận. Bạn vui lòng thử lại.');
                $this->_redirect(BASE_URL . '/upload');
            }
        }

        $canManageTag = false;
        if (Admin::isLogined()) {
            $canManageTag = Role::isAllowed(Permission::ARTICLE_MANAGE_TAG);
        }

        $dataDefault = array('category_id'=>0);

		if (in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT)))
			$this->view->isSpecialUser = true;
        My_Zend_Globals::setTitle('Đăng ảnh mới - Hay Nhức Nhói');
        $this->view->point = $userPoint;
        $this->view->limit = $limit;
        $this->view->userPostCountToday = $userPostCountToday;
        $this->view->canManageTag = $canManageTag;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
        $this->view->userInfo = $userInfo;
        $this->view->lsCat    = Category::selectCategoryList();
        $this->view->dataDefault = array_merge($dataDefault,isset($data) ? $data : array());
    }

    public function videoAction()
    {
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/upload/video');
        }
        // check login user info
        $userInfo = User::getUser(LOGIN_UID);
        if (empty($userInfo)) {
            $this->_redirect(BASE_URL);
        }

        if (isset($userInfo['like_point']) && $userInfo['all_point'] > $userInfo['like_point'])
            $userPoint = $userInfo['all_point'];
        else
            $userPoint = $userInfo['like_point'];

        $point = !empty($userPoint) ? $userPoint : 0;
        $userPostCountToday  = User::countPostToday($userInfo['user_id']);
        $limit = Upload::getUploadLimit($point);

        if ($this->getRequest()->isPost()) {
            if(!in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT))) {
                if ($userPostCountToday['total'] >= $limit['total_limit']) {
                    $this->_helper->flashMessenger->addMessage('Bạn đã đăng đủ số lượng bài đươc cho phép hôm nay.');
                    $this->_redirect(BASE_URL . '/upload/video');
					exit;
                } elseif ($userPostCountToday['video'] >= $limit['video_limit']) {
					$this->_helper->flashMessenger->addMessage('Bạn đã đăng đủ số lượng video clip cho phép hôm nay.');
					$this->_redirect(BASE_URL . '/upload/video');
					exit;
				}
            }

            $params = $this->_request->getPost();
            $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($params['title']), ''));
            if (isset($params['description']) && !empty($params['description'])) {
                $description = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($params['description']), ''));
            }
            $videoUrl = trim($params['url']);
            if (empty($title) || empty($videoUrl)) {
                $this->_helper->flashMessenger->addMessage('Vui lòng nhập đầy đủ thông tin');
                $this->_redirect(BASE_URL . '/upload/video');
            }

            $videoType = My_Zend_Media::getMediaType($videoUrl);

            if ($videoType == '') {
                $this->_helper->flashMessenger->addMessage('Link Video không được hỗ trợ');
                $this->_redirect(BASE_URL . '/upload/video');
            }

            $isNSFW = isset ($params['isNSFW']) ? (int) $params['isNSFW'] : 0;

            $insertData = array(
                'user_id' => LOGIN_UID,
                'story' => $title,
                'created_at' => time(),
                'updated_at' => time(),
                'is_approved' => 0,
                'is_photo' => 0,
                'is_gif' => 0,
                'is_actived' => 1,
                'tags' => '',
                'source' => 'Sưu tầm',
                'pip' => My_Zend_Globals::getAltIp(),
                'category_id' => 0,
                'nsfw' => $isNSFW
            );

            if (isset($description)) {
                $insertData['description'] = $description;
            }

            if ($userInfo['approved_new_post'] || Admin::isLogined()) {
                $insertData['is_approved'] = 1;
                $insertData['approved_at'] = time();
                $insertData['category_id'] = isset($params['category_id']) ? (int)$params['category_id'] : 0;
            }

            if ($userInfo['new_post_to_home']) {
                $insertData['phase'] = Post::_PHASE_HOMEPAGE;
            }

            if ($videoType == My_Zend_Media::_MEDIA_TYPE_YOUTUBE) {

                $youtubeInfo = My_Zend_Media::retrieveYouTubeInfo($videoUrl);
                if (empty($youtubeInfo)) {
                    $this->_helper->flashMessenger->addMessage('Video YouTube không đúng định dạng');
                    $this->_redirect(BASE_URL . '/upload/video');
                }
                $insertData['youtube_key'] = $youtubeInfo['id'];
                $insertData['picture'] = isset($youtubeInfo['thumb']['maxres']) ? $youtubeInfo['thumb']['maxres'] : isset($youtubeInfo['thumb']['standard']) ? $youtubeInfo['thumb']['standard'] : $youtubeInfo['thumb']['high'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_VIMEO) {

                $pos = strpos($videoUrl, 'channels');
                if ($pos !== false) { // tìm thấy
                    $vimeo_id = My_Zend_Media::getVimeoIDFromURL($videoUrl);
                    $videoUrl = 'https://vimeo.com/' . $vimeo_id;
                }
                $insertData['vmo_key'] = My_Zend_Media::parseVimeo($videoUrl);
                $insertData['picture'] = My_Zend_Media::getVimeoThumbnail($insertData['vmo_key']);

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_ZINGTV) {

                $videoInfo = My_Zend_Media::retrieveZingTVInfo($videoUrl);
                if (empty($videoInfo)) {
                    $this->_helper->flashMessenger->addMessage('Video Zing TV không đúng định dạng cho phép');
                    $this->_redirect(BASE_URL . '/upload/video');
                }
                $insertData['zingtv_key'] = $videoInfo['id'];
                $insertData['picture'] = $videoInfo['thumb'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_NCT_MP3) {

                $videoInfo = My_Zend_Media::getSongNhaccuatui($videoUrl);
                if (empty($videoInfo)) {
                    $this->_helper->flashMessenger->addMessage('MP3 NhacCuaTui không đúng định dạng cho phép');
                    $this->_redirect(BASE_URL . '/upload/video');
                }
                $insertData['nct_mp3'] = $videoInfo['key'];
                $insertData['picture'] = $videoInfo['imageURL'];
                $insertData['url']     = $videoInfo['url'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_NCT_VIDEO) {

                $videoInfo = My_Zend_Media::getVideoNhaccuatui($videoUrl);
                if (empty($videoInfo)) {
                    $this->_helper->flashMessenger->addMessage('Video NhacCuaTui không đúng định dạng cho phép');
                    $this->_redirect(BASE_URL . '/upload/video');
                }
                $insertData['nct_video']    = $videoInfo['key'];
                $insertData['picture']      = $videoInfo['imageURL'];
                $insertData['url']          = $videoInfo['url'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_MECLOUD) {
	            $videoId = My_Zend_Media::getMeCloudIDFromUrl($videoUrl);
	            $videoInfo = My_Zend_Media::retrieveMeCloudVideoInfo($videoId);
	            if (empty($videoInfo)) {
		            $this->_helper->flashMessenger->addMessage('Không tìm thấy thông tin clip MeCloud');
		            $this->_redirect(BASE_URL . '/upload/video');
	            }
	            $insertData['mecloud_key'] = $videoInfo['video']['aliasId'];
	            $insertData['picture'] = $videoInfo['video']['thumbnails']['maxres'];
	            $insertData['url'] = $videoInfo['video']['embed']['script'];
            }

            // get share data
            if ($sharePic = Post::generateShareImage($insertData['picture'])) {
                $insertData['picture_share'] = $sharePic;
            }

            $postId = Post::insert($insertData);
            if ($userInfo['approved_new_post']) {

            }

            if ($postId > 0) {
                $params['tag_id']  = isset($params['tag_id']) ? $params['tag_id'] : array();
                if(!empty($params['tag_id'])) {
                    $tagIds =  $params['tag_id'];
                    foreach ($tagIds as $tagId) {
                        Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                    }
                }

                $this->_redirect(Post::postUrl(Post::getPost($postId)));
            } else {
                $this->_helper->flashMessenger->addMessage('Hệ thống đang bận. Bạn vui lòng thử lại.');
                $this->_redirect(BASE_URL . '/upload');
            }
        }// end post
        $canManageTag = false;
        if (Admin::isLogined()) {
            $canManageTag = Role::isAllowed(Permission::ARTICLE_MANAGE_TAG);
        }

		if (in_array($userInfo['user_id'],  explode(',', EXCLUDED_USER_ID_POINT))) {
			$this->view->isSpecialUser = true;
		}
        $this->view->point = $userPoint;
        $this->view->limit = $limit;
        $this->view->userPostCountToday = $userPostCountToday;
        $this->view->canManageTag = $canManageTag;
		$this->view->messages = $this->_helper->flashMessenger->getMessages();
        $dataDefault = array('category_id'=>0);
        $this->view->lsCat    = Category::selectCategoryList();
        $this->view->dataDefault = array_merge($dataDefault,isset($params) ? $params : array());
        My_Zend_Globals::setTitle('Đăng video mới - Hay Nhức Nhói');
    }

    public function memeAction()
    {

        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/meme');
        }

        $id = $this->_getParam('id', 0);
        if ($id > 0) {
            $this->_forward('memedetail');
        } else {
            $this->_forward('listmeme');
        }
    }

    public function listmemeAction()
    {
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/meme/id/' . $id);
        }
        // get list default meme
        $list = Meme::getList(array('is_active' => 1), 0, 1000);
        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle("Chế meme - " . $this->view->dataMeta['title']);
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        $this->view->list = $list;
    }

    public function memedetailAction()
    {
        $id = $this->_getParam('id', 0);
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/meme/id/' . $id);
        }

        $meme = Meme::getDetail($id);

        if (empty($meme)) {
            $this->_forward('page-not-found');
        }
        $layout = Zend_Layout::getMvcInstance();
        My_Zend_Globals::setTitle($this->view->dataMeta['title']);
        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        $this->view->meme = $meme;
    }

    public function memecustomAction()
    {

    }

    public function getmemedataAction()
    {
        if (LOGIN_UID == 0) {
            exit();
        }

        $id = $this->_getParam('id', 0);
        $meme = Meme::getDetail($id);
        if (empty($meme)) {
            exit();
        }

        $imagePath = STATIC_PATH . '/images/meme/' . $id . '.jpg';

	    if (!file_exists($imagePath)) {
            exit();
        }

        $content = file_get_contents($imagePath);
        $base64 = base64_encode($content);
        $array['image'] = "data:image/jpeg;base64,$base64";
        header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
        header("Pragma: no-cache"); //HTTP 1.0
        echo Zend_Json::encode($array);
        exit;
    }

    public function uploadmemeAction()
    {
        if (!isset($_REQUEST['imageData'])) {
            exit();
        }

        $title = $this->_getParam('title', '');
        if (empty($title)) {
            exit();
        }

        //$title = My_Zend_Globals::strip_word_html(trim($title), '');
        $title = htmlspecialchars($title);
        $userId = LOGIN_UID;

        if ($userId == 0) {
            exit();
        }

        $userInfo = User::getUser(LOGIN_UID);

        if (isset($userInfo['like_point']) && $userInfo['all_point'] > $userInfo['like_point'])
            $userPoint = $userInfo['all_point'];
        else
            $userPoint = $userInfo['like_point'];

		$point = !empty($userPoint) ? $userPoint : 0;
		$userPostCountToday  = User::countPostToday($userInfo['user_id']);
		$limit = Upload::getUploadLimit($point);

		if(!in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT)))
		{
			if ($userPostCountToday['total'] >= $limit['total_limit'])
			{
                $this->getResponse()->setHttpResponseCode(403);
                echo "Bạn đã đăng đủ số lượng bài cho phép hôm nay. Hãy tiếp tục vào ngày mai.";
                exit;
                //throw new Zend_Controller_Action_Exception('Bạn đã đăng đủ số lượng bài cho phép hôm nay. Hãy thử lại vào ngày mai', 403);
			}
		}

        $rawData = My_Zend_Globals::strip_word_html($_REQUEST['imageData'], '');;
        $memeUrl = Meme::saveMeme($userId, $rawData);
        if (!empty($memeUrl)) {
            // create post
            $insertData = array(
                'user_id' => LOGIN_UID,
                'story' => $title,
                'source' => '',
                'picture' => $memeUrl,
                'created_at' => time(),
                'updated_at' => time(),
                'is_approved' => 0,
                'is_photo' => 1,
                'is_meme' => 1,
                'is_gif' => 0,
                'is_actived' => 1,
                'category_id' => 0,
				'picture_share' => '',
                'tags' => '',
                'pip' => My_Zend_Globals::getAltIp()
            );

            $postId = Post::insert($insertData);

            if ($postId > 0) {
                echo Zend_Json::encode(array('Success' => 'true', 'Redirect' => Post::postUrl(Post::getPost($postId))));
                exit();
            }
        }

        echo Zend_Json::encode(array('Success' => 'false', 'Redirect' => ''));
    }

    public function editAction()
    {
        $postId = $this->_getParam('post_id', 0);
        $postId = intval($postId);
        $post = Post::getPost($postId);
        $post_id_old = isset($post['post_id']) ? $post['post_id'] : 0;
        if ($post_id_old == 0) {
            $this->_redirect(BASE_URL . '/error/pagenotfound');
        }

        if (!Admin::isLogined()) {
            $is_approved = isset($post['is_approved']) ? $post['is_approved'] : 0;
            if ($is_approved != 0 || LOGIN_UID == 0 || LOGIN_UID != $post['user_id']) {
                $this->_redirect(BASE_URL . '/error/pagenotfound');
            }
        }

        $arrErrors = array('errors' => 0, 'msg' => '');

        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {

                $title = $this->getRequest()->getPost('title', null);
                $source = $this->getRequest()->getPost('source', null);
                $description = $this->getRequest()->getPost('description', null);

                if (strlen(trim($title)) == 0) {
                    $arrErrors['errors'] = 1;
                    $arrErrors['msg'] = 'Vui lòng nhập tiêu đề';
                } else if (My_Zend_Globals::is_HTML($title)) {
                    $arrErrors['errors'] = 1;
                    $arrErrors['msg'] = 'Tiêu đề không chứa các script!';
                } else {
                    $source = isset($source) ? $source : '';
                    $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($title), ''));
                    $description = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($description), ''));
                    $data = array('post_id' => $post_id_old, 'source' => $source, 'story' => $title, 'description' => $description);
                    Post::update($data);
                    $arrErrors['errors'] = 2;
                    $arrErrors['msg'] = 'Chỉnh sửa thành công!';
                }
            }
            $this->_helper->json($arrErrors);
        }

        $this->view->post = $post;

    }

    public function xeditAction()
    {
        if (LOGIN_UID == 0) {
            $this->_forward('page-not-found');
        }

        $userId = LOGIN_UID;
        $postId = $this->_getParam('post_id', 0);
        $postId = intval($postId);

        $canEditPost = false;
        if (Admin::isLogined() && SYSTEM_USER_ROLE) {
            $canEditPost = Role::isAllowed(Permission::ARTICLE_EDIT);
        }
        // get article detail
        $post = Post::getPost($postId);
        if (empty($post) || ($post['user_id'] != $userId) && !$canEditPost) {
            $this->_forward('page-not-found');
            return;
        }


        if (!Admin::isLogined()) {
            $is_approved = isset($post['is_approved']) ? $post['is_approved'] : 0;
            if ($is_approved != 0) {
                $this->_forward('page-not-found');
                return;
            }

        }

        $oldTags = Post::getPostTags($postId);
        if ($this->getRequest()->isPost()) {
            $params = $this->_request->getPost();
            $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($params['title']), ''));
            if (empty($title)) {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Tiêu đề không được để trống.'));
                $this->_redirect(BASE_URL . '/upload/edit/post_id/' . $postId);
            }

            $data['post_id'] = $postId;
            $data['story'] = $title;
            $data['updated_at'] = time();
            if ($post['is_photo']) {
                $isSelfMade = isset($params['isSelfMade']) ? intval($params['isSelfMade']) : 0;
                $source = My_Zend_Globals::strip_word_html(trim($params['source']), '');
                if ($isSelfMade) {
                    $source = 'Tự làm';
                }

                $data['source'] = $source;
            }

            if (count($data) > 1) {
                if (Post::update($data)) {
                    // manage tags
                    $tagIds = $params['tag_id'];
                    if (!empty($oldTags) && empty($tagIds)) {
                        $rs = Post::deletePostTag($postId, 0, $oldTags);
                    } else if (empty($oldTags) && !empty($tagIds)) {
                        foreach ($tagIds as $tagId) {
                            $rs = Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                        }
                    } else if (!empty($tagIds) && !empty($oldTags)) {
                        foreach ($oldTags as $tagId) {
                            if (!in_array($tagId, $tagIds)) {
                                $rs = Post::deletePostTag($postId, $tagId);
                            }
                        }
                        foreach ($tagIds as $tagId) {
                            if (!in_array($tagId, $oldTags)) {
                                $rs = Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                            }
                        }
                    }
                    $this->_redirect(Post::postUrl($post));
                } else {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Hệ thống đang bận. Vui lòng thử lại sau'));
                    $this->_redirect(BASE_URL . '/upload/edit/post_id/' . $postId);
                }
            } else {
                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => 'Tiêu đề không được để trống.'));
                $this->_redirect(BASE_URL . '/upload/edit/post_id/' . $postId);
            }
        }
        $canManageTag = false;

        if (Admin::isLogined()) {
            $canManageTag = Role::isAllowed(Permission::ARTICLE_MANAGE_TAG);
        }

        $this->view->canManageTag = $canManageTag;
        $this->view->tags = Tag::getList(array('tag_id' => $oldTags));;
        $this->view->post = $post;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }
}
