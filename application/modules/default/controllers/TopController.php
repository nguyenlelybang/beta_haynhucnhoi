<?php

class TopController extends Zend_Controller_Action {
    public function init(){
    }
    public function indexAction()
    {
        $time = $this->getRequest()->getParam('time','');

        if ($time != 'month' && $time != 'week')
        {
            $time = 'all';
        }

		$type = 'point';
        $limit      = 50;
        $filters    = array(
            'status'      => 1,
            'is_approved' => 1,
            'is_actived'  => 1
        );

        $users = UserPoint::getTopPoint($time, $limit);
        $this->view->type = $type;
		$this->view->users = $users;
        $this->view->time = $time;
        My_Zend_Globals::setTitle('Bảng xếp hạng - Hay Nhức Nhói');
    }
}
