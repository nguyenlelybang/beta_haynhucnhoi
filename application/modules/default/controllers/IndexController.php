<?php

class IndexController extends Zend_Controller_Action
{
    const _LIMIT = 15;
    const _LIMIT_GRID = 12;
    const _VIEW_TO_HOT = 5000;

    private $_isAjax = false;

    public function init()
    {
        $x = $this->_getParam('r', '');
        if (!empty($x)) {
            die();
        }
        $page = $this->_getParam('page', 1);
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if ($isAjax && $page > 1) {
            $this->_helper->layout()->disableLayout();
        } else {
            $this->view->topUserPoint = UserPoint::getTopPoint('month', 10);
			$pinnedPostIDs = "";
            if (!empty($pinnedPostIDs)) {
                $pinnedPosts = Post::getListMultiPost(explode(',', $pinnedPostIDs));
                $currentTime = time();
                foreach ($pinnedPosts as &$pin) {
                    $pin['is_pinned'] = true;
                    $pin['number_of_view'] += (($currentTime - 1447231718) / 60);
                }
            }

            $filterNewPosts = array(
                'is_actived' => 1,
                'is_approved' => 1,
                'phase' => Post::_PHASE_NEW_POST,
                'from_date' => time() - 86400 * 2
            );

            $newPosts = Post::getList($filterNewPosts, 0, 60, array('order_by' => 'post_id DESC'), true, 900, false, 'new_posts_to_vote');

            if (!empty($newPosts) && is_array($newPosts)) {
                shuffle($newPosts);
                if (!empty($pinnedPosts)) {
                    $newPosts = array_slice($newPosts, 0, 5 - count($pinnedPosts));
                } else {
                    $newPosts = array_slice($newPosts, 0, 5);
                }
                foreach ($newPosts as $nPost) {
                    $pinnedPosts[] = $nPost;
                }
            }

            if (isset($pinnedPosts)) {
                $pinnedPosts = array_values($pinnedPosts);
                $this->view->newPosts = $pinnedPosts;
            }

            //$this->view->postOnStaticMenu = Post::getPostOnStaticMenu();
        }
        $this->view->lsCatogory = Category::selectCategoryList(array('exclude_empty' => false, 'is_hidden = 0'), true, Category::_CATEGORY_LIST_HOME_MENU);
        $this->view->params = $this->_getAllParams();
        $this->view->isAjax = $isAjax;
        $this->view->isNavBar = 1;
        $this->view->pageType = 'index';
        $this->view->dataMeta = Seo::getSeo(array('id' => 1));
        //$this->view->now = time();
    }

    public function indexAction()
    {
        $pageType = $this->_getParam('pageType', 'index');
        $page = $this->_getParam('page', 1);
        $offset = ($page - 1) * self::_LIMIT;
        $filters = array('is_actived' => 1, 'is_approved' => 1);
        $order_by = 'updated_at DESC';
        $isLoginRequired = false;

        if ($pageType == 'new') {

            $filters['phase'] = Post::_PHASE_NEW_POST;
            $order_by = 'approved_at DESC';
            $title = 'Bài mới';

        } elseif ($pageType == 'hot') {

            $filters['number_of_view'] = self::_VIEW_TO_HOT;
            $title = 'Nhức nhói';

        } elseif ($pageType == 'video') {

            $filters['phase'] = array('0', '1');
            $filters['is_photo'] = 0;
            $title = 'Clip hot';

        } elseif ($pageType == 'meme') {

            $filters['is_meme'] = 1;
            $title = 'Meme';

        } elseif ($pageType == 'hong') {

            $userId = LOGIN_UID;
            if ($userId > 0) {
                $followedList = Follow::getList(array('follower_id' => $userId, 'is_actived' => 1), 0, Follow::_MAX_FOLLOWING, array('get_user_info' => false));
                $followedIds = array();
                if (!empty($followedList)) {
                    foreach ($followedList as $value) {
                        $followedIds[] = $value['followed_id'];
                    }
                }
                if (!empty($followedIds)) {
                    $filters['user_id'] = $followedIds;
                    $order_by = 'approved_at DESC';
                }
            }
            $isLoginRequired = true;

        } elseif ($pageType == 'chuaxem') {

            if (LOGIN_UID > 0) {
                $userId = LOGIN_UID;
                $lsViewPost = User::getUserViewPost($userId);
                if (!empty($lsViewPost)) {
                    $lv = array();
                    foreach ($lsViewPost as $key => $value) {
                        $lv[] = $value['post_id'];
                    }
                    $lv = implode(',', $lv);
                    $filters['no_view'] = $lv;
                }
            }
            $isLoginRequired = true;

        } else {

            $filters['phase'] = Post::_PHASE_HOMEPAGE;

        }

        if (LOGIN_UID > 0) {

            $userId = LOGIN_UID;
            $userInfo = User::getUser($userId);
            if (!empty($userInfo) && !empty($userInfo['filter']) && ($userInfo['filter']['photo'] || $userInfo['filter']['music'] || $userInfo['filter']['clip'])) {
                if ($userInfo['filter']['photo'] && !$userInfo['filter']['clip']) {
                    $filters['is_photo'] = 1;
                } else if ($userInfo['filter']['clip'] && !$userInfo['filter']['photo']) {
                    $filters['is_photo'] = 0;
                }
            }

        }

        $posts = Post::getList($filters, $offset, self::_LIMIT, array('order_by' => $order_by), true, 600);
        $breadcrumb = new My_Helper_Breadcrumb($pageType);
        $breadcrumb = $breadcrumb->getBreadcrumb();

        $this->view->page = $page;
        $this->view->nextPage = BASE_URL . '/' . $pageType . '/' . ($page + 1);
        $this->view->posts = $posts;
        $this->view->pageType = $pageType;
        $this->view->isLoginRequired = $isLoginRequired;
        $this->view->breadcrumb = $breadcrumb;

        if ($pageType == 'new') {
            $timeToken = time();
            $doesUserLikeThisPost = array();
            $listOfUserLikeThisPost = array();
            $likeButtonTooltip = array();

            foreach ($posts as $post) {
                $doesUserLikeThisPost[$post['post_id']] = Like::doesUserLikeThis(LOGIN_UID, $post['post_id']);
                $listOfUserLikeThisPost[$post['post_id']] = Like::getPostLikeList($post['post_id']);
                $likeButtonTooltip[$post['post_id']] = Like::parseLikeButtonTooltip($listOfUserLikeThisPost[$post['post_id']]);
            }

            $this->view->doesUserLikeThisPost = $doesUserLikeThisPost;
            $this->view->listOfUserLikeThisPost = $listOfUserLikeThisPost;
            $this->view->likeButtonTooltip = $likeButtonTooltip;
            $this->view->tokenTime = $timeToken;
        }

        $layout = Zend_Layout::getMvcInstance();
        if (isset($title)) {
            My_Zend_Globals::setTitle($title . ' - ' . $this->view->dataMeta['title']);
        } else {
            My_Zend_Globals::setTitle($this->view->dataMeta['title']);
        }

        if (LOGIN_UID > 0) {
            $userInfo = User::getUser(LOGIN_UID);
            if (isset($userInfo['like_point']) && $userInfo['all_point'] > $userInfo['like_point'])
                $point = !empty($userInfo['all_point']) ? $userInfo['all_point'] : 0;
            else
                $point = !empty($userInfo['like_point']) ? $userInfo['like_point'] : 0;

            $isPostVideo = $point > 500 ? true : false;
            $userPostCountToday = User::countPostToday($userInfo['user_id']);
            $limit = Upload::getUploadLimit($point);
            if (in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT))) {
                $this->view->isSpecialUser = true;
            }

            $this->view->point = $userInfo['all_point'];
            $this->view->limit = $limit;
            $this->view->userPostCountToday = $userPostCountToday;
            $this->view->isPostVideo = $isPostVideo;
        }


        My_Zend_Globals::setMeta('keywords', $this->view->dataMeta['keyword']);
        My_Zend_Globals::setMeta('description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:url', BASE_URL);
        My_Zend_Globals::setProperty('og:title', $this->view->dataMeta['metatitle'] . ' - HayNhucNhoi.tv');
        My_Zend_Globals::setProperty('og:description', $this->view->dataMeta['description']);
        My_Zend_Globals::setProperty('og:image', $layout->_general['server']['img']['path'] . '/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('image_src', $layout->_general['server']['img']['path'] . '/600-thumbnail.jpg');
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    public function newAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'new'));
    }

    public function followAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'hong'));
    }

    public function hotAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'hot'));
    }

    public function cliphotAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'video'));
    }

    public function chuaxemAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'chuaxem'));
    }

    public function memeAction()
    {
        $this->_forward('index', null, null, array('pageType' => 'meme'));
    }

    public function searchAction()
    {
        My_Zend_Globals::setTitle('Tìm kiếm trên Hay Nhức Nhói');
        $this->view->pageType = 'search';
    }

    public function clipHaiAction()
    {
        $url = $this->getRequest()->getRequestUri();
        $url = str_replace('/clip-hai', '/video', $url);
        $this->getHelper('Redirector')->setCode(301);
        $this->_redirect(BASE_URL . $url);
    }

    public function categoryAction()
    {
        $alias = $this->_getParam('alias', '');
        $page = $this->_getParam('page', 1);
        if (empty($alias)) {
            $this->_forward('page-not-found');

            return;
        }
        $category = Category::selectCategoryByAlias($alias);
        $is_grid = isset($category['is_grid']) ? $category['is_grid'] : 0;
        if ($is_grid == 1) {
            $grid = self::_LIMIT_GRID;
        } else {
            $grid = self::_LIMIT;
        }
        if (empty($category)) {
            $this->_forward('page-not-found');

            return;
        }
        // get list homepage
        $filters = array('category_id' => $category['category_id'], 'is_actived' => 1, 'is_approved' => 1);
        $offset = ($page - 1) * $grid;
        $posts = Post::getList($filters, $offset, $grid);
        $posts = array_values($posts);
        $breadcrumb = new My_Helper_Breadcrumb('category', $category['category_id']);
        $breadcrumb = $breadcrumb->getBreadcrumb();
        $this->view->category = $category;
        $this->view->pageType = 'category';
        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->nextPage = BASE_URL . '/chu-de/' . $category['alias'] . '/' . ($page + 1);
        $this->view->alias = $alias;
        $this->view->breadcrumb = $breadcrumb;
        My_Zend_Globals::setTitle($this->view->category['page_title']);
        My_Zend_Globals::setMeta('keywords', $this->view->category['meta_keyword']);
        My_Zend_Globals::setMeta('description', $this->view->category['meta_description']);
    }

    public function tagAction()
    {
        $alias = $this->_getParam('tag_alias', '');
        $tagId = $this->_getParam('tag_id', 0);
        $page = $this->_getParam('page', 1);
        $limit = 20;
        $offset = ($page - 1) * $limit;
        if (empty($alias) || empty($tagId)) {
            $this->_forward('page-not-found');

            return;
        }
        $tag = Tag::getTag($tagId);
        if (empty($tag)) {
            $this->_forward('page-not-found');

            return;
        }
        // get articles
        $total = Post::countPostIdByTagId($tag['id']);
        $posts = array();
        if ($total > 0) {
            $postIds = Post::getPostIdByTagId($tag['id'], $offset, $limit);
            if (!empty($postIds)) {
                $posts = Post::getList(array('post_id' => $postIds, 'is_actived' => 1, 'status' => 1), $offset, $limit);
            }
        }
        $this->view->tag = $tag;
        $this->view->page = $page;
        $this->view->posts = $posts;
        $this->view->nextPage = Tag::tagUrl($tag) . '?page=' . ($page + 1);
        $this->view->canonical = Tag::tagUrl($tag);
        My_Zend_Globals::setProperty('og:url', Tag::tagUrl($tag));
        My_Zend_Globals::setProperty('og:title', 'Xem ' . $tag['tag_name'] . ' tại Hay Nhức Nhói');
        My_Zend_Globals::setProperty('og:description', '');
        if (!empty($tag['share_image'])) {
            My_Zend_Globals::setProperty('og:image', $tag['share_image']);
            My_Zend_Globals::setProperty('image_src', $tag['share_image']);
        }
        My_Zend_Globals::setProperty('og:type', 'website');
    }

    function testcacheAction()
    {
        $key = $this->_getParam('key', '');
        $value = $this->_getParam('value', '');
        $type = $this->_getParam('type', '');
        // get cache instance
        $cache = My_Zend_Globals::getCaching(1);
        if ($type == 'set') {
            $rs = $cache->write($key, $value, 5 * 60);
            var_dump($rs);
        } elseif ($type == 'get') {
            $data = $cache->read($key);
            echo "<pre>";
            print_r($data);
        } else {
            $data = User::getListMultiUser(array(1, 2, 3, 4));
            echo "<pre>";
            print_r($data);
        }
        exit;
    }

    function pageNotFoundAction()
    {

    }
}
