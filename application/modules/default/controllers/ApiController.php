<?php

class ApiController extends Zend_Controller_Action
{
    const LIMIT = 6;
    const LIMIT_POST_IFRAME_2 = 6;
    const FACEBOOK_GRAPH_URL = 'https://graph.facebook.com/me';
    const API_SECRET_KEY = 'YOUR_KEY'; //TODO
    const EXPIRED_TIME = 86400;

    public function init()
    {
        $this->view->pageType = 'detail';
        $this->_helper->layout->disableLayout();
//		$layout = Zend_Layout::getMvcInstance();
//		$layout->setLayout('layout_api');
    }

    public function doanhthuAction() {
        $products = array(
            'haynhucnhoi' => 0,
            'vitaminl' => 0,
            'vechai' => 0,
            'thanhniengame' => 0,
            'kulnews' => 0,
            'gamenoob' => 0,
        );

        echo Zend_Json::encode($products);
        exit;
    }

    public function indexAction()
    {
        $this->_redirect(BASE_URL);
    }

    public function vechaiAction()
    {
        $id = (int)$this->_getParam('id', 0);
        if (!in_array($id, array(1, 2))) {
            $this->_redirect(BASE_URL);
        }
        if (!empty($id) && is_numeric($id)) {
            if ($id == 1) {
                $posts = Post::getList(
                    array('phase' => Post::_PHASE_HOT,
                          'is_actived' => 1,
                          'is_approved' => 1,
                          'is_photo' => 0,
                          'from_date' => time() - 86400 * 720),
                    0, 50, array('order_by' => 'post_id desc'), true, 1800, 'vechai_' . $id, 'vechai_iframe_' . $id);
                shuffle($posts);
                $posts = array_slice($posts, 0, self::LIMIT);
                $this->view->posts = $posts;
            } else {
//				$categoryList = Category::selectCategoryList();
                $categoryList = Category::selectCategoryList(array('exclude_empty' => false, 'is_hidden = 0', 'category_ids' => array(1, 13, 3, 4, 8, 17, 14)), true, 'vechai_category_list_v2');
                shuffle($categoryList);
                $category_id = $categoryList[0]['category_id'];
                $posts = Post::getList(
                    array('category_id' => $category_id,
                          'is_actived' => 1,
                          'is_approved' => 1,
                          'is_photo' => 0,
                          'from_date' => time() - 86400 * 7), 0, 50,
                    array('order_by' => 'post_id desc'), true, 1800, false, 'vechai_iframe_category_' . $category_id);
                shuffle($posts);
                $posts = array_slice($posts, 0, self::LIMIT_POST_IFRAME_2);
                $this->view->posts = $posts;
                $this->view->category = $categoryList[0];
            }
        }
        $this->view->pos = $id;
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('layout_vechai');
    }

    public function kulAction()
    {

        $return = array();

        $categoryList = Category::selectCategoryList(array('exclude_empty' => true, 'is_hidden = 0', 'category_ids' => array(1, 16, 3, 14)), true, 'kul_category_list_v3');

        if (empty($categoryList)) {

            $return['status'] = false;

        } else {

            $return['status'] = true;

            foreach ($categoryList as $category) {

                $categoryData = array();

                switch ($category['category_id']) {

                    case '1':
                        $categoryData['name'] = 'Video bàn thắng bóng đá';
                        break;
                    case '16':
                        $categoryData['name'] = 'Clip hot Facebook';
                        break;
                    case '3':
                        $categoryData['name'] = 'Hài Hoài Linh, Trấn Thành, Trường Giang';
                        break;
                    case '14':
                        $categoryData['name'] = 'Clip hot LMHT, LOL, Liên Minh Huyền Thoại';
                        break;

                }
                $categoryData['name'] = $category['category_customize'];
                $categoryData['link'] = BASE_URL . Category::categoryUrl($category);

                $categoryPost = Post::getList(array(
                    'category_id' => $category['category_id'],
                    'is_actived' => 1,
                    'is_approved' => 1,
                    'is_photo' => 0
                ), 0, 5, array('order_by' => 'post_id'), true, 3600, false, 'kul_api_v2_cat_' . $category['category_id']);

                foreach ($categoryPost as $post) {

                    $postData['title'] = htmlspecialchars($post['story']);
                    $postData['link'] = Post::postUrl($post, true, true) . '?utm_source=kul&utm_medium=menu_bar&utm_campaign=haynhucnhoi_promotion';
                    $postData['play'] = Post::postUrl($post, true, true) . '?utm_source=kul&utm_medium=menu_bar&utm_campaign=haynhucnhoi_promotion';

                    if ($post['is_photo']) {
                        $postData['image'] = My_Zend_Globals::getThumbImage($post['picture']);
                    } else {
                        $postData['image'] = My_Zend_Globals::getThumbImageVideo($post['picture']);
                    }

                    $categoryData['rows'][] = $postData;

                }

                $return['rows'][] = $categoryData;
            }


        }

        echo 'var jsonResponse = ' . Zend_Json::encode($return);
        exit;
    }

    public function formatPost($post)
    {

        $postData['id'] = $post['post_id'];
        $postData['name'] = htmlspecialchars($post['story']);
        $postData['url'] = Post::postUrl($post, true, true);
        $postData['createById'] = $post['user_id'];
        $postData['createByName'] = User::getUser($post['user_id'], false)['fullname'];
        $postData['createTime'] = $post['created_at'];
        $postData['likes'] = $post['number_of_like'];
        $postData['comments'] = $post['number_of_comment'];
        $postData['views'] = $post['number_of_view'];
        if ($post['is_photo'])
            $postData['photo'] = My_Zend_Globals::getThumbImage($post['picture']);
        else
            $postData['photo'] = My_Zend_Globals::getThumbImageVideo($post['picture']);
        if (!empty($post['picture_share']))
            $postData['photo_share'] = My_Zend_Globals::getThumbImage($post['picture_share']);
        if (!empty($post['picture_upload']))
            $postData['photo_upload'] = My_Zend_Globals::getThumbImage($post['picture_upload']);

        if (!empty($post['youtube_key']))
            $postData['video_youtube'] = 'http://www.youtube.com/embed/' . $post['youtube_key'];
        if (!empty($post['vmo_key']))
            $postData['video_vimeo'] = 'player.vimeo.com/video/' . $post['vmo_key'];
        if (!empty($post['zingtv_key']))
            $postData['video_zingtv'] = 'http://tv.zing.vn/embed/video/' . $post['zingtv_key'];
        if (!empty($post['nct_mp3']))
            $postData['video_nct_mp3'] = 'http://www.nhaccuatui.com/m/' . $post['nct_mp3'];
        if (!empty($post['nct_video']))
            $postData['video_nct_key'] = 'http://www.nhaccuatui.com/video/xem-clip/' . $post['nct_video'];
        if (!empty($post['mecloud_key']))
            $postData['video_mecloud'] = 'http://embed.mecloud.vn/play/' . $post['mecloud_key'];

        return $postData;
    }

    public function formatUser($user)
    {

        $userDetail['id'] = $user['user_id'];
        $userDetail['facebookID'] = $user['facebookid'];
        $userDetail['name'] = $user['fullname'];
        $userDetail['email'] = $user['email'];
        $userDetail['avatar'] = "https://graph.facebook.com/" . $user['facebookid'] . "/picture?height=100&type=normal&width=100";
        $userDetail['gender'] = $user['gender'];
        $userDetail['birthDate'] = $user['birthday'];
        $userDetail['description'] = $user['description'];
        $userDetail['location'] = $user['country'];
        $userDetail['createTime'] = $user['addtime'];
        $userDetail['point'] = $user['like_point'];
        $userDetail['rank'] = $user['rank'];
        $userDetail['website'] = $user['website'];
        $userDetail['postedCount'] = Post::countTotal(array('user_id' => $user['user_id'], 'is_actived' => 1));
        $userDetail['likeCount'] = $user['like_count'];
        $userDetail['commentCount'] = $user['comment_count'];
        $userDetail['watchedCount'] = $user['yourviewed'];
        $userDetail['followByCount'] = $user['followed_number'];
        $userDetail['followedCount'] = $user['follow_number'];
        $userDetail['isFollowed'] = 1;
        $userDetail['isImageOn'] = 1;
        if ($user['like_point'] > 500)
            $userDetail['isClipOn'] = 1;
        else
            $userDetail['isClipOn'] = 0;

        return $userDetail;
    }

    private static function createToken($userID, $time)
    {
        $secretKey = self::API_SECRET_KEY;
        $token = hash('md5', $secretKey . $userID . $time);
        return $token;
    }

    private function checkToken($tokenReceived, $userID = '', $timeReceived = '')
    {
        if (strlen($tokenReceived) > 64 && empty($userID) && empty($timeReceived)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::FACEBOOK_GRAPH_URL . '?access_token=' . $tokenReceived);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CERTINFO, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            if (!$facebookResult = curl_exec($ch)) {
                trigger_error(curl_error($ch));
            }
//            $facebookResult = curl_exec($ch);
            $facebookResult = json_decode($facebookResult, true);
            if (!empty($facebookResult['error']))
                return false;
            else
                return true;

        } else {
            if (time() - $timeReceived > self::EXPIRED_TIME) {
                return false;
            } else {
                $validToken = self::createToken($userID, $timeReceived);
                if ($validToken != $tokenReceived)
                    return false;
                else
                    return true;
            }
        }
    }

    public function loginFacebookAction()
    {
        $access_token = $this->_getParam('token', null);
        $result = array();
        if (empty($access_token)) {
            $result['error_code'] = 1;
            $result['error_msg'][] = 'Access token invalid';
        } else {
            if (!self::checkToken($access_token)) {
                $result['error_code'] = 1;
                $result['error_msg'][] = 'Wrong token';
            } else {
                $user = User::getUserFacebook($access_token);
                if (empty($user)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'User not exists';
                } else {
                    $birthday = '0000-00-00';
                    if (!empty($user['birthday'])) {
                        $birthday = explode('/', $user['birthday']);
                        $birthday = $birthday[2] . '-' . $birthday[0] . '-' . $birthday[1];
                    }

                    $arrData = array(
                        'username' => NULL,
                        'fullname' => $user['name'],
                        'email' => $user['email'],
                        'gender' => isset($user['gender']) ? $user['gender'] : '',
                        'birthday' => $birthday,
                        'is_verified' => 1,
                        'status' => 1,
                        'addtime' => time(),
                        'ip' => My_Zend_Globals::getAltIp(),
                        'facebookid' => $user['id'],
                    );
                    $facebookid = User::getUserBySocialId($arrData['facebookid']);
                    $userInfo = User::getUserByEmail($arrData['email']);

                    if (empty($facebookid['facebookid'])) {

                        if (!empty($userInfo)) {
                            $update = array(
                                'user_id' => $userInfo['user_id'],
                                'facebookid' => $arrData['facebookid']
                            );
                            User::updateUser($update);
                            User::setlogin($userInfo['user_id'], $arrData['email']);

                            $getUser = User::getUserBySocialId($arrData['facebookid']);
                            if (empty($getUser)) {
                                $result['error_code'] = 1;
                            } else {
                                $result['error_code'] = 0;
                                $userData = self::formatUser($getUser);
                                $result['sessionToken'] = $access_token;
                                $result['User'][] = $userData;
                            }

                        } else if ($userId = User::insertUser($arrData)) {
                            $data['user_id'] = $userId;
                            User::setlogin($userId, $arrData['email']);

                            $getUser = User::getUserBySocialId($arrData['facebookid']);
                            if (empty($getUser)) {
                                $result['error_code'] = 1;
                            } else {
                                $result['error_code'] = 0;
                                $userData = self::formatUser($getUser);
                                $result['sessionToken'] = $access_token;
                                $result['User'][] = $userData;
                            }
                        }
                    } else {
                        $update = array(
                            'user_id' => $facebookid['user_id'],
                        );
                        User::updateUser($update);
                        User::setlogin($userInfo['user_id'], $arrData['email']);

                        $getUser = User::getUserBySocialId($arrData['facebookid']);
                        if (empty($getUser)) {
                            $result['error_code'] = 1;
                        } else {
                            $result['error_code'] = 0;
                            $userData = self::formatUser($getUser);
                            $result['sessionToken'] = $access_token;
                            $result['User'][] = $userData;
                        }
                    }
                }
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function loginEmailAction()
    {
        $email = $this->_getParam('email', null);
        $password = $this->_getParam('password', null);
        if (empty($email) || empty($password)) {
            $result['error_code'] = 1;
            $result['User'][] = 'Email or Password invalid';
        }

        $login = User::login($email, $password);
        if ($login) {
            $result['error_code'] = 0;
            $getUser = User::getUserByEmail($email);
            $userData = self::formatUser($getUser);
            $token = self::createToken($getUser['user_id'], time());

            $result['sessionToken'] = $token;
            $result['sessionTime'] = time();
            $result['User'][] = $userData;
        } else {
            $result['error_code'] = 1;
            $result['User'][] = 'Account not exists';
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function logoutAction()
    {
        $userID = $this->_getParam('id', null);
        $sessionToken = $this->_getParam('token', null);
        $expiredTime = $this->_getParam('time', null);

        if (strlen($sessionToken) > 64) {
            $auth = Zend_Auth::getInstance();
            $auth->setStorage(new Zend_Auth_Storage_Session('Default'));
            $auth->clearIdentity();
            $result['error_code'] = 0;
            $result['User'] = 'Logout success';
        } else {
            $checkToken = self::checkToken($sessionToken, $userID, $expiredTime);
            if ($checkToken) {
                $result['error_code'] = 0;
                $result['User'] = 'Logout success';
            } else {
                $result['error_code'] = 1;
                $result['User'] = 'Wrong token or Token was expired';
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function updateProfileAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');

        if (empty($userID) || empty($sessionToken) || empty($expiredTime)) {
            $result['error_code'] = 1;
            $result['User'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }
        $userInfo = User::getUser($userID);

        $name = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($this->_getParam('name', $userInfo['fullname'])), ''));
        $gender = My_Zend_Globals::strip_word_html(trim($this->_getParam('gender', '')), '');
        $birthDate = $this->_getParam('birthDate', 0);
        $description = My_Zend_Globals::strip_word_html(trim($this->_getParam('description', '')), '');
        $location = My_Zend_Globals::strip_word_html(trim($this->_getParam('location', '')), '');
        $website = My_Zend_Globals::strip_word_html(trim($this->_getParam('website', '')), '');
        if ($birthDate != 0) {
            $_birthDate = explode('/', $birthDate);
            $birthDay = $_birthDate[0];
            $birthMonth = $_birthDate[1];
            $birthYear = $_birthDate[2];
        } else {
            $birthDay = 00;
            $birthMonth = 00;
            $birthYear = 0000;
        }

        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            $result['error_code'] = 0;
            $data = array(
                'user_id' => $userID,
                'fullname' => $name,
                'website' => $website,
                'gender' => $gender,
                'location' => $location,
                'description' => $description,
                'birthday' => $birthYear . '-' . $birthMonth . '-' . $birthDay
            );
            User::updateUser($data);
            $userInfo = self::formatUser(array_merge($userInfo, $data));
            $result['User'][] = $userInfo;
        } else {
            $result['error_code'] = 1;
            $result['User'] = 'Wrong token or Token was expired';
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function changePasswordAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');
        $newPassword = $this->_getParam('newpassword', '');

        if (empty($userID) || empty($sessionToken) || empty($expiredTime) || empty($newPassword)) {
            $result['error_code'] = 1;
            $result['User'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }
        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            $data['user_id'] = $userID;
            $data['salt'] = User::createUserSalt();
            $data['password'] = User::hashPassword($newPassword, $data['salt']);
            if (User::updateUser($data))
                $result['error_code'] = 0;
            else
                $result['error_code'] = 1;
        } else {
            $result['error_code'] = 1;
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function hayAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $filters = array('is_actived' => 1, 'is_approved' => 1);
        $order_by = 'updated_at DESC';
        $filters['phase'] = Post::_PHASE_HOMEPAGE;

        $result = array();
        $posts = Post::getList($filters, $offset, $limit, array('order_by' => $order_by), true, 600, false, 'hnn_api_hay_' . $offset . $limit);

        if (empty($posts))
            $result['error_code'] = 1;
        else {
            $result['error_code'] = 0;
            foreach ($posts as $post) {
                $postData = self::formatPost($post);
                $result['List'][] = $postData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function chuaxemAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $filters = array('is_actived' => 1, 'is_approved' => 1);
        $order_by = 'updated_at DESC';
        $result = array();

        if (!LOGIN_UID > 0) {
            $result['error_code'] = 1;
            echo Zend_Json::encode($result);
            exit;
        }

        if (LOGIN_UID > 0) {
            $userId = LOGIN_UID;
            $lsViewPost = User::getUserViewPost($userId);
            if (!empty($lsViewPost)) {
                $lv = array();
                foreach ($lsViewPost as $key => $value) {
                    $lv[] = $value['post_id'];
                }
                $lv = implode(',', $lv);
                $filters['no_view'] = $lv;
            }
            $posts = Post::getList($filters, $offset, $limit, array('order_by' => $order_by), true, 600, false, 'hnn_api_chuaxem_' . $offset . $limit);

            if (empty($posts))
                $result['error_code'] = 1;
            else {
                $result['error_code'] = 0;
                foreach ($posts as $post) {
                    $postData = self::formatPost($post);
                    $result['List'][] = $postData;
                }
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function nhucnhoiAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $filters = array('is_actived' => 1, 'is_approved' => 1);
        $order_by = 'updated_at DESC';
        $filters['phase'] = Post::_PHASE_HOT;

        $result = array();
        $posts = Post::getList($filters, $offset, $limit, array('order_by' => $order_by), true, 600, false, 'hnn_api_nhucnhoi_' . $offset . $limit);

        if (empty($posts))
            $result['error_code'] = 1;
        else {
            $result['error_code'] = 0;
            foreach ($posts as $post) {
                $postData = self::formatPost($post);
                $result['List'][] = $postData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function videoAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $filters = array('is_actived' => 1, 'is_approved' => 1);
        $filters['phase'] = array('0', '1');
        $filters['is_photo'] = 0;
        $order_by = 'updated_at DESC';

        $result = array();
        $posts = Post::getList($filters, $offset, $limit, array('order_by' => $order_by), false, 600, false, 'hnn_api_video_' . $offset . $limit);

        if (empty($posts))
            $result['error_code'] = 1;
        else {
            $result['error_code'] = 0;
            foreach ($posts as $post) {
                $postData = self::formatPost($post);
                $result['List'][] = $postData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function categoriesAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $categories = Category::selectCategoryList(array('parent_id' => 0, 'offset' => $offset, 'limit' => $limit), true, 'hnn_api_categories_' . $offset . $limit);
        $result = array();

        if (empty($categories))
            $result['error_code'] = 1;
        else {
            $result['error_code'] = 0;
            foreach ($categories as $category) {
                $categoryData['id'] = $category['category_id'];
                $categoryData['name'] = $category['category_name'];
                $result['List'][] = $categoryData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function getbycategoryAction()
    {

        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);
        $categoryID = $this->_getParam('id', 1);
        $filters = array('category_id' => $categoryID);
        $order_by = 'updated_at DESC';
        $posts = Post::getList($filters, $offset, $limit, array('order_by' => $order_by), true, 600, false, 'hnn_api_getByCategory_' . $offset . $limit . $categoryID);
        $result = array();

        if (empty($posts))
            $result['error_code'] = 1;
        else {
            $result['error_code'] = 0;
            foreach ($posts as $post) {
                $postData = self::formatPost($post);
                $result['List'][] = $postData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function userDetailAction()
    {
        $userID = $this->_getParam('id', 1);
        $profile = User::getUser($userID);

        if (empty($profile)) {
            $result['error_code'] = 1;
            $result['error_msg'] = 'Wrong user ID';
        }
        else {
            $userData = self::formatUser($profile);
            $result['error_code'] = 0;
            $result['User'] = $userData;
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function userPostedAction()
    {
        $userID = $this->_getParam('id', 1);
        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 10);
        $posts = Post::getList(array('user_id' => $userID, 'is_actived' => 1), $offset, $limit, array('order_by' => 'post_id desc', 'get_views' => false), true, 300);
        if (empty($posts)) {
            $result['error_code'] = 1;
            $result['error_msg'] = 'This user has not post';
        }
        else {
            $result['error_code'] = 0;
            foreach ($posts as $post) {
                $postData = self::formatPost($post);
                $result['List'][] = $postData;
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function userFollowedAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');
        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);

        if (empty($userID) || empty($sessionToken) || empty($expiredTime)) {
            $result['error_code'] = 1;
            $result['List'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }

        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            $result['error_code'] = 0;
            $listFollowed = Follow::getList(array('follower_id' => $userID), $offset, $limit);

            foreach ($listFollowed as $user) {
                $profile = User::getUser($user['user_id']);
                $userData = self::formatUser($profile);
                $result['List'][] = $userData;
            }
        } else {
            $result['error_code'] = 1;
            $result['List'][] = 'Wrong token or Token was expired';
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function userFollowerAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');
        $offset = $this->_getParam('offset', 0);
        $limit = $this->_getParam('limit', 1);

        if (empty($userID) || empty($sessionToken) || empty($expiredTime)) {
            $result['error_code'] = 1;
            $result['List'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }

        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            $result['error_code'] = 0;
            $listFollowed = Follow::getList(array('followed_id' => $userID), $offset, $limit);

            foreach ($listFollowed as $user) {
                $profile = User::getUser($user['user_id']);
                $userData = self::formatUser($profile);
                $result['List'][] = $userData;
            }
        } else {
            $result['error_code'] = 1;
            $result['List'][] = 'Wrong token or Token was expired';
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function userFollowAction()
    {
        $followerID = $this->_getParam('follower', '');
        $followedID = $this->_getParam('followed', '');
        $result = array();

        if (empty($followerID) || empty($followedID)) {
            $result['error_code'] = 1;
            $result['Follow'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }

        $user_follower = User::getUser($followerID);
        $user_followed = User::getUser($followedID);
        $check_follow = Follow::getDetailByUserIdAndFollowId($followerID, $followedID);

        if ($check_follow) {
            $result['error_code'] = 0;
            $result['Follow'] = 'Success';
            $following_id = isset($check_follow['following_id']) ? $check_follow['following_id'] : 0;
            $arrMSG['showhide'] = 1;
            Follow::delete($following_id);

            $follow_number = isset($user_follower['follow_number']) ? $user_follower['follow_number'] : 0;
            $followed_number = isset($user_followed['followed_number']) ? $user_followed['followed_number'] : 0;
            if ($follow_number > 0) {
                $follow_number = $user_follower['follow_number'] - 1;
            }

            if ($followed_number > 0) {
                $followed_number = $user_followed['followed_number'] - 1;
            }

            $data = array('user_id' => $followerID, 'follow_number' => $follow_number);
            $data_follow = array('user_id' => $followedID, 'followed_number' => $followed_number);
            User::updateUser($data);
            User::updateUser($data_follow);

        } else {
            if ($followerID == $followedID) {
                $result['error_code'] = 1;
                $result['Follow'] = 'Not user';
            } elseif (!empty($user_follow)) {
                $data = array(
                    'follower_id' => $followerID,
                    'followed_id' => $followedID,
                    'created_time' => date('Y-m-d H:i:s'));

                $insertFollow = Follow::insert($data);
                if ($insertFollow) {
                    $arrUser = array('user_id' => $followerID, 'follow_number' => $user_follower['follow_number'] + 1);
                    $arrUserFollow = array('user_id' => $followedID, 'followed_number' => $user_followed['followed_number'] + 1);
                    User::updateUser($arrUser);
                    User::updateUser($arrUserFollow);
                    $arrNotice = array(
                        'userID' => $followedID,
                        'type' => Notification::_NOTIFICATION_TYPE_FOLLOWING,
                        'time' => date('Y-m-d H:i:s'),
                        'isActive' => 1,
                        'status' => 'active',
                        'followingID' => $followerID,
                        'message' => ''
                    );
                    Notification::insert($arrNotice);
                    $result['error_code'] = 0;
                    $result['Follow'] = 'Success';
                }
            } else {
                $result['error_code'] = 1;
                $result['Follow'] = 'Not user';
            }
        }
        echo Zend_Json::encode($result);
        exit;
    }

    public function postImageAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');

        $file = $this->_getParam('file', '');
        $title = $this->_getParam('title', '');
        $isSelfMade = $this->_getParam('ismine', '');
        $isNSFW = $this->_getParam('isnsfw', '');
        $source = $this->_getParam('source', '');
        $result = array();

        if (empty($userID) || empty($sessionToken) || empty($expiredTime) || empty($file) || empty($title) || empty($isSelfMade) || empty($isNSFW) || empty($source)) {
            $result['error_code'] = 1;
            $result['User'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }

        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            $file = isset($file) ? $file : null;
            $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($title), ''));
            $isSelfMade = isset($isSelfMade) ? (int)$isSelfMade : 0;
            $isNSFW = isset($isNSFW) ? (int)$isNSFW : 0;
            $source = isset($source) ? $source : '';
            $source = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($source), ''));

            $data = $this->_getAllParams();

            if (!My_Zend_Globals::is_image($file['tmp_name'])) {
                $result['error_code'] = 1;
                $result['error_msg'] = 'Error with file';
                echo Zend_Json::encode($result);
                exit;
            }

            $maxsize = round($file["size"] / 1024 / 1024);
            if ($maxsize > 4) {
                $result['error_code'] = 1;
                $result['error_msg'] = 'Image size not more than 4MB';
                echo Zend_Json::encode($result);
                exit;
            }

            $upload = new Upload();
            $destFolder = array('photo', date('Y'), date('m'));
            $rsUpload = $upload->upload($file, $destFolder);
            $rsUpload = $rsUpload[0];
            if (!isset($rsUpload['url']) || empty($rsUpload['url'])) {
                $result['error_code'] = 1;
                $result['error_msg'] = 'Server were busy';
                echo Zend_Json::encode($result);
                exit;
            }

            if ($isSelfMade) {
                $source = 'Tự làm';
            }

            $isGif = $rsUpload['is_animated'] ? 1 : 0;
            $insertData = array(
                'user_id' => LOGIN_UID,
                'story' => $title,
                'source' => $source,
                'picture' => $rsUpload['url'],
                'picture_share' => $rsUpload['share_image'],
                'created_at' => time(),
                'updated_at' => time(),
                'is_approved' => 0,
                'is_photo' => 1,
                'is_gif' => $isGif,
                'is_actived' => 1,
                'tags' => '',
                'nsfw' => $isNSFW,
                'category_id' => 0,
                'pip' => My_Zend_Globals::getAltIp()
            );

            $userInfo = User::getUser($userID, false);
            if ($userInfo['approved_new_post']) {
                $insertData['is_approved'] = 1;
                $insertData['approved_at'] = time();
                $insertData['category_id'] = isset($data['category_id']) ? (int)$data['category_id'] : 0;
            }

            if ($userInfo['new_post_to_home']) {
                $insertData['phase'] = Post::_PHASE_HOMEPAGE;
            }
            $postId = Post::insert($insertData);

            if ($userInfo['approved_new_post']) {

            }

            if ($postId > 0) {
                $params['tag_id'] = isset($params['tag_id']) ? $params['tag_id'] : array();
                if (!empty($params['tag_id'])) {
                    $tagIds = $data['tag_id'];
                    foreach ($tagIds as $tagId) {
                        Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                    }
                }

                // redirect to photo
                $this->_redirect(Post::postUrl(Post::getPost($postId)));
            } else {
                $result['error_code'] = 1;
                $result['error_msg'] = 'Server were busy';
                echo Zend_Json::encode($result);
                exit;
            }

        } else {
            $result['error_code'] = 1;
            $result['error_msg'] = 'Wrong token';
        }

        echo Zend_Json::encode($result);
        exit;
    }

    public function postVideoAction()
    {
        $userID = $this->_getParam('id', '');
        $sessionToken = $this->_getParam('token', '');
        $expiredTime = $this->_getParam('time', '');

        $videoUrl = $this->_getParam('url', '');
        $title = $this->_getParam('title', '');
        $isNSFW = $this->_getParam('isnsfw', '');
        $description = $this->_getParam('description', '');
        $result = array();

        if (empty($userID) || empty($sessionToken) || empty($expiredTime)) {
            $result['error_code'] = 1;
            $result['error_msg'][] = 'Missing data';
            echo Zend_Json::encode($result);
            exit;
        }

        if (self::checkToken($sessionToken, $userID, $expiredTime)) {
            // check login user info
            $userInfo = User::getUser($userID);
            if (empty($userInfo)) {
                $result['error_code'] = 1;
                $result['error_msg'][] = 'ID not exist';
                echo Zend_Json::encode($result);
                exit;
            }

            if (isset($userInfo['like_point']) && $userInfo['all_point'] > $userInfo['like_point'])
                $userPoint = $userInfo['all_point'];
            else
                $userPoint = $userInfo['like_point'];

            $point = !empty($userPoint) ? $userPoint : 0;
            $userPostCountToday = User::countPostToday($userInfo['user_id']);
            $limit = Upload::getUploadLimit($point);

            if (!in_array($userInfo['user_id'], explode(',', EXCLUDED_USER_ID_POINT))) {

                if ($userPostCountToday['total'] >= $limit['total_limit']) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Has enough post';
                    echo Zend_Json::encode($result);
                    exit;
                } elseif ($userPostCountToday['video'] >= $limit['video_limit']) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Has enough video';
                    echo Zend_Json::encode($result);
                    exit;
                }
            }

            $params = $this->_getAllParams();
            $title = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($title), ''));

            $videoUrl = trim($videoUrl);
            $videoType = My_Zend_Media::getMediaType($videoUrl);

            if ($videoType == '') {
                $result['error_code'] = 1;
                $result['error_msg'][] = 'This link is not support';
                echo Zend_Json::encode($result);
                exit;
            }
            $isNSFW = isset ($isNSFW) ? (int)$isNSFW : 0;

            $insertData = array(
                'user_id' => LOGIN_UID,
                'story' => $title,
                'created_at' => time(),
                'updated_at' => time(),
                'is_approved' => 0,
                'is_photo' => 0,
                'is_gif' => 0,
                'is_actived' => 1,
                'tags' => '',
                'source' => 'Sưu tầm',
                'pip' => My_Zend_Globals::getAltIp(),
                'category_id' => 0,
                'nsfw' => $isNSFW
            );

            if (isset($description)) {
                $insertData['description'] = $description;
            }

            if ($userInfo['approved_new_post'] || Admin::isLogined()) {
                $insertData['is_approved'] = 1;
                $insertData['approved_at'] = time();
                $insertData['category_id'] = isset($params['category_id']) ? (int)$params['category_id'] : 0;
            }

            if ($userInfo['new_post_to_home']) {
                $insertData['phase'] = Post::_PHASE_HOMEPAGE;
            }

            if ($videoType == My_Zend_Media::_MEDIA_TYPE_YOUTUBE) {

                $youtubeInfo = My_Zend_Media::retrieveYouTubeInfo($videoUrl);
                if (empty($youtubeInfo)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Wrong url';
                    echo Zend_Json::encode($result);
                    exit;
                }
                $insertData['youtube_key'] = $youtubeInfo['id'];
                $insertData['picture'] = isset($youtubeInfo['thumb']['maxres']) ? $youtubeInfo['thumb']['maxres'] : isset($youtubeInfo['thumb']['standard']) ? $youtubeInfo['thumb']['standard'] : $youtubeInfo['thumb']['high'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_VIMEO) {

                $pos = strpos($videoUrl, 'channels');
                if ($pos !== false) { // tìm thấy
                    $vimeo_id = My_Zend_Media::getVimeoIDFromURL($videoUrl);
                    $videoUrl = 'https://vimeo.com/' . $vimeo_id;
                }
                $insertData['vmo_key'] = My_Zend_Media::parseVimeo($videoUrl);
                $insertData['picture'] = My_Zend_Media::getVimeoThumbnail($insertData['vmo_key']);

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_ZINGTV) {

                $videoInfo = My_Zend_Media::retrieveZingTVInfo($videoUrl);
                if (empty($videoInfo)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Wrong url';
                    echo Zend_Json::encode($result);
                    exit;
                }
                $insertData['zingtv_key'] = $videoInfo['id'];
                $insertData['picture'] = $videoInfo['thumb'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_NCT_MP3) {

                $videoInfo = My_Zend_Media::getSongNhaccuatui($videoUrl);
                if (empty($videoInfo)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Wrong url';
                    echo Zend_Json::encode($result);
                    exit;
                }
                $insertData['nct_mp3'] = $videoInfo['key'];
                $insertData['picture'] = $videoInfo['imageURL'];
                $insertData['url'] = $videoInfo['url'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_NCT_VIDEO) {

                $videoInfo = My_Zend_Media::getVideoNhaccuatui($videoUrl);
                if (empty($videoInfo)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Wrong url';
                    echo Zend_Json::encode($result);
                    exit;
                }
                $insertData['nct_video'] = $videoInfo['key'];
                $insertData['picture'] = $videoInfo['imageURL'];
                $insertData['url'] = $videoInfo['url'];

            } elseif ($videoType == My_Zend_Media::_MEDIA_TYPE_MECLOUD) {
                $videoId = My_Zend_Media::getMeCloudIDFromUrl($videoUrl);
                $videoInfo = My_Zend_Media::retrieveMeCloudVideoInfo($videoId);
                if (empty($videoInfo)) {
                    $result['error_code'] = 1;
                    $result['error_msg'][] = 'Wrong url';
                    echo Zend_Json::encode($result);
                    exit;
                }
                $insertData['mecloud_key'] = $videoInfo['video']['aliasId'];
                $insertData['picture'] = $videoInfo['video']['thumbnails']['maxres'];
                $insertData['url'] = $videoInfo['video']['embed']['script'];
            }

            // get share data
            if ($sharePic = Post::generateShareImage($insertData['picture'])) {
                $insertData['picture_share'] = $sharePic;
            }

            $postId = Post::insert($insertData);
            if ($userInfo['approved_new_post']) {

            }

            if ($postId > 0) {
                $params['tag_id'] = isset($params['tag_id']) ? $params['tag_id'] : array();
                if (!empty($params['tag_id'])) {
                    $tagIds = $params['tag_id'];
                    foreach ($tagIds as $tagId) {
                        Post::insertPostTag(array('post_id' => $postId, 'tag_id' => $tagId));
                    }
                }

                $this->_redirect(Post::postUrl(Post::getPost($postId)));
            } else {
                $result['error_code'] = 1;
                $result['error_msg'][] = 'Server were busy';
                echo Zend_Json::encode($result);
                exit;
            }
        } else {
            $result['error_code'] = 1;
            $result['error_msg'] = 'Wrong token';
        }
        echo Zend_Json::encode($result);
        exit;
    }
}

