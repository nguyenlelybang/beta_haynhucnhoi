<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
    }

    public function loginAction()
    {
        $redirectUrl = $this->_getParam('url', '');

        if (LOGIN_UID > 0) {
            $this->_redirect(BASE_URL);
        }

        if ($this->getRequest()->isPost()) {
            $accessToken = $this->getRequest()->getPost('accessToken', null);
            $url = $this->getRequest()->getPost('url', null);
            $url = isset($url) ? $url : BASE_URL;
            $user = User::getUserFacebook($accessToken);

            if (!empty($user)) {
                $birthday = '0000-00-00';
                if (!empty($user['birthday'])) {
                    $birthday = explode('/', $user['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[0] . '-' . $birthday[1];
                }

                $arrData = array(
                    'username' => NULL,
                    'fullname' => $user['name'],
                    'email' => $user['email'],
                    'gender' => isset($user['gender']) ? $user['gender'] : '',
                    'birthday' => $birthday,
                    'is_verified' => 1,
                    'status' => 1,
                    'addtime' => time(),
                    'ip' => My_Zend_Globals::getAltIp(),
                    'facebookid' => $user['id'],
                );

                $userByFacebook = User::getUserBySocialId($arrData['facebookid']);
                if (empty($userByFacebook['facebookid'])) {
                    $userInfo = User::getUserByEmail($arrData['email']);
                    if (!empty($userInfo)) {
                        $update = array(
                            'user_id' => $userInfo['user_id'],
                            'facebookid' => $arrData['facebookid']
                        );
                        User::updateUser($update);
                        User::setlogin($userInfo['user_id'], $arrData['email']);

                        $this->_redirect($url);
                    } else if ($userId = User::insertUser($arrData)) {
                        $data['user_id'] = $userId;
                        User::setlogin($userId, $arrData['email']);

                        $this->_redirect($url);

                    }
                } else {
                    $update = array(
                        'user_id' => $userByFacebook['user_id'],
                    );
                    User::updateUser($update);
                    User::setlogin($userByFacebook['user_id'], $arrData['email']);
                    $this->_redirect($url);
                }
            }
        }

        $this->view->redirectUrl = $redirectUrl;
    }

    public function loginPasswordAction()
    {

        $request = $this->getRequest();
        $redirectUrl = '';
        if (LOGIN_UID > 0) {
            $this->_redirect(BASE_URL);
        }

        if ($request->isPost()) {
            $email = trim($request->getParam('email', ''));
            $password = trim($request->getParam('password', ''));
            $redirectUrl = $this->_getParam('url', '');
            $login = User::login($email, $password);
            if ($login) {
                $this->_redirect($redirectUrl);
            } else {
                $this->_helper->flashMessenger->addMessage('Email hoặc mật khẩu không chính xác. Vui lòng thử lại.');
                $this->_redirect(BASE_URL . '/user/login-password');
            }
        }

        $this->view->redirectUrl = $redirectUrl;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function getlogininfoAction()
    {
        header("Access-Control-Allow-Origin: *");
        $arrResult = array('uname' => '');
        if (LOGIN_UID > 0) {
            $arrResult['uid'] = LOGIN_UID;
            $arrResult['uname'] = LOGIN_DISPLAYNAME;
        }
        echo Zend_Json::encode($arrResult);
        exit;
    }

    public function registerAction()
    {
        $params = $this->_getAllParams();
        if ($this->getRequest()->isPost()) {
            $params = $this->_request->getPost();
            $displayName = isset($params['display_name']) ? $params['display_name'] : '';
            $password = isset($params['password']) ? $params['password'] : '';
            $passwordconfirm = isset($params['passwordconfirm']) ? $params['passwordconfirm'] : '';
            $dd = isset($params['dd']) ? $params['dd'] : 0;
            $mm = isset($params['mm']) ? $params['mm'] : 0;
            $yyyy = isset($params['yyyy']) ? $params['yyyy'] : 0;
            $gender = isset($params['gender']) ? $params['gender'] : 0;
            $relationshipStatus = isset($params['relationship_status']) ? $params['relationship_status'] : 0;
            $userId = isset($params['user_id']) ? $params['user_id'] : 0;
            $seckey = isset($params['seckey']) ? $params['seckey'] : '';
            $email = isset($params['email']) ? $params['email'] : '';
            $error = false;
            $msg = array();

            if ($seckey !== $this->genseckey($userId, $email)) {
                $error = true;
                $msg[] = 'Mã bảo mật không hợp lệ';
            } elseif (strlen($displayName) < 5) {
                $error = true;
                $msg[] = 'Tên hiển thị quá ngắn';
            } elseif (strlen($password) < 5 || $password !== $passwordconfirm) {
                $error = true;
                $msg[] = 'Mật khẩu quá ngắn hoặc 2 mật khẩu không khớp';
            }

            if (!$error) {
                $salt = User::createUserSalt();
                $password = User::hashPassword($password, $salt);
                $data = array(
                    'user_id' => $userId,
                    'display_name' => htmlspecialchars($displayName),
                    'password' => $password,
                    'salt' => $salt,
                    'gender' => $gender,
                    'birthday' => $birthday,
                    'last_update' => time(),
                    'relationship_status' => strtolower($relationshipStatus),
                    'rank' => User::_MAX_RANK
                );

                if (User::updateUser($data)) {
                    User::setlogin($userId, $password);
                    echo "<script>window.focus();window.close();</script>";
                    exit;
                }
            }
            $this->view->msg = $msg;
        }

        $this->view->params = $params;
        $this->view->noRightSide = true;
    }

    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('Default'));
        $auth->clearIdentity();
        User::deleteRememberMe();
        header("Location: " . BASE_URL);
        exit;
    }

    public function settingsAction()
    {
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/user/settings');
        }

        $userId = LOGIN_UID;
        $userInfo = User::getUser($userId);
        $arErrors = array('error' => 0, 'msg' => '');


        if (empty($userInfo)) {
            $this->_redirect(BASE_URL);
        }


        if ($this->getRequest()->isPost()) {

            $fullname = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($this->_getParam('fullname', '')), ''));
            $website = My_Zend_Globals::strip_word_html(trim($this->_getParam('website', '')), '');
            $gender = My_Zend_Globals::strip_word_html(trim($this->_getParam('gender', '')), '');
            $description = My_Zend_Globals::strip_word_html(trim($this->_getParam('description', '')), '');
            $birthDay = $this->_getParam('birth_day', 0);
            $birthMonth = $this->_getParam('birth_month', 0);
            $birthYear = $this->_getParam('birth_year', 0);
            $location = My_Zend_Globals::strip_word_html(trim($this->_getParam('location', '')), '');
            $avatar = isset($_FILES['avatar']) ? $_FILES['avatar'] : null;
            $newPass = My_Zend_Globals::strip_word_html($this->_getParam('new-password', ''), '');
            $confirmPass = My_Zend_Globals::strip_word_html($this->_getParam('verify-password', ''), '');

            if ($fullname == '') {
                $arErrors['error'] = 1;
                $arErrors['msg'] = 'Tên hiển không được để trống hoặc tên không chứa các dạng script.';
                $fullname = htmlspecialchars($this->_getParam('fullname', ''));
            }

            $data = array(
                'user_id' => $userId,
                'fullname' => $fullname,
                'website' => $website,
                'gender' => $gender,
                'location' => $location,
                'description' => $description,
                'birthday' => $birthYear . '-' . $birthMonth . '-' . $birthDay
            );

            if ($newPass != '') {
                if ($newPass !== $confirmPass) {
                    $arErrors['error'] = 1;
                    $arErrors['msg'] = 'Mật khẩu mới và nhập lại mật khẩu phải giống nhau!';
                }
                $data['salt'] = User::createUserSalt();
                $data['password'] = User::hashPassword($newPass, $data['salt']);
            }


            if ($arErrors['error'] == 0) {
                $upload = new Upload();
                if (!empty($avatar)) {
                    $rsUpload = $upload->upload($avatar, array('avatar'));
                    $rsUpload = $rsUpload[0];
                    if (isset($rsUpload['url']) && !empty($rsUpload['url'])) {
                        $data['profilepicture'] = $rsUpload['url'];
                    }
                }
                User::updateUser($data);
                $arErrors['error'] = 2;
                $arErrors['msg'] = 'Cập nhật thành công.';

            }
            $userInfo = array_merge($userInfo, $data);
        }

        $birthday = explode('-', $userInfo['birthday']);
        $year = $birthday[0] > 0 ? $birthday[0] : '';
        $month = $birthday[1] > 0 ? $birthday[1] : '';
        $day = $birthday[2] > 0 ? $birthday[2] : '';
        $this->view->birthday = array('year' => $year, 'month' => $month, 'day' => $day);
        $this->view->userInfo = $userInfo;
        $this->view->arrErrors = $arErrors;
        $this->view->currURL = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
    }

    public function ajaxsavesettingsAction()
    {
        $this->_helper->layout->disableLayout();
        if (LOGIN_UID == 0) {
            $this->_redirect(BASE_URL . '/user/login?url=' . BASE_URL . '/user/settings');
        }

        $userId = LOGIN_UID;
        $userInfo = User::getUser($userId);
        if (empty($userInfo)) {
            exit();
        }

        $enablePhoto = intval($this->_getParam('enablePhoto', 0));
        $enableMusic = intval($this->_getParam('enableMusic', 0));
        $enableClip = intval($this->_getParam('enableClip', 0));
        $settings = array(
            'photo' => $enablePhoto,
            'music' => $enableMusic,
            'clip' => $enableClip
        );
        if (!empty($userInfo['filter'])) {
            $settings = array_merge($userInfo['filter'], $settings);
        }

        $updateData = array(
            'user_id' => $userId,
            'filter' => serialize($settings)
        );

        if (User::updateUser($updateData)) {
            echo "OK";
            exit();
        }
        exit();
    }

    private function genseckey($id, $email)
    {
        return md5($id . $email . 'cmlm*733j^73600UY');
    }

    public function facebookAction()
    {
        $this->_helper->layout->disableLayout();
        $adapter = $this->_getFacebookAdapter();
        $adapter->authenticate();
    }

    protected function _getFacebookAdapter()
    {
        $scope = 'email,user_birthday,user_relationships,user_relationship_details,user_location';
        $redirecturi = BASE_URL . '/user/login';
        return new My_Auth_Adapter_Facebook(FACEBOOK_APPID, FACEBOOK_SECKEY, $redirecturi, $scope);
    }

}
