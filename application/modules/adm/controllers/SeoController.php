<?php

class Adm_SeoController extends Zend_Controller_Action {

    public function init() {}
    public function indexAction() 
    {
        $this->view->data = Seo::getSeo(array('id'=>1));
        if($this->getRequest()->isPost())
        {
            $data = array_merge(
                array('id'=>1,
                      'title'=>NULL,
                      'metatitle'=>NULL,
                      'keyword'=>NULL,
                      'description'=>NULL
                      ),
                $this->_request->getPost()
            );
            unset($data['submit']);
            $arrError = array();
            $tagtitle = trim($data['title']);
            if(empty($tagtitle))
            {
               $arrError['title'] ='Please enter Title'; 
            }
            if(empty($arrError))
            {
                Seo::update($data);
                $this->view->succes = 'Successfull!';
            }
          $this->view->arrError =$arrError; 
        }
        
        $this->view->dataDefault = array_merge(
            array(
                  'id'=>$this->view->data['id'],  
                  'title'=>$this->view->data['title'],
                  'metatitle'=>$this->view->data['metatitle'],
                  'keyword'=>$this->view->data['keyword'],
                  'description'=>$this->view->data['description']
                  ),
            isset($data) ? $data : array()
            );
                                                  
    }
}
