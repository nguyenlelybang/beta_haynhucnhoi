<?php

class Adm_IndexController extends Zend_Controller_Action {

    public function init() {
        setcookie("lcache", 1, time() + 999999, '/');
        //setcookie("is_hnn_mobile", $isMobile, time()+3600, '/');
    }
    public function indexAction() {}
    public function loginAction() 
    {
        if (defined('LOGIN_UID') && LOGIN_UID > 0) {
            $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'');
        }

        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $userName = trim($request->getParam('user_name', ''));
            $password = trim($request->getParam('password', ''));
            $login    = Admin::login($userName, $password);
            if ($login) {
                $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'');
            } else {
                $this->view->error_code = 1;
            }
        }

        My_Zend_Globals::setTitle('Administrator Control Panel - Login');
    }

    public function logoutAction() 
    {
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN1111'));
        $auth->clearIdentity();
        header("Location: " . BASE_URL . '/'. ADM_FOLDER .'/');
        exit;
    }

    public function buildhomeAction() 
    {
        $message ='';
        try {
            
            $links = array('home' => BASE_URL . '/?buildhome=fuck', 'sitemap' => BASE_URL . '/sitemap_generator.php');
            foreach ($links as $name => $url) {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_TIMEOUT, 60);
                curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $content = curl_exec($curl);
                $response = curl_getinfo($curl);
                curl_close($curl);

                $message .= 'Build ' . $name . ' - ';
                if ($response['http_code'] == 200) {
                    $message .= 'success. ';
                } else {
                    $message .= 'fail';
                }
                $message .= "<br/>";
            }
        } catch (Exception $e) {
            $message .= $e->getMessage();
        }

        echo Zend_Json::encode(array('msg' => $message));
        exit;
    }

    public function infoAction() {
        phpinfo();
        exit;
    }

}
