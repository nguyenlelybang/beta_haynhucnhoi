<?php

class Adm_StaticMenuController extends Zend_Controller_Action {

    public function init() {

    }
    public function indexAction()
    {
        $this->view->data = Setting::getSetting('static_menu');
        if($this->getRequest()->isPost())
        {
            $data = array_merge(
                array('setting_key'=>'static_menu',
                      'setting_value'=>NULL
                      ),
                $this->_request->getPost()
            );
            unset($data['submit']);
            $arrError = array();
            if(empty($arrError))
            {
                Setting::update($data);
                $this->view->succes = 'Successful!';
            }
          $this->view->arrError =$arrError;
        }

        $this->view->dataDefault = array_merge(
            array(
                  'setting_key'=>$this->view->data['setting_key'],
                  'setting_value'=>$this->view->data['setting_value']
            ),
            isset($data) ? $data : array()
            );
    }
}
