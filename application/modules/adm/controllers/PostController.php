<?php

class Adm_PostController extends Zend_Controller_Action
{

	public function init()
	{
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
	}

	public function indexAction()
	{

		$params = $this->_getAllParams();
		$params['is_actived'] = 1;
		$limit  = 30;
		$page   = isset($params['page']) ? intval($params['page']) : 1;
		$offset = ($page - 1) * $limit;
		$posts  = Post::getList($params, $offset, $limit, array('order_by' => 'post_id desc'), false);
		$total  = Post::countTotal($params);

		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$arrErrors = array('error_code' => 1, 'error_message' => 'Bạn không có quyền duyệt bài');

			$postId = $this->_getParam('post_id', 0);
			if ( ! Admin::isLogined()) {
				$arrErrors['error_code']    = 1;
				$arrErrors['error_message'] = 'Admin is not logged in yet.';
				$this->_helper->json($arrErrors, true);
			}

			if ( ! Admin::isLogined()) {
				$arrErrors['error_code']    = 1;
				$arrErrors['error_message'] = 'Data is invalid';
				$this->_helper->json($arrErrors, true);

			}

			if ( ! Role::isAllowed(Permission::ARTICLE_APPROVE)) {
				$arrErrors['error_code']    = 1;
				$arrErrors['error_message'] = 'Bạn không có quyền duyệt bài';
				$this->_helper->json($arrErrors, true);
			}

			$data = array('post_id' => $postId, 'is_approved' => 1, 'is_actived' => 1, 'approved_at' => time());

			Post::update($data);
			$post_new                = Post::getPost($postId, false);
			$post_new['is_actived']  = 1;
			$post_new['is_approved'] = 1;
			MembersPoint::getShare($post_new);
			$arrErrors['error_code']    = 0;
			$arrErrors['error_message'] = 'Bài viết đã được duyệt thành công';
			$this->_helper->json($arrErrors, true);


			//            $post_new = Post::getPost($postId);
			//            MembersPoint::getShare($post_new);

		}

		$this->view->categoryTree = Category::selectCategoryTree();
		$this->view->addHelperPath('My/Helper/', 'My_Helper');
		$this->view->params = $params;

		//$this->view->paging = $this->view->admpaging($params['module'], $params['controller'], $params['action'], $params, $total, $page, $limit, PAGE_SIZE, "");
		$this->view->paging      = $this->view->admpaging(ADM_FOLDER, $params['controller'], $params['action'], $params, $total, $page, $limit, PAGE_SIZE, "");
		$this->view->posts       = $posts;
		$this->view->defaultData = array_merge(array('created_date_day_from' => null, 'created_date_month_from' => null, 'created_date_year_from' => null, 'created_date_day_to' => null, 'created_date_month_to' => null, 'created_date_year_to' => null,), isset($params) ?
			$params : array());
	}

	public function createAction()
	{
		$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/index');
		Role::isAllowed(Permission::ARTICLE_ADD, true);
		if ($this->getRequest()->isPost()) {
			$formData          = $this->_request->getPost();
			$formData['title'] = trim($formData['title']);
			$formData['title'] = str_replace('"', "'", $formData['title']);
			$content           = $formData['content'];
			$content           = My_Zend_Globals::strip_word_html($content, '<a><h1><h2><h3><style><p><br /><br><strong><i><u><span><b><center><dd><dt><font><img><ul><li><ol><pre><table><td><title><td><tr><tt><object><iframe>');
			preg_match_all('/<img[^>]+>/i', $content, $result);

			if (count($result[0]) > 0) {
				foreach ($result[0] as $item) {
					preg_match('/src(?: )*=(?: )*[\'"](.*?)[\'"]/i', $item, $images);
					if (count($images) > 0) {
						$image = $images[1];
						if (strpos($image, 'camnanglamme.vn') === false) {
							$rs = My_Zend_Globals::leechImage($image, 0);
							if ( ! isset($rs['error_code'])) {
								echo "Leed:" . $newImage . "<br/>";
								$newImage = $rs['url'];
								$content  = str_replace($image, $newImage, $content);
							}
						}
					}
				}
			}

			if ( ! empty($formData['title'])) {
				$data = array('title' => $formData['title'], 'alias' => My_Zend_Globals::aliasCreator($formData['title']), 'description' => trim($formData['description']), 'content' => $content, 'category_id' => intval($formData['category_id']), 'picture' => trim($formData['picture']), 'status' => intval($formData['status']), 'meta_keywords' => trim($formData['meta_keywords']), 'meta_desciption' => trim($formData['meta_desciption']), 'created_date' => time(), 'updated_date' => time());
				if ($postId = Post::insert($data)) {
					$tagIds = $formData['tag_id'];
					foreach ($tagIds as $tagId) {
						$rs = Post::insertArticleTag(array('post_id' => $postId, 'tag_id' => $tagId));
					}
					$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/index');
				}
			}

			//My_Zend_Cache::cacheHTML_CleanAll();
		}

		$this->view->categoryTree = Category::selectCategoryTree();
	}

	public function editAction()
	{
        $config = My_Zend_Globals::getConfiguration();
		$metaFields = array('force_youtube');
		Role::isAllowed(Permission::ARTICLE_EDIT, true);
		$postId = $this->_getParam('post_id', 0);
		if ($postId == 0) {
			$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
		}

		if ( ! $post = Post::getPost($postId, false)) {
			$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
		}

		$postMeta = PostMeta::get($postId, null, false);

		$this->view->lsPostChap = PostsChap::getChapsByPostID($postId, false);

		if ($this->getRequest()->isPost()) {

			$formData          = $this->_request->getPost();
			$formData['story'] = trim($formData['story']);
			$formData['story'] = str_replace('"', "'", $formData['story']);
            $isMeme = isset($formData['is_meme']) ? 1 : 0;

            //custom thumb
            $image = isset($_FILES['thumb_upload']) ? $_FILES['thumb_upload'] : null;
            $maxsize = round($_FILES["thumb_upload"]["size"] / 1024 / 1024);
            if ($maxsize > 4) {
                $this->_helper->flashMessenger->addMessage('Hay Nhức Nhói hiện tại chỉ hỗ trợ đăng ảnh 4MB. Vui lòng chọn lại.');
                $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/edit/post_id/' . $post['post_id']);
            }
            $upload = new Upload();
            $destFolder = array('photo', date('Y'), date('m'));
            $rsUpload = $upload->upload($image, $destFolder);
            $rsUpload = $rsUpload[0];
            $upload->uploadImage(400, 230, UPLOAD_PATH . $rsUpload['url'], UPLOAD_PATH . $rsUpload['url']);

            if ( ! empty($formData['story'])) {
				$data = array(
					'post_id' => $postId,
					'story' => $formData['story'],
					'description' => $formData['description'],
					'category_id' => intval($formData['category_id']),
					'source' => trim($formData['source']),
					'is_approved' => intval($formData['is_approved']),
					'is_actived' => intval($formData['is_actived']),
					'is_featured' => intval($formData['is_featured']),
					'number_of_like' => intval($formData['number_of_like']),
					'number_of_share' => intval($formData['number_of_share']),
					'number_of_comment' => intval($formData['number_of_comment']),
                    'nsfw' => intval($formData['nsfw']),
                    'is_meme' => $isMeme,
					'phase' => intval($formData['phase']),
					'pip2' => My_Zend_Globals::getAltIp(),
                    'picture_upload' => $rsUpload['url']
				);

				if (!empty($formData['youtube_key'])) {
					if ( ! $post['is_photo'] && trim($formData['youtube_key']) != $post['youtube_key']) {
						$youtubeInfo = My_Zend_Media::retrieveYouTubeInfo($formData['youtube_key']);
                        if ( ! empty($youtubeInfo)) {
							$data['youtube_key'] = $youtubeInfo['id'];
							$data['picture']     = isset($youtubeInfo['thumb']['maxres']) ?
								$youtubeInfo['thumb']['maxres'] :
								isset($youtubeInfo['thumb']['standard']) ?
									$youtubeInfo['thumb']['standard'] :
									$youtubeInfo['thumb']['high'];
							if ($sharePic = Post::generateShareImage($data['picture'])) {
								$data['picture_share'] = $sharePic;
							}
						}
					}
				} else {
					$data['youtube_key'] = '';
				}

				if (!empty($formData['mecloud_key'])) {
					if ( ! $post['is_photo'] && trim($formData['mecloud_key']) != $post['mecloud_key']) {
						$meCloudInfo = My_Zend_Media::retrieveMeCloudVideoInfo($formData['mecloud_key']);
						if ( ! empty($meCloudInfo) && $meCloudInfo['error'] == 0) {
							$data['mecloud_key'] = $meCloudInfo['video']['aliasId'];
							$data['picture']     = $meCloudInfo['video']['thumbnails']['maxres'];
							if ($sharePic = Post::generateShareImage($data['picture'])) {
								$data['picture_share'] = $sharePic;
							}
						}
					}
				} else {
					$data['mecloud_key'] = '';
				}

				if (empty($formData['check_update'])) {
					$data['updated_at'] = time();
				}

				if ( ! $post['is_approved'] && $formData['is_approved']) {
					$data['approved_at'] = time();
				}
                $data['picture_share'] = isset($rsUpload['share_image']) ? $rsUpload['share_image'] : $data['picture_share'];

                Post::update($data);
				foreach ($metaFields as $field => $value) {
					PostMeta::update($postId, $value, $formData[$value]);
				}
				$this->_helper->_flashMessenger->addMessage('Cập nhật thành công.');
				$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/edit/post_id/' . $post['post_id']);
			}

			//My_Zend_Cache::cacheHTML_CleanAll();
		}
		$this->view->messages = $this->_helper->_flashMessenger->getMessages();
		$this->view->post         = $post;
		$this->view->postMeta     = $postMeta;
		$this->view->categoryTree = Category::selectCategoryTree();
	}

	public function deleteAction()
	{
		Role::isAllowed(Permission::ARTICLE_DELETE, true);
		$postId = $this->_getParam('post_id', 0);
		if ($postId == 0) {
			exit;
		}

		if ( ! Admin::isLogined()) {
			$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/index');
		}

		$rs = Post::delete($postId);
		if ($this->getRequest()->isXmlHttpRequest()) {
			if ($rs) {
				echo 1;
			} else {
				echo 0;
			}
			exit;
		} else {
			if ($rs) {
				$cache    = My_Zend_Globals::getCaching();
				$cacheKey = sprintf(Post::_cache_key, $postId);
				$cache->delete($cacheKey);
				$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/?delete_success=0');
			} else {
				$cache    = My_Zend_Globals::getCaching();
				$cacheKey = sprintf(self::_cache_key, $postId);
				$cache->delete(Post::_cache_key);
				$this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post/?delete_success=1');
			}
		}
		//My_Zend_Cache::cacheHTML_CleanAll();
	}

	public function reportAction()
	{

		$params = $this->_getAllParams();
		$limit  = 30;
		$page   = isset($params['page']) ? intval($params['page']) : 1;
		$offset = ($page - 1) * $limit;
		$posts  = PostReport::getList($params, $offset, $limit);
		$total  = PostReport::countTotal($params);

		$this->view->addHelperPath('My/Helper/', 'My_Helper');
		$this->view->params = $params;
		$this->view->paging = $this->view->admpaging(ADM_FOLDER, $params['controller'], $params['action'], $params, $total, $page, $limit, PAGE_SIZE, "");
		$this->view->posts  = $posts;
	}

    private function editThumb()
    {
        $file = isset($_FILES['thumb']) ? $_FILES['thumb'] : null;
        $upload = new Upload();
        $destFolder = array('thumb', date('Y'), date('m'));
        $rsUpload = $upload->upload($file, $destFolder);
        $rsUpload = $rsUpload[0];
        if (!isset($rsUpload['url']) || empty($rsUpload['url'])) {
            $this->_helper->flashMessenger->addMessage('Hệ thống đang bận. Bạn vui lòng thử lại.');
        }
        return $rsUpload;
    }
}
