<?php

class Adm_StaticpageController extends Zend_Controller_Action {

    public function init() {

    }

    /**
     * Default action
     */
    public function indexAction() {
        $params = $this->_getAllParams();
        Role::isAllowed(Permission::ADMIN_VIEW_LIST, true);

        $request = $this->getRequest();

        $page = $this->_getParam('page', 1);

        $listPages = StaticPage::getList();

        $this->view->addHelperPath('My/Helper/', 'My_Helper');

        $this->view->paging = $this->view->paging("default", $params['controller'], $params['action'], 'page', sizeof($listPages), 1, 10000, PAGE_SIZE, '');
        $this->view->listPages = $listPages;
    }

    public function addAction() {

        Role::isAllowed(Permission::ADMIN_MANAGE, true);

        $request = $this->getRequest();

        $error = 0;

        if ($request->isPost()) {
            $pageName = $request->getParam('page_name', '');
            $pageName = My_Zend_Globals::strip_word_html(trim($pageName), '');
            $fileName = $request->getParam('file_name', '');
            $fileName = My_Zend_Globals::strip_word_html(trim($fileName), '');
            $uri = $request->getParam('uri', '');
            $uri = My_Zend_Globals::strip_word_html(trim($uri), '');
            $ttl = $request->getParam('ttl', '');
            $ttl = My_Zend_Globals::strip_word_html(trim($ttl), '');
            $postId = intval($request->getParam('post_id', 0));
            $status = $request->getParam('status', 0);

            $data = array(
                'page_name' => $pageName,
                'file_name' => $fileName,
                'uri' => $uri,
                'post_id' => $postId,
                'ttl' => $ttl,
                'status' => $status,
                'created_at' => time(),
                'updated_at' => time()
            );

            $rs = StaticPage::insert($data);

            $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'/staticpage');
        }
    }

    public function editAction() {
        Role::isAllowed(Permission::ADMIN_MANAGE, true);
        $request = $this->getRequest();
        $id = intval($request->getParam('id', 0));
        $page = StaticPage::getPage($id);

        if (empty($page)) {
            $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'/staticpage/');
        }

        if ($request->isPost()) {
            $id = $request->getParam('id', '');

            $pageName = $request->getParam('page_name', '');
            $pageName = My_Zend_Globals::strip_word_html(trim($pageName), '');
            $fileName = $request->getParam('file_name', '');
            $fileName = My_Zend_Globals::strip_word_html(trim($fileName), '');
            $uri = $request->getParam('uri', '');
            $uri = My_Zend_Globals::strip_word_html(trim($uri), '');
            $ttl = $request->getParam('ttl', '');
            $ttl = My_Zend_Globals::strip_word_html(trim($ttl), '');
            $postId = intval($request->getParam('post_id', 0));
            $status = $request->getParam('status', 0);

            $data = array(
                'id' => $id,
                'page_name' => $pageName,
                'file_name' => $fileName,
                'uri' => $uri,
                'post_id' => $postId,
                'ttl' => $ttl,
                'status' => $status,
                'updated_at' => time()
            );

            StaticPage::update($data);
            $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'/staticpage/');
        }

        $this->view->page = $page;
    }

    public function deleteAction() {
        Role::isAllowed(Permission::ADMIN_MANAGE, true);

        $id = $this->_getParam('id', 0);

        $id = intval($id);

        $rs = StaticPage::delete($id);

        if ($rs) {

        }

        $this->_redirect(BASE_URL . '/'. ADM_FOLDER .'/staticpage/');
    }

}
