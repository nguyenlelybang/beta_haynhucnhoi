<?php
/**
 * Created by PhpStorm.
 * User: NamNT
 * Date: 4/25/2015
 * Time: 2:37 PM
 */
class Adm_PostchapController extends Zend_Controller_Action {


    public function init(){}
    public function indexAction()
    {
        $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
    }
    public function insertAction()
    {
        if (!Admin::isLogined()) {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
        }

        $this->_helper->layout()->disableLayout();
        $arrErrors   =   array('errors' => 0,'msg' => '');
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {

                $id     = (int) $this->getRequest()->getPost('id',null);
                $post   =   Post::getPost($id, false);
                if(empty($post)) {
                    $arrErrors['errors'] = 1;
                    $arrErrors['msg'] = 'PostID Not Found';

                } else {
                    $lastPost   = PostsChap::getPostChapRalativeLast($post['post_id']);

                    $key        = isset($lastPost['weight']) ? $lastPost['weight'] : 0;
                    $value      = $key + 1;
                    $chap       = $value  +1;
                    $lastid     = PostsChap::insert(array(
	                    'post_id' => $post['post_id'],
	                    'youtube_id' => '',
	                    'url' => '',
	                    'weight' => $value,
	                    'chap_name' => 'Tập '. $chap,
	                    'chap_title' => '',
                        'chap_description' => '',
                            'is_active' => 1)
	                    );

                    Post::update(array('post_id'=>$post['post_id'],'is_grid'=>'1'));
                    $arrErrors['errors'] = 2;
                    $arrErrors['msg'] = '<div id="showChap_'.$lastid.'" style="margin-bottom:4px;">
                            <input type="text" id="chap_name_'.$lastid.'" value="Tập '.$chap.'"  placeholder="Tên Tập" style="width: 80px">
                            <input type="text" id="chap_title_'.$lastid.'" placeholder="Title của chap" style="width: 200px">
                            <input type="text" id="txt_'.$lastid.'"  placeholder="URL YouTube / Shortcode MeCloud" style="width: 200px">
                            <textarea id="description_'.$lastid.'"  placeholder="Mô tả ngắn của chap" style="width: 200px; vertical-align: middle"></textarea>
                            <input type="text" id="weight_'.$lastid.'" value="'.$value.'"  placeholder="STT" style="width: 30px">
                            <a href="#" class="btnNewSmall green UpdateChap" data-id="'.$lastid.'">Cập nhật</a>
                            <a href="#" class="btnNewSmall red DeleteChap" data-id="'.$lastid.'">Xóa</a>
                        </div>';
                }
            }
            echo $this->_helper->json($arrErrors, true);
        }
    }
    public function updateAction()
    {
        if (!Admin::isLogined()) {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
        }
        $this->_helper->layout()->disableLayout();
        $arrErrors   =   array('errors'=>0,'msg'=>'');
        if ($this->getRequest()->isXmlHttpRequest())
        {
            if ($this->getRequest()->isPost())
            {
                $url        = $this->getRequest()->getPost('url',null);
                $id         = (int)$this->getRequest()->getPost('id',null);
                $post_id    = (int)$this->getRequest()->getPost('post_id',null);
                $chap_name  = $this->getRequest()->getPost('chap_name',null);
                $chap_title  = $this->getRequest()->getPost('chap_title',null);
                $weight     = (int)$this->getRequest()->getPost('weight',null);
                $description= $this->getRequest()->getPost('description',null);
	            $description = My_Zend_Globals::strip_word_html(trim($description));

	            if(empty($chap_name)) {

                    $arrErrors['errors'] = 3;
                    $arrErrors['msg'] = 'Vui lòng nhập tên tập!';

                } elseif (empty($url)) {

                    $arrErrors['errors'] = 1;
                    $arrErrors['msg'] = 'Vui lòng nhập URL';

                } else {
		            if (strpos($url, '[mecloud]') !== false && strpos($url, '[/mecloud]') !== false) {
			            $videoID = $url;
		            } else {
			            $videoID = My_Zend_Globals::getIDYoutube($url);
		            }

                    $post = Post::getPost($post_id);

                    if(empty($videoID)) {
                        $arrErrors['errors'] = 1;
                        $arrErrors['msg'] = 'Video Not Found';

                    } else if(empty($post)) {
                        $arrErrors['errors'] = 1;
                        $arrErrors['msg'] = 'Post ID Not Found';

                    } else {
                        PostsChap::update(array(
	                        'id' => $id,
	                        'youtube_id' => $videoID,
	                        'url' => $url,
	                        'chap_name' => $chap_name,
                            'chap_title' => $chap_title,
                            'chap_description' => $description,
	                        'weight' => $weight,
                            'post_id' => $post_id
	                        ));
                        $arrErrors['errors'] = 2;
                        $arrErrors['msg'] = ' Successfully';
                    }
                }
            }
            echo $this->_helper->json($arrErrors, true);
        } else {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
        }
    }
    public function deleteAction()
    {
        if (!Admin::isLogined())
        {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
        }
        $this->_helper->layout()->disableLayout();
        $arrErrors   =   array('errors'=>0,'msg'=>'','id'=>0);
        if ($this->getRequest()->isXmlHttpRequest())
        {
            if ($this->getRequest()->isPost())
            {
                    $id = (int)$this->getRequest()->getPost('id',null);
                    $post_id = (int)$this->getRequest()->getPost('post_id',null);
                    $post = PostsChap::getChapByID($id, false);
                    if(empty($post)) {
                        $arrErrors['errors']=1;
                        $arrErrors['msg']='ID của bài viết không tồn tại';

                    } else {
                        PostsChap::delete($id, $post_id);
                        $nPostChap = PostsChap::getChapsByPostID($post_id, false);
                        $nPost = Post::getPost($post_id, false);
                        if(empty($nPostChap)) {
                            if(!empty($nPost)) {
                                Post::update(array(
                                    'post_id' => $post_id,
                                    'is_grid' => 0)
                                );
                            }
                        }
                        $arrErrors['errors']=2;
                        $arrErrors['msg']='Đã xóa thành công';
                        $arrErrors['id']=$id;
                    }
            }

            echo $this->_helper->json($arrErrors, true);
        } else {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/post');
        }

    }
}