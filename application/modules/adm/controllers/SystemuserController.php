<?php

class Adm_SystemuserController extends Zend_Controller_Action {

    public function init() {
        $general = $this->_helper->layout()->_general;

        if (SYSTEM_USER_ROLE != Role::ROLE_SUPER_ADMIN_ID) {
            echo 'Bạn không có quyền truy cập';
            exit();
        }
    }

    public function userlistAction() {
        $list = User::selectSystemuserList();

        $roleList = Role::selectRoleList();

        $this->view->userList = $list;
        $this->view->roleList = $roleList;
    }

    public function deletesystemuserAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $meId = max(0, (int) $params['me_id']);
        if (!$meId)
        {
            echo 'MeId ko hợp lệ';
            exit();
        }

        if ($meId == ME_ID)
        {
            $this->_returnJson(0);
        }

        $status = (int) User::deleteSystemUser($meId);
        $this->_returnJson($status);
    }

    public function rolesAction() {
        $this->view->roles = Role::selectRoleList();
    }

    public function addroleAction() {
        $this->_helper->layout->disableLayout();

        $this->view->permissionDetails = Permission::$details;
    }

    public function submitaddroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $roleName = htmlspecialchars(trim($params['role_name']));
        $permissions = Permission::validate($params['permissions']);

        if (!$roleName || !$permissions) {
            $this->_returnJson(0);
        }

        $status = 0;
        if (Role::insertRole(array(
                    'role_name' => $roleName,
                    'permissions' => $permissions
                ))) {
            $status = 1;
        }

        $this->_returnJson($status);
    }

    public function editroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $roleId = Role::validate(max(0, (int) $params['role_id']));
        if (!$roleId) {
            echo 'Chưa nhập role Id';
            exit();
            //$this->_returnJson(0);
        }

        $roleInfo = Role::selectRole($roleId);
        if (!$roleInfo) {
            echo 'Role ko tồn tại';
            exit();
        }

        $permissions = Role::getRolePermissions($roleId);

        $this->view->permissionDetails = Permission::$details;
        $this->view->permissions = $permissions;
        $this->view->roleInfo = $roleInfo;
    }

    public function deleteroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $roleId = Role::validate(max(0, (int) $params['role_id']));
        if (!$roleId)
        {
            echo 'Role Id ko hợp lệ';
            exit();
        }

        $status = (int) Role::deleteRole($roleId);
        $this->_returnJson($status);
    }

    public function submiteditroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $roleId = Role::validate(max(0, (int) $params['role_id']));
        $permissions = Permission::validate($params['permissions']);

        if (!$roleId || !$permissions) {
            $this->_returnJson(0);
        }

        $status = 0;
        if (Role::updateRolePermissions(array(
                    'role_id' => $roleId,
                    'permissions' => $permissions
                ))) {
            $status = 1;
        }

        $this->_returnJson($status);
    }

    public function setroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $meId = max(0, (int) $params['me_id']);
        $userRole = Role::getUserRole($meId);
        if (!$userRole) {
            echo '<div>User này chưa được cấp quyền</div>';
            exit();
        }

        $roleList = Role::selectRoleList();
        $this->view->roleList = $roleList;
        $this->view->userRole = $userRole;
    }

    public function submitsetroleAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $meId = max(0, $params['me_id']);
        $roleId = Role::validate((int) $params['role_id']);
        if ($roleId === false || $meId == ME_ID) {
            $this->_returnJson(0);
        }

        $status = (Role::setUserRole($meId, $roleId)) ? 1 : 0;
        $this->_returnJson($status);
    }

    public function adduserAction()
    {
        $this->_helper->layout->disableLayout();
        
        $roleList = Role::selectRoleList();
        $this->view->roleList = $roleList;
    }

    public function submitadduserAction()
    {
        $this->_helper->layout->disableLayout();
        $params = $this->_getAllParams();

        $roleId = Role::validate((int) $params['role_id']);
        $username = trim($params['user_name']);               		              
        $cellPhone = trim(strip_tags($params['cell_phone']));
        $fullName = htmlspecialchars(trim($params['full_name']));
        $address = htmlspecialchars(trim($params['address']));
        $contactEmail = trim(strip_tags($params['contact_email']));	        
	        	        
        if (!$username || !$contactEmail || !$fullName)
        {
            $this->_returnJson(0);
        }

        $name2id = My_Zend_Globals::getName2IDObject();
        $userId = $name2id->getID($username);

        if ($userId <= 0 || $userId >= 4294967295)
        {
            $this->_returnJson(0);
        }
        
        $time = time();

        $data = array(
            'role_id'   =>  $roleId,
            'me_id' =>  $userId,
            'full_name' =>  $fullName,
            'department'    =>  'WEC',
            'contact_email' =>  $contactEmail,
        	'cell_phone'	=> $cellPhone,
        	'address'		=> $address,
            'created_date'  =>  $time,
            'updated_date'  =>  $time,
        );

        if (User::insertSystemUser($data))
        {
            $this->_returnJson(1);
        }

        $this->_returnJson(0);
    }
    
    public function edituserAction()
    {
    	$this->_helper->layout->disableLayout();
    	
    	// Check POST
    	if($this->getRequest()->isPost()) {
    		$params = $this->_getAllParams();
    		$params['role_id'] = Role::validate((int) $params['role_id']);
	        $params['cell_phone'] = trim(strip_tags($params['cell_phone']));
	        $params['full_name'] = htmlspecialchars(trim($params['full_name']));
	        $params['address'] = htmlspecialchars(trim($params['address']));
	        $params['contact_email'] = trim(strip_tags($params['contact_email']));
	        $params['updated_date'] = time();
	        if (!$params['full_name'] || !$params['cell_phone'] || !$params['contact_email'] || !$params['role_id']) {
	            $this->_returnJson(0);
	        }
	        if(User::updateSystemUser($params)) {
	        	$this->_returnJson(1);
	        }
	        $this->_returnJson(0);
    	}
    	
    	// Get ME_ID
    	$meId = $this->_getParam('me_id');
    	if($meId == 0) exit;
    	
    	// Get system user 
    	$user = User::selectSystemuser($meId);
    	$roleList = Role::selectRoleList();
    	// Set to viewer    
    	$this->view->params = $params;
        $this->view->roleList = $roleList;
        $this->view->user = $user;
        
	}

    private function _returnJson($status) {
        echo json_encode(array(
            'status' => $status
        ));
        exit();
    }

}