<?php

class Adm_PostsadvertismentController extends Zend_Controller_Action {

    public function init() {
        if (!Admin::isLogined()) {
            $this->_redirect(BASE_URL);
        }
    }
    public function indexAction() 
    {
        $params = $this->_getAllParams();
        $limit  = 20;
        $page   = isset($params['page']) ? intval($params['page']) : 1;
        $offset = ($page - 1) * $limit;
        $total  = Postsadvertisment::getTotal();
        $this->view->values   = Postsadvertisment::getList($limit,$offset);
        $this->view->addHelperPath('My/Helper/', 'My_Helper');
        $this->view->paging = $this->view->admpaging(ADM_FOLDER, $params['controller'], $params['action'], $params, $total, $page, $limit, PAGE_SIZE, "");
    }

    public function  addAction()
    {
        $arrError   = array('error'=>0,'msg'=>'');
        $arrData    = array(
            'title'=>NULL,
            'url'=>NULL,
            'images'=>NULL,
            'pos'=>1,
            'weight'=>NULL
        );

        if($this->getRequest()->isPost())
        {
            $data       = array_merge($arrData,$this->_request->getPost());
            $title      = trim($data['title']);
            $url        = trim($data['url']);
            $images     = trim($data['images']);

            if(empty($title))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter title';
            }else if(empty($url))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter URL';
            }else if(filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please check URL';

            }else if(empty($images))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter Image';
            }else if(filter_var($images, FILTER_VALIDATE_URL) === FALSE)
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please check URL Image';

            }else
            {
                $arrError['error']  =   2;
                $arrError['msg']    =   'Successfull!';
                Postsadvertisment::insert($data);
                $this->_redirect(BASE_URL.'/'.ADM_FOLDER.'/postsadvertisment');
            }
            $this->view->arrError = $arrError;
        }
        $this->view->arrDataDefault = array_merge($arrData,isset($data) ? $data : array());
    }

    public function editAction()
    {
        $id = $this->_getParam('id', 0);
        $this->view->data =Postsadvertisment::edit($id);
        $arrError   = array('error'=>0,'msg'=>'');
        if(empty($this->view->data['id']))
        {
            $this->_redirect(BASE_URL.'/'.ADM_FOLDER.'/postsadvertisment');
        }

        if($this->getRequest()->isPost())
        {
            $dataPost  = array(
                'id'=>$this->view->data['id'],
                'title'=>NULL,
                'url'=>NULL,
                'images'=>NULL,
                'pos'=>NULL
            );

            $data       = array_merge($dataPost,$this->_request->getPost());
            unset($data['submit']);
            $title      = trim($data['title']);
            $url        = trim($data['url']);
            $images     = trim($data['images']);

            if(empty($title))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter title';
            }else if(empty($url))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter URL';
            }else if(filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please check URL';

            }else if(empty($images))
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please enter Image';
            }else if(filter_var($images, FILTER_VALIDATE_URL) === FALSE)
            {
                $arrError['error']  =   1;
                $arrError['msg']    =   'Please check URL Image';

            }else
            {
                $arrError['error']  =   2;
                $arrError['msg']    =   'Successfull!';
                Postsadvertisment::update($data);
                $this->_redirect(BASE_URL.'/'.ADM_FOLDER.'/postsadvertisment');
            }
            $this->view->arrError = $arrError;
        }
        $this->view->arrDataDefault = array_merge($this->view->data,isset($data) ? $data :array());

    }
    public function deleteAction()
    {
        $id = $this->_getParam('id', 0);
        $this->view->data =Postsadvertisment::edit($id);
        if(empty($this->view->data['id']))
        {
            $this->_redirect(BASE_URL.'/'.ADM_FOLDER.'/postsadvertisment');
        }
        Postsadvertisment::delete($id);
        $this->_redirect(BASE_URL.'/'.ADM_FOLDER.'/postsadvertisment');
    }
}
