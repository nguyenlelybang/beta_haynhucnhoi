<?php

class Adm_LogController extends Zend_Controller_Action {

	public function init() {

	}

	public function indexAction() {

		if ($this->getRequest()->isPost()) {

			$params = $this->_getAllParams();

			if (!empty($params['ip']) && inet_pton($params['ip']) == false) {
				$this->view->error = 'Địa chỉ IP không hợp lệ';
				return;
			}

			$logs = PostLog::get($params);

			$this->view->params = $params;

			if ($logs) $this->view->logs = $logs;
			else $this->view->error = 'Không tìm thấy dữ liệu';

		}

	}

}