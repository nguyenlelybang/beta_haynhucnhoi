<?php

class Adm_BlogController extends Zend_Controller_Action {
    public function init() {

    }

    public function indexPostsAction() {
        $params = $this->_getAllParams();
        $params['is_actived'] = 1;
        $posts = BlogPost::getList(array(),false);
        $this->view->posts = $posts;
        $this->view->addHelperPath('My/Helper/', 'My_Helper');
        $this->view->paging      = $this->view->admpaging(ADM_FOLDER, $params['controller'], $params['action'], $params);
    }

    public function indexCategoriesAction() {
        $categories = BlogCategory::getAll();
        $this->view->categories = $categories;
    }

    public function addPostAction() {
        if($this->getRequest()->isPost()) {
            $data = $this->_request->getPost();

            $image = isset($_FILES['image']) ? $_FILES['image'] : null;

            $maxsize = round($image["size"] / 1024 / 1024);
            if ($maxsize > 4) {
                $this->_helper->flashMessenger->addMessage('Hay Nhức Nhói hiện tại chỉ hỗ trợ đăng ảnh 4MB. Vui lòng chọn lại.');
                $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/add-post');
            }

            $upload = new Upload();
            $destFolder = array('blog', date('Y'), date('m'));
            $rsUpload = $upload->upload($image, $destFolder);

            $rsUpload = $rsUpload[0]; //rsUpload trả về mảng 2 chiều ==> gán key = 0
            $upload->uploadImage(728, 364, UPLOAD_PATH . $rsUpload['url'], UPLOAD_PATH . $rsUpload['url']);

            $insertData = array(
                'category_id' => 0,
                'user_id' => LOGIN_UID,
                'post_status' => $data['post_status'],
                'post_title' => $data['title'],
                'post_summary' => $data['summary'],
                'post_content' => $data['post_content'],
                'post_image' => $rsUpload['url']
            );

            $insertData['category_id'] = isset($data['category_id']) ? $data['category_id'] : 0;
            BlogPost::insert($insertData);
            $postID = BlogPost::getListOrderByID();
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/edit-post/post_id/' . $postID[0]['post_id']);
        }
        $this->view->listCategory = BlogCategory::getAll();
    }

    public function addCategoryAction() {
        if($this->getRequest()->isPost()) {
            $data = $this->_request->getPost();

            $insertData = array(
                'category_name' => $data['name'],
                'category_alias' => $data['alias'],
            );
            BlogCategory::insert($insertData);
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/add-category');
        }
    }

    public function editPostAction() {
        $postId = $this->_getParam('post_id', 0);
        $post = BlogPost::get($postId, false);
        $this->view->post = $post;
        if($this->getRequest()->isPost()) {

            $data = $this->_request->getPost();
            $image = isset($_FILES['image']) ? $_FILES['image'] : null;

            $maxsize = round($image["size"] / 1024 / 1024);
            if ($maxsize > 4) {
                $this->_helper->flashMessenger->addMessage('Hay Nhức Nhói hiện tại chỉ hỗ trợ đăng ảnh 4MB. Vui lòng chọn lại.');
                $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/edit-post');
            }
            $upload = new Upload();
            $destFolder = array('blog', date('Y'), date('m'));
            $rsUpload = $upload->upload($image, $destFolder);
            $rsUpload = $rsUpload[0]; //rsUpload trả về mảng 2 chiều ==> gán key = 0
            $upload->uploadImage(728, 364, UPLOAD_PATH . $rsUpload['url'], UPLOAD_PATH . $rsUpload['url']);

            $insertData = array(
                'post_id' => $postId,
                'category_id' => 0,
                'post_status' => $data['post_status'],
                'post_title' => $data['title'],
                'post_summary' => $data['summary'],
                'post_content' => $data['post_content'],
                'post_image' => $rsUpload['url'],
            );
            $insertData['category_id'] = isset($data['category_id']) ? (int)$data['category_id'] : 0;
            BlogPost::update($insertData);
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/edit-post/post_id/' . $post['post_id']);
        }
        $this->view->listCategory = BlogCategory::getAll();

    }

    public function editCategoryAction() {
        $categoryID = $this->_getParam('category_id', 0);
        if($this->getRequest()->isPost()) {
            $data = $this->_request->getPost();

            $insertData = array(
                'category_id' => $categoryID,
                'category_name' => $data['name'],
                'category_alias' => $data['alias'],
            );
            BlogCategory::update($insertData);
        }
        $category = BlogCategory::get(array('category_id'=>$categoryID));
        $this->view->category = $category;
    }

    public function deletePostAction() {
        $postId = $this->_getParam('post_id', 0);
        $rs = BlogPost::delete($postId);
        if ($rs) {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/index-posts/?delete_success=0');
        } else {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/index-posts/?delete_success=1');
        }
    }

    public function deleteCategoryAction() {
        $categoryID = $this->_getParam('category_id', 0);
        $rs = BlogCategory::delete($categoryID);
        if ($rs) {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/index-categories/?delete_success=0');
        } else {
            $this->_redirect(BASE_URL . '/' . ADM_FOLDER . '/blog/index-categories/?delete_success=1');
        }
    }

    public function CkfinderAction() {

    }
}