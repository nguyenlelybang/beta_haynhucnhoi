<?php
class Adm_ErrorController extends Zend_Controller_Action
{
	/**
	 * Error handle action
	 */
    public function errorAction()
    {
    	$error = $this->_getParam('error_handler');    	
        switch(get_class($error->exception)) {
          case 'PageNotFoundException':
            $this->_forward('page-not-found');
            break;
     
          case 'NotAuthorizedException':
            $this->_forward('not-authorized');
            break;
     
          default:
            if (APPLICATION_ENVIRONMENT != 'staging' && APPLICATION_ENVIRONMENT != 'production')
            {
                $ex = $error->exception;
                print($ex);exit;
            }
            break;
        }
      
    }
    
    public function pageNotFoundAction()
    {

    }
   
    public function notAuthorizedAction()
    {
      
    }
    
}