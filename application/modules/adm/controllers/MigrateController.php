<?php

class Adm_MigrateController extends Zend_Controller_Action {

    private $_storage = null;

    public function init() {
        echo "<meta charset=utf-8 />";
    }

    public function postsAction() {
        $page = $this->_getParam('page', 1);
        $limit = 100;

        $offset = ($page - 1) * $limit;
        $posts = $this->getListTopic($offset, $limit);

        if (!empty($posts)) {
            foreach ($posts as $post) {
                if ($this->migratePost($post)){
                    echo "Insert post '". $post['story'] ."' success <br/>";
                }
                else {
                    echo "<span style='color:#333'>Insert post '". $post['story'] ."' FAIL </span><br/>";
                }
            }

            $url = BASE_URL .'/'. ADM_FOLDER .'/migrate/posts/page/'. ($page + 1);
            header("Refresh: 1; url=". $url);
        }

        echo "FINISHED";
        exit();
    }

    private function getListMember($offset = 0, $limit = 1000) {
        $storage = $this->getStorage();

        $select = $storage->select()
                    ->from('posts', '*')
                    ->limit($limit, $offset);

        $posts = $storage->fetchAll($select);

        return $posts;
    }

    private function migrateMember($user) {
        $fields = array(
            'user_id' => $user['USERID'],
            'email' => $user['email'],
            'username' => $user['username'],
            'password' => $user['password'],
            'salt' => $user['salt'],
            'fullname' => $user['fullname'],
            'gender' => $user['gender'],
            'birthday' => $user['birthday'],
            'description' => $user['description'],
            'country' => $user['country'],
            'posts' => $user['posts'],
            'yourviewed' => $user['yourviewed'],
            'profileviews' => $user['profileviews'],
            'youviewed' => $user['youviewed'],
            'addtime' => $user['addtime'],
            'lastlogin' => $user['lastlogin'],
            'verified' => $user['verified'],
            'status' => $user['status'],
            'profilepicture' => $user['profilepicture'],
            'remember_me_key' => $user['remember_me_key'],
            'remember_me_time' => $user['remember_me_time'],
            'ip' => $user['ip'],
            'lip' => $user['lip'],
            'location' => $user['location'],
            'filter' => $user['filter'],
            'website' => $user['website'],
            'news' => $user['news'],
            'theme' => $user['theme'],
            'points' => $user['points'],
            'facebookid' => $user['facebookid'],
            'like_count' => $user['like_count'],
            'comment_count' => $user['comment_count'],
            'rank' => $user['rank'],
            'follow_number' => $user['follow_number'],
            'followed_number' => $user['followed_number'],
            'approved_new_post' => $user['approved_new_post'],
            'new_post_to_home' => 0
            );

        return User::insertUser($data);
    }

    private function getListTopic($offset = 0, $limit = 1000) {
        $storage = $this->getStorage();

        $select = $storage->select()
                    ->from('posts', '*')
                    ->limit($limit, $offset);

        $posts = $storage->fetchAll($select);

        return $posts;
    }

    private function migratePost($oldPost) {
        $data = array(
            'post_id' => $oldPost['PID'],
            'story' => $oldPost['story'],
            'user_id' => $oldPost['USERID'],
            'source' => $oldPost['source'],
            'category_id' => $oldPost['CID'],
            'url' => $oldPost['url'],
            'tags' => $oldPost['tags'],
            'phase' => $oldPost['phase'],
            'is_photo' => $oldPost['isPhoto'],
            'is_gif' => $oldPost['isGIF'],
            'picture' => $oldPost['dir'] . $oldPost['pic'],
            'youtube_key' => $oldPost['youtube_key'],
            'vmo_key' => $oldPost['vmo_key'],
            'zingtv_key' => $oldPost['ztv_key'],
            'is_approved' => $oldPost['approved'],
            'pip' => $oldPost['pip'],
            'pip2' => $oldPost['pip2'],
            'is_actived' => $oldPost['active'],
            'is_featured' => $oldPost['isFeatured'],
            'created_at' => $oldPost['time_added'],
            'updated_at' => strtotime($oldPost['time_updated']),
            'approved_at' => '',
            'number_of_view' => $oldPost['number_of_view'],
            'number_of_comment' => $oldPost['number_of_comment'],
            'number_of_share' => $oldPost['number_of_share'],
            'number_of_like' => $oldPost['number_of_like'],
            'last_viewed' => $oldPost['last_viewed']
            );

        return Post::insert($data);
    }

    private function getStorage() {
        if (!is_null($this->_storage)) {
            return $this->_storage;
        }

        $options = array(
            'host'  => 'localhost',
            'port'  => '3306',
            'username'  => 'phuongnd_hnn',
            'password'  => 'kVe3FBm3',
            'dbname'    => 'phuongnd_hnn',
            'driver_options' => array(
                        MYSQLI_INIT_COMMAND => 'SET NAMES utf8;'
                    )
        );

        $this->_storage = Zend_Db::factory('mysqli', $options);

        return $this->_storage;
    }
}
