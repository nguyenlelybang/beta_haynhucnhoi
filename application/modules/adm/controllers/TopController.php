<?php

class Adm_TopController extends Zend_Controller_Action {

	public function init() {

	}

	public function listAction() {
		$timeCode = $this->_getParam('code', 0);
		if ($timeCode !== 0 && preg_match('/(m|w)(\d{2})(\d{4})/', $timeCode, $code)) {
			$data = UserPoint::getWeekMonthTop($timeCode);
			if (!$data) {
				$this->view->error = "Point not found: " . $timeCode;
			} else {
				$this->view->data = $data;
			}
			if ($code[1] == 'w') {
				$range = self::getStartAndEndDate($code[2], $code[3]);
				$this->view->title = 'Tuần ' . $code[2] . ' (' . $range['week_start'] . ' - ' . $range['week_end'] . ')';
			} elseif ($code[1] == 'm') {
				$this->view->title = 'Tháng ' . $code[2] . '/' . $code[3];
			}
		} else {
			die('No code');
		}
	}

	function getStartAndEndDate($week, $year) {
		$dto = new DateTime();
		$dto->setISODate($year, $week);
		$ret['week_start'] = $dto->format('d/m/Y');
		$dto->modify('+6 days');
		$ret['week_end'] = $dto->format('d/m/Y');
		return $ret;
	}

}