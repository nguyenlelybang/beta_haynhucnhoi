<?php

class Adm_Reborn_UserController extends Zend_Controller_Action {

    public function init() {
        if (!Admin::isLogined())
        {
            $this->_redirect(BASE_URL);
        }
    }
    public function indexAction() 
    {
        $params = $this->_getAllParams();
        $limit  = 30;
        $page   = isset($params['page']) ? intval($params['page']) : 1;
        $offset = ($page - 1) * $limit;

        $filters = array();
        if (isset($params['email'])) {
            $filters['email'] = trim($params['email']);
        }

        if (isset($params['user_id'])) {
            $filters['user_id'] = trim($params['user_id']);
        }

        if (isset($params['status']) && $params['status'] != -1) {
            $filters['status'] = $params['status'];
        }

        if (isset($params['created_date_month_from']) && $params['created_date_day_from'] && $params['created_date_month_from'] && $params['created_date_year_from']) {
            $filters['created_from'] = strtotime($params['created_date_day_from'] .'-'. $params['created_date_month_from'] .'-'. $params['created_date_year_from']);
        }

        if (isset($params['created_date_month_to']) && $params['created_date_day_to'] && $params['created_date_month_to'] && $params['created_date_year_to']) {
            $filters['created_to'] = strtotime($params['created_date_day_to'] .'-'. $params['created_date_month_to'] .'-'. $params['created_date_year_to']);
        }
        $users = User::getList($filters, $offset, $limit);
        $total = User::countTotal($filters);
        $this->view->defaultData = array_merge(
            array(
                'created_date_day_from'   => NULL,
                'created_date_month_from' => NULL,
                'created_date_year_from'  => NULL,
                'created_date_day_to'     => NULL,
                'created_date_month_to'   => NULL,
                'created_date_year_to'    => NULL,
            ),
        isset($params) ? $params : array()
        );

        $this->view->addHelperPath('My/Helper/', 'My_Helper');
        $this->view->params = $params;
        $this->view->paging = $this->view->admpaging(ADM_REBORN_FOLDER, $params['controller'], $params['action'], $params, $total, $page, $limit, PAGE_SIZE, "");
        $this->view->users = $users; 
    }

    public function editAction() 
    {
        Role::isAllowed(Permission::USER_EDIT, true);
        $userId = $this->_getParam('user_id', 0);
        if ($userId == 0) {
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/user');
        }
        if (!$user = User::getUser($userId, false)) {
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/user');
        }
        if ($this->getRequest()->isPost()) 
        {
            $formData          = $this->_request->getPost();
            $formData['email'] = My_Zend_Globals::strip_word_html(trim($formData['email']));
            $birthDay          = $formData['birth_day'];
            $birthMonth        = $formData['birth_month'];
            $birthYear         = $formData['birth_year'];

            if (!empty($formData['email'])) {
                $data = array(
                    'user_id'               => $userId,
                    'email'                 => $formData['email'],
                    'fullname'              => My_Zend_Globals::strip_word_html(trim($formData['fullname'])),
                    'gender'                => trim($formData['gender']),
                    'location'              => My_Zend_Globals::strip_word_html(trim($formData['location'])),
                    'website'               => My_Zend_Globals::strip_word_html(trim($formData['website'])),
                    'status'                => isset($formData['status']) ? 1 : 0,
                    'approved_new_post'     => isset($formData['approved_new_post']) ? 1 : 0,
                    'new_post_to_home'      => isset($formData['new_post_to_home']) ? 1 : 0,
                    'birthday'              => $birthYear .'-'. $birthMonth .'-'. $birthDay
                );
                if ($formData['password'] != '') {
                    $data['salt'] = User::createUserSalt();
                    $data['password'] = User::hashPassword($formData['password'], $data['salt']);
                }
                if (User::updateUser($data)) {
                    $this->_helper->flashMessenger->addMessage('Cập nhật thông tin tài khoản thành công');
                    $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/user/edit/user_id/'. $userId);
                }
            }
        }

        $birthday = explode('-', $user['birthday']);
        $year     = $birthday[0] > 0 ? $birthday[0] : '';
        $month    = $birthday[1] > 0 ? $birthday[1] : '';
        $day      = $birthday[2] > 0 ? $birthday[2] : '';

        $this->view->birthday = array('year' => $year, 'month' => $month, 'day' => $day);
        $this->view->user     = $user;
        $this->view->messages = $this->_helper->flashMessenger->getMessages();
    }

    public function lockAction() 
    {
        Role::isAllowed(Permission::USER_DELETE, true);
        $userId = $this->_getParam('user_id', 0);
        $status = (int)$this->_getParam('status', 1);
        if ($userId == 0) {
            exit;
        }
        $data = array(
            'user_id'   => $userId,
            'status'    => $status
        );

        if (User::updateUser($data)) {
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/user');
        } else {
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/user');
        }
    }
}
