<?php

class Adm_Reborn_LogController extends Zend_Controller_Action {

	public function init() {

	}

	public function indexAction() {

		if ($this->getRequest()->isPost()) {

			$params = $this->_getAllParams();

			if (!empty($params['ip']) && inet_pton($params['ip']) == false) {
				$this->view->error = 'IP Invalid';
				return;
			}

			$logs = PostLog::get($params);

			$this->view->params = $params;

			if ($logs) $this->view->logs = $logs;
			else $this->view->error = 'Missing Data';

		}

	}

}