<?php

class Adm_Reborn_ScanVideoController extends Zend_Controller_Action {

    public function init() {

    }
    public function indexAction()
    {
        if ($this->getRequest()->isPost()) {
            $fromid = $this->_request->getPost()['fromid'];
            $toid = $this->_request->getPost()['toid'];
            $this->scanVideos($fromid, $toid);
        }
    }
    public function scanVideos($fromid, $toid)
    {
        $storage = My_Zend_Globals::getStorage();
        $table   = 'posts';
        $select  = $storage->select()
            ->from($table,array('post_id','youtube_key','is_photo','is_actived'))
            ->where('post_id >= ?', $fromid)
            ->where('post_id <= ?', $toid)
            ->where('is_photo = 0')
            ->where('is_actived = 1');
        $rows = $storage->fetchAll($select);

        $errorPosts = array();
        foreach($rows as $value) {
            $url = "https://www.googleapis.com/youtube/v3/videos?id=".$value['youtube_key']."&key=".YOUTUBE_API_KEY."&part=status";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $output=curl_exec($ch);
            $response = json_decode($output, TRUE);
            $totalResult = $response['pageInfo']['totalResults'];
            $uploadStatus = $response['items'][0]['status']['uploadStatus'];

            if ($totalResult == 0 || $uploadStatus != 'processed') {
                $errorPosts[] = $value['post_id'];
            }
        }
        $this->view->errorPosts = $errorPosts;
    }
}
