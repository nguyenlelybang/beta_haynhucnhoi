<?php

class Adm_Reborn_CategoryController extends Zend_Controller_Action {

    public function init() {
        $general = $this->_helper->layout()->_general;
        $this->view->headScript()->appendFile($general['server']['js']['path'] . '/category.js');
	    $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
	    $this->view->messages = $this->_helper->_flashMessenger->getMessages();
    }

    public function indexAction()
    {
        $parentId    = 0;
        $total = '';
        $arrCategory = Category::selectCategoryList(array('parent_id' => 0), false);
        if (is_array($arrCategory) && !empty($arrCategory)) {
            $total = count($arrCategory);
        }

        $arrMenuCategory             = Category::selectCategoryTree();
        $this->view->arrCategory     = $arrCategory;
        $this->view->total           = $total;
        $this->view->arrMenuCategory = $arrMenuCategory;
    }

    public function categorylistAction()
    {

        Role::isAllowed(Permission::CATEGORY_VIEW, true);
        $params          = $this->_getAllParams();
        $category_id     = intval($this->_getParam('id', 0));
        $arrMenuCategory = Category::selectCategoryTree();
        $arrCategory     = Category::selectCategoryList(array('parent_id' => $category_id), false);
        $arrDetail       = Category::selectCategory($category_id);

        $this->view->addHelperPath('My/Helper/', 'My_Helper');
        $this->view->params          = $params;
        $this->view->arrCategory     = $arrCategory;
        $this->view->arrMenuCategory = $arrMenuCategory;
        $this->view->id              = $category_id;
        $this->view->arrDetail       = $arrDetail;
        $this->render();
    }

    public function categorycreateAction()
    {
        Role::isAllowed(Permission::CATEGORY_ADD, true);
        $flag = 0;
        if ($this->getRequest()->isPost())
        {
            $arrError = array();
            $data = array_merge(
                array('category_name'=>NULL,
                        'alias'=>NULL,
                        'page_title'=>NULL,
                        'meta_keyword'=>NULL,
                        'meta_description'=>NULL,
                        'parent_id'=>0,
                        'ordering'=>0,
                        'is_active_home'=>0,
                        'is_grid'=>0,
                        'is_hot'=>0,
                        'category_customize'=>NULL,
                        'is_url'=>NULL
                      ),
                $this->_request->getPost()
            );

            unset($data['act']);
            unset($data['submit_x']);
            unset($data['submit_y']);
            if(empty($arrError)){
                Category::insertCategory($data);
                unset($data);
                $cache = My_Zend_Globals::getCaching();
                $cache->cleanAllRecord();
                $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/category/index');
            }
            $this->view->arrError = $arrError;
        }

        $arrMenuCategory             = Category::selectCategoryTree();
        $arrParent                   = Category::selectCategoryList(array('parent_id' => 0));
        $this->view->name            = isset($formData['category_name']) ? $formData['category_name'] : '';
        $this->view->arrMenuCategory = $arrMenuCategory;
        $this->view->arrParent       = $arrParent;
        $this->view->flag            = $flag;
    }
    public function categoryeditAction()
    {
        Role::isAllowed(Permission::CATEGORY_EDIT, true);

	    $categoryId  = intval($this->_getParam('id', 0));
        $parent_id   = 0;
        $flag        = 0;
        $category = Category::selectCategory($categoryId);
        $this->view->category = $category;

        if (!empty($category)) {
            if ($this->getRequest()->isPost())
            {
                $formData = $this->_request->getPost();
	            if (!empty($formData['description'])) {
		            $formData['description'] = htmlspecialchars(My_Zend_Globals::strip_word_html(trim($formData['description']), ''));
	            }
                unset($formData['id']);
                unset($formData['act']);
                $data = array_merge(
                    array('category_id'=> $categoryId,
                          'category_name'=> NULL,
                          'description'=> NULL,
                          'alias' => NULL,
                          'page_title'=> NULL,
                          'meta_keyword'=> NULL,
                          'meta_description'=> NULL,
                          'parent_id' => 0,
                          'ordering' => 0,
                          'is_active_home' => 0,
                          'is_grid' => 0,
                          'is_hot' => $category['is_hot'],
                          'category_customize' => isset($category['category_customize']) ? $category['category_customize'] : '',
                          'is_url' => isset($category['is_url']) ? $category['is_url'] : ''
                    ), $formData );

	            $updateResult = Category::updateCategory($data);

                if ($updateResult) {
	                $cache = My_Zend_Globals::getCaching();
	                $cache->delete(Category::_CATEGORY_LIST_HOME_MENU);
	                $this->_helper->_flashMessenger->addMessage('Update success!.');
                } else {
	                $this->_helper->_flashMessenger->addMessage('Something wrong!.');
                }
	            $url = $this->_helper->url('categoryedit', 'category', 'adm_reborn', array('id' => $categoryId));
	            $this->_redirect($url);
            }
        }

        $arrParent                   = Category::selectCategoryList(array('parent_id' => 0));
        $arrMenuCategory             = Category::selectCategoryTree();
        $this->view->parent_id       = $parent_id;
        $this->view->flag            = $flag;
        $this->view->category        = $category;
        $this->view->arrParent       = $arrParent;
        $this->view->arrMenuCategory = $arrMenuCategory;

        //$this->view->tagList = $tagList; remove by Stephen Nguyễn 02/03/2015 5:27 PM
    }
    public function categorydeleteAction()
    {
        Role::isAllowed(Permission::CATEGORY_DELETE, true);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $category_id = intval($this->_getParam('id', 0));
        $flag = 0;

        if ($this->getRequest()->isPost())
        {
            $formData = $this->_request->getPost();
            if ($formData['id'] > 0 && $formData['act'] == 'del')
            {
                //AdminLog::log('categorydeleteAction', Permission::CATEGORY_DELETE, array('category_id'=>$formData['id']));
                // Xoa danh muc con
                $arrCate = Category::selectCategoryList(array('parent_id' => $formData['id']));
                if (is_array($arrCate) && !empty($arrCate)) {
                    foreach ($arrCate as $value) {
                        if ($value['category_id'] > 0) {
                            Category::deleteCategory($value['category_id']);
                        }
                    }
                }
                Category::deleteCategory($formData['id']);
                $cache = My_Zend_Globals::getCaching();
                $cache->cleanAllRecord();
            }
        }

        echo $flag;
    }

    public function categorypublishAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $category_id = intval($this->_getParam('id', 0));
        $flag = 0;
        if ($category_id > 0)
        {
            $status = 1;
            $data['category_id'] = $category_id;
            $data['is_published'] = 1;
            $flag = Category::updateCategory($data);
        }
        echo $flag;
    }

}
