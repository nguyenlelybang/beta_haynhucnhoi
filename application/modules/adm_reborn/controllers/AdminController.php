<?php

class Adm_Reborn_AdminController extends Zend_Controller_Action {

    public function init() {

    }

    /**
     * Default action
     */
    public function listAction() 
    { 
        Role::isAllowed(Permission::ADMIN_VIEW_LIST, true);
        $request   = $this->getRequest();
        $page      = $this->_getParam('page', 1);
        $limit     = 30;
        $offset    = ($page - 1) * $limit;
        $listAdmin = Admin::getListAdmin($offset, $limit);

        foreach ($listAdmin as &$admin) {
            $adminDetail = Admin::getUser($admin['user_id']);
            $admin['user_name'] = $adminDetail['user_name'];
        }

        $total  = Admin::getTotalListAdmin();
        $params = array(
            'module'     => 'default',
            'controller' => 'admin',
            'action'     => 'list'
        );

        $this->view->addHelperPath('My/Helper/', 'My_Helper');
        $this->view->paging    = $this->view->paging("default", $params['controller'], $params['action'], 'page', $total, $page, $limit, PAGE_SIZE, '');
        $this->view->listAdmin = $listAdmin;
    }

    public function addAction() 
    {

        Role::isAllowed(Permission::ADMIN_MANAGE, true);
        $request = $this->getRequest();
        $error   = 0;

        if ($this->getRequest()->isPost()) 
        {
            
            $userName   = $request->getParam('user_name', '');
            $userName   = My_Zend_Globals::strip_word_html(trim($userName), '');
            $fullName   = $request->getParam('fullname', '');
            $fullName   = My_Zend_Globals::strip_word_html($fullName, '');
            $password   = $request->getParam('password', '');
            $salt       = Admin::createUserSalt();
            $password   = Admin::hashPassword($password, $salt);
            $roleId     = $request->getParam('role_id', '');
            $email      = $request->getParam('email', '');
            $email      = My_Zend_Globals::strip_word_html(trim($email), '');

            $data       = array(
            'user_name'    => $userName,
            'role_id'      => $roleId,
            'fullname'     => $fullName,
            'email'        => $email,
            'password'     => $password,
            'created_date' => time(),
            'updated_date' => time());

            $rs = Admin::insertUser($data);
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/list');
        }

        $roleList = Role::selectRoleList(0, 100);
        $this->view->roleList = $roleList;
    }

    public function editAction()
    {
        Role::isAllowed(Permission::ADMIN_MANAGE, true);
        $request    = $this->getRequest();
        $userId     = intval($request->getParam('user_id', 0));
        $userDetail = Admin::getUser($userId);

        if ($request->isPost()) 
        {
            
            $userId   = $request->getParam('user_id', '');
            $userName = $request->getParam('user_name', '');
            $userName = My_Zend_Globals::strip_word_html(trim($userName), '');
            $fullName = $request->getParam('fullname', '');
            $fullName = My_Zend_Globals::strip_word_html($fullName, '');
            $roleId   = $request->getParam('role_id', '');
            $email    = $request->getParam('email', '');
            $email    = My_Zend_Globals::strip_word_html(trim($email), '');
            $password = $request->getParam('password', '');

            $data = array(
                'user_id'       => $userId,
                'user_name'     => $userName,
                'role_id'       => $roleId,
                'fullname'      => $fullName,
                'email'         => $email,
                'updated_date'  => time()
            );

            if ($password != '') {
                $data['password'] = Admin::hashPassword($password, $userDetail['salt']);
            }
            $rs = Admin::updateUser($data);
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/list');
        }

        $roleList = Role::selectRoleList(0,100);
        $this->view->roleList = $roleList;
        $this->view->user     = $userDetail;
    }

    public function deleteAction() 
    {
        Role::isAllowed(Permission::ADMIN_MANAGE, true);
        $userId     = $this->_getParam('user_id', 0);
        $userId     = intval($userId);
        $userDetail = Admin::getAdmin($userId);

        if (empty($userDetail))
            $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/list');

        $data = array(
            'user_id'      => $userId,
            'created_date' => time(),
            'updated_date' => time()
        );
        $data['is_locked'] = ($userDetail['is_locked'] == 1) ? 0 : 1;
        $data              = array_merge($userDetail, $data);
        $rs                = Admin::updateAdmin($data);
        $this->_redirect(BASE_URL . '/admin/list');
    }

    public function changepassAction() 
    {
        $request = $this->getRequest();
        $userId  = LOGIN_UID;
        $error   = $request->getParam('error', 0);

        if ($request->isPost()) 
        {
            $password        = $request->getParam('password');
            $confirmPassword = $request->getParam('confirm_password');

            if ($password != $confirmPassword) {
                $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/changepass/error/1');
            }

            if (strlen($password) < 6) {
                $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/changepass/error/2');
            }

            $salt     = Admin::createUserSalt();
            $password = Admin::hashPassword($password, $salt);
            $data     = array(
                'user_id'  => $userId,
                'password' => $password,
                'salt'     => $salt
            );

            if (Admin::updateUser($data)) {
                $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/changepass/error/0');
            } else {
                $this->_redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/admin/changepass/error/3');
            }

            $this->view->error = $error;
        }
    }

}
