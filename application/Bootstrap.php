<?php

require_once 'Zend/Loader/Autoloader.php';

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    /**
     * Front Controller
     */
    private static $front = null;

    /**
     * Application configuration
     */
    private static $configuration = null;

    /**
     * Registration of my name space
     */
    protected function initNamespace() {
        Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
    }




    /**
     * Init application configuration
     */
    protected function initAppConfiguration() {
        if (null === self::$configuration) {

            $configs = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENVIRONMENT);

            self::$configuration = $configs;
        }

        Zend_Registry::set(APPLICATION_CONFIGURATION, self::$configuration);
    }

    /**
     * Init admin log configuration
     */
    public static function initAdminLogConfiguration() {

    }

    /**
     * Logger setup
     */
    protected function initLogger() {
        //Logging system
        $logger = new Zend_Log();
        $logger->addWriter(new Zend_Log_Writer_Stream(self::$configuration->logger->dump->path));
        Zend_Registry::set(LOGGER_DUMP, $logger);
    }

    /**
     * Routers setup
     */
    protected function initRouters() {
        self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/adm/controllers', 'adm');
        self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/adm_reborn/controllers', 'adm_reborn');

        //Default module
        // detect mobile device
        if (isset($_GET['ismobile'])) {
            $isMobile = intval($_GET['ismobile']) > 0 ? 1 : 0;
            setcookie("is_hnn_mobile", $isMobile, time()+3600, '/');
        } elseif (isset($_COOKIE['is_hnn_mobile'])) {
            $isMobile = $_COOKIE['is_hnn_mobile'];
        } else {
            $mobileDetect = new My_Helper_MobileDetect();
            $isMobile = $mobileDetect->isMobile() ? 1 : 0;
            setcookie("is_hnn_mobile", $isMobile, time()+3600, '/');
        }

        if (isset($_GET['is_mobile_page']) && $_GET['is_mobile_page'] == 'iscnlmmobilepage') {
            $isMobile = 1;
        }

        $cache = My_Zend_Globals::getCaching();
        $cacheKey = '011020151configuration'. $isMobile;
        $loadIni = $cache->read($cacheKey);
        $loadIni = '';

        if (empty($loadIni)) {
            if ($isMobile) {
                self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/mobile/controllers', 'default');
                $default = new Zend_Controller_Router_Route(':controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'mobile'));
                $loadIni = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes_mobile.ini', 'routes');
            } else {
                self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/default/controllers', 'default');
                $default = new Zend_Controller_Router_Route(':controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'default'));
                $loadIni = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini', 'routes');
            }

            if (!empty($loadIni)) {
                // $cache->write($cacheKey, $loadIni, 99999999);
            }
        } else {
            if ($isMobile) {
                self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/mobile/controllers', 'default');
                $default = new Zend_Controller_Router_Route(':controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'mobile'));
            } else {
                self::$front->addControllerDirectory(APPLICATION_PATH . '/modules/default/controllers', 'default');
                $default = new Zend_Controller_Router_Route(':controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'default'));
            }
        }

        //Admin module
        $adminRouter = new Zend_Controller_Router_Route('admhnn01/:controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'adm'));
        //Admin reborn module
        $admin_rebornRouter = new Zend_Controller_Router_Route('adm_reborn/:controller/:action/*', array('controller' => 'index', 'action' => 'index', 'module' => 'adm_reborn'));

        //Add router
        $routers = self::$front->getRouter();
        $routers->addRoute('default', $default);

        // Add From ini
        $routers->addConfig($loadIni, 'routes');
        $routers->addRoute('admhnn01', $adminRouter);
        $routers->addRoute('adm_reborn', $admin_rebornRouter);

        //Set new router
        self::$front->setRouter($routers);

        //Cleanup
        unset($routers, $default, $adminRouter);
    }

    /**
     * Helper controller setup
     */
    protected function initControllerHelper() {
        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/modules/default/controllers/helpers');
        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH . '/modules/adm/controllers/helpers');
    }

    /**
     * Zend_Front_Controller created on each request
     */
    protected function initFrontController() {
        //Init frontController
        self::$front = Zend_Controller_Front::getInstance();

        //Set the current environment
        self::$front->setParam(ENVIRONMENT, APPLICATION_ENVIRONMENT);

        //Enable error controller plugin
        if (in_array(APPLICATION_ENVIRONMENT, array('development', 'development-nam', 'development-phuong','development-khanh'))) {
            self::$front->throwExceptions(true);
        } else {
            self::$front->throwExceptions(false);
        }

        //Set new router
        $this->initRouters();

        //Controller helpers
        $this->initControllerHelper();
    }

    /**
     * Plugin Front Init
     */
    protected function initFrontPlugin() {
        //Add Env Plugin
        self::$front->registerPlugin(new My_Zend_Plugin_Env());
        //self::$front->registerPlugin(new My_Zend_Plugin_SEO());
        self::$front->registerPlugin(new My_Zend_Plugin_Module());
    }

    /**
     * Run the application
     *
     * Checks to see that we have a default controller directory. If not, an
     * exception is thrown.
     *
     * If so, it registers the bootstrap with the 'bootstrap' parameter of
     * the front controller, and dispatches the front controller.
     *
     * @return void
     * @throws Zend_Exception
     */
    public function run() {
        //Init namspace
        $this->initNamespace();

        //Init Application Configuration
        $this->initAppConfiguration();

        //Init Logger
        //$this->initLogger();
        //Init Front Controller
        $this->initFrontController();

        //Check default module
        if (null === self::$front->getControllerDirectory(self::$front->getDefaultModule())) {
            throw new Zend_Exception('No default controller directory registered with front controller');
        }

        //Set controller plugin
        $this->initFrontPlugin();

        My_Zend_Globals::setDocType('XHTML1_TRANSITIONAL');
        My_Zend_Globals::setTitle('Ảnh hài, clip vui Hay Nhức Nhói - haynhucnhoi.tv');
        My_Zend_Globals::setMeta('keywords', 'ảnh hài, hay, clip, vui, hài hước, clip hot, ảnh chế, haivl, hài vl');
        My_Zend_Globals::setMeta('description', 'Cười xả láng mỗi ngày tại Hay Nhức Nhói với những hình ảnh hài hước, vui nhộn, ảnh động vui, clip hay, clip độc và clip cực hot');

        //Dispatch controller
        self::$front->setParam('bootstrap', $this);
        self::$front->dispatch();
        exit(0);
    }

}
