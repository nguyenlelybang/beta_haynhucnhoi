/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.103_3306
Source Server Version : 50173
Source Host           : 192.168.1.103:3306
Source Database       : haynhucnhoidb

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2015-05-14 12:20:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` smallint(5) NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL,
  `created_date` int(10) NOT NULL,
  `updated_date` int(10) unsigned NOT NULL,
  `last_login_date` int(10) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `page_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_active_home` tinyint(1) DEFAULT NULL,
  `is_grid` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`,`is_hidden`),
  KEY `parent_id_2` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for following
-- ----------------------------
DROP TABLE IF EXISTS `following`;
CREATE TABLE `following` (
  `following_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `follower_id` bigint(20) NOT NULL,
  `followed_id` bigint(20) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`following_id`),
  KEY `follower_id` (`follower_id`),
  KEY `followed_id` (`followed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for following_log
-- ----------------------------
DROP TABLE IF EXISTS `following_log`;
CREATE TABLE `following_log` (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(20) DEFAULT NULL,
  `follower_id` bigint(20) DEFAULT NULL,
  `followed_id` bigint(20) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(80) NOT NULL DEFAULT '',
  `username` varchar(80) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `salt` varchar(10) DEFAULT NULL,
  `fullname` varchar(200) NOT NULL DEFAULT '',
  `gender` varchar(6) NOT NULL DEFAULT '',
  `birthday` date NOT NULL,
  `description` text,
  `country` varchar(100) NOT NULL DEFAULT '',
  `posts` int(20) NOT NULL DEFAULT '0',
  `yourviewed` int(20) NOT NULL DEFAULT '0',
  `profileviews` int(20) NOT NULL DEFAULT '0',
  `youviewed` bigint(20) NOT NULL DEFAULT '0',
  `addtime` int(10) NOT NULL,
  `lastlogin` varchar(20) NOT NULL DEFAULT '',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `profilepicture` varchar(100) NOT NULL DEFAULT '',
  `remember_me_key` varchar(32) DEFAULT NULL,
  `remember_me_time` datetime DEFAULT NULL,
  `ip` varchar(20) NOT NULL,
  `lip` varchar(20) NOT NULL,
  `location` varchar(256) DEFAULT NULL,
  `website` varchar(200) NOT NULL,
  `news` int(1) NOT NULL DEFAULT '0',
  `theme` tinyint(2) DEFAULT '1',
  `mylang` varchar(20) NOT NULL,
  `color1` varchar(6) NOT NULL DEFAULT '000000',
  `color2` varchar(6) NOT NULL DEFAULT 'FFFFFF',
  `filter` text NOT NULL,
  `points` bigint(20) NOT NULL,
  `facebookid` varchar(20) NOT NULL,
  `like_count` int(11) NOT NULL DEFAULT '0',
  `comment_count` int(11) DEFAULT '0',
  `rank` int(11) DEFAULT '0',
  `post_count_week` int(20) NOT NULL DEFAULT '0',
  `post_count_month` int(20) NOT NULL DEFAULT '0',
  `like_count_week` int(20) NOT NULL DEFAULT '0',
  `like_count_month` int(20) NOT NULL DEFAULT '0',
  `notification` int(10) NOT NULL DEFAULT '0',
  `unread_notification` int(10) NOT NULL DEFAULT '0',
  `unread_notification_follow` int(11) DEFAULT '0',
  `unread_notification_message` int(11) DEFAULT '0',
  `follow_number` int(11) DEFAULT '0',
  `followed_number` int(11) DEFAULT '0',
  `approved_new_post` tinyint(1) NOT NULL DEFAULT '0',
  `new_post_to_home` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `facebookid` (`facebookid`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for members_passcode
-- ----------------------------
DROP TABLE IF EXISTS `members_passcode`;
CREATE TABLE `members_passcode` (
  `USERID` bigint(20) NOT NULL DEFAULT '0',
  `code` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for members_verifycode
-- ----------------------------
DROP TABLE IF EXISTS `members_verifycode`;
CREATE TABLE `members_verifycode` (
  `USERID` bigint(20) NOT NULL DEFAULT '0',
  `code` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for meme
-- ----------------------------
DROP TABLE IF EXISTS `meme`;
CREATE TABLE `meme` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for notification_detail
-- ----------------------------
DROP TABLE IF EXISTS `notification_detail`;
CREATE TABLE `notification_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `time` datetime NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(100) DEFAULT NULL,
  `itemID` bigint(20) DEFAULT NULL,
  `userID` int(10) NOT NULL DEFAULT '0',
  `followingID` bigint(20) DEFAULT NULL,
  `message` text,
  `like` bigint(20) DEFAULT '0',
  `isPublic` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `userID` (`userID`),
  KEY `followingID` (`followingID`),
  KEY `itemID` (`itemID`),
  KEY `type` (`type`,`itemID`,`like`)
) ENGINE=InnoDB AUTO_INCREMENT=94194 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for notification_stats
-- ----------------------------
DROP TABLE IF EXISTS `notification_stats`;
CREATE TABLE `notification_stats` (
  `user_id` int(10) unsigned NOT NULL,
  `unread_notification` int(10) unsigned NOT NULL DEFAULT '0',
  `unread_notification_follow` int(10) unsigned NOT NULL DEFAULT '0',
  `unread_notification_message` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `notification_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`notification_id`,`user_id`),
  KEY `userID` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `story` text NOT NULL,
  `tags` varchar(200) NOT NULL,
  `source` varchar(200) NOT NULL,
  `category_id` int(20) NOT NULL,
  `nsfw` int(1) NOT NULL DEFAULT '0',
  `is_photo` tinyint(1) DEFAULT '1',
  `is_gif` tinyint(1) NOT NULL DEFAULT '0',
  `picture` varchar(100) NOT NULL,
  `picture_share` varchar(255) NOT NULL,
  `youtube_key` varchar(20) DEFAULT NULL,
  `vmo_key` varchar(50) NOT NULL,
  `zingtv_key` varchar(50) DEFAULT '',
  `url` text NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `approved_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `time_sort` datetime DEFAULT NULL,
  `is_actived` tinyint(1) NOT NULL DEFAULT '1',
  `is_approved` tinyint(1) DEFAULT '0',
  `phase` bigint(1) NOT NULL DEFAULT '0',
  `favclicks` bigint(50) NOT NULL DEFAULT '0',
  `unfavclicks` bigint(50) NOT NULL DEFAULT '0',
  `last_viewed` varchar(20) NOT NULL DEFAULT '',
  `mod_yes` bigint(20) NOT NULL DEFAULT '0',
  `mod_no` bigint(20) NOT NULL DEFAULT '0',
  `pip` varchar(20) NOT NULL,
  `pip2` varchar(20) NOT NULL,
  `fix` bigint(20) NOT NULL DEFAULT '0',
  `short` varchar(20) NOT NULL,
  `dir` varchar(20) NOT NULL,
  `number_of_view` bigint(20) NOT NULL DEFAULT '1',
  `number_of_like` int(10) unsigned DEFAULT '0',
  `number_of_comment` int(10) unsigned DEFAULT '0',
  `number_of_share` int(11) DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `is_grid` tinyint(1) NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `USERID` (`user_id`),
  KEY `phase` (`phase`),
  KEY `CID` (`category_id`),
  KEY `is_actived` (`is_actived`,`is_approved`,`phase`)
) ENGINE=InnoDB AUTO_INCREMENT=25221 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_advertisment
-- ----------------------------
DROP TABLE IF EXISTS `posts_advertisment`;
CREATE TABLE `posts_advertisment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` tinytext,
  `images` tinytext,
  `pos` varchar(20) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_chap
-- ----------------------------
DROP TABLE IF EXISTS `posts_chap`;
CREATE TABLE `posts_chap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `youtube_id` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `url` tinytext,
  `chap_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for posts_favorited
-- ----------------------------
DROP TABLE IF EXISTS `posts_favorited`;
CREATE TABLE `posts_favorited` (
  `fid` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(25) NOT NULL DEFAULT '0',
  `post_id` bigint(25) NOT NULL DEFAULT '0',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fid`),
  UNIQUE KEY `user_id` (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_read
-- ----------------------------
DROP TABLE IF EXISTS `posts_read`;
CREATE TABLE `posts_read` (
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `post_ids` text,
  `time_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_reports
-- ----------------------------
DROP TABLE IF EXISTS `posts_reports`;
CREATE TABLE `posts_reports` (
  `report_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `ip` varchar(20) NOT NULL,
  `reason` bigint(1) NOT NULL,
  `reason_detail` text,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_tags
-- ----------------------------
DROP TABLE IF EXISTS `posts_tags`;
CREATE TABLE `posts_tags` (
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `post_id` (`post_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for posts_unfavorited
-- ----------------------------
DROP TABLE IF EXISTS `posts_unfavorited`;
CREATE TABLE `posts_unfavorited` (
  `fid` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(25) NOT NULL DEFAULT '0',
  `post_id` bigint(25) NOT NULL DEFAULT '0',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fid`),
  UNIQUE KEY `user_id` (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for posts_views_daily
-- ----------------------------
DROP TABLE IF EXISTS `posts_views_daily`;
CREATE TABLE `posts_views_daily` (
  `date` date NOT NULL,
  `data` text,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for quote
-- ----------------------------
DROP TABLE IF EXISTS `quote`;
CREATE TABLE `quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `roles_permissions`;
CREATE TABLE `roles_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for seo
-- ----------------------------
DROP TABLE IF EXISTS `seo`;
CREATE TABLE `seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext CHARACTER SET utf8,
  `metatitle` tinytext CHARACTER SET utf8,
  `keyword` tinytext CHARACTER SET utf8,
  `description` tinytext CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for static_pages
-- ----------------------------
DROP TABLE IF EXISTS `static_pages`;
CREATE TABLE `static_pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ttl` smallint(3) unsigned NOT NULL DEFAULT '1',
  `last_updated` int(10) unsigned NOT NULL DEFAULT '0',
  `status` smallint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2769 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_name_ascii` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_tag_id` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_alias` (`tag_alias`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
