------------------Thay đổi ngày  05/04/2015 ----------------------------------------------
DROP TABLE IF EXISTS `posts_chap`;
CREATE TABLE `posts_chap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `youtube_id` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `url` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `posts` ADD `is_grid` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_featured`;
ALTER TABLE `categories` ADD `is_active_home` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ordering`;
ALTER TABLE `categories` ADD `is_grid` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_active_home`;