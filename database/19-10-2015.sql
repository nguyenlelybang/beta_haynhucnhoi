ALTER TABLE `posts` ADD COLUMN `description` TEXT CHARSET utf8 COLLATE utf8_general_ci NULL AFTER `story`;
ALTER TABLE `categories` ADD COLUMN `description` TEXT CHARSET utf8 COLLATE utf8_unicode_ci NULL AFTER `category_name`;
ALTER TABLE `posts_chap` ADD COLUMN `chap_description` TEXT CHARSET utf8 COLLATE utf8_general_ci NULL AFTER `chap_title`;
ALTER TABLE `posts_chap` ADD COLUMN `is_active` BOOLEAN DEFAULT 1 NOT NULL AFTER `chap_description`;
