CREATE TABLE `members_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type_point` varchar(20) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `time_point_datetime` datetime DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `msg` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;