<?php

//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT . '/../../application'));
define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../../data'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR, array(
    LIBS_PATH,
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENVIRONMENT);
Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/models/',
    get_include_path()
)));

//$listPage = array('index', 'new', 'hot', 'clip-hai', 'photo/16619', 'photo/16761', 'photo/16914');

$listPage = StaticPage::getList(array('status' => 1));

if (empty($listPage)) {
    echo "None static pages exist";
    exit();
}

$domain = APPLICATION_ENVIRONMENT == 'production' ? 'http://haynhucnhoi.tv' : 'http://local-v2.haynhucnhoi.com';

foreach ($listPage as $page) {
    if (($page['last_updated'] + $page['ttl'] * 60) > time()) {
        continue;;
    }
    $uri = $page['uri'];

    if (strpos($uri, '?') !== false) {
        $uri = $uri . '&';
    } else {
        $uri = $uri . '?';
    }

    $uri .= 'build_static_page=1&static_page_name=' . $page['file_name'] . '.html';

    try {
        $url = $domain . $uri;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        $response = curl_getinfo($curl);
        curl_close($curl);

        if ($response['http_code'] == 200) {
            echo "Create static page (" . $url . ") thanh cong \n";
        } else {
            echo "Create static page (" . $url . ") that bai (http_code:" . $response['http_code'] . ") \n";
        }
    } catch (Exception $e) {
        echo "Exception - " . $e->getMessage() . '\n';
    }
}

// build for mobile page
foreach ($listPage as $page) {
    if (($page['last_updated'] + $page['ttl'] * 60) > time()) {
        continue;
    }

    $uri = $page['uri'];

    if (strpos($uri, '?') !== false) {
        $uri = $uri . '&';
    } else {
        $uri = $uri . '?';
    }

    $uri .= 'build_static_page=1&static_page_name=' . $page['file_name'] . '.html&is_mobile_page=iscnlmmobilepage';

    try {
        $url = $domain . $uri;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        $response = curl_getinfo($curl);
        curl_close($curl);

        if ($response['http_code'] == 200) {
            echo "Create static page (" . $url . ") thanh cong \n";
        } else {
            echo "Create static page (" . $url . ") that bai (http_code:" . $response['http_code'] . ") \n";
        }
    } catch (Exception $e) {
        echo "Exception - " . $e->getMessage() . '\n';
    }

    // update data
    $data = array(
        'id' => $page['id'],
        'last_updated' => time()
    );
    StaticPage::update($data);
}