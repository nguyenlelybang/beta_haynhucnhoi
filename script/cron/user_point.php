<?php
//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT.'/../../application'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR,array(
    LIBS_PATH,
    get_include_path()
)));

set_include_path(implode(PATH_SEPARATOR,array(
    APPLICATION_PATH.'/models',
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',APPLICATION_ENVIRONMENT);
Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

     $data_week     = array();
     $data_month    = array();
     $data_all      = array();
     $storage   = My_Zend_Globals::getStorage();
     $table     = MembersPoint::TABLE_NAME;
     $select    = $storage->select()
         ->from($table,array('points'=>'SUM(points)', 'user_id'))
         ->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)')
         ->group('user_id');
     $data_week = $storage->fetchAll($select);
     if(!empty($data_week))
     {
         foreach($data_week as $key=>$value)
         {
             $arrData  = array('user_id'=>$value['user_id'],'week_point'=>$value['points']);
             User::updateUser($arrData);
             echo "\n\n===================".$value['user_id']." : =====".$value['points']."=================== \n ";
         }
         echo "\n\n===================7 DAY======================== \n ";
     }


     $select = $storage->select()
         ->from($table,array('points'=>'SUM(points)', 'user_id'))
         ->where(' time_point_datetime >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ')
         ->group('user_id');
     $data_month = $storage->fetchAll($select);
     if(!empty($data_month))
     {
         foreach($data_month as $key=>$value)
         {
             $arrData  = array('user_id'=>$value['user_id'],'month_point'=>$value['points']);
             User::updateUser($arrData);
             echo "\n\n===================".$value['user_id']." : =====".$value['points']."=================== \n ";
         }
         echo "\n\n===================7 MONTHS======================== \n ";
     }


     $select = $storage->select()
         ->from($table,array('points'=>'SUM(points)', 'user_id'))
         ->group('user_id');
     $data_all = $storage->fetchAll($select);
     if(!empty($data_all))
     {
         foreach($data_all as $key=>$value)
         {
             $arrData  = array('user_id'=>$value['user_id'],'all_point'=>$value['points']);
             User::updateUser($arrData);
             echo "\n\n===================".$value['user_id']." : =====".$value['points']."=================== \n ";
         }
         echo "\n\n===================ALL======================== \n ";
     }

echo "===== DONE =====";
exit;