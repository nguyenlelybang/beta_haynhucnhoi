<?php

//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT . '/../../application'));
define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../../data'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));

// Define SITEMAP PATH
define('LIVE_SITE', 'http://haynhucnhoi.tv');
define('SITE_ROOT', realpath(DOCUMENT_ROOT . '/../../public_html'));
define('SITEMAP_DIR', 'sitemap');
define('BS', '/');
define('LIMIT', 5000);
define('GZ_ENABLE', false);
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR, array(
    LIBS_PATH,
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENVIRONMENT);

Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/models',
    get_include_path()
)));

$storage = My_Zend_Globals::getStorage();

function buildListId($phase, $getVideo = 0)
{
    global $storage;

    $select = $storage->select()
                        ->from(Post::_TABLE, 'post_id')
                        ->where('is_actived = 1')
                        ->where('is_approved = 1');

    if ($phase == 0) {
        $select->where('phase = ?', $phase);
        $select->order('approved_at desc');
    } else {
        $select->where('phase >= ?', $phase);
        $select->order('updated_at desc');
    }

    if ($getVideo) {
        $select->where('is_photo = 0');
    }

    $select->limit(100, 0);

    $data = $storage->fetchAll($select);

    if (!empty($data)) {
        $tmp = array();
        foreach ($data as $post) {
            $tmp[] = $post['post_id'];
        }
        $data = $tmp;
        unset($tmp);
        $cache = My_Zend_Globals::getCaching();
        $cache->write(Post::_cache_key_active_list . $phase .'_'. $getVideo, $data, 900);
    }
}

echo "\n\nbuild list home page \n";
buildListId(Post::_PHASE_HOMEPAGE);
echo "build list hot \n";
buildListId(Post::_PHASE_HOT);
echo "build list new post \n";
buildListId(Post::_PHASE_NEW_POST);
echo "build list hot video \n";
buildListId(Post::_PHASE_HOT, 1);