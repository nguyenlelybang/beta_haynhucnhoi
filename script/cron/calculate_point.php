<?php
//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT.'/../../application'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR,array(
	LIBS_PATH,
	get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines_cli.php';

//Load Autoloader
require_once LIBS_PATH . '/Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration

$configuration = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',APPLICATION_ENVIRONMENT);
Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

set_include_path(implode(PATH_SEPARATOR, array(
	APPLICATION_PATH . '/models/',
	get_include_path()
)));

$point = new UserPoint();
$point->calculateAllUserPoint();
$point->updateUserPostCount();
$point->updateUserRank();

exit;