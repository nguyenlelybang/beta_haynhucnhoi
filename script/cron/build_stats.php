<?php
//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT.'/../../application'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR,array(
    LIBS_PATH,
    get_include_path()
)));

set_include_path(implode(PATH_SEPARATOR,array(
    APPLICATION_PATH.'/models',
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',APPLICATION_ENVIRONMENT);
Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

function reset_stats()
{
    $storage = My_Zend_Globals::getStorage();
    $sql = 'UPDATE '. User::_TABLE_USER ." SET post_count_month = 0, like_count_month = 0, like_count_week = 0, post_count_week=0";
    $storage->query($sql);
}

function build_stats($time = 'week', $type = 'post_id')
{
    switch ($time) {
        case 'month':
            $numberOfDate = 30;
            switch ($type) {
                case 'number_of_like':
                    $queryField = 'SUM(number_of_like)';
                    $field = 'like_count_month';
                    break;
                case 'post_id':
                    $queryField = 'count(post_id)';
                    $field = 'post_count_month';
                default:
                    return;
                    break;
            }
            break;
        case 'week':
            $numberOfDate = 7;
            switch ($type) {
                case 'number_of_like':
                    $queryField = 'SUM(number_of_like)';
                    $field = 'like_count_week';
                    break;
                case 'post_id':
                    $queryField = 'count(post_id)';
                    $field = 'post_count_week';
                default:
                    return;
                    break;
            }
            break;
        case 'all':
            $numberOfDate = 0;
            switch ($type) {
                case 'number_of_like':
                    $queryField = 'SUM(number_of_like)';
                    $field = 'like_count';
                    break;
                case 'post_id':
                    $queryField = 'count(post_id)';
                    $field = 'post_count';
                default:
                    return;
                    break;
            }
            break;
        default:
            return;
    }

    $storage = My_Zend_Globals::getStorage();

    $select = $storage->select()
                        ->from(Post::_TABLE, array('user_id', 'total' => $queryField))
                        ->where('is_approved = 1')
                        ->where('is_actived = 1')
                        ->group('user_id');

    if ($numberOfDate > 0) {
        $select->where('created_at >= ?', (time() - $numberOfDate * 24 * 3600));
    }

    $result = $storage->fetchAll($select);

    if (!empty($result)) {
        foreach ($result as $item) {
            $data = array(
                'user_id' => $item['user_id'],
                $field  => $item['total']
            );

            if (User::updateUser($data)) {
                echo "Update stats for user id ". $item['user_id'] ." successful \n";
            } else {
                echo "Update stats for user id ". $item['user_id'] ." fail \n";
            }
        }
    }
}

$types = array('week', 'month');

echo "\n\n============================================";
echo "\n========== BUILD POST COUNT STATS (". date('H:i d/m/Y') .") ==========";
echo "\n============================================";
echo "\n === RESET DATA ==== ";
reset_stats();
foreach ($types as $time) {
    echo "\n===== BUILD STATS ". strtoupper($time) ." =========\n";
    build_stats($time, 'post_id');
}

echo "\n\n============================================";
echo "\n========== BUILD LIKE COUNT STATS ==========";
echo "\n============================================";

foreach ($types as $time) {
    echo "\n===== BUILD STATS ". strtoupper($time) ." =========\n";
    build_stats($time, 'number_of_like');
}

echo "\n\n============================================";
echo "\n========== BUILD LIKE COUNT STATS FOR ALL MEMBERS ==========";
echo "\n============================================";
build_stats('all', 'number_of_like');

echo "===== DONE =====";
exit;