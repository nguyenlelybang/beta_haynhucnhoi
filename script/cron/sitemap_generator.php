<?php

//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT . '/../../application'));
define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../../data'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));

// Define SITEMAP PATH
define('LIVE_SITE', 'http://haynhucnhoi.tv');
define('BASE_URL', 'http://haynhucnhoi.tv');
define('SITE_ROOT', realpath(DOCUMENT_ROOT . '/../../public_html'));
define('SITEMAP_DIR', 'sitemap');
define('BS', '/');
define('LIMIT', 5000);
define('GZ_ENABLE', false);
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR, array(
    LIBS_PATH,
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENVIRONMENT);

Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/models',
    get_include_path()
)));

// BUILD SITE MAP INDEX$
$siteMapIndex = array();
$categorySiteMap = '';

echo "build article ... <br/>\n";
$table = Post::_TABLE;

$storage = My_Zend_Globals::getStorage();
$select = $storage->select()
        ->from($table, 'count(post_id) as total')
        ->where('is_actived = 1 AND is_approved = 1');

$data = $storage->fetchRow($select);
$total = $data['total'];
$offset = 0;
$limit = 1000;
$count = 0;
$totalPage = ceil($total / LIMIT);

echo "Total record: $total - Total part: $totalPage \n";

if ($total > 0) {
    while ($offset < $total) {
        if ($offset % LIMIT == 0 || $count == ($totalPage - 1)) {
            $xmlString = '';
            $xmlString .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            $xmlString .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:news=\"http://www.google.com/schemas/sitemap-news/0.9\" xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n";
        }

        $select = $storage->select()
                ->from($table, '*')
                ->where('is_actived = 1 AND is_approved = 1')
                ->order('approved_at desc')
                ->limit(LIMIT, $offset);

        $posts = $storage->fetchAll($select);

        foreach ($posts as $post) {
            // Content
            $xmlString .= "\t<url>\n";
            $xmlString .= "\t\t<loc>" . Post::postUrl($post, true) . "</loc>\n";
//            $pos = strpos($post['picture'], 'https');
//            if ($pos !== false) {
//                $xmlString .= "\t\t<image:image>\n";
//                $xmlString .= "\t\t\t<image:loc>".$post['picture']."</image:loc>\n";
//                $xmlString .= "\t\t</image:image>\n";
//            }else
//            {
//                $config = My_Zend_Globals::getConfiguration();
//                $url_image =$config->photo->domain.$post['picture'];
//                $xmlString .= "\t\t<image:image>\n";
//                $xmlString .= "\t\t\t<image:loc>".$url_image."</image:loc>\n";
//                $xmlString .= "\t\t</image:image>\n";
//            }

            $xmlString .= "\t\t<priority>1</priority>\n";
            $xmlString .= "\t\t<lastmod>" . date('c', $post['updated_at']) . "</lastmod>\n";
            $xmlString .= "\t\t<changefreq>hourly</changefreq>\n";
            $xmlString .= "\t</url>\n";
        }

        $offset += LIMIT;

        if ($offset % LIMIT == 0 || $count == ($totalPage - 1)) {
            // End
            $xmlString .= '</urlset>';

            $fileName = 'sitemap_' . $count . '.xml';

            // Create file
            if (GZ_ENABLE) {
                $fileName .= '.gz';
            }

            // Write to gz
            $ok = writeToGz($fileName, SITE_ROOT . BS . SITEMAP_DIR, $xmlString);

            if ($ok) {
                $siteMapIndex[] = BASE_URL . BS . SITEMAP_DIR . BS . $fileName;

                echo "[OK] $fileName\n";
            }

            $count++;

            // restart connection
            My_Zend_Globals::closeAllStorage();
            $storage = My_Zend_Globals::getStorage();
        }
    }
}

My_Zend_Globals::closeAllStorage();
echo "build categories ... <br/>\n";
/** sitemap for categories * */
$storage = My_Zend_Globals::getStorage();
$fileName = 'sitemap_categories.xml';
$xmlString = '';
$xmlString .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$xmlString .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:news=\"http://www.google.com/schemas/sitemap-news/0.9\" xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n";

$queryTag = $storage->select()
        ->from(Category::table_category, '*');

$categories = $storage->fetchAll($queryTag);

if (!empty($categories)) {
    foreach ($categories as $category) {
        $xmlString .= "\t<url>\n";
        $xmlString .= "\t\t<loc>" . Category::categoryUrl($category, true) . "</loc>\n";
        $xmlString .= "\t\t<priority>1</priority>\n";
        $xmlString .= "\t\t<lastmod>" . date('c') . "</lastmod>\n";
        $xmlString .= "\t\t<changefreq>hourly</changefreq>\n";
        $xmlString .= "\t</url>\n";
    }
}

$xmlString .= '</urlset>';

$ok = writeToGz($fileName, SITE_ROOT . BS . SITEMAP_DIR, $xmlString);

if ($ok) {
    $siteMapIndex[] = LIVE_SITE . BS . SITEMAP_DIR . BS . $fileName;
    echo "[OK] $fileName\n";
}
/** End site map for categories */
$siteMap = '';
echo "create files ... <br/>\n";
// Create sitemap index
if (sizeof($siteMapIndex) > 0) {

    // Create update date
    $updatedDate = date('c');

    // Begin
    $siteMap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $siteMap .= "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

    foreach ($siteMapIndex as $link) {

        $siteMap .= "\t<sitemap>\n";
        $siteMap .= "\t\t<loc>{$link}</loc>\n";
        $siteMap .= "\t\t<lastmod>{$updatedDate}</lastmod>\n";
        $siteMap .= "\t</sitemap>\n";
    }

    // End
    $siteMap .= "</sitemapindex>\n";

    // Write to sitemap index
    $fileName = SITE_ROOT . BS . SITEMAP_DIR . BS . 'sitemap.xml';
    $fh = fopen($fileName, 'w');
    if ($fh) {

        fwrite($fh, $siteMap);

        // Close
        fclose($fh);
    }
}

echo "[OK] $fileName\n";

// create sitemap for tag
// Clean
unset($siteMapIndex, $fh, $siteMap);


## FUNCTION ##

function writeToGz($fileName, $pathDir, $data) {
    if (empty($fileName) || empty($pathDir) || empty($data))
        return false;

    $isCompleted = true;

    if (file_exists($pathDir . BS . $fileName)) {

        // Delete
        unlink($pathDir . BS . $fileName);
    } else {

        // Create file
        touch($pathDir . BS . $fileName);
    }

    if (GZ_ENABLE) {
        // Write data
        $gz = gzopen($pathDir . BS . $fileName, 'w9');

        if ($gz) {

            gzwrite($gz, $data);
            gzclose($gz);
        } else {

            $isCompleted = false;
        }
    } else {

        $fh = fopen($pathDir . BS . $fileName, 'w');
        if ($fh) {
            fwrite($fh, $data);
            fclose($fh);
        } else {

            $isCompleted = false;
        }
    }

    // Unset
    unset($data);

    // Return
    return $isCompleted;
}
