<?php

//Setup Include Paths
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT . '/../../application'));
define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../../data'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../../library'));
define('PAGES_PATH', realpath(DOCUMENT_ROOT . '/../../public/static_pages'));
define('DEBUG_MODE', false);

//Setup Include Paths
set_include_path(implode(PATH_SEPARATOR, array(
    LIBS_PATH,
    get_include_path()
)));

//Load bootstrap
require_once APPLICATION_PATH . '/configs/defines.php';

//Load Autoloader
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

//Configuration
$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENVIRONMENT);
Zend_Registry::set(APPLICATION_CONFIGURATION, $configuration);

set_include_path(implode(PATH_SEPARATOR, array(
    APPLICATION_PATH . '/models/',
    get_include_path()
)));

//$listPage = array('index', 'new', 'hot', 'clip-hai', 'photo/16619', 'photo/16761', 'photo/16914');

$maxPostId = Post::getMaxPostId();

if ($maxPostId == 0) {
    echo "cannot connect database";
    exit();
}
$listPage = StaticPage::getList(array('is_post' => 1));

if (empty($listPage)) {
    echo "None static pages exist";
    exit();
}

$domain = APPLICATION_ENVIRONMENT == 'production' ? 'http://haynhucnhoi.tv' : 'http://local-v2.haynhucnhoi.com';

foreach ($listPage as $page) {
    if ($page['post_id'] > 0 && ($maxPostId - $page['post_id']) > 1000 ) {
        if (StaticPage::delete($page['id'])) {
            echo "Delete post ID: ". $page['post_id'] . "\n";
        }
    }
}