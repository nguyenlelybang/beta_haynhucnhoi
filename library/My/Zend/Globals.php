<?php

class My_Zend_Globals {

    /**
     * Logger instance
     */
    private static $logger = null;

    /**
     * Configuration instance
     */
    public static $configuration = null;

    /**
     * Storage instance
     * @var Zend_Db
     */
    private static $storage = null;

    /**
     * cache instance
     * @var Zend_Cache
     */
    private static $caching = null;

    /**
     *
     * Mail instance
     * @var Zend_Mail
     */
    private static $mail = null;

    /**
     * Solr instance
     * @var array Apache_Solr_Service
     */
    private static $solrList = null;

    /**
     * Job client
     * @var My_Zend_Job_Adapter_Gearman_Client
     */
    private static $jobClient = null;

    /**
     * Job function
     * @var string
     */
    public static $jobFunction = '';

    /**
     * List storage
     */
    private static $arrStorage = array();

    /**
     * List solr
     */
    private static $arrSorl = array();

    /**
     * List mail
     */
    private static $arrMail = array();

    /**
     * Constructor
     *
     */
    private final function __construct() {

    }

    /**
     * Clone function
     *
     */
    private final function __clone() {

    }

    /**
     * Get job client
     */
    public static function getJobClient() {
        //Get Ini Configuration
        My_Zend_Globals::getConfiguration();

        //Get caching instance
        if (is_null(self::$jobClient)) {
            self::$jobClient = My_Zend_JobClient::getInstance(self::$configuration->job->toArray());
        }

        //Set job function
        self::$jobFunction = self::$configuration->job->function;

        //Return caching
        return self::$jobClient;
    }

    /**
     * Get caching instance
     */
    public static function getCaching($idx = 1) {
        //Get Ini Configuration
        My_Zend_Globals::getConfiguration();
        //Get caching instance
        if (is_null(self::$caching[$idx])) {
            self::$caching[$idx] = My_Zend_Cache::getInstance(self::$configuration->caching->$idx->toArray());
        }

        //Return caching
        return self::$caching[$idx];
    }

    /**
     * Get configuration instance
     */
    public static function getConfiguration() {
        //Get Ini Configuration
        if (is_null(self::$configuration)) {
            self::$configuration = Zend_Registry::get(APPLICATION_CONFIGURATION);
        }
        return self::$configuration;
    }

    /**
     * Get solr instance
     */
    public static function &getSolr($idex = 5) {
        require_once( 'Apache/Solr/Service.php' );

        //Get Ini Configuration
        My_Zend_Globals::getConfiguration();

        //Get caching instance
        if (is_null(self::$solrList[$idex])) {
            self::$solrList[$idex] = new Apache_Solr_Service(self::$configuration->solr->$idex->host, self::$configuration->solr->$idex->port, self::$configuration->solr->$idex->admin);
        }

        //Solr instance
        $solrInstance = self::$solrList[$idex];

        //Put queue
        self::$arrSorl[] = $solrInstance;

        //Return caching
        return $solrInstance;
    }

    /**
     * Get storage instance
     * @param string $adapter
     */
    public static function &getStorage($adapter = 'mysql') {
        //Get Ini Configuration
        My_Zend_Globals::getConfiguration();

        //Get storage instance
        if (is_null(self::$storage)) {
            switch (strtolower($adapter)) {
                case 'mysql':
                    //Set UTF-8 Collate and Connection
                    $options_storage = self::$configuration->storage->mysql->toArray();
                    $options_storage['params']['driver_options'] = array(
                        MYSQLI_INIT_COMMAND => 'SET NAMES utf8;'
                    );

                    //Create object to Connect DB
                    self::$storage = Zend_Db::factory($options_storage['adapter'], $options_storage['params']);

                    //Changing the Fetch Mode
                    self::$storage->setFetchMode(Zend_Db::FETCH_ASSOC);

                    //Create Adapter default is Db_Table
                    Zend_Db_Table::setDefaultAdapter(self::$storage);

                    // Set profiler
                    if (DEBUG_MODE == true) {
                        self::$storage->getProfiler()->setEnabled(true);
                    } else {
                        self::$storage->getProfiler()->setEnabled(false);
                    }

                    //Unclean
                    unset($options_storage);
                    break;
                default:
                    throw new Exception('No adapter configuration for business !!!');
                    break;
            }
        }

        //Push to queue
        self::$arrStorage[] = self::$storage;

        //Return Db
        return self::$storage;
    }

    /**
     * Get mail instance
     */
    public static function &getMail() {
        //Get Ini Configuration
        My_Zend_Globals::getConfiguration();

        //Get storage instance
        if (is_null(self::$mail)) {
            $smtp = self::$configuration->mail->smtp->toArray();
            $config = array('auth' => 'login',
                'username' => $smtp['username'],
                'password' => $smtp['password'],
                'port' => $smtp['port']);

            $transport = new Zend_Mail_Transport_Smtp($smtp['host'], $config);
            Zend_Mail::setDefaultTransport($transport);

            //Create object mail
            self::$mail = new Zend_Mail();

            //Unset variable
            unset($transport);
            unset($config);
            unset($smtp);
        }

        //Push to queue
        self::$arrMail[] = self::$mail;

        //Return Mail
        return self::$mail;
    }

    /**
     * Close all connection
     */
    public function closeAllStorage() {
        foreach (self::$arrStorage as $storage) {
            //Try close
            if (!is_null($storage)) {
                $storage->closeConnection();
            }
        }
        return true;
    }

    /**
     * Close all object mail
     */
    public function closeAllMail() {
        foreach (self::$arrMail as $mailInstance) {
            //Try close
            if (!is_null($mailInstance)) {
                unset($mailInstance);
            }
        }
        return true;
    }

    /**
     * Dump logger
     * @param string $content
     */
    public static function dumpLogger($content, $br = false) {
        //Check content logger
        if (empty($content)) {
            return false;
        }

        //Check instance
        if (!isset(self::$logger)) {
            self::$logger = Zend_Registry::get(LOGGER_DUMP);
        }

        //Write dump
        self::$logger->log($content, $br);
    }

    /**
     * Send logger to scribe
     * @param int $meid
     * @param int $actionid
     * @param string $logdata
     */
    public static function scribeLogger($meid, $actionid, $logdata) {
        return My_Zend_Scribe::sendLog($meid, $actionid, $logdata);
    }

    /**
     * Gen key private
     * @param <string> $string
     * @param <string> $keyphrase
     * @return <string>
     */
    public static function keyED($string, $keyphrase) {
        $keyphraseLength = strlen($keyphrase);
        $stringLength = strlen($string);
        for ($i = 0; $i < $stringLength; $i++) {
            $rPos = $i % $keyphraseLength;
            $r = ord($string[$i]) ^ ord($keyphrase[$rPos]);
            $string[$i] = chr($r);
        }
        return $string;
    }

    /**
     * encrypt string
     * @param <string> $string
     * @param <string> $keyphrase
     * @return <string>
     */
    public static function encode($string, $keyphrase = 'HAeCwcaAhdm') {
        $string = self::keyED($string, $keyphrase);
        $string = base64_encode($string);
        return $string;
    }

    /**
     * decrypt string
     * @param <string> $string
     * @param <string> $keyphrase
     * @return <string>
     */
    public static function decode($string, $keyphrase = 'HAeCwcaAhdm') {
        $string = base64_decode($string);
        $string = self::keyED($string, $keyphrase);
        return $string;
    }

    /**
     * Fetches an alternate IP address of the current visitor
     * attempting to detect proxies etc.
     */
    public static function getAltIp() {
        $alt_ip = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $alt_ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
            // make sure we dont pick up an internal IP defined by RFC1918
            foreach ($matches[0] AS $ip) {
                if (!preg_match('#^(10|172\.16|192\.168)\.#', $ip)) {
                    $alt_ip = $ip;
                    break;
                }
            }
        } elseif (isset($_SERVER['HTTP_FROM'])) {
            $alt_ip = $_SERVER['HTTP_FROM'];
        }

        return $alt_ip;
    }

    /**
     * Removes HTML characters and potentially unsafe scripting words from a string
     * @param string $message
     */
    public static function xssClean($message) {
        //If empty
        if (empty($message)) {
            return '';
        }

        //Fix & but allow unicode
        $message = preg_replace('#&(?!\#[0-9]+;)#si', '', $message);
        $message = str_replace("<", "", $message);
        $message = str_replace(">", "", $message);
        //$message = str_replace("\"", "", $message);
        //Removes CSS
        static $preg_find = array('#javascript#i', '#vbscript#i');
        static $preg_replace = array('java script', 'vb script');
        return preg_replace($preg_find, $preg_replace, $message);
    }

    /**
     * Convert UTF8 to ASCII string
     * @param $tring
     * @param $bit
     * @return string
     */
    public static function utf8ToAscii($tring, $bit = '-') {
        $utf8 = array(
            'ấ' => 'a',
            'ầ' => 'a',
            'ẩ' => 'a',
            'ẫ' => 'a',
            'ậ' => 'a',
            'Ấ' => 'a',
            'Ầ' => 'a',
            'Ẩ' => 'a',
            'Ẫ' => 'a',
            'Ậ' => 'a',
            'ắ' => 'a',
            'ằ' => 'a',
            'ẳ' => 'a',
            'ẵ' => 'a',
            'ặ' => 'a',
            'Ắ' => 'a',
            'Ằ' => 'a',
            'Ẳ' => 'a',
            'Ẵ' => 'a',
            'Ặ' => 'a',
            'á' => 'a',
            'à' => 'a',
            'ả' => 'a',
            'ã' => 'a',
            'ạ' => 'a',
            'â' => 'a',
            'ă' => 'a',
            'Á' => 'a',
            'À' => 'a',
            'Ả' => 'a',
            'Ã' => 'a',
            'Ạ' => 'a',
            'Â' => 'a',
            'Ă' => 'a',
            'ế' => 'e',
            'ề' => 'e',
            'ể' => 'e',
            'ễ' => 'e',
            'ệ' => 'e',
            'Ế' => 'e',
            'Ề' => 'e',
            'Ể' => 'e',
            'Ễ' => 'e',
            'Ệ' => 'e',
            'é' => 'e',
            'è' => 'e',
            'ẻ' => 'e',
            'ẽ' => 'e',
            'ẹ' => 'e',
            'ê' => 'e',
            'É' => 'e',
            'È' => 'e',
            'Ẻ' => 'e',
            'Ẽ' => 'e',
            'Ẹ' => 'e',
            'Ê' => 'e',
            'í' => 'i',
            'ì' => 'i',
            'ỉ' => 'i',
            'ĩ' => 'i',
            'ị' => 'i',
            'Í' => 'i',
            'Ì' => 'i',
            'Ỉ' => 'i',
            'Ĩ' => 'i',
            'Ị' => 'i',
            'ố' => 'o',
            'ồ' => 'o',
            'ổ' => 'o',
            'ỗ' => 'o',
            'ộ' => 'o',
            'Ố' => 'o',
            'Ồ' => 'o',
            'Ổ' => 'o',
            'Ô' => 'o',
            'Ộ' => 'o',
            'ớ' => 'o',
            'ờ' => 'o',
            'ở' => 'o',
            'ỡ' => 'o',
            'ợ' => 'o',
            'Ớ' => 'o',
            'Ờ' => 'o',
            'Ở' => 'o',
            'Ỡ' => 'o',
            'Ợ' => 'o',
            'ó' => 'o',
            'ò' => 'o',
            'ỏ' => 'o',
            'õ' => 'o',
            'ọ' => 'o',
            'ô' => 'o',
            'ơ' => 'o',
            'Ó' => 'o',
            'Ò' => 'o',
            'Ỏ' => 'o',
            'Õ' => 'o',
            'Ọ' => 'o',
            'Ô' => 'o',
            'Ơ' => 'o',
            'ứ' => 'u',
            'ừ' => 'u',
            'ử' => 'u',
            'ữ' => 'u',
            'ự' => 'u',
            'Ứ' => 'u',
            'Ừ' => 'u',
            'Ử' => 'u',
            'Ữ' => 'u',
            'Ự' => 'u',
            'ú' => 'u',
            'ù' => 'u',
            'ủ' => 'u',
            'ũ' => 'u',
            'ụ' => 'u',
            'ư' => 'u',
            'Ú' => 'u',
            'Ù' => 'u',
            'Ủ' => 'u',
            'Ũ' => 'u',
            'Ụ' => 'u',
            'Ư' => 'u',
            'ý' => 'y',
            'ỳ' => 'y',
            'ỷ' => 'y',
            'ỹ' => 'y',
            'ỵ' => 'y',
            'Ý' => 'y',
            'Ỳ' => 'y',
            'Ỷ' => 'y',
            'Ỹ' => 'y',
            'Ỵ' => 'y',
            'đ' => 'd',
            'Đ' => 'd',
            ' ' => $bit,
            '/' => $bit,
            '&' => $bit,
            '.' => $bit,
            '---' => $bit,
            '--' => $bit,
            '%20' => $bit
        );
        return str_replace(array_keys($utf8), array_values($utf8), $tring);
    }

    /**
     * Convert Iso88591 to Utf8 string
     * @param string $tring
     * @return string
     */
    public static function iso88591ToUtf8($tring) {
        $utf8 = array(
            'ấ' => '&#7845;',
            'ầ' => '&#7847;',
            'ẩ' => '&#7849;',
            'ẫ' => '&#7851;',
            'ậ' => '&#7853;',
            'Ấ' => '&#7844;',
            'Ầ' => '&#7846;',
            'Ẩ' => '&#7848;',
            'Ẫ' => '&#7850;',
            'Ậ' => '&#7852;',
            'ắ' => '&#7855;',
            'ằ' => '&#7857;',
            'ẳ' => '&#7859;',
            'ẵ' => '&#7861;',
            'ặ' => '&#7863;',
            'Ắ' => '&#7854;',
            'Ằ' => '&#7856;',
            'Ẳ' => '&#7858;',
            'Ẵ' => '&#7860;',
            'Ặ' => '&#7862;',
            'á' => '&aacute;',
            'à' => '&agrave;',
            'ả' => '&#7843;',
            'ã' => '&atilde;',
            'ạ' => '&#7841;',
            'â' => '&acirc;',
            'ă' => '&#259;',
            'Á' => '&Aacute;',
            'À' => '&Agrave;',
            'Ả' => '&#7842;',
            'Ã' => '&Atilde;',
            'Ạ' => '&#7840;',
            'Â' => '&Acirc;',
            'Ă' => '&#258;',
            'ế' => '&#7871;',
            'ề' => '&#7873;',
            'ể' => '&#7875;',
            'ễ' => '&#7877;',
            'ệ' => '&#7879;',
            'Ế' => '&#7870;',
            'Ề' => '&#7872;',
            'Ể' => '&#7874;',
            'Ễ' => '&#7876;',
            'Ệ' => '&#7878;',
            'é' => '&eacute;',
            'è' => '&egrave;',
            'ẻ' => '&#7867;',
            'ẽ' => '&#7869;',
            'ẹ' => '&#7865;',
            'ê' => '&ecirc;',
            'É' => '&Eacute;',
            'È' => '&Egrave;',
            'Ẻ' => '&#7866;',
            'Ẽ' => '&#7868;',
            'Ẹ' => '&#7864;',
            'Ê' => '&Ecirc;',
            'í' => '&iacute;',
            'ì' => '&igrave;',
            'ỉ' => '&#7881;',
            'ĩ' => '&#297;',
            'ị' => '&#7883;',
            'Í' => '&Iacute;',
            'Ì' => '&Igrave;',
            'Ỉ' => '&#7880;',
            'Ĩ' => '&#296;',
            'Ị' => '&#7882;',
            'ố' => '&#7889;',
            'ồ' => '&#7891;',
            'ổ' => '&#7893;',
            'ỗ' => '&#7895;',
            'ộ' => '&#7897;',
            'Ố' => '&#7888;',
            'Ồ' => '&#7890;',
            'Ổ' => '&#7892;',
            'Ô' => '&Ocirc;',
            'Ộ' => '&#7896;',
            'ớ' => '&#7899;',
            'ờ' => '&#7901;',
            'ở' => '&#7903;',
            'ỡ' => '&#7905;',
            'ợ' => '&#7907;',
            'Ớ' => '&#7898;',
            'Ờ' => '&#7900;',
            'Ở' => '&#7902;',
            'Ỡ' => '&#7904;',
            'Ợ' => '&#7906;',
            'ó' => '&oacute;',
            'ò' => '&ograve;',
            'ỏ' => '&#7887;',
            'õ' => '&otilde;',
            'ọ' => '&#7885;',
            'ô' => '&ocirc;',
            'ơ' => '&#417;',
            'Ó' => '&Oacute;',
            'Ò' => '&Ograve;',
            'Ỏ' => '&#7886;',
            'Õ' => '&Otilde;',
            'Ọ' => '&#7884;',
            'Ô' => '&Ocirc;',
            'Ơ' => '&#416;',
            'ứ' => '&#7913;',
            'ừ' => '&#7915;',
            'ử' => '&#7917;',
            'ữ' => '&#7919;',
            'ự' => '&#7921;',
            'Ứ' => '&#7912;',
            'Ừ' => '&#7914;',
            'Ử' => '&#7916;',
            'Ữ' => '&#7918;',
            'Ự' => '&#7920;',
            'ú' => '&uacute;',
            'ù' => '&ugrave;',
            'ủ' => '&#7911;',
            'ũ' => '&#361;',
            'ụ' => '&#7909;',
            'ư' => '&#432;',
            'Ú' => '&Uacute;',
            'Ù' => '&Ugrave;',
            'Ủ' => '&#7910;',
            'Ũ' => '&#360;',
            'Ụ' => '&#7908;',
            'Ư' => '&#431;',
            'ý' => '&yacute;',
            'ỳ' => '&#7923;',
            'ỷ' => '&#7927;',
            'ỹ' => '&#7929;',
            'ỵ' => '&#7925;',
            'Ý' => '&Yacute;',
            'Ỳ' => '&#7922;',
            'Ỷ' => '&#7926;',
            'Ỹ' => '&#7928;',
            'Ỵ' => '&#7924;',
            'đ' => '&#273;',
            'Đ' => '&#272;'
        );
        return str_replace(array_values($utf8), array_keys($utf8), $tring);
    }

    /**
     * Remove whitespace more in content
     * @param string $bff
     * @return string
     */
    public static function stripBuffer($bff) {
        /* carriage returns, new lines */
        //$bff = str_replace(array("\r\r\r", "\r\r", "\r\n", "\n\r", "\n\n\n", "\n\n", "\n"), "", $bff);

        /* tabs */
        $bff = str_replace(array("\t\t\t", "\t\t", "\t\n", "\n\t", "\t"), "", $bff);

        /* opening HTML tags */
        $bff = str_replace(array(">\r<a", ">\r <a", ">\r\r <a", "> \r<a", ">\n<a", "> \n<a", "> \n<a", ">\n\n <a"), "><a", $bff);
        $bff = str_replace(array(">\r<b", ">\n<b"), "><b", $bff);
        $bff = str_replace(array(">\r<d", ">\n<d", "> \n<d", ">\n <d", ">\r <d", ">\n\n<d"), "><d", $bff);
        $bff = str_replace(array(">\r<f", ">\n<f", ">\n <f"), "><f", $bff);
        $bff = str_replace(array(">\r<h", ">\n<h", ">\t<h", "> \n\n<h"), "><h", $bff);
        $bff = str_replace(array(">\r<i", ">\n<i", ">\n <i"), "><i", $bff);
        $bff = str_replace(array(">\r<i", ">\n<i"), "><i", $bff);
        $bff = str_replace(array(">\r<l", "> \r<l", ">\n<l", "> \n<l", ">  \n<l", "/>\n<l", "/>\r<l"), "><l", $bff);
        $bff = str_replace(array(">\t<l", ">\t\t<l"), "><l", $bff);
        $bff = str_replace(array(">\r<m", ">\n<m"), "><m", $bff);
        $bff = str_replace(array(">\r<n", ">\n<n"), "><n", $bff);
        $bff = str_replace(array(">\r<p", ">\n<p", ">\n\n<p", "> \n<p", "> \n <p"), "><p", $bff);
        $bff = str_replace(array(">\r<s", ">\n<s"), "><s", $bff);
        $bff = str_replace(array(">\r<t", ">\n<t"), "><t", $bff);

        /* closing HTML tags */
        $bff = str_replace(array(">\r</a", ">\n</a"), "></a", $bff);
        $bff = str_replace(array(">\r</b", ">\n</b"), "></b", $bff);
        $bff = str_replace(array(">\r</u", ">\n</u"), "></u", $bff);
        $bff = str_replace(array(">\r</d", ">\n</d", ">\n </d"), "></d", $bff);
        $bff = str_replace(array(">\r</f", ">\n</f"), "></f", $bff);
        $bff = str_replace(array(">\r</l", ">\n</l"), "></l", $bff);
        $bff = str_replace(array(">\r</n", ">\n</n"), "></n", $bff);
        $bff = str_replace(array(">\r</p", ">\n</p"), "></p", $bff);
        $bff = str_replace(array(">\r</s", ">\n</s"), "></s", $bff);

        /* other */
        $bff = str_replace(array(">\r<!", ">\n<!"), "><!", $bff);
        $bff = str_replace(array("\n<div"), " <div", $bff);
        $bff = str_replace(array(">\r\r \r<"), "><", $bff);
        $bff = str_replace(array("> \n \n <"), "><", $bff);
        $bff = str_replace(array(">\r</h", ">\n</h"), "></h", $bff);
        $bff = str_replace(array("\r<u", "\n<u"), "<u", $bff);
        $bff = str_replace(array("/>\r", "/>\n", "/>\t"), "/>", $bff);
        $bff = preg_replace("# {2,}#", ' ', $bff);
        $bff = preg_replace("#  {3,}#", '  ', $bff);
        $bff = str_replace("> <", "><", $bff);
        $bff = str_replace("  <", "<", $bff);

        /* non-breaking spaces */
        $bff = str_replace(" &nbsp;", "&nbsp;", $bff);
        $bff = str_replace("&nbsp; ", "&nbsp;", $bff);

        return $bff;
    }

    /**
     * random number generator
     *
     * @param	integer	Minimum desired value
     * @param	integer	Maximum desired value
     * @param	mixed	Seed for the number generator
     * (if not specified, a new seed will be generated)
     */
    public static function randNumber($min, $max, $seed = -1) {
        if (!defined('RAND_SEEDED')) {
            if ($seed == -1) {
                $seed = (double) microtime() * 1000000;
            }
            mt_srand($seed);
            define('RAND_SEEDED', true);
        }
        return mt_rand($min, $max);
    }

    /**
     * Random an word
     * @param int $number
     * @return string
     */
    public static function randWord($number) {
        $arrChar = 'abRTUcdefghEFGHnr4678tuxMyzABCNvwPQVjkmWXYZ2pq3DJK9';
        $string = '';
        $arrCharLength = strlen($arrChar) - 1;
        for ($j = 0; $j < $number; $j++) {
            $string .= substr($arrChar, self::randNumber(0, $arrCharLength), 1);
        }
        return $string;
    }

    public static function formatDate($dateTime) {
        $curTime = time();
        $listCurDate = array(date('H', $curTime), date('i', $curTime), date('d', $curTime), date('m', $curTime), date('Y', $curTime));
        $listTimeDate = array(date('H', $dateTime), date('i', $dateTime), date('d', $dateTime), date('m', $dateTime), date('Y', $dateTime));
        $limitTime = 24 * 60 * 60;
        $pastTime = $curTime - $dateTime;
        if ($pastTime <= $limitTime && ($listCurDate[2] == $listTimeDate[2])) {
            if ($pastTime > 0 && $pastTime <= 60) {
                $dateText = $pastTime . ' giây trước';
            } elseif ($pastTime > 60 && $pastTime <= 3600) {
                $dateText = ceil(($pastTime / 60)) . ' phút trước';
            } elseif ($pastTime > 3600 && $pastTime <= 24 * 3600) {
                $dateText = ceil(($pastTime / (60 * 60))) . ' tiếng trước';
            } else {
                $dateText = '1 giây trước';
            }
        } else {
            $dateText = self::getTextTime($listTimeDate) . ' ' . self::getTextDate($listCurDate, $listTimeDate);
        }

        return $dateText;
    }

    public static function timeToDate($mktime) {
        $strTextual = array(
            'Mon' => 'Thứ hai',
            'Tue' => 'Thứ ba',
            'Wed' => 'Thứ tư',
            'Thu' => 'Thứ năm',
            'Fri' => 'Thứ sáu',
            'Sat' => 'Thứ bảy',
            'Sun' => 'Chủ nhật'
        );

        $str = date('d/m/Y', $mktime) . ' GMT+7';

        return $strTextual[date('D', $mktime)] . ', ' . $str;
    }

    public static function getTextTime($timeInfo) {
        $timeText = ($timeInfo[0] > 12) ? ($timeInfo[0] - 12) : $timeInfo[0];
        $timeText .= ':' . $timeInfo[1] . ' ';
        if ($timeInfo[0] >= 1 && $timeInfo[0] <= 10) {
            $timeText .= 'sáng';
            if ($timeInfo[0] == 10 && $timeInfo[1] > 59) {
                $timeText .= 'trưa';
            }
        } elseif ($timeInfo[0] >= 11 && $timeInfo[0] <= 13) {
            $timeText .= 'trưa';
            if ($timeInfo[0] == 13 && $timeInfo[1] > 59) {
                $timeText .= 'chiều';
            }
        } elseif ($timeInfo[0] >= 13 && $timeInfo[0] <= 18) {
            $timeText .= 'chiều';
            if ($timeInfo[0] == 18 && $timeInfo[1] > 59) {
                $timeText .= 'tối';
            }
        } else {
            $timeText .= 'tối';
        }

        return $timeText;
    }

    public static function getTextDate($curInfo, $timeInfo) {
        $dateText = '';
        if ($curInfo[2] == $timeInfo[2]) {
            if ($curInfo[3] == $timeInfo[3]) {
                $dateText = 'hôm nay';
            } else {
                $dateText = $timeInfo[2] . '/' . $timeInfo[3] . '/' . $timeInfo[4];
            }
        } elseif (($curInfo[2] - $timeInfo[2]) == 1) {
            if ($curInfo[3] == $timeInfo[3]) {
                $dateText = 'hôm qua';
            } else {
                $dateText = $timeInfo[2] . '/' . $timeInfo[3] . '/' . $timeInfo[4];
            }
        } else {
            $dateText = $timeInfo[2] . '/' . $timeInfo[3] . '/' . $timeInfo[4];
        }

        return $dateText;
    }

  public static  function JConvertTime($iTime){
        $iSeconds = (int)round(abs(time() - $iTime));
        $iMinutes = (int)round($iSeconds / 60);
        $day    = date('d',$iTime);
        $month  = date('m',$iTime);
        $year   = date('Y',$iTime);
        $hours  = date('h',$iTime).':'.date('i',$iTime);

        if ($iMinutes < 1) {
            if ($iSeconds === 0 || $iSeconds === 1) {
                return '1 giây trước';
            }
            return $iSeconds .' giây trước';
        }

        if ($iMinutes < 60) {
            if ($iMinutes === 0 || $iMinutes === 1) {
                return '1 phút trước';
            }
            return  $iMinutes .' phút trước';
        }

        $iHours = (int)round(floatval($iMinutes) / 60.0);

        if ($iHours < 24) {
            if ($iHours === 0 || $iHours === 1) {
                return '1 giờ trước';
            }

            return $iHours.' giờ trước' ;
        }

        if ($iHours < 48 && ((int)date('d', time()) - 1) == date('d', $iTime)) {
            return 'Hôm qua lúc '.$hours;
        }

        $strTime  = $day.' tháng '.$month.' lúc '.$hours;
        return $strTime;

  }
    /**
     * cut UTF8 string
     *
     * @param string $str
     * @param int $len
     * @param string $charset
     * @return string
     */
    public static function cutString($str, $start, $len, $charset = 'UTF-8') {
        //Neu la chuoi rong
        if (empty($str)) {
            return '';
        }

        //Kiem tra do dai chuoi
        if (iconv_strlen($str, $charset) <= $len) {
            return $str;
        }

        //Tien hanh cat chuoi
        $str = html_entity_decode($str, ENT_QUOTES, $charset);
        $str = iconv_substr($str, $start, $len, $charset);
        return $str . "...";
    }

    /**
     * cut word string
     *
     * @param string $text
     * @param int $num
     * @return string
     */
    public static function JCutWord($text , $num = 20){
        $text   = preg_replace("/ +/i", " ",$text);
        $a      = explode(" ", $text);
        $count  = count($a);
        if($num > $count)
            return $text;

        $l=0;
        for($i  =  0 ;$i< $num ; $i++)
        {
            $l+=strlen($a[$i])+1;
        }
        return substr($text,0,$l)."...";
        // return substr($text,0,$l);
        }

    /**
     * Return emotion list
     * @return string
     */
    public static function getEmotionJsonString() {
        return json_encode(self::$arr_emotion);
    }

    /**
     * Return html emotion
     */
    public static function getEmotionHtml($length = 39) {
        $img_smile_server = 'http://static.me.zing.vn/images/smilley/default/';
        $str = '<table width="450" border="0" cellpadding="0" cellspacing="0">';
        $str .= '<tbody>';
        $number = 0;
        $start = 0;
        $idex = 0;
        foreach (self::$arr_emotion as $key => $value) {
            if ($idex > $length) {
                break;
            }

            if ($number == 0) {
                $str .= '<tr>';
            }

            //Add td
            $str .= '<td><img title="' . $key . '" src="' . $img_smile_server . $value . '.jpg" onClick="emoticon_msg(this);" style="cursor: pointer;"></td>';
            $number++;

            if ($number == 10) {
                $number = 0;

                //Check start
                if ($start == 0) {
                    $str .= '<td rowspan="4" align="right" valign="middle"><img src="http://123mua.apps.zing.vn/c/images/emoticon_bar_close.gif" style="cursor: pointer;" onClick="emoticon_close()"></td>';
                }
                $start++;

                //Add Tr
                $str .= '</tr>';
            }
            $idex++;
        }

        //Loop number
        if ($number > 0) {
            for ($i = 0; $i < (10 - $number); $i++) {
                $str .= '<td>&nbsp;</td>';
            }
            $str .= '</tr>';
        }

        $str .= '</tbody>';
        $str .= '</table>';
        return $str;
    }

    /**
     * Strip word html
     * @param string $text
     */
    public static function strip_word_html($text, $allowed_tags = '<a><h1><h2><h3><style><p><br /><br><strong><i><u><span><b><center><dd><dt><font><img><ul><li><ol><pre><table><td><title><td><tr><tt><div><em>') {
        $text = strip_tags($text, $allowed_tags);

        $search = array('@<script[^>]*?>.*?</script>@si', // Strip out javascript
            '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );

        $text = preg_replace($search, '', $text);

        return $text;
    }

    public static function removeNonUTF8($string) {
        //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
        $string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
                '|[\x00-\x7F][\x80-\xBF]+' .
                '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
                '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
                '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S', '?', $string);

        //reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
        $string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
                '|\xED[\xA0-\xBF][\x80-\xBF]/S', '?', $string);

        return $string;
    }

    /**
     * convert object to array
     *
     * @param object $obj
     * @return array|boolean
     */
    public static function objectToArray($obj) {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? self::objectToArray($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    /**
     * Check is Ascii string
     * @param string $string
     */
    public static function isAsciiString($string) {
        return !(preg_match('/[^\x00-\x7F]/S', $string));
    }

    /**
     * Lay do dai cua chuoi
     * @param string $string
     * @return int
     */
    public static function vbstrlen($string) {
        $string = preg_replace('#&\#([0-9]+);#', '_', $string);
        return strlen($string);
    }

    /**
     * Loc du lieu
     * @param string $text
     * @param string $censorwords
     * @param string $censorchar
     * @param boolean $flag
     * @return string
     */
    public static function fetchCensoredText($text, $censorwords = "{fuck},{shit},{ifastnet},{sitesled},{buồi},{địt},{đụ},{nứng},{lồn},{fuck},{fucking},{cock},{bitch},{cặc},{cặt},{kặc},{Đụ},{đéo},{suck},{vcl},{bullshit},{minh râu},{đụ má},{Đĩ},{việt cộng},{Nông Thị Xuân},{triều đình cộng sản},{vương triều cộng sản},{vương triều Hà Nội},{đảng trị},{đả đảo cộng sản},{đả đảo XHCN},{cộng sản bán nước},{tội ác cộng sản},{tội ác của chế độ CSVN},{tội ác của chế độ cộng sản},{cộng sản độc tài},{độc tài cộng sản},{cộng sản thối nát},{cộng sản mị dân},{cộng sản mỵ dân},{địt mẹ mày},{đéo mẹ},{đụ mẹ},{lồn mẹ},{lỗ đít},{móc lồn},{liếm lồn},{ăn lồn},{liếm lìn},{mặt lìn},{con phò},{con kặc},{Minh Râu},{việt minh},{việt cộng},{địt},{đéo},{lồn},{buồi},{cặc},{địt mẹ},{lịt mẹ},{đụ mạ},{đù má},{đụ mẹ},{đéo mẹ},{địt mẹ mày},{đụ mạ mày},{đù má mày},{đụ mẹ mày},{đéo mẹ mày},{địt mẹ chúng mày},{đụ mạ chúng mày},{đù má chúng mày},{đụ mẹ chúng mày},{đéo mẹ chúng mày},{lồn mẹ mày},{tiên sư bố},{tổ sư bố},{mả mẹ mày},{mả cha mày},{mả bố mày},{mả tổ mày},{mả mẹ chúng mày},{mả cha chúng mày},{mả bố chúng mày},{mả tổ chúng mày},{lỗ đít},{mặt lồn},{liếm lồn},{móc lồn},{ăn lồn},{mặt lìn},{liếm lìn},{móc lìn},{ăn lìn},{liếm đít},{con phò},{con đĩ},{con điếm},{đĩ đực},{điếm đực},{đồ chó},{đồ lợn},{liếm cứt},{bốc cứt},{ăn cứt},{nhét cứt},{ăn lồn},{máu lồn},{ăn buồi},{mút buồi},{ăn cặc},{bú cặc},{con cặc},{mút cặc},{ăn kặc},{bú kặc},{củ kặc},{con kặc},{mút kặc},{ăn kẹc},{bú kẹc},{củ kẹc},{con kẹc},{bú dái},{bú dzái},{mút zái},{bú buồi},{đầu buồi},{bú cu},{bú ku},{con ku},{ĐỊT MẸ},{DIT ME},{Đy~ chó},{Đĩ chó},{ĂN KỨT},{ăn kứt},{ngứa dzái},{bán dzâm},{ngứa zdái},{cờ ba sọc},{cờ 3 sọc},{nguoi-viet.com},{bỏ mẹ},{MINH RÂU},{LỒN},{CỨT},{ĐỴTĐỊT},{ĐĨ},{ĐỈ},{đĩ},{đỉ},{đĩ},{loz`},{đĩ?},{đĨ},{Địt mẹ},{Địt},{đỵt mẹ},{đỵt},{Đỵt mẹ},{Đỵt},{đỴt},{nứng cẹc},{nứng kẹt},{dis mẹ},{đis mẹ},{đIs mẹ},{đỤ mẸ},{nhu lon},{lon`},{như lồn},{phim sex},{film sex},{djs},{zú bự},{zu' bu},{zu' bự},{khoe hang`},{KHOE HÀNG},{KHOE HANG`},{Minh Trị},{Minh trị},{minh trị},{MINH TRỊ},{MINH TRI},{minh tri},{CỜ VÀNG BA SỌC ĐỎ},{Cờ Vàng Ba Sọc Đỏ},{cờ vàng ba sọc đỏ},{CỜ VÀNG 3 SỌC ĐỎ},{Cờ Vàng 3 Sọc Đỏ},{cờ vàng ba sọc đỏ},{đàn áp tôn giáo},{ĐÀN ÁP TÔN GIÁO},{Đàn Áp Tôn Giáo},{Thăng Tiến Việt Nam},{THĂNG TIẾN VIỆT NAM},{thăng tiến việt nam},{thăng tiến Việt Nam},{Cụ Hồ},{Cu Hồ},{đéo},{ĐỤ},{đụ},{ĐỊT},{địt tổ},{nứng},{lồn},{mất dạy},{địt},{ngứa},{nứng lồn},{đéo},{dis},{hot^. le.},{hột le},{cleversky.net},{Đjt},{lỒn},{đĩ},{LỒN},{deo'},{liem^'},{cho'},{cut'},{hiep'},{trYm},{đĩ},{Cộng sản Quốc tế},{Việt Nam Cộng Hòa},{VN CH},{CS VN},{Trung cộng},{Viêt Cộng},{TRung Cong},{Viet COng},{TRung COng},{Thanh Minh Thiền viện},{đ ĩ},{mặt LỒN},{chó},{ĐỴT},{trYm},{ỈA},{Đỵt mẸ},{BUỒI},{hãm},{Cứt},{CỨT},{cứt},{cỨt},{cứT},{CỨt},{CPVNTD},{sjp},{ĐÝT},{BUỒI},{Đụ},{ĐỊT},{LO^`N},{BUÔ`i},{d!ck},{đỤ},{đ ĩ},{trYm},{nứng},{cặc},{lồn},{địt},{đỊT},{lÒl},{đít},{lỒn},{ĐỴT},{ĐỴT mje},{Đỵt},{Đỵt mẸđ ĩ},{ỈA},{CỨT},{cứt},{đéo},{chó đẻ},{FUCK},{loZ},{đỊT},{lÒl},{mẹ mày},{má mày},{MẤT DẠY},{mất dạy},{f.u.c.k},{F.U.C.K},{f u c k},{MÁ MÀY},{MẸ MÀY},{Fuck},{FucK},{F U C K},{đú},{đỉ},{đĩ},{đỉ},{vú},{dú},{đái},{dY~},{diE^n},{cH0},{cH0'},{mE.},{zú},{fản động},{phản động},{djs},{ch.ó},{F*ck},{đ.é.o},{đít ghẻ},{đ.e'o}", $censorchar = '*', $flag = 1) {
        static $arrcensorwords;
        if (empty($text)) {
            return "";
        }
        if ($flag == true AND !empty($censorwords)) {
            if (empty($arrcensorwords)) {
                $censorwords = preg_quote($censorwords, '#');
                $arrcensorwords = preg_split('#[,\r\n\t]+#', $censorwords, -1, PREG_SPLIT_NO_EMPTY);
            }

            foreach ($arrcensorwords AS $arrcensorword) {
                if (substr($arrcensorword, 0, 2) == '\\{') {
                    if (substr($arrcensorword, -2, 2) == '\\}') {
                        // prevents errors from the replace if the { and } are mismatched
                        $arrcensorword = substr($arrcensorword, 2, -2);
                    }
                    // ASCII character search 0-47, 58-64, 91-96, 123-127
                    $nonword_chars = '\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f';
                    // words are delimited by ASCII characters outside of A-Z, a-z and 0-9
                    $text = preg_replace(
                            '#(?<=[' . $nonword_chars . ']|^)' . $arrcensorword . '(?=[' . $nonword_chars . ']|$)#si', str_repeat($censorchar, self::vbstrlen($arrcensorword)), $text
                    );
                } else {
                    $text = preg_replace("#$arrcensorword#si", str_repeat($censorchar, self::vbstrlen($arrcensorword)), $text);
                }
            }
        }
        return $text;
    }

    /**
     * Attempts to intelligently wrap excessively long strings onto multiple lines
     *
     * @param	string	Text to be wrapped
     * @param	integer	If specified, max word wrap length
     * @param	string	Text to insert at the wrap point
     *
     * @return	string
     */
    public function fetch_word_wrapped_string($text, $limit = 20, $wraptext = ' ') {
        $limit = intval($limit);

        if ($limit > 0 AND !empty($text)) {
            return preg_replace('
				#((?>[^\s&/<>"\\-\[\]]|&[\#a-z0-9]{1,7};){' . $limit . '})(?=[^\s&/<>"\\-\[\]]|&[\#a-z0-9]{1,7};)#i', '$0' . $wraptext, $text
            );
        } else {
            return $text;
        }
    }

    /**
     * get partition global
     *
     * @param array $arr
     * @param int $id
     * @return string
     */
    private static function getPartitionDb($userid, $maxlength) {
        //Neu maxlenght = 0
        if ($maxlength == 0) {
            return '';
        }

        //Check userid
        $userid = (int) ($userid % 5000000);

        //Lay phan div
        $div = intval($userid / $maxlength);

        //Lay thong tin table
        return ($div * $maxlength + 1) . '_' . ($div + 1) * $maxlength;
    }

    public static function distanceOfTimeInWords($fromTime) {
        $distanceInSeconds = round(abs(time() - $fromTime));
        $distanceInMinutes = round($distanceInSeconds / 60);

        if ($distanceInMinutes <= 1) {
            return 'mới tức thì';
        }
        if ($distanceInMinutes <= 2) {
            return '1 phút trước';
        }
        if ($distanceInMinutes < 45) {
            return $distanceInMinutes . ' phút trước';
        }
        if ($distanceInMinutes < 90) {
            return 'cách đây 1 giờ';
        }
        if ($distanceInMinutes < 1440) {
            return 'cách đây ' . round(floatval($distanceInMinutes) / 60.0) . ' giờ';
        }
        if ($distanceInMinutes < 2880) {
            return '1 ngày';
        }
        if ($distanceInMinutes < 43200) {
            return round(floatval($distanceInMinutes) / 1440) . ' ngày trước';
        }
        if ($distanceInMinutes < 86400) {
            return '1 tháng trước';
        }
        if ($distanceInMinutes < 525600) {
            return round(floatval($distanceInMinutes) / 43200) . ' tháng';
        }
        if ($distanceInMinutes < 1051199) {
            return '1 năm trước';
        }

        return 'hơn ' . round(floatval($distanceInMinutes) / 525600) . ' năm';
    }

    public static function mktimeToDate($mktime) {
        return date("Y-m-d G:i:s", $mktime);
    }

    /**
     * Ham tinh thoi gian giua 2 thoi diem, tra ve so giay
     *
     * @param string $time1
     * @param string $time2
     * @return int
     */
    public static function dateDiff($startDate, $endDate) {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        if ($startDate === false || $startDate < 0 || $endDate === false || $endDate < 0 || $startDate > $endDate)
            return false;

        $years = date('Y', $endDate) - date('Y', $startDate);

        $endMonth = date('m', $endDate);
        $startMonth = date('m', $startDate);

        // Calculate months
        $months = $endMonth - $startMonth;
        if ($months <= 0) {
            $months += 12;
            $years--;
        }
        if ($years < 0)
            return false;

        // Calculate the days
        $offsets = array();
        if ($years > 0)
            $offsets[] = $years . (($years == 1) ? ' year' : ' years');
        if ($months > 0)
            $offsets[] = $months . (($months == 1) ? ' month' : ' months');
        $offsets = count($offsets) > 0 ? '+' . implode(' ', $offsets) : 'now';

        $days = $endDate - strtotime($offsets, $startDate);
        $days = date('z', $days);

        return array($years, $months, $days);
    }

    public static function getStringReplaceEmotion($string) {

        global $emoticonsList;
        //Neu la chuoi rong
        if (empty($string)) {
            return '';
        }

        //$img_smile_server = 'http://static.me.zing.vn/images/smilley/default/';
        $img_smile_server = 'http://st.haynhucnhoi.tv/emoticons/';

        foreach ($emoticonsList as $key => $value) {
            $str_replace = '<img src="' . $img_smile_server . $value . '"/>';
//            echo "#\b" . $key. "\b#";
//            echo $str_replace;
            $string = str_replace($key, $str_replace, $string);
//            $string = preg_replace($key, $str_replace, $string);
        }

        return $string;
    }

    public static function escape($string) {
        $match = array('\\', '+', '-', '&', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ');
        $replace = array('\\\\', '\\+', '\\-', '\\&', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ');
        $string = str_replace($match, $replace, $string);
        return $string;
    }

    /**
     *
     * Get thumb image
     * @param string $url
     * @param int $width
     * @param int $height
     */
    public static function getThumbImage($url, $type = '') 
    {

        $ext       = explode('.', $url);
        $extension = $ext[count($ext) - 1];
        $config = My_Zend_Globals::getConfiguration();

        if ($type == "thumb1")
        {
            $url    = $ext[0]."-thumb1" . "." . $extension;
            $url2   = $ext[0]. "." . $extension;
            $exp    = explode('/', $ext[0]);
            if($exp[1]=='meme')
            {
                $url    = $config->photo->domain . $url2; 
            }else
            {
                $url    = $config->photo->domain . $url; 
            }

        }else if ($type == "thumb2")
        {
            $url    = $ext[0]."-thumb2" . "." . $extension;
            $url2   = $ext[0]. "." . $extension;
            $exp    = explode('/', $ext[0]);
            $meme    = isset($exp[1]) ? $exp[1] : '';
            if($meme=='meme')
            {
                $url    = $config->photo->domain . $url2; 
            }else
            {
                $url    = $config->photo->domain . $url; 
            }  
        }
        else if ($type == "custom-thumb") {
            $url    = $ext[0]."-custom-thumb" . "." . $extension;
            $url2   = $ext[0]. "." . $extension;
            $exp    = explode('/', $ext[0]);
            $meme    = isset($exp[1]) ? $exp[1] : '';
            if($meme=='meme')
            {
                $url    = $config->photo->domain . $url2;
            }else
            {
                $url    = $config->photo->domain . $url;
            }
        }
        else
        {
            $config = My_Zend_Globals::getConfiguration();
            $url    = $ext[0]. "." . $extension;
            $url    = $config->photo->domain.$url;
                
        }
       return $url;    
    }

    public static function getThumbImageVideo($picture ='')
    {
        $config = My_Zend_Globals::getConfiguration();

        if (strpos($picture, 'https://') !== false || strpos($picture, 'http://') !== false ) {

        } else {
            $picture    = $config->photo->domain.$picture;
        }

        return $picture;
    }

    /**
     *
     * URL Alias Creator
     * @param String $string
     */
    public static function aliasCreator($string) {
        //remove any '-' from the string they will be used as concatonater
        $string = str_replace('-', ' ', trim($string));
        $string = self::transliterate($string);
        // remove any duplicate whitespace, and ensure all characters are alphanumeric
        //$string = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $string);
        $string = trim(preg_replace(array('/[^A-Za-z0-9\-]/'), array(' '), $string));
        $string = preg_replace('/\s+/', ' ', $string);
        $string = preg_replace('/\s+/', '-', $string);
        // lowercase and trim
        $string = trim(strtolower($string));
        return $string;
    }

    /**
     * Clean UTF-8 Charactor
     * @param String $string
     */
    private static function transliterate($string) {
        $string = trim(htmlentities(self::utf8ToAscii($string, " ")));
        $string = preg_replace(
                array('/&szlig;/', '/&(..)lig;/', '/&([aouAOU])uml;/', '/&(.)[^;]*;/'), array('ss', "$1", "$1" . 'e', "$1"), $string);

        return $string;
    }

    private static $_viewInstance;

    private static function _getViewInstance() {
        if (self::$_viewInstance == null) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            if (null === $viewRenderer->view) {
                $viewRenderer->initView();
            }
            self::$_viewInstance = $viewRenderer->view;
        }
        return self::$_viewInstance;
    }

    public static function setTitle($title) {
        $view = self::_getViewInstance();
        $view->headTitle()->setSeparator(' - ');
        $view->headTitle()->set($title);
    }

    public static function setMeta($name, $content, $split = false) {
        $view = self::_getViewInstance();

        if ($split) {
            if ($name == 'keywords') {
                $content = preg_replace('/[\s]+/', ', ', trim($content));
            }
        }

        $view->headMeta()->setName($name, $content);
    }

    public static function setProperty($name, $content) {
        self::setDocType('XHTML1_RDFA');
        $view = self::_getViewInstance();
        $view->headMeta()->setProperty($name, $content);
    }

    public static function setDocType($doctype) {
        $view = self::_getViewInstance();
        $view->doctype($doctype);
    }

    public static function sizeOfVar($var) {
        $start_memory = memory_get_usage();
        $tmp = $var;
        return memory_get_usage() - $start_memory;
    }

    public static function filterTitle($title) {
        $title = self::utf8ToAscii($title, ' ');
        $title = preg_replace('/[^a-zA-Z0-9\s]/', '', $title);

        return $title;
    }

    function replace_br($data) {
        $data = preg_replace('#<p>(\s|&nbsp;|</?\s?br\s?/?>)*</?p>#', '', $data);
        $data = preg_replace('#(?:<br\s*/?>\s*?){2,}#', '</p><p>', $data);
        $data = preg_replace('#<br\s*/?>#', '</p><p>', $data);
        $data = '<p>' . $data . '</p>';
        //$data = preg_replace("/<[^\/>]*>([\s]?)*<\/[^>]*>/", '', $data);
        return $data;
    }

    public static function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);
    }

    public static function getFacebookStats($url) {
        try {
            //Build url
            $api = 'http://api.facebook.com/restserver.php?method=links.getStats&urls='. $url;

            //Post curl
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $api);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
            curl_setopt($curl, CURLOPT_VERBOSE, 0);

            //Get respone
            $response = curl_exec($curl);

            //Check response
            if (empty($response)) {
                return array();
            }

            //Close curl
            curl_close($curl);

            $xml = new SimpleXMLElement($response);

            $object = $xml->link_stat[0];
            $data = array(
                    'share_count'   => intval($object->share_count),
                    'like_count'    => intval($object->like_count),
                    'comment_count' => intval($object->comment_count),
                    'total_count'   => intval($object->total_count),
                    'click_count'   => intval($object->click_count),
                    'comments_fbid' => (string)$object->comments_fbid,
                    'commentsbox_count' => intval($object->commentsbox_count)
            );

            return $data;
        } catch (Exception $ex) {
            My_Zend_Logger::log('My_Zend_Globals::getFacebookStats - ' . $ex->getMessage());
            return false;
        }
    }

    public static function encrypt($string) {
        $key = '48350943850943';
        $encrypt = serialize($string);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
        return $encoded;
    }

    /**
     * Returns decrypted original string
     */
    public static function decrypt($string) {
        $key = '48350943850943';
        $decrypt = explode('|', $string);
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
        if($calcmac!==$mac){ return false; }
        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    public static function download($file_source, $file_target)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $file_source);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);

        $data = curl_exec($curl);
        $rsInfo = curl_getinfo($curl);
        curl_close($curl);

        if (!$data || $rsInfo['http_code'] == 404) {
            return false;
        }

        @file_put_contents($file_target, $data);

        return true;
    }

    public static function is_image($path)
    {
        $mime = getimagesize($path);
        $image_type = $mime[2];
        
        if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
        {
            return true;
        }
     return false;
    }
    public static function is_HTML($str ='')
    {
        if($str != strip_tags($str)) {
            return true; // like preg_match("/<[^<]+>/",$str,$m);
        }
        return false;
    }
    public static function getIDYoutube($url = '')
    {
        if($url !='')
        {
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
            $url  = isset($matches[1]) ? $matches[1] : '';
        }
        return $url;
    }

    public static function getInfoVideoYoutube($youtubeID)
    {
        $url = "http://gdata.youtube.com/feeds/api/videos/$youtubeID?v=2&alt=json";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);
        $result = json_decode($output,true);
//        $viewCount = $result['entry']['yt$statistics']['viewCount']; //view count
//        $dislikes = $result['entry']['yt$rating']['numDislikes']; //dislikes
//        $likes = $result['entry']['yt$rating']['numLikes']; //likes

        return $result;

    }

    public static function shortenNumber($number)
    {
        if (is_numeric($number) && $number > 1000) {
            if ($number < 1000000) {
                $number = ($number / 1000);
            } else {
                $number = ($number / 1000000);
            }
                $number = (string) $number;
                preg_match("/(-+)?\d+(\.\d{1,1})?/" , $number, $matches);
                $result = $matches[0] . "K";
                return $result;
        }
        return $number;
    }

    public static function number_format($number)
    {
        return number_format($number, 0, ',', '.');
    }

    public static function convertToSEO($str, $separator = 'dash', $lowercase = true)
    {

//    $str = utf8_encode($str);
        $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');

        //Remove emoticon
        $str = preg_replace('/:([a-z0-9A-Z_-]+):/', '', $str);
        //replace special chars, for UTF8 encoding it needs to be defined as array
        $trans = array(
            'ơ' => 'o',
            'Ơ' => 'o',
            'ó' => 'o',
            'Ó' => 'o',
            'ò' => 'o',
            'Ò' => 'o',
            'ọ' => 'o',
            'Ọ' => 'o',
            'ỏ' => 'o',
            'Ỏ' => 'o',
            'õ' => 'o',
            'Õ' => 'o',
            'ớ' => 'o',
            'Ớ' => 'o',
            'ờ' => 'o',
            'Ờ' => 'o',
            'ợ' => 'o',
            'Ợ' => 'o',
            'ở' => 'o',
            'Ở' => 'o',
            'ỡ' => 'o',
            'Ỡ' => 'o',
            'ô' => 'o',
            'Ô' => 'o',
            'ố' => 'o',
            'Ố' => 'o',
            'ồ' => 'o',
            'Ồ' => 'o',
            'ộ' => 'o',
            'Ộ' => 'o',
            'ổ' => 'o',
            'Ổ' => 'o',
            'ỗ' => 'o',
            'Ỗ' => 'o',
            'ú' => 'u',
            'Ú' => 'u',
            'ù' => 'u',
            'Ù' => 'u',
            'ụ' => 'u',
            'Ụ' => 'u',
            'ủ' => 'u',
            'Ủ' => 'u',
            'ũ' => 'u',
            'Ũ' => 'u',
            'ư' => 'u',
            'Ư' => 'u',
            'ứ' => 'u',
            'Ứ' => 'u',
            'ừ' => 'u',
            'Ừ' => 'u',
            'ự' => 'u',
            'ữ' => 'u',
            'Ự' => 'u',
            'ử' => 'u',
            'Ử' => 'u',
            'ữ' => 'u',
            'Ữ' => 'u',
            'â' => 'a',
            'Â' => 'a',
            'á' => 'a',
            'Á' => 'a',
            'à' => 'a',
            'À' => 'a',
            'ạ' => 'a',
            'Ạ' => 'a',
            'ả' => 'a',
            'Ả' => 'a',
            'ã' => 'a',
            'Ã' => 'a',
            'ấ' => 'a',
            'Ấ' => 'a',
            'ầ' => 'a',
            'Ầ' => 'a',
            'ậ' => 'a',
            'ạ' => 'a',
            'ò' =>' o',
            'Ậ' => 'a',
            'ẩ' => 'a',
            'Ẩ' => 'a',
            'ẫ' => 'a',
            'Ẫ' => 'a',
            'ă' => 'a',
            'Ă' => 'a',
            'ắ' => 'a',
            'Ắ' => 'a',
            'ằ' => 'a',
            'Ằ' => 'a',
            'ặ' => 'a',
            'Ặ' => 'a',
            'ẳ' => 'a',
            'Ẳ' => 'a',
            'ẵ' => 'a',
            'Ẵ' => 'a',
            'ế' => 'e',
            'Ế' => 'e',
            'ề' => 'e',
            'Ề' => 'e',
            'ệ' => 'e',
            'Ệ' => 'e',
            'ể' => 'e',
            'Ể' => 'e',
            'ễ' => 'e',
            'Ễ' => 'e',
            'é' => 'e',
            'É' => 'e',
            'è' => 'e',
            'È' => 'e',
            'ẹ' => 'e',
            'Ẹ' => 'e',
            'ẻ' => 'e',
            'Ẻ' => 'e',
            'ẽ' => 'e',
            'Ẽ' => 'e',
            'ê' => 'e',
            'Ê' => 'e',
            'í' => 'i',
            'Í' => 'i',
            'ì' => 'i',
            'Ì' => 'i',
            'ỉ' => 'i',
            'Ỉ' => 'i',
            'ĩ' => 'i',
            'Ĩ' => 'i',
            'ị' => 'i',
            'Ị' => 'i',
            'ý' => 'y',
            'Ý' => 'y',
            'ỳ' => 'y',
            'Ỳ' => 'y',
            'ỷ' => 'y',
            'Ỷ' => 'y',
            'ỹ' => 'y',
            'Ỹ' => 'y',
            'ỵ' => 'y',
            'Ỵ' => 'y',
            'đ' => 'd',
            'Đ' => 'd',
            '[' => '',
            ']' => '',
            ')' => '',
            '(' => '',
            ';' => '',
            '^' => '',
            '@' => '',
            '$' => '',
            '&' => '',
            '?' => '',
            ':' => '',
            '>' => '',
            '<' => '',
            '~' => '',
            '{' => '',
            '}' => '',
            '‘' => '',
            '’' => '',
            ',' => '',
            '\'' => '',
            '…' => '',
            '"' => '',
            '+' => '',
            'ồ' => 'o',
            'ấ' => 'a',
            'ớ' => 'o',
            'ý' => 'y',
            'ế' => 'e',
            'ì' => 'i',
            'ả' => 'a',
            '*' => ' ',
            'ó' => 'o',
            'ể' => 'e',
            'Ấ' => 'a',
            'ậ' => 'a',
            'ộ' => 'o',
            'à' => 'a',
            'ợ' => 'o',
            'ệ' => 'e',
            '`' => '',
            '&gt;' => '',
            '&lt;' => '',
            '&quot;' => '',
            '&amp;' => '',
            '.' => '',
            '%' => '',
            'á' => 'a',
            'ầ' => 'a',
            '|' => '',
            '!' => '',
            '“' => '',
            '”' => '',
            '–' => '',
            '=' => '',
            '/' => '-',
            'ặ' => 'a',
            'ờ' => 'o',
            'ố' => 'o',
            'ắ' => 'a',
            'ỳ' => 'y',
            'é' => 'e',
            'ẹ' => 'e',
            'ú' => 'u',
            '#'  => ''
        );
        $str = strtr($str, $trans);

        if ($separator == 'dash')
        {
            $search = '_';
            $replace = '-';
        }
        else
        {
            $search = '-';
            $replace = '_';
        }

        $trans = array(
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replace,
            $replace . '+' => $replace,
            $replace . '$' => $replace,
            '^' . $replace => $replace,
            '\.+$' => '');

        $str = strip_tags($str);
        $str = preg_replace("#\/#ui", '-', $str);

        foreach ($trans as $key => $val)
        {
            $str = preg_replace("#" . $key . "#ui", $val, $str);
        }

        if ($lowercase === true)
        {
            $str = mb_strtolower($str, 'UTF-8');
        }

        return self::removeUTF8BOM(trim(stripslashes($str),"-"));
    }

    protected static function removeUTF8BOM($text)
    {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }

    public static function getUserAgent()
    {
        $userAgent = new Zend_Http_UserAgent();
        $device = $userAgent->getDevice();
        $agent = $device->getUserAgent();

        return $agent;
    }

    public static function getDataFromCache($cacheKey) {
        $cache = My_Zend_Globals::getCaching();
        $data = $cache->read($cacheKey);
        if (!empty($data)) {
            return $data;
        } else {
            return null;
        }
    }

    public static function deleteDataFromCache($cacheKey) {
        $cache = My_Zend_Globals::getCaching();
        $data = $cache->delete($cacheKey);
    }

    public static function putDataToCache($cacheKey, $data, $timeout = 900) {
        $cache = My_Zend_Globals::getCaching();
        $cache->write($cacheKey, $data, $timeout);
    }

}
