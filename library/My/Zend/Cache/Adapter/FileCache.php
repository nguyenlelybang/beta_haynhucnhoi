<?php

class My_Zend_Cache_Adapter_FileCache extends My_Zend_Cache_Adapter_Abstract {

    /**
     * Memcache object
     *
     * @var object
     */
    private $instance = null;
    private $frontendOptions = null;
    private $backendOptions = null;

    /**
     * Constructor
     *
     */
    public function __construct($options) {
        $this->frontendOptions = array('automatic_serialization' => $options['compression']);
        $this->backendOptions = array('cache_dir' => $options['dir']);
    }

    /**
     * Destructor
     */
    public function __destruct() {
    }

    private function _getInstance($expiredTime = 0) {
        if ($expiredTime > 0) {
            $this->frontendOptions['lifetime'] = $expiredTime;
        }

        $this->instance = Zend_Cache::factory('Core', 'File', $this->frontendOptions, $this->backendOptions);
    }

    /**
     * Get single cache
     *
     * @param string $key
     * @return var
     */
    public function read($key) {
        //If empty
        if (empty($key)) {
            return false;
        }

        //Time start
        if ($this->debug) {
            $start = gettimeofday(true);
        }

        // get instance
        $this->_getInstance();

        //Get data
        $data = $this->instance->load($key);

        //Check hit
        if ($data === false) {
            //Write log
            if ($this->log) {
                $this->logger->log("Miss key Single:($key)");
            }

            //Write Profiler
            if ($this->debug) {
                $this->profiler->addTotalMissesCache($key, 1);
            }
        } else {
            //Write log
            if ($this->log) {
                $this->logger->log("Hit key Single:($key)");
            }

            //Write Profiler
            if ($this->debug) {
                $this->profiler->addTotalHitsCache($key, 1);
            }
        }

        //Add time ellapsed
        if ($this->debug) {
            //Write Profiler
            $end = gettimeofday(true);
            $this->profiler->addTotalEllapsedTime($key, ($end - $start));
            $this->profiler->addDataCache($key, $data);
        }

        return $data;
    }

    /**
     * Get List cache
     *
     * @param array $arrKeys
     * Example : array('key1', 'key2')
     * @return var
     */
    public function readMulti($arrKeys) {
        return false;
    }

    /**
     * Write single cache
     *
     * @param string $key
     * @param var $data
     * @param int $expire
     * @return boolean
     */
    public function write($key, $data, $expire = 0, $flag = 0) {
        // get instance
        $this->_getInstance($expire);

        //Write log
        if ($this->log) {
            $this->logger->log("Write key ($key) width data (" . serialize($data) . ")");
        }

        return $this->instance->save($data, $key);
    }

    /**
     * Write list cache
     * @param array $arrKeys
     * Example : array('key1'=>'value1', 'key2'=>'value2')
     * @param int $flag
     * @param int $expire
     */
    public function writeMulti($arrKeys, $flag = 0, $expire = 0) {
        return false;
    }

    /**
     * Delete single cache
     *
     * @param string $key
     * @param int $timeout
     * @return boolean
     */
    public function delete($key, $timeout = 0) {
        //Write log
        if ($this->log) {
            $this->logger->log("Delete key ($key)");
        }

        return $this->instance->remove($key);
    }

    public function cleanAllRecord()
    {
      return  $this->instance->clean(Zend_Cache::CLEANING_MODE_ALL);
    }

    public function cleanOnlyOutdated()
    {
        return  $this->instance->clean(Zend_Cache::CLEANING_MODE_OLD);
    }

    public function cleanCacheChoice($arrChoice = array('getListv3'))
    {
        return  $this->instance->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,$arrChoice);
    }

    /**
     * Delete list cache
     * @param array $arrKeys
     * Example : array('key1', 'key2')
     * @param int $timeout
     */
    public function deleteMulti($arrKeys, $timeout = 0) {
        return false;
    }

    /**
     * Increment single
     * @param string $key
     * @param int $value
     */
    public function increment($key, $value = 1) {
        return false;
    }

    /**
     * Increment multiple
     * @param array $arrKeys
     * Example : array('key1', 'key2')
     * @param int $value
     */
    public function incrementMulti($arrKeys, $value = 1) {
        return false;
    }

    /**
     * Decrement single
     * @param string $key
     * @param int $value
     */
    public function decrement($key, $value = 1) {
        return false;
    }

    /**
     * Decrement multiple
     * @param array $arrKeys
     * Example : array('key1', 'key2')
     * @param int $value
     */
    public function decrementMulti($arrKeys, $value = 1) {
        return false;
    }

}
