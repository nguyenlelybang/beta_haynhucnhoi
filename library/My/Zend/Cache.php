<?php

class My_Zend_Cache {

    static $cacheDb = false;

    static $cacheHtml = false;

    static $cacheCore = false;

    static $_cacheData = array();

    static function initCache()
    {
        if(false === self::$cacheDb) {

            self::$cacheDb = Zend_Cache::factory(
                'Core',
                'File',
                array(
                    'automatic_serialization' => true
                ,'cache_id_prefix' => 'db_'
                ,'lifetime' => GENERAL_CACHE_TIMEOUT
                ),
                array(
                    'cache_dir' => GENERAL_CACHE_DIR,
                )
            );

        }

        if(false === self::$cacheHtml) {
            self::$cacheHtml = Zend_Cache::factory(
                'Page',
                'File',
                array(
                    'lifetime' => GENERAL_CACHE_TIMEOUT
                ,'cache_id_prefix' => 'html_'
                    ,'debug_header' => false
                ),
                array(
                    'cache_dir' => GENERAL_CACHE_DIR,
                )
            );
        }

        if(false === self::$cacheCore) {
            self::$cacheCore = Zend_Cache::factory(
                'Core',
                'File',
                array(
                    'automatic_serialization' => true
                ,'cache_id_prefix' => 'cc_'
                ,'lifetime' => GENERAL_CACHE_TIMEOUT
                ),
                array(
                    'cache_dir' => GENERAL_CACHE_DIR
                )
            );
        }

        self::$_cacheData['getCurrentUrl'] = My_Zend_Util::getCurrentUrl(false);

        self::$_cacheData['cachehtml_keycache'] = false;

        if(self::checkIsRequestUriIsCacheable('html')) {
            self::$_cacheData['cachehtml_keycache'] = My_Zend_Util::getCurrentKeyUrl();
        }
    }

    public static function checkIsRequestUriIsCacheable($type_cache)
    {
        $keyCheck = 'checkIsRequestUriIsCacheable_'.$type_cache;
        if(!isset(self::$_cacheData[$keyCheck])) {

            $isCacheableStatus = true;

            if(!CACHE_ENABLE) {
                $isCacheableStatus = false;
            }

            if($isCacheableStatus) {
                if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']) {
                    if('GET' !== strtoupper($_SERVER['REQUEST_METHOD'])) {
                        $isCacheableStatus = false;
                    }
                }
            }

            if($isCacheableStatus) {
                if(isset($_POST) && $_POST && !empty($_POST)) {
                    $isCacheableStatus = false;
                }
            }

            if($isCacheableStatus) {
                if(defined('IS_AJAX') && IS_AJAX) {
                    $isCacheableStatus = false;
                }
            }

            if($isCacheableStatus) {
                if(preg_match('#^'.preg_quote(BASE_URL,'#').'/(admin|article/preview|queue|dang-nhap|dang-ky|thoat|captcha)#is', self::$_cacheData['getCurrentUrl'])) {
                    $isCacheableStatus = false;
                }
            }

            if('html' === $type_cache) {
                if($isCacheableStatus) {
                    if(defined('IS_LOGGED') && IS_LOGGED) {
                        $isCacheableStatus = false;
                    }
                }

                if($isCacheableStatus) {
                    if(preg_match('#\.(json)#is', self::$_cacheData['getCurrentUrl'])) {
                        $isCacheableStatus = false;
                    }
                }
            }

            self::$_cacheData[$keyCheck] = $isCacheableStatus;
        }

        return self::$_cacheData[$keyCheck];
    }

    public static function createKeyCache($params)
    {
        $findsReplaces = array();

        $arrayFields = array(
            'CurrentPublishedDate'
        ,'FromPublishedDate'
        ,'ToPublishedDate'
        );

        foreach($arrayFields as $val1) {
            if(isset($params[$val1]) && !empty($params[$val1])) {
                $valTemp1 = date('Y-m-d H:i:s',ceil(strtotime($params[$val1]) / DB_CACHE_TIMEOUT) * DB_CACHE_TIMEOUT);
                $findsReplaces[$params[$val1]] = $valTemp1;
                $params[$val1] = $valTemp1;
            }
        }

        $params = serialize($params);

        if(!empty($findsReplaces)) {
            $params = str_replace(array_keys($findsReplaces), array_values($findsReplaces), $params);
        }

        return hash('crc32b', $params);
    }

    public static function cleanAll()
    {
        if(false !== self::$cacheDb) {
            self::$cacheDb->clean(Zend_Cache::CLEANING_MODE_ALL);
        }

        if(false !== self::$cacheCore) {
            self::$cacheCore->clean(Zend_Cache::CLEANING_MODE_ALL);
        }

        self::cacheHTML_CleanAll();
    }

    public static function cleanAnyTags($tags)
    {
        $tags = (array)$tags;
        if(!empty($tags)) {
            $tags = array_unique($tags);

            if(false !== self::$cacheDb) {
                self::$cacheDb->clean(
                    Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
                    $tags
                );
            }
        }
    }


    public static function cacheHTML_Start()
    {
        if(isset(self::$_cacheData['cachehtml_keycache']) && self::$_cacheData['cachehtml_keycache']) {
            self::$cacheHtml->start(self::$_cacheData['cachehtml_keycache']);
        }
    }

    public static function cacheHTML_Cancel()
    {

        if(self::$_cacheData['cachehtml_keycache']) {
            self::$cacheHtml->cancel();
        }

    }

    public static function cacheHTML_CleanAll()
    {

        if(false !== self::$cacheHtml) {
            self::$cacheHtml->clean(Zend_Cache::CLEANING_MODE_ALL);
        }

        try {

            $client = new Zend_Http_Client(BASE_URL, array(
                'maxredirects' => 1,
                'timeout'      => 6	//seconds
            ));

            $client->request('PURGE');

        } catch (Exception $e) {

        }

    }




    /**
     * List instance
     *
     * @var array
     */
    private static $instances = array();

    /**
     * Constructor
     *
     */
    private final function __construct() {

    }

    /**
     * Get instance of class
     *
     * @param string $className
     * @return object
     */
    public final static function getInstance($options = array()) {
        //Check class name
        $adapter = $options['adapter'];

        //If empty className
        if (empty($adapter)) {
            throw new My_Zend_Cache_Exception("No instance of empty class when call My_Zend_Cache");
        }

        //Switch to get classname
        switch (strtolower($adapter)) {
            case 'memcache':
                $className = 'My_Zend_Cache_Adapter_Memcache';
                $key = $adapter;
                break;
            case 'memcachev1':
                $className = 'My_Zend_Cache_Adapter_MemcacheV1';
                $key = md5($options['memcachev1']['host'] . $options['memcachev1']['port']);
                break;
            case 'filecache':
                $className = 'My_Zend_Cache_Adapter_FileCache';
                $key = $adapter;
                break;
            default:
                throw new My_Zend_Cache_Exception("No instance of class $className");
                break;
        }

        //Put to list
        if (!isset(self::$instances[$key])) {
            self::$instances[$key] = new $className($options);
        }

        //Return object class
        return self::$instances[$key];
    }

    /**
     * Clone function
     *
     */
    private final function __clone() {

    }

}
My_Zend_Cache::initCache();
