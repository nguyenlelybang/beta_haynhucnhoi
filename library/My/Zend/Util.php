<?php

class My_Zend_Util
{
    public static function trim($arrInput) {
        if (!is_array($arrInput)) {
            $arrInput = trim($arrInput);
        } else {
            foreach ($arrInput as $key => $value) {
                $arrInput[$key] = My_Zend_Util::trim($value);
            }
        }
        return $arrInput;
    }

    public static function filterEntities($input) {
        if (is_array($input)) {
            foreach ($input as $key => $value) {
                $input[$key] = My_Zend_Util::filterEntities($value);
            }

            return $input;
        }

        return str_replace(
            array("<", ">", "\'", "\"", "'", '"'), array("&lt;", "&gt;", "&#39;", "&quot;", "&#39;", "&quot;"), trim(strip_tags($input))
        );
    }

    public static function removeEntities($string) {
        $str = strip_tags($str);
        return str_replace(array('/', '<', '?', '>'), '', $string);
    }

    public static function urlencode_rfc3986($input) {
        if (is_array($input)) {
            return array_map(array('My_Zend_Util', 'urlencode_rfc3986'), $input);
        } else if (is_scalar($input)) {
            return str_replace('+', ' ', str_replace('%7E', '~', rawurlencode($input)));
        }
        return '';
    }

    public static function urldecode_rfc3986($string) {
        return urldecode($string);
    }

    public static function substr($strInput, $intLenght, $strEnchar = '...', $bolCath = true) {
        ini_set('iconv.internal_encoding', 'utf-8');
        $strReturn = $strInput;
        if ($strInput) {
            if (iconv_strlen($strInput) > $intLenght) {
                if ($bolCath) {
                    $arrTemp = explode(' ', $strInput);
                    $strReturn = '';
                    foreach ($arrTemp as $strString) {
                        if (iconv_strlen($strReturn . $strEnchar) > $intLenght) {
                            break;
                        }
                        $strReturn .= $strString . ' ';
                    }
                    return trim($strReturn) . $strEnchar;
                } else {
                    return iconv_substr($strInput, 0, $intLenght) . $strEnchar;
                }
            }
        }
        return $strReturn;
    }

    public static function getRealIpAddr() {
        $ip = NULL;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif(isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function getCurrentUrl($hasPort = true) {
        $pageURL = 'http';
        $_SERVER["HTTPS"] = isset($_SERVER["HTTPS"]) ? $_SERVER["HTTPS"] : null;
        $_SERVER["SERVER_PORT"] = isset($_SERVER["SERVER_PORT"]) ? $_SERVER["SERVER_PORT"] : null;
        $_SERVER["SERVER_NAME"] = isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : null;
        $_SERVER["REQUEST_URI"] = isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : null;
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {

            if($hasPort) {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public static function getCurrentKeyUrl($url = '') 
    {
        if(!$url) $url = self::getCurrentUrl();

        $url = md5('ismobile' . $url);

        return $url;
    }
    
    public static function getDatesBetween2Dates($startTime, $endTime, $format = 'Y-m-d') {
        $day = 86400;
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    public static function isubstr($strInput, $intLenght, $strEnchar = '...', $bolCath = TRUE) {
        ini_set('iconv.internal_encoding', 'utf-8');
        $strReturn = $strInput;
        if ($strInput) {
            if (iconv_strlen($strInput) > $intLenght) {
                if ($bolCath) {
                    $arrTemp = explode(' ', $strInput);
                    $strReturn = '';
                    foreach ($arrTemp as $strString) {
                        if (iconv_strlen($strReturn . $strEnchar) > $intLenght) {
                            break;
                        }
                        $strReturn .= $strString . ' ';
                    }
                    return trim($strReturn) . $strEnchar;
                } else {
                    return iconv_substr($strInput, 0, $intLenght) . $strEnchar;
                }
            }
        }
        return $strReturn;
    }

    public static function minifyHTML($string) 
    {
        $keyCache = md5($string);

        $result = My_Zend_Cache::$cacheCore->load($keyCache);

        if($result) {
            return $result;
        } else {
            $string = Minify_HTML::minify($string, array(
                'cssMinifier' => array('Minify_CSS', 'minify'),
                'jsMinifier' => array('JSMin', 'minify')
            ));

            $search = array(
                '#(/?>)[\s \t]+#s',  // strip whitespaces after tags, except space
                '#[\s \t]+<#s',  // strip whitespaces before tags, except space
                //'#([\s \t]){2,}#s',       // shorten multiple whitespace sequences
                '#<([a-z0-9]+)[\s \t]+#s'
            );

            $replace = array(
                '$1 ',
                ' <',
                //'$1',
                '<$1 '
            );

            $string = preg_replace($search, $replace, $string);

            $result = trim($string); unset($string);

            My_Zend_Cache::$cacheCore->save($result, $keyCache);
        }
        
        return $result;
    }

}