<?php

if (isset($_GET['debug']) && $_GET['debug'] == 'cnlm') {
    register_shutdown_function("fatal_handler");
}

function fatal_handler() {
    $errfile = "unknown file";
    $errstr = "shutdown";
    $errno = E_CORE_ERROR;
    $errline = 0;

    $error = error_get_last();

    if ($error !== NULL) {
        $errno = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr = $error["message"];
    }

    echo 'Fatal Error::errorNo: ' . $errno . ' - ErrorString: ' . $errstr . ' - ErrorFile: ' . $errfile . ' - ErrorLine: ' . $errline;
    die;
}

class My_Zend_Plugin_Env extends Zend_Controller_Plugin_Abstract {

    //Debug mode
    private $_buildStaticPage = 0;
    private $_staticPageName = '';
    //Starttime and stoptime
    private $starttime = 0;
    private $stoptime = 0;
    //Caching object
    private $caching = null;

    // static cache key
    private $_module;
    private $_controller;
    private $_action;
    private $_generalConfig = null;

    /**
     * Called before Zend_Controller_Front calls on the router
     * to evaluate the request against the registered routes.
     * @param Zend_Controller_Request_Abstract $request
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request) {
        //Get debug
        $debug = $request->getParam('debug');
        $this->_buildStaticPage = $request->getParam('build_static_page');
        $this->_staticPageName = $request->getParam('static_page_name');

        if (isset($_GET['buildhome']) && $_GET['buildhome'] == 'fuck') {
            $this->_buildStaticPage = 1;
            $this->_staticPageName = 'index.html';
        }

        //Logging
        if ($debug && in_array(APPLICATION_ENVIRONMENT, array('development', 'staging','development-phuong','development-nam','development-khanh'))) {
            define('DEBUG_MODE', true);
            $this->starttime = gettimeofday(true);
            My_Zend_Globals::dumpLogger('Called before Zend_Controller_Front calls on the router(' . gettimeofday(true) . ')');
        } else {
            define('DEBUG_MODE', false);
        }
    }

    /**
     * Called after the router finishes routing the request.
     * @param Zend_Controller_Request_Abstract $request
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request) {
        //Logging
        if (DEBUG_MODE == true) {
            My_Zend_Globals::dumpLogger('Called after the router finishes routing the request(' . gettimeofday(true) . ')');
        }
    }

    /**
     * Called before Zend_Controller_Front enters its dispatch loop.
     * @param Zend_Controller_Request_Abstract $request
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
        //Get module
        $this->_module = strtolower($request->getParam('module'));
        //Get controller
        $this->_controller = strtolower($request->getParam('controller'));
        //Get action
        $this->_action = strtolower($request->getParam('action'));

        //If empty then set default
        $layout = 'default';
        if (empty($this->_module)) {
            $this->_module = 'default';
        }

        $layout = $this->_module;

        // get general config
        //Set config path
        $configs_path = APPLICATION_PATH . '/configs/' . $this->_module;

        //Load configuration
        $generalConfig = new Zend_Config_Ini($configs_path . '/general.ini', APPLICATION_ENVIRONMENT);

        //To array config
        $this->_generalConfig = $generalConfig->toArray();

        //Get Ini Configuration
        $configuration = My_Zend_Globals::getConfiguration();

        //Setup Include Paths
        set_include_path(implode(PATH_SEPARATOR, array(
            APPLICATION_PATH . '/models',
            get_include_path()
        )));

        if ($this->_action !== 'login') {
            //Init User
            $this->initUser($this->_module, $request);
        }
        else {
            define('LOGIN_DISPLAYNAME', '');
            define('LOGIN_UNAME', '');
            define('LOGIN_UID', 0);
        }

        //Layout setup
        $layout_instance = Zend_Layout::startMvc(
            array(
                'layout' => 'layout',
                'layoutPath' => APPLICATION_PATH . '/layouts/' . $layout,
                'contentKey' => 'content'
            )
        );

        $GLOBALS['categories'] = Category::selectCategoryList();

        //Set configuration
        $layout_instance->_general = $this->_generalConfig;

        //Logging
        if (DEBUG_MODE == true) {
            My_Zend_Globals::dumpLogger('Debug !!!' . ' remote=' . My_Zend_Globals::getAltIp() . ' url=' . $_SERVER["REQUEST_URI"] . ' - module:' . $request->getModuleName() . ' - controller:' . $request->getControllerName() . ' - action:' . $request->getActionName());
        }

        //Cleanup
        unset($configs_path, $general_config);
        define('CURRENT_CONTROLLER', $this->_controller);
        define('CURRENT_ACTION', $this->_action);
    }

    /**
     * Called before an action is dispatched by the dispatcher.
     * This callback allows for proxy or filter behavior.
     * By altering the request and resetting its dispatched flag
     * via Zend_Controller_Request_Abstract::setDispatched(false),
     * the current action may be skipped and/or replaced.
     * @param Zend_Controller_Request_Abstract $request
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        //Logging
        if (DEBUG_MODE == true) {
            My_Zend_Globals::dumpLogger('Called before an action is dispatched by the dispatcher(' . gettimeofday(true) . ')');
        }
    }

    /**
     * Called after an action is dispatched by the dispatcher.
     * This callback allows for proxy or filter behavior.
     * By altering the request and resetting its dispatched flag
     * via Zend_Controller_Request_Abstract::setDispatched(false)),
     * a new action may be specified for dispatching.
     * @param Zend_Controller_Request_Abstract $request
     */
    public function postDispatch(Zend_Controller_Request_Abstract $request) {
        /* if(!isset($_COOKIE['sync']))
          {
          $configuration = My_Zend_Globals::getConfiguration();
          $api_passport  = $configuration->api->passport->toArray();
          $url = BASE_URL.'/user/session?redirect='.$_SERVER["REQUEST_URI"];
          $this->redirect($api_passport['ssoredir'].'?url='.urlencode($url).'&getsession=1');
          } */

        //Logging
        if (DEBUG_MODE == true) {
            My_Zend_Globals::dumpLogger('Called after an action is dispatched by the dispatcher(' . gettimeofday(true) . ')');
        }
    }

    /**
     * Called after Zend_Controller_Front exits its dispatch loop.
     */
    public function dispatchLoopShutdown() {
        //Logging for debug
        if (DEBUG_MODE == true) {
            //Write Logging
            My_Zend_Globals::dumpLogger("Called after Zend_Controller_Front exits its dispatch loop(" . gettimeofday(true) . ")", true);

            //Add respone Page rander
            $this->stoptime = gettimeofday(true);
            $print = '<br/><br/><br/><table border="1" cellspacing="5" cellpadding="5">';
            $print .= '<tr><td><b>page render time : ' . ($this->stoptime - $this->starttime) . ' Seconds</b></td></tr>';
            $print .= "</table>";

            //Add respone Memory Usage
            $print .= '<br/><br/><br/><table border="1" cellspacing="5" cellpadding="5">';
            $print .= '<tr><td><b>Memory : ' . round(memory_get_usage(true) / 1048576, 2) . ' Mb</b></td></tr>';
            $print .= "</table>";

            //Get Block Cache profiler
            //$configuration = Zend_Registry::get(BLOCK_CONFIGURATION);
            if (is_null($configuration)) {
                $configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/block.ini', APPLICATION_ENVIRONMENT);
            }
            $profiler = My_Zend_Cache_Profiler::getInstance($configuration->caching->profiler->toArray());
            $print .= $profiler->getProfilerData('Block');
            $print .= $this->_getDBProfiler();
            //Add body
            $this->getResponse()->appendBody($print);

            if (extension_loaded('xhprof')) {
                $profiler_namespace = 'myapp';  // namespace for your application
                $xhprof_data = xhprof_disable();
                $xhprof_runs = new XHProfRuns_Default();
                $run_id = $xhprof_runs->save_run($xhprof_data, $profiler_namespace);

                // url to the XHProf UI libraries (change the host name and path)
                $profiler_url = sprintf('http://xhprof.123mua.vn/index.php?run=%s&source=%s', $run_id, $profiler_namespace);
                echo '<a href="' . $profiler_url . '" target="_blank">Profiler output</a>';
            }
        }

        if ($this->_buildStaticPage && $this->_staticPageName != '') {
            $output = $this->getResponse()->getBody();
            //compress html
            $output = My_Zend_Globals::stripBuffer($output);
            $output .= "<!-- " . date('H:i:s d/m/Y') . " --> \n";
            // set output
            $this->getResponse()->setBody($output);
            $this->buildStaticPage($this->_staticPageName, $output);
        }
    }

    private function _getDBProfiler() {
        $db = My_Zend_Globals::getStorage();
        $profiler = $db->getProfiler();

        $totalTime = $profiler->getTotalElapsedSecs();
        $queryCount = $profiler->getTotalNumQueries();
        $longestTime = 0;
        $longestQuery = null;

        if ($queryCount > 0) {
            $print = '';
            $count = 1;
            foreach ($profiler->getQueryProfiles() as $query) {
                if ($query->getElapsedSecs() > $longestTime) {
                    $longestTime = $query->getElapsedSecs();
                    $longestQuery = $query->getQuery();
                }
                $print .= '<tr><td>' . $count . '</td><td>' . $query->getElapsedSecs() . '</td><td>' . $query->getQuery() . '</td></tr>';
                $count++;
            }
        } else {
            $print = '<tr><td colspan="3" align="center">No query found</td></tr>';
        }

        $html = '<br/><br/><br/><table border="1" cellspacing="2" cellpadding="2" width="100%"><tr><th colspan="8" bgcolor=\'#dddddd\'>DB Profiler</th></tr>';
        $html .= '<tr><td align="left" width="20">Number</td><td align="left" width="180">Elapsed time</td><td align="left">Query</td></tr>';
        $html .= $print;
        if ($queryCount > 0) {
            $html .= '<tr><td colspan="3">';
            $html .= '- Executed <b>' . $queryCount . ' queries</b> in <b>' . $totalTime . ' seconds</b>' . "<br/>";
            $html .= '- Average query length: <b>' . $totalTime / $queryCount . ' seconds</b>' . "<br/>";
            $html .= '- Queries per second: <b>' . $queryCount / $totalTime . "</b><br/>";
            $html .= '- Longest query length: <b>' . $longestTime . "</b><br/>";
            $html .= "- Longest query: <br/><b>" . $longestQuery . "</b><br/>";
            $html .= '</td></tr>';
        }
        return $html;
    }

    /**
     * Check login
     * @param $module
     * @param $request
     */
    private function checkLogin($module = 'default', $request = null) {
        /*
          return true;
          // */
        switch ($module) {
            case 'default':
                if (LOGIN_UID <= 0) {
                    //Redirect login page
                    //$this->redirect(LOGIN_URL.'?redirect='.urlencode($_SERVER["REQUEST_URI"]));
                    $this->redirect(LOGIN_URL);
                }
                break;
            case 'adm':
                if (LOGIN_UID <= 0) {
                    //Redirect login page
                    $this->redirect(LOGIN_ADM_URL . '?redirect=' . urlencode($_SERVER["REQUEST_URI"]));
                }
                break;
            case 'adm_reborn':
                if (LOGIN_UID <= 0) {
                    //Redirect login page
                    $this->redirect(LOGIN_ADM_REBORN_URL . '?redirect=' . urlencode($_SERVER["REQUEST_URI"]));
                }
                break;
            default:
                //$this->redirect(LOGIN_URL.'?redirect='.urlencode($_SERVER["REQUEST_URI"]));
                $this->redirect(LOGIN_URL);
                break;
        }

        return false;
    }

    private function setStaticCache($controller, $action) {

    }

    /**
     * Init User Information
     * @param $module
     * @param $request
     */
    private function initUser($module = 'default', $request = null) {
        //*
        $userName = '';
        $displayName = '';
        $userId = 0;
        $roleId = 0;

        if ($this->_action != 'login') {

        }

        switch ($module) {
            case 'default':
                if (!empty($_COOKIE['_hu'])) {
                    $auth = Zend_Auth::getInstance();
                    $auth->setStorage(new Zend_Auth_Storage_Session('Default'));

                    if ($auth->hasIdentity()) {
                        $identity = $auth->getIdentity();
                        $userId = $identity->user_id;
                    }

                    if ($userId !== false && $userId > 0) {
                        //Check user
                        $user = User::getUser($userId, false);

                        if ($user['status'] == 0) {
                            $this->redirect(LOCKED_URL);
                        }

                        //Startup user
                        if (!empty($user)) {
                            $displayName = $user['fullname'];
                        } else {
                            $userId = 0;
                        }
                    }

                    // check if admin
                    $auth = Zend_Auth::getInstance();
                    $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN'));

                    if ($auth->hasIdentity()) {
                        $identity = $auth->getIdentity();
                        $adminId = $identity->user_id;
                        $admin = Admin::getUser($adminId);
                        $roleId = $admin['role_id'];
                    }
                }
                break;
            case 'adm':
                setcookie('lcache', 1, time() + 9999999 , '/');
                $auth = Zend_Auth::getInstance();
                $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN1111'));

                if ($auth->hasIdentity()) {
                    $identity = $auth->getIdentity();

                    $userId = $identity->user_id;
                    $userName = $identity->user_name;
                } else {
                    $this->redirect(BASE_URL . '/'. ADM_FOLDER .'/index/login');
                }

                $this->checkLock($module, $userId);
                break;
            case 'adm_reborn':
                setcookie('lcache', 1, time() + 9999999 , '/');
                $auth = Zend_Auth::getInstance();
                $auth->setStorage(new Zend_Auth_Storage_Session('AdmHNN1111'));

                if ($auth->hasIdentity()) {
                    $identity = $auth->getIdentity();

                    $userId = $identity->user_id;
                    $userName = $identity->user_name;
                } else {
                    $this->redirect(BASE_URL . '/'. ADM_REBORN_FOLDER .'/index/login');
                }

                $this->checkLock($module, $userId);
                break;
            default:
                break;
        }

        //*/
        define('LOGIN_DISPLAYNAME', $displayName);
        define('LOGIN_UNAME', $userName);
        define('LOGIN_UID', (int) $userId);
        defined('SYSTEM_USER_ROLE') || define('SYSTEM_USER_ROLE', $roleId);

    }

    private function checkLock($module = 'default', $userId) {
        switch ($module) {
            case 'default':
                //Check empty
                if (empty($userId)) {
                    $this->redirect(LOGIN_URL);
                }

                //Select user
                $user = User::selectUser($userId);

                //Check status lock
                if ($user['is_locked']) {
                    //Redirect
                    $this->redirect(LOCKED_URL);
                }

                break;
            case 'adm':
                //Check empty
                if (empty($userId)) {
                    $this->redirect(LOGIN_URL);
                }

                //Select user
                $user = Admin::getUser($userId);

                if (empty($user) || $user['is_locked']) {
                    $this->redirect(LOCKED_ADM_URL);
                }

                Zend_Registry::set(SYSTEM_USER, $user);
                define('SYSTEM_USER_ROLE', $user['role_id']);
                define('SYSTEM_USER_IP', My_Zend_Globals::getAltIp());

                break;
            case 'adm_reborn':
                //Check empty
                if (empty($userId)) {
                    $this->redirect(LOGIN_URL);
                }

                //Select user
                $user = Admin::getUser($userId);

                if (empty($user) || $user['is_locked']) {
                    $this->redirect(LOCKED_ADM_URL);
                }

                Zend_Registry::set(SYSTEM_USER, $user);
                define('SYSTEM_USER_ROLE', $user['role_id']);
                define('SYSTEM_USER_IP', My_Zend_Globals::getAltIp());

                break;
            default:
                break;
        }
    }

    /**
     * Redirect
     * @param string $url
     */
    private function redirect($url) {
        //Hien thi thong tin content
        if (ob_get_length() != false) {
            ob_end_clean();
        }
        echo '<script type="text/javascript">window.parent.location = "' . $url . '";</script>';
        exit();
    }

    /**
     *
     * set static page to cache
     * @param $fileName
     * @param $data
     */
    private function buildStaticPage($fileName, $data) {
        $staticFolder = 'p';

        if (isset($_GET['is_mobile_page']) && $_GET['is_mobile_page'] == 'iscnlmmobilepage') {
            $staticFolder = 'mp';
        }

        preg_match('/photo\/(\d+)/', $_SERVER['REQUEST_URI'], $matches);
        if (!empty($matches)) {
            $staticFolder = 'post/'. $staticFolder;
            $fileName = 'post-'. $matches[1] . '.html';
        }


        $cacheFile = PAGES_PATH . "/" . $staticFolder . "/" . $fileName;
        if (!is_dir(PAGES_PATH . "/" . $staticFolder)) {
            $folders = explode('/', $staticFolder);

            if (count($folders) > 0) {
                //unset($folders[count($folders) - 1]);
                $path = PAGES_PATH . "/";

                foreach ($folders as $folder) {
                    $path .= $folder . '/';

                    if (!is_dir($path)) {
                        @mkdir($path);
                    }
                }
            }
        }

        if (strpos($fileName, '/') !== false) {
            $folders = explode('/', $fileName);

            if (count($folders) > 1) {
                unset($folders[count($folders) - 1]);
                $path = PAGES_PATH . "/" . $staticFolder . "/";

                foreach ($folders as $folder) {
                    $path .= $folder . '/';

                    if (!is_dir($path)) {
                        @mkdir($path);
                    }
                }
            }
        }

        $fh = fopen($cacheFile, 'w');
        fwrite($fh, $data);
    }
}
