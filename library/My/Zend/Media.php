<?php

class My_Zend_Media
{

	const _MEDIA_TYPE_YOUTUBE   = 'youtube';
	const _MEDIA_TYPE_VIMEO     = 'vimeo';
	const _MEDIA_TYPE_ZINGTV    = 'zing_tv';
	const _MEDIA_TYPE_ZINGMP3   = 'zing_mp3';
	const _MEDIA_TYPE_NCT_MP3   = 'nct_mp3';
	const _MEDIA_TYPE_NCT_VIDEO = 'nct_video';
	const _MEDIA_TYPE_MECLOUD = 'mecloud';

	public static function getMediaType($link)
	{

		$link = urldecode(trim($link));

		if (strpos($link, 'youtube.com') !== false || strpos($link, 'youtu.be') !== false) {
			return self::_MEDIA_TYPE_YOUTUBE;
		} else if (strpos($link, 'vimeo.com') !== false) {
			return self::_MEDIA_TYPE_VIMEO;
		} else if (strpos($link, 'tv.zing.vn/video') !== false) {
			return self::_MEDIA_TYPE_ZINGTV;
		} else if (strpos($link, 'mp3.zing.vn') !== false) {
			return self::_MEDIA_TYPE_ZINGMP3;
		} else if (strpos($link, 'nhaccuatui.com/bai-hat') !== false) {
			return self::_MEDIA_TYPE_NCT_MP3;
		} elseif (strpos($link, 'nhaccuatui.com/video') !== false) {
			return self::_MEDIA_TYPE_NCT_VIDEO;
		} elseif (strpos($link, '[mecloud]') !== false) {
			return self::_MEDIA_TYPE_MECLOUD;
		}

		return '';
	}

	public static function getDataByCurl($url, $params)
	{

		$connection = curl_init();
		curl_setopt($connection, CURLOPT_URL, $url);
		curl_setopt($connection, CURLOPT_VERBOSE, 1);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $params);
		$data = curl_exec($connection);

		/*
		if(curl_errno($connection))
		{
			echo 'error:' . curl_error($connection);
		}*/

		if ($data !== false) {
			return $data;
		} else {
			return false;
		}

	}

	public static function parseYoutube($link)
	{

		$regexstr = '~
            # Match Youtube link and embed code
            (?:                             # Group to match embed codes
                (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                |(?:                        # Group to match if older embed
                    (?:<object .*>)?      # Match opening Object tag
                    (?:<param .*</param>)*  # Match all param tags
                    (?:<embed [^>]*src=")?  # Match embed tag to the first quote of src
                )?                          # End older embed code group
            )?                              # End embed code groups
            (?:                             # Group youtube url
                https?:\/\/                 # Either http or https
                (?:[\w]+\.)*                # Optional subdomains
                (?:                         # Group host alternatives.
                youtu\.be/                  # Either youtu.be,
                | youtube\.com              # or youtube.com
                | youtube-nocookie\.com     # or youtube-nocookie.com
                )                           # End Host Group
                (?:\S*[^\w\-\s])?           # Extra stuff up to VIDEO_ID
                ([\w\-]{11})                # $1: VIDEO_ID is numeric
                [^\s]*                      # Not a space
            )                               # End group
            "?                              # Match end quote if part of src
            (?:[^>]*>)?                       # Match any extra stuff up to close brace
            (?:                             # Group to match last embed code
                </iframe>                 # Match the end of the iframe
                |</embed></object>          # or Match the end of the older embed
            )?                              # End Group of last bit of embed code
            ~ix';

		preg_match($regexstr, $link, $matches);

		return $matches[1];
	}

	public static function parseVimeo($link)
	{

		$regexstr = '~
            # Match Vimeo link and embed code
            (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
            (?:                         # Group vimeo url
                https?:\/\/             # Either http or https
                (?:[\w]+\.)*            # Optional subdomains
                vimeo\.com              # Match vimeo.com
                (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                \/                      # Slash before Id
                ([0-9]+)                # $1: VIDEO_ID is numeric
                [^\s]*                  # Not a space
            )                           # End group
            "?                          # Match end quote if part of src
            (?:[^>]*></iframe>)?        # Match the end of the iframe
            (?:<p>.*</p>)?              # Match any title information stuff
            ~ix';
		preg_match($regexstr, $link, $matches);

		return $matches[1];
	}


	/**
	 * @param string $youtubeId ID of the YouTube clip
	 * @param string $type Thumbnail size. 'sd', 'hq', 'maxres'
	 *
	 * @return string
	 */
	public static function getYouTubeThumbnail($youtubeId, $type = 'sd')
	{

		$thumb = 'http://img.youtube.com/vi/' . $youtubeId . '/sddefault.jpg';

		return $thumb;
	}

	public static function getVimeoThumbnail($vimeoId)
	{

		try {
			$curl     = curl_init();
			$vimeoUrl = 'http://vimeo.com/api/v2/video/' . $vimeoId . '.php';
			curl_setopt($curl, CURLOPT_URL, $vimeoUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5);
			$data      = curl_exec($curl);
			$data      = @unserialize($data);
			$thumbnail = $data[0]['thumbnail_large'];

			return $thumbnail;
		} catch (Exception $ex) {
			return false;
		}
	}

	public static function getYouTubeIDFromURL($url)
	{

		if (strpos($url, 'http://') === false && strpos($url, 'https://') === false) {
			$url = 'https://' . $url;
		}

		$youtubeId = preg_replace('~
            # Match non-linked youtube URL in the wild. (Rev:20130823)
            https?://         # Required scheme. Either http or https.
            (?:[0-9A-Z-]+\.)? # Optional subdomain.
            (?:               # Group host alternatives.
              youtu\.be/      # Either youtu.be,
            | youtube         # or youtube.com or
              (?:-nocookie)?  # youtube-nocookie.com
              \.com           # followed by
              \S*             # Allow anything up to VIDEO_ID,
              [^\w\s-]       # but char before ID is non-ID char.
            )                 # End host alternatives.
            ([\w-]{11})      # $1: VIDEO_ID is exactly 11 chars.
            (?=[^\w-]|$)     # Assert next char is non-ID or EOS.
            (?!               # Assert URL is not pre-linked.
              [?=&+%\w.-]*    # Allow URL (query) remainder.
              (?:             # Group pre-linked alternatives.
                [\'"][^<>]*>  # Either inside a start tag,
              | </a>          # or inside <a> element text contents.
              )               # End recognized pre-linked alts.
            )                 # End negative lookahead assertion.
            [?=&+%\w.-]*        # Consume any URL (query) remainder.
            ~ix', '$1', $url);

		return $youtubeId;
	}

	public static function getVimeoIDFromURL($url)
	{

		$videoId = 0;
		if (preg_match("/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/", $url, $id)) {
			$videoId = $id[3];
		}

		return $videoId;
	}

	public static function getZingMp3IDFromURL($url)
	{

		$regExp = "/^(http:\/\/)?mp3\.zing\.vn\/(bai-hat|album|playlist)\/([A-Za-z-\d]+)\/(\w{8})(\.html)$/";
		preg_match($regExp, $url, $matches);
		if ($matches && strlen($matches[4]) == 8) {
			return $matches[4];
		}

		return '';
	}

	public static function getZingTVIDFromURL($url)
	{
		$regExp = "/^(?:http(?:s)?:\/\/)?(www\.)?tv\.zing\.vn\/video\/([A-Za-z-\d]+)(\/(\w{8}\.html))?\/(\w{8})(\.html)$/";
		preg_match($regExp, $url, $matches);
		if ($matches && strlen($matches[5]) == 8) {
			return $matches[5];
		}
		return '';
	}

	public static function retrieveYouTubeInfo($youtubeId)
	{

		$youtubeId = trim($youtubeId);
		if (strpos($youtubeId, 'http://') !== false || strpos($youtubeId, 'https://') !== false || strpos($youtubeId, 'youtube.com') !== false) {
			$youtubeId = self::getYouTubeIDFromURL($youtubeId);
		}

		if (empty($youtubeId)) {
			return array();
		}

		$apiUrl = 'https://www.googleapis.com/youtube/v3/videos?id=' . $youtubeId . '&key=' . YOUTUBE_API_KEY . '&fields=items(id,snippet,contentDetails)&part=snippet,contentDetails';
		try {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $apiUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5);
			$data = curl_exec($curl);
			if ( ! $data) {
				throw new Exception(curl_error($curl), curl_errno($curl));
			}

			if (empty($data)) {
				return array();
			}

			$data = Zend_Json::decode($data);
			if (count($data['items']) == 0) {
				return array();
			}
			$info   = $data['items'][0];
			$thumbs = array();
			foreach ($info['snippet']['thumbnails'] as $key => $thumb) {
				$thumbs[$key] = $thumb['url'];
			}

			$duration = $info['contentDetails']['duration'];
			preg_match_all('/(\d+)/', $duration, $parts);

			$strDuration = '';
			foreach ($parts[0] as $item) {
				$strDuration .= $item . ':';
			}

			if (count($parts[0]) == 1) {
				$strDuration = '0:' . $strDuration;
			}

			$strDuration = trim($strDuration, ':');
			$returnData  = array('id' => $youtubeId, 'title' => $info['snippet']['title'], 'desc' => $info['snippet']['description'], 'thumb' => $thumbs, 'duration' => $duration, 'duration_formatted' => $strDuration, 'categoryId' => $info['snippet']['categoryId']);

			return $returnData;
		} catch (Exception $ex) {
			echo $ex->getMessage();

			return array();
		}
	}

	public static function retrieveVimeoInfo($vimeoId)
	{

		$vimeoId = trim($vimeoId);
		//if (strpos($youtubeId, 'youtube.com') !== false)
		if (strpos($vimeoId, 'http://') !== false || strpos($vimeoId, 'https://') !== false || strpos($vimeoId, 'vimeo.com') !== false) {
			$vimeoId = self::getVimeoIDFromURL($vimeoId);

		}
		if (empty($vimeoId)) {
			return array();
		}

		$apiUrl = 'http://vimeo.com/api/v2/video/' . $vimeoId . '.json';

		try {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $apiUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5);
			$data = curl_exec($curl);
			if (empty($data)) {
				return array();
			}

			$data   = Zend_Json::decode($data);
			$info   = $data[0];
			$thumbs = array(
				'default' => $info['thumbnail_small'],
				'media' => $info['thumbnail_medium'],
				'heigh' => $info['thumbnail_large']
			);

			$duration = $info['duration'];
			$hours    = floor($duration / 3600);
			$mins     = floor(($duration - ($hours * 3600)) / 60);
			$secs     = floor($duration % 60);

			$strDuration = '';
			if ($hours > 0) {
				$strDuration .= $hours . ':';
			}

			if ($mins > 0) {
				if ($hours > 0) {
					$strDuration .= sprintf('%02d', $mins) . ':';
				} else {
					$strDuration .= $mins . ':';
				}
			}
			$strDuration .= sprintf('%02d', $secs);
			$returnData = array(
				'id' => $vimeoId,
				'title' => $info['title'],
				'desc' => $info['description'],
				'thumb' => $thumbs,
				'duration' => $duration,
				'duration_formatted' => $strDuration,
				'uploader' => $info['user_name'],
				'categoryId' => 0
			);

			return $returnData;
		} catch (Exception $ex) {
			return array();
		}
	}

	public static function retrieveZingTVInfo($videoId)
	{

		$videoId = trim($videoId);
		//if (strpos($youtubeId, 'youtube.com') !== false)
		if (strpos($videoId, 'http://') !== false || strpos($videoId, 'https://') !== false || strpos($videoId, 'tv.zing.vn') !== false) {
			$videoId = self::getZingTVIDFromURL($videoId);
		}

		if (empty($videoId)) {
			return array();
		}

		$apiUrl = 'http://api.tv.zing.vn/2.0/media/info?api_key=d803c39d0d1112b5c8814ba93103b8f5&media_id=' . $videoId;

		try {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $apiUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5);
			$data = curl_exec($curl);
			if (empty($data)) {
				return array();
			}

			$data     = Zend_Json::decode($data);
			$info     = $data['response'];
			$duration = $info['duration'];
			$hours    = floor($duration / 3600);
			$mins     = floor(($duration - ($hours * 3600)) / 60);
			$secs     = floor($duration % 60);

			$strDuration = '';
			if ($hours > 0) {
				$strDuration .= $hours . ':';
			}

			if ($mins > 0) {
				if ($hours > 0) {
					$strDuration .= sprintf('%02d', $mins) . ':';
				} else {
					$strDuration .= $mins . ':';
				}
			}
			$strDuration .= sprintf('%02d', $secs);
			$returnData = array(
				'id' => $videoId,
				'title' => $info['full_name'],
				'desc' => $info['program_name'],
				'thumb' => "http://image.mp3.zdn.vn/tv_media_854_475/" . $info['thumbnail'],
				'duration' => $duration,
				'duration_formatted' => $strDuration,
				'uploader' => 'Zing TV',
				'categoryId' => 0
			);

			return $returnData;
		} catch (Exception $ex) {
			return array();
		}
	}

	public static function retrieveZingMp3Info($mp3Id)
	{

		$mp3Id = trim($mp3Id);
		//if (strpos($youtubeId, 'youtube.com') !== false)
		if (strpos($mp3Id, 'http://') !== false || strpos($mp3Id, 'https://') !== false || strpos($mp3Id, 'mp3.zing.vn') !== false) {
			$mp3Id = self::getZingMp3IDFromURL($mp3Id);
		}

		if (empty($mp3Id)) {
			return array();
		}
		$apiUrl = 'http://api.tv.zing.vn/2.0/media/info?api_key=d803c39d0d1112b5c8814ba93103b8f5&media_id=' . $videoId;
		try {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $apiUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 5);
			$data = curl_exec($curl);
			if (empty($data)) {
				return array();
			}

			$data        = Zend_Json::decode($data);
			$info        = $data['response'];
			$duration    = $info['duration'];
			$hours       = floor($duration / 3600);
			$mins        = floor(($duration - ($hours * 3600)) / 60);
			$secs        = floor($duration % 60);
			$strDuration = '';

			if ($hours > 0) {
				$strDuration .= $hours . ':';
			}

			if ($mins > 0) {
				if ($hours > 0) {
					$strDuration .= sprintf('%02d', $mins) . ':';
				} else {
					$strDuration .= $mins . ':';
				}
			}

			$strDuration .= sprintf('%02d', $secs);
			$returnData = array('id' => $videoId, 'title' => $info['full_name'], 'desc' => $info['program_name'], 'thumb' => "http://image.mp3.zdn.vn/tv_media_210_120/" . $info['thumbnail'], 'duration' => $duration, 'duration_formatted' => $strDuration, 'uploader' => 'Zing TV', 'categoryId' => 0);

			return $returnData;
		} catch (Exception $ex) {
			return array();
		}
	}

	public static function generateZingTVEmbedURLForYouTube($youtubeID)
	{

		$firstSegment = 'http://tv.zing.vn/cms-embed';
		$youtubeURL   = 'https://www.youtube.com/embed/' . $youtubeID;
		$cpID         = '4169';
		//$secureKey = '093644b2605ca74d5ca2e2ea1c932636';
		$const     = 'ztv@ZTV_123@z';
		$secureKey = md5($const . $cpID . $youtubeURL);

		return $firstSegment . '?url=' . $youtubeURL . '&cp_id=' . $cpID . '&key=' . $secureKey;
	}

	public static function getIDNCT($url)
	{

		$regExp = "/\.([a-zA-Z0-9]+)\.html$/";
		preg_match($regExp, $url, $matches);
		$str = ! empty($matches[1]) ? $matches[1] : '';

		return $str;
	}

	public static function getTokenNCT()
	{

		$token = '';
		try
		{
			$APIurl = 'https://id.nct.vn/oauth/token';
			$params = array(
				'grant_type' => 'get_token',
				'client_id' => 4356226465,
				'client_secret' => '82a60db402314188b8b534ec77b7c3ac'
			);
			$result = self::getDataByCurl($APIurl, $params);
			if ($result === false) {
				My_Zend_Logger::log('Post::getTokenNCT - ' . curl_error($ch));
			}
			$arr   = Zend_Json::decode($result);
			$token = ! empty($arr['token']['access_token']) ? $arr['token']['access_token'] : '';

		} catch (Exception $ex) {
			My_Zend_Logger::log('Post::getTokenNCT - ' . $ex->getMessage());
		}

		return $token;
	}

	public static function getVideoNhaccuatui($nctURL = null)
	{

		$data = array();
		$key  = '';
		if ($nctURL != '') {
			$key = My_Zend_Media::getIDNCT($nctURL);
		}

		try {
			$token = My_Zend_Media::getTokenNCT();
			$url   = "http://api.nhaccuatui.com/o/video?access_token=$token&action=getVideoInfo&key=$key";
			if ($token != '') {
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_TIMEOUT, 5);
				$data = curl_exec($curl);
				if (empty($data)) {
					return $data;
				}

				$songinfo = Zend_Json::decode($data);
				$imageURL = ! empty($songinfo['data']['image']) ?
					$songinfo['data']['image'] : '';
				if ($imageURL != '') {
					$end   = explode('.', $songinfo['data']['image']);
					$ext   = end($end);
					$image = substr($songinfo['data']['image'], 0, -4) . '_640.' . $ext;
				} else {
					$layout = Zend_Layout::getMvcInstance();
					$image  = $layout->_general['server']['img']['path'] . '/default/600-thumbnail.jpg';
				}

				$data = array('key' => $songinfo['data']['videokey'], 'title' => $songinfo['data']['title'], 'url' => $songinfo['data']['url'], 'imageURL' => $image);

			}

		} catch (Exception $ex) {
			My_Zend_Logger::log('getSongNhaccuatui::info - ' . $ex->getMessage());
		}

		return $data;
	}

	public static function getSongNhaccuatui($nctURL = null)
	{

		$data    = array();
		$songkey = '';
		if ($nctURL != '') {
			$songkey = My_Zend_Media::getIDNCT($nctURL);
		}

		try {
			$token = My_Zend_Media::getTokenNCT();
			$url   = "http://api.nhaccuatui.com/o/song?access_token=$token&action=getSongInfo&songkey=$songkey";
			if ($token != '') {
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_TIMEOUT, 5);
				$data = curl_exec($curl);
				if (empty($data)) {
					return $data;
				}

				$songinfo = Zend_Json::decode($data);
				$imageURL = ! empty($songinfo['data']['imageURL_600']) ?
					$songinfo['data']['imageURL_600'] : '';
				if ($imageURL != '') {
					$image = $imageURL;
				} else {
					$layout = Zend_Layout::getMvcInstance();
					$image  = $layout->_general['server']['img']['path'] . '/default/600-thumbnail.jpg';
				}

				$data = array('key' => $songinfo['data']['songkey'], 'title' => $songinfo['data']['title'], 'url' => $songinfo['data']['url'], 'imageURL' => $image,);
			}

		} catch (Exception $ex) {
			My_Zend_Logger::log('getSongNhaccuatui::info - ' . $ex->getMessage());
		}

		return $data;
	}

	public static function getMeCloudSecureKeys() {

		$secureKeys = array();

		$time = time() * 1000;
		$randomKey = 'HayNhucNhoi';
		$email = 'mrtrandinhbao@gmail.com';
		$token = sha1(MECLOUD_APPID . '.' . $time . '.' . $randomKey . '.' . MECLOUD_SECRET_KEY);
		$secureKeys['token'] = $token;
		$secureKeys['time-signkey'] = $time;

		$accessTokenParams = array(
			'appId' => MECLOUD_APPID,
			'token' => $token,
			'time' => $time,
			'random-key' => $randomKey,
			'email' => $email
		);

		$accessTokenUrl = 'http://sdk.mecloud.vn/authen';
		$getAccessToken = self::getDataByCurl($accessTokenUrl, $accessTokenParams);

		if ($getAccessToken) {
			$getAccessToken = Zend_Json::decode($getAccessToken);
			if (intval($getAccessToken['error']) !== 0) {
				return false;
			} else {
				$secureKeys['accesstoken'] = $getAccessToken['accesstoken'];

				//get Signkey based on accessToken

				$signkeyUrl = 'http://sdk.mecloud.vn/signkey';
				$signkeyParams = array(
					'access-token' => $getAccessToken['accesstoken'],
					'time-signkey' => $time
				);
				$getSignkey = self::getDataByCurl($signkeyUrl, $signkeyParams);
				if ($getSignkey) {
					$getSignkey = Zend_Json::decode($getSignkey);
					if (intval($getSignkey['error']) !== 0) {
						return false;
					} else {
						$secureKeys['signkey'] = $getSignkey['signkey'];
						return $secureKeys;
					}
				}
			}

		} else {

			return false;

		}
	}

	public static function getMeCloudIDFromUrl($url) {

		if (strpos($url, '[mecloud]') !== false && strpos($url, '[/mecloud]') !== false) {
			return str_replace('[mecloud]', '', str_replace('[/mecloud]', '', $url));
		} else {
			return false;
		}
	}

	public static function retrieveMeCloudVideoInfo($videoIds) {

		if (empty($videoIds)) {
			return false;
		}

		if (is_array($videoIds)) {
			$videoIds = implode(',', $videoIds);
		}

		$apiUrl = 'http://sdk.mecloud.vn/video';
		$secureKeys = self::getMeCloudSecureKeys();

		if (false == $secureKeys) {
			return false;
		} else {
			$params = array(
				'access-token' => $secureKeys['accesstoken'],
				'signkey' => $secureKeys['signkey'],
				'time-signkey' => $secureKeys['time-signkey'],
				'user-type' => 'CHANNEL',
				'query-type' => 'VIDEO',
				'video-id' => trim($videoIds)
			);
		}

		$data = self::getDataByCurl($apiUrl, $params);

		if ($data) {
			return Zend_Json::decode($data);
		} else {
			return false;
		}

	}

	public static function generateEmbedScript($type, $videoId, $jsPath = null, $postUrl = false) {

		switch ($type) {
			case 'youtube' :
				$script = '<iframe id="ytPlayer" width="728" height="410" src="http://www.youtube.com/embed/' . $videoId . '?rel=0&showinfo=0&vq=large&iv_load_policy=3&modestbranding=1&nologo=1" frameborder="0" allowfullscreen="1"></iframe>';
				break;
			case 'vimeo' :
				$script = '<iframe src="//player.vimeo.com/video/' . $videoId .'" width="728" height="410" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe>';
				break;
			case 'zing_tv' :
				$script = '<iframe id="ztvPlayer" width="650" height="365" src="http://tv.zing.vn/embed/video/' . $videoId . '" frameborder="0" allowfullscreen="true" frameborder="0" webkitAllowFullScreen="true" mozallowfullscreen="true"></iframe>';
				break;
			case 'nct_mp3' :
				$script = '<object width="728" height="728">
				<param name="movie" value="http://www.nhaccuatui.com/m/' . $videoId . '" />
				<param name="quality" value="high" />
				<param name="wmode" value="transparent" />
				<param name="allowscriptaccess" value="always" />
				<param name="allowfullscreen" value="true"/>
				<param name="flashvars" value="autostart=false" />
				<embed src="http://www.nhaccuatui.com/m/' . $videoId . '" flashvars="target=blank&autostart=false" allowscriptaccess="always" allowfullscreen="true" quality="high" wmode="transparent" type="application/x-shockwave-flash" width="728" height="728"></embed>
				</object>';
				break;
			case 'nct_video' :
				$script = '<object width="728" height="410">
				<param name="movie" value="http://www.nhaccuatui.com/video/xem-clip/' . $videoId .'" />
				<param name="quality" value="high" />
				<param name="wmode" value="transparent" />
				<param name="allowscriptaccess" value="always" />
				<param name="allowfullscreen" value="true"/>
				<param name="flashvars" value="autostart=false" />
				<embed src="http://www.nhaccuatui.com/video/xem-clip/' . $videoId . '" flashvars="target=blank&autostart=false" allowscriptaccess="always" allowfullscreen="true" quality="high" wmode="transparent" type="application/x-shockwave-flash" width="728" height="410"></embed>
				</object>';break;
			case 'mecloud' :
				$script = '<script type="text/javascript" src="http://embed.mecloud.vn/play/' . $videoId .'"></script>';
				break;
			case 'ambient' :
				if (time() % 2 == 0) {
					$tag = 'http://delivery.adnetwork.vn/247/xmlvideoad/zid_1435823876/wid_1435823768/type_inline/cb_[timestamp]';
//					$tag = 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/15875645/HayNhucNhoi-DesktopPreroll&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=' . $postUrl . '&description_url=' . $postUrl . '&correlator=' . time() . md5(My_Zend_Globals::getAltIp());
					$skipoffset = 5;
				} else {
					$tag = 'http://blueserving.com/vast.xml?key=e25e9dc9780c57ef77b69f4dd90fc2b5&cat=haihuoc,tamly,hanhdong&country=vietnam,trungquoc,my,nhatban';
					$skipoffset = 5;
				}
				$script = '<div id="vod" >Đang tải clip...</div>
				<script type="text/javascript">
						var jwp = jwplayer("vod").setup({
						menu: true,
						allowscriptaccess: "always",
						wmode: "opaque",
						file: "https://www.youtube.com/watch?v=' . $videoId . '",
						width: 728,
						height: 410,
						autostart: false,
						primary: "flash",
						skin: "' . $jsPath . '/jwplayer/skins/bekle.xml",
						advertising: {
							client: "vast",
							skipoffset: ' . $skipoffset . ',
							skiptext : "Bỏ qua",
							skipmessage : "Bỏ qua sau xxs",
							admessage: "Nhấn Bỏ Qua (góc phải bên dưới VIDEO) để xem VIDEO ngay. Quảng cáo hết sau XX giây",
							tag: ["' . $tag . '"]
//							tag: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/15875645/HayNhucNhoi-DesktopPreroll&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=' . $postUrl . '&description_url=' . $postUrl . '&correlator=' . time() . md5(My_Zend_Globals::getAltIp()) . '"
						},
						events: {
							onComplete: function() {
								$("#hot_videos").fadeIn("slow");
								ga("send", "event", "Videos", "finish", "Ambient Player");
							},
							onPlay: function() {
								ga("send", "event", "Videos", "play", "Ambient Player");
							},
							onAdImpression: function() {
								ga("send", "event", "Advertising", "play", "Vivid");
							}
						}
						});
				</script>';
				break;
			default:
				$script = '';
				break;
		}

		return $script;

	}

	public static function generateNctMobileEmbed($type, $url, $picture = null) {

		switch ($type) {

			case 'nct_mp3' :
				$script =
					'<div style="margin-top: 30px">
                        <audio preload="auto" controls>
                            <source src="' . $url . '" type="audio/mpeg">
                        </audio>
                    </div>';
				break;

			case 'nct_video' :
				$script =
	                '<div style="margin-top: 30px">
	                    <video  id="video"  poster="' . $picture . '" src="' . $url . '" controls>
	                        <source src="' . $url . '">
	                        <p>Your browser does not support the video tag</p>
	                    </video>
	                </div>';
				break;

			default:
				$script = '';
				break;
		}

		return $script;

	}

	public static function retrieveVideoInfo($key, $type)
	{

		switch ($type) {

		}

	}
}

?>