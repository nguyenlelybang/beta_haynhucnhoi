<?php

/**
 * Created by PhpStorm.
 * User: PC
 * Date: 21/10/2015
 * Time: 4:35 CH
 */
class My_Helper_Breadcrumb
{
    protected $pageType;
    protected $objectId;

    public function __construct($pageType, $objectId = null) {

        $this->pageType = $pageType;
        $this->objectId = $objectId;

        self::getBreadcrumb();

    }

    public function getBreadcrumb() {

        $type = $this->pageType;
        $id = $this->objectId;


        if (in_array($type, array('hot', 'new', 'video', 'blog'))) {
            $currentPage = self::convertPhaseToUrl($type);
        }

        if ($type == 'category') {

            if (empty($id) || !is_int($id)) {
                return false;
            };
            $category = Category::selectCategory($id);
            if (empty($category)) {
                return false;
            }
            $currentPage['url'] = BASE_URL . '/chu-de/' . $category['alias'];
            $currentPage['name'] = $category['category_customize'];

        }

        if ($type == 'detail') {

            if (empty($id) || ! is_numeric($id)) {
                return false;
            }

            $post = Post::getPost($id);

            if (empty($post)) {
                return false;
            }

            $category = Category::selectCategory($post['category_id']);

            if (!empty($category)) {
                $parentPage['url'] = BASE_URL . '/chu-de/' . $category['alias'];
                $parentPage['name'] = $category['category_customize'];
            } else {
                if ($post['phase'] == 0) {
                    $parentPage['url'] = BASE_URL . '/new';
                    $parentPage['name'] = 'Bình chọn';
                } elseif ($post['phase'] == 2) {
                    $parentPage['url'] = BASE_URL . '/hot';
                    $parentPage['name'] = 'Nhức Nhói';
                } else {
                    $parentPage['url'] = BASE_URL . '';
                    $parentPage['name'] = 'Hay';
                }

            }
            $currentPage['url'] = Post::postUrl($post, true);
            $currentPage['name'] = $post['story'];
        }

        if($type == 'blogDetail') {

            if (empty($id) || ! is_numeric($id)) {
                return false;
            }
            $blogPost = BlogPost::get($id);
            if (empty($blogPost)) {
                return false;
            }
            $category = BlogCategory::get(array('category_id'=>$blogPost['category_id']));
            if (!empty($category)) {
                $parentPage['url'] = BASE_URL . '/blog/chu-de/' . $category['category_alias'];
                $parentPage['name'] = $category['category_name'];
            }
            $currentPage['url'] = BlogPost::postUrl($blogPost);
            $currentPage['name'] = $blogPost['post_title'];
            $blogPage['url'] = BASE_URL . '/blog';
            $blogPage['name'] = 'Blog';
        }

        if($type == 'blogCategory') {

            $alias = Zend_Controller_Front::getInstance()->getRequest()->getParam('alias');
            $category = BlogCategory::get(array('category_alias'=>$alias));
            if (empty($category)) {
                return false;
            }
            $currentPage['url'] = BASE_URL . '/blog/chu-de/' . $category['category_alias'];
            $currentPage['name'] = $category['category_name'];
            $blogPage['url'] = BASE_URL . '/blog';
            $blogPage['name'] = 'Blog';
        }

        $breadcrumb = array();
        $breadcrumb['homeUrl'] = BASE_URL;

        if (isset($currentPage) && !empty($currentPage)) {
            $breadcrumb['currentPage'] = $currentPage;
        }
        if (isset($parentPage) && !empty($parentPage)) {
            $breadcrumb['parentPage'] = $parentPage;
        }
        if (isset($blogPage) && !empty($blogPage)) {
            $breadcrumb['blogPage'] = $blogPage;
        }
        return $breadcrumb;
    }

    protected function convertPhaseToUrl($phaseId) {

        if (!isset($phaseId)) return false;

        switch($phaseId) {
            case 'new':
                $phase['url'] = BASE_URL . '/new';
                $phase['name'] = 'Bình chọn';
                break;
            case 'hot':
                $phase['url'] = BASE_URL . '/hot';
                $phase['name'] = 'Nhức Nhói';
                break;
            case 'video':
                $phase['url'] = BASE_URL . '/video';
                $phase['name'] = 'Video';
                break;
            case 'blog':
                $phase['url'] = BASE_URL . '/blog';
                $phase['name'] = 'Blog';
                break;
            default:
                $phase['url']  = BASE_URL;
                $phase['name'] = 'Hay';
                break;
        }
        return $phase;
    }

    protected function getPhaseFromReferer() {

        if (isset($_SERVER['HTTP_REFERER'])) {
            $referer = $_SERVER['HTTP_REFERER'];
        }

        if (!empty($referer)) {
            if (strpos(BASE_URL . '/new', $referer) !== false) {
                $phase = 0;
            }
            if (strpos(BASE_URL . '/hot', $referer) !== false) {
                $phase = 2;
            }
            if ($referer == BASE_URL) {
                $phase = 1;
            }
        }

        if (isset($phase)) {
            return $phase;
        } else {
            return false;
        }
    }

}