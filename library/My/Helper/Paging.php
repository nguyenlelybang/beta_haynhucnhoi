<?php
class My_Helper_Paging
{
    /**
     * View instance
     *
     * @var Zend_View_Instance
     */
    public $view;
    
    /**
     * Paging   
     * @param string $uri
     * @param array  $params
     * @param int    $total
     * @param int    $page
     * @param int    $limit
     * @param int    $page_size
     * @param string $style
     */
    public function paging($uri, $params=array(), $total=0, $page=0, $limit=30, $page_size=8, $style='page_item')
    {
        //Start paging
        $query = '';
        $querystring = '?';
        $paging = '';
        $url = BASE_URL . $uri;
        $total = intval($total);
        $limit = intval($limit);
        $page_size = intval($page_size);
        
        if(strpos($uri, 'http://') !== false)
        {
        	$url = $uri;
        }
      
        if(!empty($params) && is_array($params))
        {
        	unset($params['page']);
        	
        	ksort($params);
            //Add query
            foreach($params as $key => $value)
            {            	
                $querystring .= $key.'='.urlencode($value) .'&';
            }                        
        }               
             
        //Check total
        if($total>0)
        {
            //Current page number
            $total_pages = ceil($total / $limit);
            $start   = max($page - intval($page_size/2), 1);
            $end     = ($total > $limit) ? $start + $page_size -1 : $total;
            
            $from_rs = ($page - 1) * $limit + 1;
            $to_rs   = $from_rs + $limit - 1;
            $to_rs   = ($to_rs>$total) ? $total : $to_rs;
            $paging = '';
            
            if($total > $limit)
            {
            	//Add total
            	$paging  .= '<div class="paging">';
                   	
            	//Previous
            	if($page > 1)
            	{
            		$i = $page-1;
            		$query = $url .$querystring .'page='. $i;
            		$paging .= '<a title="Trang trước" href="'.$query.'">&laquo; Trang trước</a>';
            	}
            	                                                    
                //Loop
                for($i = $start; $i <= $end && $i <= $total_pages; $i++)
                {
                	
                    if($i == $page)
                    {                    	
                        $paging .= '<a class="pagingAct">'.$i.'</a>';
                    }
                    else
                    {
                        $query = $url . $querystring .'page='. $i;
                        $paging .= '<a href="'.$query.'">'.$i.'</a>';                        
                    }
                }
                                              
                //Check total pages
                if($page < $total_pages)
                {
                	$i = $page + 1;
                	$query = $url . $querystring .'page='. $i;
                	$paging .= '<a title="Trang sau" href="'.$query.'">Trang sau &raquo;</a>';
                }
                
                //$paging .= '<p class="right">Có <b>'. $total .'</b> kết quả phù hợp</p>';
                $paging .= '<div class="clear"></div></div>';
            }            
        }
        
        return $paging;
    }

    public function blogPaging($pageType,$total=0, $offset=0, $limit=5, $page_size=5, $style='page_item', $uri = array()) {
        $paging = '';
        if ($pageType == 'posts' && isset($uri['user_id']))
            $url = BASE_URL . '/user/' . $uri['user_id'] . '/' . $pageType;
        else
            $url = BASE_URL . '/blog/' . $pageType;
        //Check total
        if($total>0)
        {
            //Current page number
            $total_pages = ceil($total/$limit);

            $start   = max($offset-intval($page_size/2), 1);

            $end     = ($total>$limit)? $start + $page_size -1 : $total;


            $from_rs = ($offset-1)*$limit+1;
            $to_rs   = $from_rs+$limit-1;
            $to_rs   = ($to_rs>$total) ? $total : $to_rs;
            //Add total
            $paging  = '<div class="pagination '.$style.'"><div class="wrap">';
            $paging  .= '<div class="pageOn">';
            if($total>$limit)
            {
                //Start
                if($start > 1)
                {
                    $i = 1;
                    $query = $url . '/' . $i;
                    $paging .= '<span class="page"><a href="'.$query.'">'.$i.'</a></span>';
                }

                //Previous
                if($offset > 1)
                {
                    $i = $offset-1;
                    $query = $url . '/' . $i;
                    $paging .= '<span><a class="page" href="'.$query.'">Trở về</a></span>';
                }

                //Loop
                for($i = $start; $i <= $end && $i <= $total_pages; $i++)
                {

                    if($i == $offset)
                    {
                        $paging .= '<span class="page active">'.$i.'</span>';
                    }
                    else
                    {
                        $query = $url . '/' . $i;
                        $paging .= '<span><a class="page" href="'.$query.'">'.$i.'</a></span>';
                    }
                }

                //Check total pages
                if($offset < $total_pages)
                {
                    $i = $offset + 1;
                    $query = $url . '/' . $i;
                    $paging .= '<span><a class="page" href="'.$query.'">Tiếp theo</a></span>';
                }

                //Check total pages
                if($total_pages > $end)
                {
                    $i = $total_pages;
                    $query = $url . '/' . $i;
                    $paging .= '<span class="page"><a href="'.$query.'">'.$i.'</a></span>';
                }
            }
            $paging .= '</div></div></div>';
        }
        return $paging;
    }


    /**
     * Sets the view instance.
     *
     * @param  Zend_View_Interface $view View instance
     * @return Zend_View_Helper_Paging
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }
  
}