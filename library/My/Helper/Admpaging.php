<?php
class My_Helper_AdmPaging
{
    /**
     * View instance
     *
     * @var Zend_View_Instance
     */
    public $view;
    
    /**
     * Paging
     * @param string $module
     * @param string $controller
     * @param string $action
     * @param array  $params
     * @param int    $total
     * @param int    $offset
     * @param int    $limit
     * @param int    $page_size
     * @param string $style
     */
    public function admpaging($module, $controller, $action, $params, $total=0, $offset=0, $limit=30, $page_size=8, $style='page_item')
    {
        //Start paging
        $query = '';
        $querystring = '';
        $paging = '';

        if ($module == 'default')
        {
            $module = '';
        }
        else
        {
            $module = '/'.$module;
        }
        
        if(!empty($params) && is_array($params))
        {
            //Add query
            foreach($params as $key => $value)
            {            	
                $querystring .= '/'.$key.'/'.urlencode($value);
            }
        }
        
        if(!empty($action)){
        	$action = '/' . $action;
        }

        //Check total
        if($total>0)
        {
            //Current page number
            $total_pages = ceil($total/$limit);
            $start   = max($offset-intval($page_size/2), 1);
            $end     = ($total>$limit)? $start + $page_size -1 : $total;
            
            $from_rs = ($offset-1)*$limit+1;
            $to_rs   = $from_rs+$limit-1;
            $to_rs   = ($to_rs>$total) ? $total : $to_rs;
            //Add total            
            $paging  = '<div class="paging grey '.$style.'"><div class="wrap">';	
          	$paging  .= '<div class="total">Has <b>'.$total.'</b> result </div>';
          	$paging  .= '<div class="pageOn">';
            if($total>$limit)
            {
                //Start
                if($start > 1)
                {
                    $i = 1;
                    $query = BASE_URL.'/'.$module.'/'.$controller.$action.'/page/'.$i.$querystring;
                    $paging .= '<span class="first"><a href="'.$query.'">'.$i.'</a></span>';
                }
                
                //Previous
                if($offset > 1)
                {
                    $i = $offset-1;
                    $query = BASE_URL.$module.'/'.$controller.$action.'/page/'.$i.$querystring;                             
                    $paging .= '<span class="prev"><a href="'.$query.'">Trở về</a></span>';
                }
                                
                //Loop
                for($i = $start; $i <= $end && $i <= $total_pages; $i++)
                {
                	
                    if($i == $offset)
                    {                    	
                        $paging .= '<span class="active">'.$i.'</span>';
                    }
                    else
                    {
                        $query = BASE_URL.$module.'/'.$controller.$action.'/page/'.$i.$querystring;
                        $paging .= '<span><a href="'.$query.'">'.$i.'</a></span>';                        
                    }
                }

               

                //Check total pages
                if($offset < $total_pages)
                {
                    $i = $offset + 1;
                    $query = BASE_URL.$module.'/'.$controller.$action.'/page/'.$i.$querystring;                    
                    $paging .= '<span class="next"><a href="'.$query.'">Tiếp theo</a></span>';
                }
                
                //Check total pages
                if($total_pages > $end)
                {
                    $i = $total_pages;
                    $query = BASE_URL.$module.'/'.$controller.$action.'/page/'.$i.$querystring;
                    $paging .= '<span class="last"><a href="'.$query.'">'.$i.'</a></span>';
                }
            }           
            $paging .= '<span class="total">(Có <b>'.$total.'</b> kết quả)</span></div></div></div>';
        }
        return $paging;
    }

    /**
     * Sets the view instance.
     *
     * @param  Zend_View_Interface $view View instance
     * @return Zend_View_Helper_Paging
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }
  
}