<?php

/*
  +--------------------------------------------------------------------------------------------+
  |   DISCLAIMER - LEGAL NOTICE -                                                              |
  +--------------------------------------------------------------------------------------------+
  |                                                                                            |
  |  This program is free for non comercial use, see the license terms available at            |
  |  http://www.francodacosta.com/licencing/ for more information                              |
  |                                                                                            |
  |  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; |
  |  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. |
  |                                                                                            |
  |  USE IT AT YOUR OWN RISK                                                                   |
  |                                                                                            |
  |                                                                                            |
  +--------------------------------------------------------------------------------------------+

 */

/**
 * phMagick - Montage function
 *
 * @package    phMagick
 * @version    0.1.0
 * @author     Nuno Costa - sven@francodacosta.com
 * @copyright  Copyright (c) 2007
 * @license    http://www.francodacosta.com/phmagick/license/
 * @link       http://www.francodacosta.com/phmagick
 * @since      2008-03-13
 */
class phMagick_watermark {

    function watermark(phmagick $p, $watermark, $gravity = 'center', $row=1, $column=1) {
        list($w, $h) = getimagesize($p->getSource());
        list($ww, $hw) = getimagesize($watermark);

        /*$cmd = $p->getBinary('convert');
        $cmd .= " ". $p->getSource();
        $cmd .= " ". $watermark;
        $cmd .= " -gravity ". $gravity ." -append";
        $cmd .= " -crop ". $w ."x". ($h + $hw) ."-0-0";

        $cmd .= " ". $p->getDestination();*/

        $cmd = $p->getBinary('composite');
        $cmd .= " -watermark 20%";
        $cmd .= " -gravity ". $gravity;
        $cmd .= " " . $watermark . " " . $p->getSource();
        //$cmd .= " -crop ". $w ."x". ($h + $hw) ."-0-0";
        $cmd .= " " . $p->getDestination();

        $p->execute($cmd);
        $p->setSource($p->getDestination());
        $p->setHistory($p->getDestination());
        return $p;
    }

}

?>