<?php
/**
 * OAuth_Util
 * This class has bean auto_generated at 04/05/2011
 * @author	Truong Dinh Truong<truongtd@vng.com.vn>
 */
class OAuth_Util {
	const CACHE_INFO_USER = 'info_user_';
	const LIMIT_TIME_REQUEST = 60;
	
	/**
 	  * Get info UserGoogle
      * return boolean
     */
	public static function getInfoUserGoogle(){
		global $hmac_method, $rsa_method, $SIG_METHODS;
		$consumer 	= 	new OAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
		$authen = OAuth_Authen::getInstance();
		if($authen->getCookie('g_access')){
			$access_token	=	explode( '-google-access-', base64_decode( $_COOKIE['g_access'] ) );
			$session['access_token']	=	$access_token[0];
			$session['token_secret']	=	$access_token[1];
		}
		$session_key	=	$session['access_token'] . '-123mua-' . $session['token_secret'];
		
		$feedUri		=	'http://www.google.com/m8/feeds/contacts/default/full';
		$feedUri1		=	'https://www-opensocial.googleusercontent.com/api/people/@me';
		$privKey		=	null;
		$token			=	$session['access_token'];
		$token_secret	=	$session['token_secret'];
		$oauth_token 	= 	$token ? new OAuthToken($token, $token_secret) : NULL;
		$sig_method 	= 	$SIG_METHODS['HMAC-SHA1'];
		$http_method	=	'GET';
		$content_type	=	'Content-Type: application/atom+xml';
		$postData		=	'';
		// create an associative array from each key/value url query param pair.
		$params = array();
		$pieces = explode('?', $feedUri);
		if (isset($pieces[1])) {
			$params = explode_assoc('=', '&', $pieces[1]);
		}
		// urlencode each url parameter key/value pair
		$tempStr = $pieces[0];
		foreach ($params as $key=>$value) {
			$tempStr .= '&' . urlencode($key) . '=' . urlencode($value);
		}
		$feedUri = preg_replace('/&/', '?', $tempStr, 1);
		/****************************Get Email*******************************/
		$req = OAuthRequest::from_consumer_and_token($consumer, $oauth_token,
													 $http_method, $feedUri, $params);
		$req->sign_request($sig_method, $consumer, $oauth_token, $privKey);
		$auth_header = $req->to_header();
		$gdataVersion = 'GData-Version: 2.0';
		$result = send_signed_request($http_method, $feedUri,
									  array($auth_header, $content_type, $gdataVersion), $postData);
		
		if (!$result) {
			die("{'html_link' : '" . $req->to_url() . "', " .
			   "'base_string' : '" . $req->get_signature_base_string() . "', " .
			   "'response' : '" . $result . "'}");  // return json
		}
		$result = split('<', $result, 2);
		/*****************************GET Avatar*******************************/
		$req1 = OAuthRequest::from_consumer_and_token($consumer, $oauth_token,
													 $http_method, $feedUri1, $params);
		$req1->sign_request($sig_method, $consumer, $oauth_token, $privKey);
		$auth_header = $req1->to_header();
		$gdataVersion = 'GData-Version: 2.0';
		$result1 = send_signed_request($http_method, $feedUri1,
									  array($auth_header, $content_type, $gdataVersion), $postData);
		$result1 = split('<', $result1, 2);
		
		/************************Boc tach id, avatar, profile******************/
		//$re_id		=	"/(?i)<id>(.*?)<\/id>/sm";
		$re_id		=	'@<id>.*?</id>@siu';
		$re_avatar	=	'@<thumbnailUrl>.*?</thumbnailUrl>@siu';
		$re_displayname	=	'@<displayName>.*?</displayName>@siu';
		
		$userInfo = json_decode(substr($result1[0], strpos($result1[0], '{"entry"')));
		
		/*preg_match_all($re_id, $result1[1], $matchesid);
		preg_match_all($re_avatar, $result1[1], $matches_avatar);
		preg_match_all($re_displayname, $result1[1], $matches_displayname);*/
		
		// If response was not XML xml_pretty_printer() will throw exception.
		// In that case, set the response body directly from the result
		$re 		= 	"/(?i)<author>(.*?)<\/author>/sm";
		$re_name	=	"/(?i)<name>(.*?)<\/name>/sm";
		$re_email	=	"/(?i)<email>(.*?)<\/email>/sm";
		
		preg_match($re, $result[1], $matches);
		preg_match($re_name, $matches[1], $matches1);
		preg_match($re_email, $matches[1], $matches2);
		if( isset($matches1[1]) && isset($matches2[1])){
			$google_username	=	str_replace('@gmail.com', '', $matches2[1]);
			$google_email		=	$matches2[1];
			if(isset( $userInfo->entry->displayName )){//if(isset( $matches_displayname[0][0] )){
				$google_fullname = $userInfo->entry->displayName;//$google_fullname	=	str_replace(array('<displayName>', '</displayName>'), array('', ''), $matches_displayname[0][0]);
			}else{
				$google_fullname = $matches1[1];
			}
			if( isset( $userInfo->entry->id ) ){//if( isset( $matchesid[0][1] ) ){
				$google_id = $userInfo->entry->id;//str_replace(array('<id>', '</id>'), array('', ''), $matchesid[0][1]);
			}else{
				$google_id = $google_username;
			}
			if(isset($userInfo->entry->thumbnailUrl)){//if( isset( $matches_avatar[0][0] ) ){
				$google_avatar	=	$userInfo->entry->thumbnailUrl;//str_replace(array('<thumbnailUrl>', '</thumbnailUrl>'), array('', ''), $matches_avatar[0][0]);;
				if(strpos($google_avatar, 'private') > 0){
					$google_avatar	=	'http://www.gstatic.com/s2/profiles/images/googleyeyes96.png';
				}
			}else{
				$google_avatar	=	'http://www.gstatic.com/s2/profiles/images/googleyeyes96.png';
			}
			$fu =	array(
						'foreign_id'		=>	$google_id,
						'foreign_type'		=>	ForeignUsers::TYPE_GOOGLE,
						'foreign_avatar'	=>	$google_avatar,
						'foreign_fullname'	=>	$google_fullname,
						'foreign_username'	=>	$google_username,
						'foreign_key'		=>	$session_key,
						'foreign_email'		=>	$google_email
					);
			return $fu;
		}
		return false;
	}
	
	/**
 	  * Get info UserGoogle
      * return boolean
     */
	public static function getInfoUserYahoo(){
		$authen = OAuth_Authen::getInstance();
		if($authen->getCookie('y_access')){
			$access_token	=	explode( '-123mua-', base64_decode( $_COOKIE['y_access'] ) );
			$session['access_token']	=	$access_token[0];
			$session['email']	=	base64_decode($access_token[1]);
			//$authen->removeCookie('y_access', '', DOMAIN); 
		}
		$oauthapp = new YahooOAuthApplication(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_APP_ID);
		//Restore access token from session
    	$oauthapp->token = YahooOAuthAccessToken::from_string($session['access_token']);
		//Fetch latest user data
		$profile  = $oauthapp->getProfile()->profile;
    	if(isset($profile->guid)){
    		$yahoo_id =	$profile->guid;
    		if('' != $profile->nickname){
				$yahoo_fullname	=	$profile->nickname;
			}else{
				$yahoo_fullname	=	str_replace('@yahoo.com', '', $session['email']);
			}
			$yahoo_avatar =	$profile->image->imageUrl;
			$yahoo_email = $session['email'];
			$yahoo_username = str_replace('@yahoo.com', '', $session['email']);
			$fu	=	array(
								'foreign_id' 		=> 	$yahoo_id, 
								'foreign_type' 		=> 	ForeignUsers::TYPE_YAHOO, 
								'foreign_avatar' 	=> 	$yahoo_avatar, 
								'foreign_fullname' 	=> 	$yahoo_fullname, 
								'foreign_username' 	=> 	$yahoo_username, 
								'foreign_key' 		=> 	'',
								'foreign_email'		=>	$yahoo_email
						);
			return $fu;
    	}
		return false;
	}
	
	/**
	 * Insert table ForeignUser
	 * @param Int $user_id
	 * @param Array $fu
	 * return boolean
	 */
	public static function insertForeignUsers($fu, $user_id = 0){
		//Insert fields in table Foreign Users
		$foreign_user =	new ForeignUsers();
		$foreign_user->user_id = $user_id;
		$foreign_user->foreign_username = $fu['foreign_username'];
		$foreign_user->foreign_password = md5(OAuth_Util::getRandId(8));
		$foreign_user->foreign_fullname = $fu['foreign_fullname'];
		$foreign_user->foreign_id = $fu['foreign_id'];
		$foreign_user->foreign_email = $fu['foreign_email'];
		$foreign_user->foreign_key = $fu['foreign_key'];
		$foreign_user->created_date = time();
		$foreign_user->foreign_type = $fu['foreign_type'];
		$foreign_user->foreign_avatar = $fu['foreign_avatar'];
		$fu_id	=	$foreign_user->getIdByForeignid($fu['foreign_id'], $fu['foreign_type']);
		if(!$fu_id){
			return $foreign_user->save();
		}
		return false;
	}
	
	/**
	 * Set cookie, session from info of ForeignUsers
	 */
	public static function setCookieFromFuid($fu_id, $fu_type){
		$authen = OAuth_Authen::getInstance();
		ForeignUsers::$table = My_Zend_Globals::getForeignUserPTN($fu_type);
		$fu = ForeignUsers::retrieveByPk($fu_id, $fu_type);
		$authen->setCookieFromUser($fu);
	}

	public static function assignRandValue($num){
	  switch($num)
	  {
		    case "1":
		     $rand_value = "a";
		    break;
		    case "2":
		     $rand_value = "b";
		    break;
		    case "3":
		     $rand_value = "c";
		    break;
		    case "4":
		     $rand_value = "d";
		    break;
		    case "5":
		     $rand_value = "e";
		    break;
		    case "6":
		     $rand_value = "f";
		    break;
		    case "7":
		     $rand_value = "g";
		    break;
		    case "8":
		     $rand_value = "h";
		    break;
		    case "9":
		     $rand_value = "i";
		    break;
		    case "10":
		     $rand_value = "j";
		    break;
		    case "11":
		     $rand_value = "k";
		    break;
		    case "12":
		     $rand_value = "l";
		    break;
		    case "13":
		     $rand_value = "m";
		    break;
		    case "14":
		     $rand_value = "n";
		    break;
		    case "15":
		     $rand_value = "o";
		    break;
		    case "16":
		     $rand_value = "p";
		    break;
		    case "17":
		     $rand_value = "q";
		    break;
		    case "18":
		     $rand_value = "r";
		    break;
		    case "19":
		     $rand_value = "s";
		    break;
		    case "20":
		     $rand_value = "t";
		    break;
		    case "21":
		     $rand_value = "u";
		    break;
		    case "22":
		     $rand_value = "v";
		    break;
		    case "23":
		     $rand_value = "w";
		    break;
		    case "24":
		     $rand_value = "x";
		    break;
		    case "25":
		     $rand_value = "y";
		    break;
		    case "26":
		     $rand_value = "z";
		    break;
		    case "27":
		     $rand_value = "0";
		    break;
		    case "28":
		     $rand_value = "1";
		    break;
		    case "29":
		     $rand_value = "2";
		    break;
		    case "30":
		     $rand_value = "3";
		    break;
		    case "31":
		     $rand_value = "4";
		    break;
		    case "32":
		     $rand_value = "5";
		    break;
		    case "33":
		     $rand_value = "6";
		    break;
		    case "34":
		     $rand_value = "7";
		    break;
		    case "35":
		     $rand_value = "8";
		    break;
		    case "36":
		     $rand_value = "9";
		    break;
		  }
		return $rand_value;
	}
	
	/**
	 * Phương thức tạo giá trị ngẫu nhiên dùng cho reset lại mật khẩu mail
	 * @param int $length Độ dài của mật khẩu mong muốn
	 * @return string Xâu có độ dài length ngẫu nhiên
	 */
	public static function getRandId($length){
		if($length > 0) { 
			$rand_id = "";
			for($i = 1; $i <= $length; $i++){
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,36);
				$rand_id .= self::assignRandValue($num);
		   }
		}	
		return $rand_id;
	}
}

?>
