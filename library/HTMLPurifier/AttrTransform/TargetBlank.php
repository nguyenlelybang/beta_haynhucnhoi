<?php

// must be called POST validation

/**
 * Adds target="blank" to all outbound links.  This transform is
 * only attached if Attr.TargetBlank is TRUE.  This works regardless
 * of whether or not Attr.AllowedFrameTargets
 */
class HTMLPurifier_AttrTransform_TargetBlank extends HTMLPurifier_AttrTransform
{
    private $parser;

    public function __construct() {
        $this->parser = new HTMLPurifier_URIParser();
    }

    public function transform($attr, $config, $context) {
echo "sdfD"; exit;
        if (!isset($attr['href'])) {
            return $attr;
        }
	
        // XXX Kind of inefficient
        $url = $this->parser->parse($attr['href']);
        $scheme = $url->getSchemeObj($config, $context);
	
        //if ($scheme !== false && $scheme->browsable) {
            if (isset($attr['target'])) {
				if (strpos($attr['target'], '_blank') == -1)
				{
					$attr['target'] .= ' _blank';
				}
            } else {
                $attr['target'] = '_blank';
            }
        //}

        return $attr;

    }

}

// vim: et sw=4 sts=4
