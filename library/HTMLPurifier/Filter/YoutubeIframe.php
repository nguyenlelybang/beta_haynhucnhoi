<?php

class HTMLPurifier_Filter_YoutubeIframe extends HTMLPurifier_Filter
{

  public $name = 'YouTubeIframe';

  public function preFilter($html, $config, $context) 
  {
      $pre_regex = '#<iframe\b[a-zA-Z0-9/"=-\s]+?\bsrc="http://www.youtube.com/embed/([A-Za-z0-9]+)"[a-zA-Z0-9/"=-\s]*?></iframe>#';
      $pre_replace = '<span class="youtube-iframe">\1</span>';
      return preg_replace($pre_regex, $pre_replace, $html);
  }

  public function postFilter($html, $config, $context) 
  {
      $post_regex = '#<span class="youtube-iframe">([A-Za-z0-9]+)</span>#';
      return preg_replace_callback($post_regex, array($this, 'postFilterCallback'), $html);
  }

  protected function postFilterCallback($matches) 
  {
      return '<object width="480" height="385">'.
          '<param name="movie" value="http://www.youtube.com/v/'.$matches[1].'?fs=1&amp;hl=en_US"></param>'.
          '<param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param>'.
          '<embed src="http://www.youtube.com/v/'.$matches[1].'?fs=1&amp;hl=en_US" type="application/x-shockwave-flash"'.
          'allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>';
  }

}