var Category = {};
Category.Categorylist = function()
{
	$(function(){
		$("#categorylist").treeview();		
		$('a.approve_category').click(function (){
			$(this).myBoxy (Boxy,{
				type: 'success',
	            message: 'Danh mục bạn chọn đã được duyệt thành công!'
	        });
		});
	});
};
Category.Categorydetete = function()
{
	
};
Category.Categorycreate = function()
{
	$(function(){
		$("#categorylist").treeview();
	});
};
Category.Categoryedit = function()
{
	$(function(){
		$("#categorylist").treeview();
	});
};

Category.doPublicProduct = function(id)
{
   $.post(Settings.baseurl + '/'+Settings.admin+'/category/categorypublish', {
        id:id
    },
    function(data){
        if(data == 0){
            var mess = 'Có lổi xãy ra, vui lòng thử lại!';
        }
        else{
            
			var mess = 'Bạn đã public danh mục thành công!';
			var html ='<span class="action"><img src="' + Settings.imgurl + '/icon_approve_off.jpg" alt="Duyệt danh mục" /></span><span class="action" title="Xóa danh mục"><img src="'+ Settings.imgurl + '/icon_del_off.jpg" alt="Xóa danh mục" /></span>';
			
		    document.getElementById("cate_" + id).innerHTML = html;
	   }
	  
        $(this).myBoxy (Boxy,{
            message: mess
        });
    });
};

Category.doDeleteProduct = function(id, parent)
{
    $(this).myBoxy (Boxy,{
        type:'confirm',
        message: "Bạn muốn xóa danh mục này không?",
        id : id,
		parent: parent,
        callback: function() {
            var boxy = Boxy.get(this);
            var options = boxy.options;
            Category.delAdminProduct(id, parent);
            boxy.hide();
        }
    });
};

Category.delAdminProduct= function(id, parent)
{
    $.post(Settings.baseurl + '/'+Settings.admin+'/category/categorydelete', {
        id:id, act: 'del'
    },
    function(data){
	    if(parent>0){
          window.location = Settings.baseurl + '/'+Settings.admin+'/category/categorylist/id/' + parent;
		}else{
		   window.location = Settings.baseurl + '/'+Settings.admin+'/category';
		}
    });
};

Category.UpdateAdminCate = function()
{

  if(document.getElementById('category_name').value !='' && document.getElementById('page_title').value !='' && document.getElementById('meta_keyword').value !='' && document.getElementById('meta_description').value !='')
  {
      document.tf.submit();
  }
  else
  {
	  if(document.getElementById('category_name').value =='') {
		  $(this).myBoxy (Boxy,{
	            message: 'Vui lòng nhập tên danh mục !'
	      });
		  document.getElementById('category_name').focus();
		  return false;
	  }
	  
	  if(document.getElementById('page_title').value =='') {
		  $(this).myBoxy (Boxy,{
	            message: 'Vui lòng nhập tiêu đề danh mục !'
	      });
		  document.getElementById('page_title').focus();
		  return false;
	  }
	  
	  if(document.getElementById('meta_keyword').value =='') {
		  $(this).myBoxy (Boxy,{
	            message: 'Vui lòng nhập từ khóa cho danh mục !'
	      });
		  document.getElementById('meta_keyword').focus();
		  return false;
	  }
	  
	  if(document.getElementById('meta_description').value =='') {
		  $(this).myBoxy (Boxy,{
	            message: 'Vui lòng nhập mô tả chung cho danh mục !'
	      });
		  document.getElementById('meta_description').focus();
		  return false;
	  }
  }
};

Category.addAdminCate = function()
{

  if(document.getElementById('category_name').value !='')
  {
      document.tf.submit();
  
  }
  else
  {
  
      $(this).myBoxy (Boxy,{
            message: 'Vui lòng nhập tên danh mục !'
        });
		return false;
  }
};

Category.doDeleteTag = function(tagId, categoryId){
    $(this).myBoxy (Boxy,{
        type: 'confirm',
        message: 'Bạn muốn xóa tag này không?',
        tagId : tagId,
        categoryId : categoryId,
        callback: function(){
            var boxy = Boxy.get(this);
            if (!boxy)
            {
                return;
            }

            var options = boxy.options;
            boxy.hideAndUnload();

            $.getJSON(Settings.baseurl+'/'+Settings.admin+'/tag/deletetag', {
                tag_id: options.tagId,
                category_id: options.categoryId
            }, function(data){
                if (data.status == 1)
                {
                    $(this).myBoxy (Boxy,{
                        type: 'success',
                        message: 'Xóa quản tag thành công!'
                    });
                    setTimeout('location.reload()', 250);
                }
                else
                {
                    $(this).myBoxy (Boxy,{
                        type: 'alert',
                        message: 'Có lỗi khi xóa!'
                    });
                }
            });
        }
    });
};
