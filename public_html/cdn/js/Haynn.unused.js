Haynn.indexPage.registerExpandClick = function()
{
    $("[id|='expand']").each(function()
    {
        if (!$(this).hasClass("expandDivProcessed"))
        {
            $(this).addClass("expandDivProcessed");
            $(this).click(function()
            {
                var id = $(this).attr('id').split("-")[1];
                var imgHeight = $("#entry-" + id).find(".pin_pic_img_a").find("img").height();
                if ($(this).hasClass("expandCollapse"))
                {
                    $("#entry-" + id).find(".pic").animate(
                        {
                            height: 600
                        },
                        {
                            duration: 500,
                            complete: function()
                            {
                                Haynn.indexPage.reLayout();
                            }
                        }
                    );
                    Haynn.indexPage.scrollToEntry(id);
                    $(this).attr("title","Nhấn để xem hình bản đầy đủ");
                    $(this).removeClass("expandCollapse");
                }
                else
                {
                    $("#entry-" + id).find(".pic").animate(
                        {
                            height: imgHeight
                        },
                        {
                            duration: 500,
                            complete: function(){
                                Haynn.indexPage.reLayout();
                            }
                        }
                    );
                    $(this).attr("title","Nhấn để thu gọn hình");
                    $(this).addClass("expandCollapse");
                }
            });
        }
    });
}
/*
Haynn.indexPage.registerVideoClick = function()
{
    $("[id|='playVimeo']").each(function(){
        if (!$(this).hasClass("videoProcessed"))
        {
            $(this).addClass("videoProcessed");
            var vimeoID = $(this).attr("data-vimeoid");
            var postID = $(this).attr("id").split("-")[1];
            var thumb = $("#videoThumb-" + postID);
            $(this).click(function(e){
                Haynn.indexPage.expandVideoContainer("vimeo", thumb, vimeoID, postID, true);
            });
        }
    });
    $("[id|='playYouTube']").each(function(){
        if (!$(this).hasClass("videoProcessed"))
        {
            $(this).addClass("videoProcessed");
            var youtubeID = $(this).attr('data-youtubeid');
            var postID = $(this).attr("id").split("-")[1];
            var thumb = $("#videoThumb-" + postID);
            $(this).click(function(){
                Haynn.indexPage.expandVideoContainer("youtube", thumb, youtubeID, postID, true);
            });
        }
    });
}*/




Haynn.indexPage.expandVideoContainer = function (type, thumb, videoID, postID, isFirst)
{
    if (isFirst)
    {
        $("#videoLoadingMessage").fadeIn();
        thumb.hide()
        if (type == "youtube") $("#playYouTube-" + postID).hide();
        else $("#playVimeo-" + postID).hide();
        $("#videoLogo-" + postID).hide();
        $("#entry-" + postID).find(".pic").css("width", "920px").css("height", "518px");
        $("#entry-" + postID).animate(
        {
            width: "940",
            height: "680"
        }, 500, function(){
            Haynn.indexPage.reLayout();
        });
        thumb.animate({
            width: "920",
            height: "518"
        }, 500, function(){
            $(this).fadeOut();
            $('html,body').animate({
                scrollTop: $("#anchor" + postID).offset().top - 45
            }, 500);
            if (type == "vimeo")
                Haynn.indexPage.setUpVimeoPlayer(thumb, videoID, postID);
            else
                Haynn.indexPage.setUpYouTubePlayer(thumb, videoID, postID);
        });
    }
    else
    {
        $("#entry-" + postID).animate(
        {
            width: "940",
            height: "680"
        }, 500);
        $("#entry-" + postID).find(".pic").css("width", "920px").css("height", "518px");
        if (type == "vimeo")
        {
            $("iframe[id='vmPlayer-"+ postID + "']").animate({
                "width": "920",
                "height": "518"
            }, 500, function(){
                Haynn.indexPage.reLayout();
            });
            Haynn.indexPage.scrollToClip(postID);
        }
        if (type="youtube")
        {
            $("iframe[id='ytPlayer-"+ postID + "']").animate({
                "width": "920",
                "height": "518"
            }, 500, function(){
                Haynn.indexPage.reLayout();
            });
            Haynn.indexPage.players[postID].setPlaybackQuality('720hd');
            Haynn.indexPage.scrollToClip(postID);
        }
    };
}

Haynn.indexPage.collapseVideoContainer = function (type, postID)
{
    if (type == "vimeo")
    {
        $("#entry-" + postID).animate(
        {
            width: "460",
            height: "410"
        }, 500);
        $("iframe[id='vmPlayer-"+ postID + "']").animate({
            "width": "440px",
            "height": "248px"
        }, 500, function(){
            $("#entry-" + postID).find(".pic").css("width", "440px").css("height", "248px");
            Haynn.indexPage.reLayout();
        });
        Haynn.indexPage.scrollToEntry(postID);
    }
    if (type == "youtube")
    {
        $("#entry-" + postID).animate(
        {
            width: "460",
            height: "410"
        }, 500);
        $("iframe[id='ytPlayer-"+ postID + "']").animate({
            "width": "440px",
            "height": "248px"
        }, 500, function(){
            $("#entry-" + postID).find(".pic").css("width", "440px").css("height", "248px");
            Haynn.indexPage.reLayout();
        });
        Haynn.indexPage.scrollToEntry(postID);
        Haynn.indexPage.players[postID].setPlaybackQuality('720hd');
    }

}

Haynn.indexPage.setUpVimeoPlayer = function(thumb, vimeoID, postID)
{
    //athis.find(".pic").css("width", "920px");
    thumb.before('<iframe id="vmPlayer-' + postID + '" src="http://player.vimeo.com/video/'+ vimeoID + '?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=53a8c1&amp;api=1&amp;player_id=vmPlayer-' + postID + '" width="920" height="518" frameborder="0" webkitAllowFullScreen="true" mozallowfullscreen="true" allowFullScreen="true"></iframe>');
    iframe = $("iframe[id='vmPlayer-"+ postID + "']");

    //var iframe = $("iframe[id='vmPlayer-"+ postID + "']")[0];
    //Haynn.indexPage.reLayout();
    var player = $f(iframe[0]);
    player.addEvent('ready', function(){
        player.api('play');
        player.addEvent('pause', function(playerID){
            Haynn.indexPage.collapseVideoContainer("vimeo", postID);
        });
        player.addEvent('play', function(playerID){
            $("#videoLoadingMessage").fadeOut();
            Haynn.indexPage.expandVideoContainer("vimeo", thumb, vimeoID, postID, false);
        });
    });
}

Haynn.indexPage.setUpYouTubePlayer = function(thumb, youtubeID, postID)
{
    var isFirstTime = true;
    thumb.before('<iframe id="ytPlayer-' + postID + '" width="920" height="518" src="//www.youtube.com/embed/' + youtubeID + '?rel=0&amp;html5=1&amp;vq=hd720&amp;showinfo=0&amp;modestbranding=1&amp;iv_load_policy=3&amp;nologo=1" frameborder="0" data-youtubeid="{$posts[i].youtube_key}" allowfullscreen=""></iframe>');
    Haynn.indexPage.players[postID] = new YT.Player("ytPlayer-" + postID, {
        events: {
            'onReady': playVideo,
            'onStateChange': stateChange
        }
    });

    function playVideo(event)
    {
        event.target.setPlaybackQuality('hd720');
        event.target.playVideo();
        $("#videoLoadingMessage").fadeOut();

    }

    function stateChange(event)
    {
        if (event.data == YT.PlayerState.PAUSED || event.data == YT.PlayerState.ENDED)
        {
            //event.target.setPlaybackQuality('hd720');
            postID = $(event.target.a).attr("id").split("-")[1];
            videoID = $(event.target.a).attr("data-youtubeid");
            Haynn.indexPage.collapseVideoContainer("youtube", postID);
            isFirstTime = false;
        }
        if (event.data == YT.PlayerState.PLAYING && isFirstTime == false)
        {
            //event.target.setPlaybackQuality('hd720');
            postID = $(event.target.a).attr("id").split("-")[1];
            videoID = $(event.target.a).attr("data-youtubeid");
            Haynn.indexPage.expandVideoContainer("youtube", thumb, videoID, postID, false);
        }
        if (event.data == YT.PlayerState.BUFFERING)
        {
            $("#videoLoadingMessage").fadeIn();
        }
        if (event.data == YT.PlayerState.PLAYING)
        {
            $("#videoLoadingMessage").fadeOut();
        }

    }
}
