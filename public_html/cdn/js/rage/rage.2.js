RageComic = {};
RageComic.initialize = function (R) {
    var Q = 1;
    var S = R.packRoot;
    var ad = R.siteUrl;
    var J = R.imageTypeId;
    var ae = R.submissionUrl;
    var k = R.builderUrl;
    var T = R.type;
    var I = "/images/";
    var al = navigator.userAgent.toLowerCase();
    var n;
    var o = 0;
    var m = 2;
    var K = "000000";
    var L = 5;
    var v = 0;
    var Y = [];

    function U(ao) {
        var ap = document.cookie.split("; ");
        for (var an = ap.length - 1; an >= 0; an--) {
            var aq = ap[an].split("=");
            if (ao = aq[0]) {
                return unescape(aq[1])
            }
        }
        return "~"
    }
    function am(an, ao) {
        document.cookie = an + "=" + escape(ao)
    }
    function M(an, ao) {
        var ap = U(an);
        if (ap.indexOf("~" + ao + "~") != -1) {
            return false
        }
        ap += ao + "~";
        am(an, ap);
        return true
    }
    function w(an) {
        return M("pWrD4jBo", an)
    }
    function ah() {
        var an = location.href.substring(location.href.lastIndexOf("/") + 1);
        pos = an.indexOf(".");
        if (pos > -1) {
            an = an.substr(0, pos)
        }
        return an
    }
    function Z(ap, ao) {
        var an = $(ap).attr("alt") || 0;
        var aq = parseInt(an) + parseInt(ao);
        var ar = "rotate(" + aq + "deg)";
        $(ap).attr("alt", aq);
        if ($.browser.mozilla) {
            ap = ap.get()[0];
            var at = ap.style.transform;
            if (at.indexOf("scaleX(-1)") >= 0) {
                ap.style.transform = ar + " scaleX(-1)"
            } else {
                ap.style.transform = ar
            }
            return false
        }
        if ($(ap).css("transform").indexOf("rotate") >= 0) {
            $(ap).css("transform", $(ap).css("transform").replace("rotate(" + an + "deg)", ar))
        } else {
            if ($(ap).css("transform") == "none") {
                $(ap).css("transform", ar)
            } else {
                $(ap).css("transform", ar + " " + $(ap).css("transform"))
            }
        }
        return false
    }
    function ab(an, ao) {
        aa();
        if (ao) {
            s(an)
        } else {
            t(an)
        }
        return false
    }
    function V(an) {
        an.parent().parent().remove();
        return false
    }
    function p(an, ao) {
        var aq = an.parent().parent().find("textarea.speechTextBox");
        var ap = parseInt(aq.css("font-size").replace("px", "")) + ao;
        aq.css("font-size", "" + ap + "px");
        return false
    }
    function ai(an) {
        var ap = $(an).parent().parent().find("textarea.speechTextBox");
        var ao = ap.css("font-weight");
        if (ao == "bold") {
            ap.css("font-weight", "")
        } else {
            ap.css("font-weight", "bold")
        }
        return false
    }
    function aj(an) {
        var ap = $(an).parent().parent().find("textarea.speechTextBox");
        var ao = ap.css("font-style");
        if (ao == "italic") {
            ap.css("font-style", "")
        } else {
            ap.css("font-style", "italic")
        }
        return false
    }
    function H(ap) {
        var aq = ap.parent().parent().find(".faceImage");
        var ao = aq.css("transform");
        if ($.browser.mozilla) {
            var an = $(aq).attr("alt") || 0;
            var ar = "rotate(" + an + "deg)";
            aq = aq.get()[0];
            var at = new String(aq.style.transform);
            if (at.indexOf("scaleX(-1)") >= 0) {
                aq.style.transform = ar
            } else {
                aq.style.transform = ar + " scaleX(-1)"
            }
            return false
        }
        if (ao.indexOf("scaleX(-1)") >= 0) {
            aq.css("filter", "");
            aq.css("transform", aq.css("transform").replace("scaleX(-1)", ""))
        } else {
            aq.css("filter", "fliph");
            aq.css("transform", aq.css("transform") != "none" ? aq.css("transform") + " scaleX(-1)" : "scaleX(-1)")
        }
        return false
    }
    var af = function (ao, au, at, aw, ar) {
        var ap = "face" + v;
        var an, aq = document.createElement("img");
        aq.title = "Kéo và thả! Có thể thay đổi kích thước!";
        aq.className = "faceImage";
        aq.src = ao.image;
        an = $(aq);
        an.bind("dragstart", function (ax) {
            ax.preventDefault()
        });
        $("<div class='face draggableFace' id='" + ap + "'><div class='objectControllerContainer'><span class='remove'><img class='objectController' title='Xóa' src='" + I + "delete.png' /></span><span class='sendToBack'><img class='objectController' title='Đính vào nền' src='" + I + "shape_move_backwards.png' /></span><span class='rotateLeft'><img class='objectController' title='Xoay trái' src='" + I + "shape_rotate_anticlockwise.png' /></span><span class='rotateRight'><img class='objectController' title='Xoay phải' src='" + I + "shape_rotate_clockwise.png' /></span><span class='flipHorizontal'><img class='objectController' title='Lật ngược' src='" + I + "shape_flip_horizontal.png' /></span><span class='cloneFace'><img class='objectController' title='Sao chép' src='" + I + "application_double.png' /></span></div></div>").append(an).appendTo("#canvasContainer").hover(function () {
            $("#" + ap + " > div.objectControllerContainer").show()
        }, function () {
            $("#" + ap + " > div.objectControllerContainer").hide()
        }).draggable({
            cursor: "move"
        });
        var av = Agr.GetScrollXY();
        Agr.$(ap).style.cssText = "top:" + (200 + av.top) + "px;left:" + (320 + av.left) + "px";
        $("#" + ap + " > div.objectControllerContainer > span.remove").click(function () {
            return V($(this))
        });
        $("#" + ap + " > div.objectControllerContainer > span.sendToBack").click(function () {
            return ab($(this).parent().parent(), true)
        });
        $("#" + ap + " > div.objectControllerContainer > span.rotateLeft").repeatedclick(function () {
            return Z($(this).parent().parent().find(".faceImage"), -1)
        }, {
            duration: 0,
            speed: 0.2,
            min: 100
        });
        $("#" + ap + " > div.objectControllerContainer > span.rotateRight").repeatedclick(function () {
            return Z($(this).parent().parent().find(".faceImage"), 1)
        }, {
            duration: 0,
            speed: 0.2,
            min: 100
        });
        $("#" + ap + " > div.objectControllerContainer > span.flipHorizontal").click(function () {
            return H($(this))
        });
        $("#" + ap + " > div.objectControllerContainer > span.cloneFace").click(function () {
            return q($(this).parent().parent())
        });
        $("#" + ap + " > img").error(function () {
            if (ar) {
                ar()
            }
            $("#" + ap + " > div.objectControllerContainer > span.remove").trigger("click")
        });
        $("#" + ap + " > img").load(function () {
            var ax = "#" + ap + " > img";
            ax = '$("' + ax + '")';
            setTimeout(ax + ".resizable({aspectRatio: false, autoHide: true});", 0);
            if (aw) {
                aw()
            }
        });
        v++;
        return $("#" + ap)
    };

    function x(aq) {
        var an = document.createElement("canvas");
        an.width = aq.width;
        an.height = aq.height;
        var ao = an.getContext("2d");
        ao.drawImage(aq, 0, 0);
        var ap = an.toDataURL("image/png");
        return ap
    }
    var y = function (ar, ap, at, ao) {
        if (ap.indexOf("http://") == -1) {
            var aq = new Image;
            aq.src = ap;
            var an = x(aq);
            af({
                image: an
            }, ar, an, at, ao)
        } else {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: k,
                data: {
                    imageUrl: ap
                },
                success: function (au) {
                    af(au, ar, ap, at, ao)
                },
                error: function () {
                    if (ao) {
                        ao()
                    }
                }
            })
        }
    };
    var a = function (ap, ao, aq, an) {
        if (ao.indexOf("data:") == -1 && !(/http/.test(ao))) {
            ao = "/RageContain/Ragecomic/" + ao;
            y(ap, ao, aq, an)
        } else {
            if (/http/.test(ao)) {
                y(ap, ao, aq, an)
            } else {
                af({
                    image: ao
                }, "", "", aq, an)
            }
        }
    };
    RageComic.addFace = a;

    function q(an) {
        var ao = an.find("img.faceImage");
        var ap = af({
            image: ao.attr("src")
        });
        ap.find("img.faceImage").attr("style", ao.attr("style"));
        ap.css("z-index", parseInt(an.css("z-index")) + 1);
        var aq = an.offset();
        var ar = parseInt(aq.left);
        var at = parseInt(aq.top);
        aq.left = ar - 200;
        aq.top = at - 200;
        ap.css(aq);
        return ap
    }
    function r(an) {
        var aq = an.find("textarea.speechTextBox");
        var ap = g();
        ap.find("textarea.speechTextBox").attr("style", aq.attr("style")).val(aq.val());
        var ao = an.offset();
        var ar = parseInt(ao.left);
        var at = parseInt(ao.top);
        ao.left = ar - 220;
        ao.top = at - 200;
        ap.css(ao);
        return ap
    }
    function g() {
        var ao = "face" + v;
        $("<div class='face draggableFace' id='" + ao + "'><div class='objectControllerContainer objectControllerContainerText'><span class='remove'><img class='objectController' title='Xóa' src='" + I + "delete.png' /></span><img class='objectController' title='Di chuyển- Kéo và thả' src='" + I + "arrow_out.png' /><span class='changeColor'><img class='objectController' title='Đặt màu chữ' src='" + I + "color_wheel.png' /></span><span class='sendToBack'><img class='objectController' title='Đính vào nền' src='" + I + "shape_move_backwards.png' /></span><span class='decrease'><img class='objectController' title='Giảm cỡ chữ' src='" + I + "edit-size-down.png' /></span><span class='increase'><img class='objectController' title='Tăng cỡ chữ' src='" + I + "edit-size-up.png' /></span><span class='bold'><img class='objectController' title='In đậm' src='" + I + "text_bold.png' /></span><span class='italic'><img class='objectController' title='In nghiêng' src='" + I + "text_italic.png' /></span><span class='cloneFace'><img class='objectController' title='Sao chép' src='" + I + "application_double.png' /></span><span class='choseFont'><select id='choseFont" + ao + "' style='padding-right: 0px !important' title='Chọn font'><option style='font-family: arial !important' value='arial'>arial</option><option style='font-family: tahoma !important' value='tahoma'>tahoma</option><option style='font-family: times new roman !important' value='\"times new roman\"'>Times new roman</option><option style='font-family: Comic Sans MS !important' value='\"Comic Sans MS\"'> Comic Sans MS</option><option style='font-family: HlComic1 !important' value='HlComic1'>HlComic1</option><option style='font-family: HlComic2 !important' value='HlComic2'>HlComic2</option><option style='font-family: HlsComic !important' value='HlsComic'>HlsComic</option><option style='font-family: HlComibm !important' value='HlComibm'>HlComibm</option></select></span></div><textarea id='text" + ao + "' style='font-size: 23px;font-family: arial' spellcheck='false' onblur=\"if(this.value=='') this.value='Click để nhập lời thoại; Dùng bảng mã BK HCM 2 cho các font HLComic';\" onfocus=\"if(this.value=='Click để nhập lời thoại; Dùng bảng mã BK HCM 2 cho các font HLComic') this.value='';\"class='speechTextBox'>Click để nhập lời thoại; Dùng bảng mã BK HCM 2 cho các font HLComic</textarea></div>").appendTo("#canvasContainer").hover(function () {
            $("#" + ao + " > div.objectControllerContainer").show();
            $("#" + ao + " > .speechTextBox").addClass("withBorder")
        }, function () {
            $("#" + ao + " > div.objectControllerContainer").hide();
            $("#" + ao + " > .speechTextBox").removeClass("withBorder")
        }).draggable({
            stack: {
                group: ".draggableFace",
                min: 500
            },
            cursor: "move"
        });
        var an = document.getElementById("choseFont" + ao);
        an.onchange = function () {
            $("#text" + ao).css("font-family", $(this).val())
        };
        $("#" + ao).find(".speechTextBox").autogrow({
            minHeight: 40,
            lineHeight: 20
        });
        var ap = Agr.GetScrollXY();
        Agr.$(ao).style.cssText = "top:" + (200 + ap.top) + "px;left:" + (150 + ap.left) + "px";
        $("#" + ao + " > div.objectControllerContainer > span.remove").click(function () {
            return V($(this))
        });
        $("#" + ao + " > div.objectControllerContainer > span.changeColor").ColorPicker({
            color: "#000000",
            onShow: function (aq) {
                $(aq).fadeIn(500);
                return false
            },
            onSubmit: function (at, ar, au, aq) {
                $(aq).ColorPickerHide();
                $(aq).parent().parent().find("textarea.speechTextBox").css("color", "#" + ar)
            },
            onChange: function (ar, aq, at) {
                $("#" + ao).find("textarea.speechTextBox").css("color", "#" + aq)
            }
        });
        $("#" + ao + " > div.objectControllerContainer > span.sendToBack").click(function () {
            return ab($(this).parent().parent(), false)
        });
        $("#" + ao + " > div.objectControllerContainer > span.increase").click(function () {
            return p($(this), 3)
        });
        $("#" + ao + " > div.objectControllerContainer > span.decrease").click(function () {
            return p($(this), -3)
        });
        $("#" + ao + " > div.objectControllerContainer > span.bold").click(function () {
            return ai($(this))
        });
        $("#" + ao + " > div.objectControllerContainer > span.italic").click(function () {
            return aj($(this))
        });
        $("#" + ao + " > div.objectControllerContainer > span.cloneFace").click(function () {
            return r($(this).parent().parent())
        });
        v++;
        return $("#" + ao)
    }
    function P(an) {
        an.html("");
        an.change(function () {
            O($("#drpPacks"))
        });
        $.ajaxSetup({
            headers: {}
        });
        $.getJSON(T, function (ao) {
            $.each(ao, function (ap, aq) {
                $("<option value='" + aq + "'>" + ap + "</option>").appendTo(an)
            });
            an.val("").trigger("change")
        })
    }
    function O(an) {
        an.html("");
        document.getElementById("drpPacks").onchange = (function () {
            var ao = $("#toolbar"),
                ap = $(this).val();
            N(ao, ap)
        });
        $.ajaxSetup({
            headers: {}
        });
        $.getJSON(S + "&typ=" + $("#drpType option:selected").val(), function (ap) {
            $.each(ap, function (at, au) {
                var ar = $("<optgroup label='" + at + "'></optgroup>").appendTo(an);
                $.each(au, function (av, aw) {
                    ar.append("<option value='" + av + "'>" + aw + "</option>")
                })
            });
            var ao = $("#toolbar"),
                aq = an.val();
            N(ao, aq)
        })
    }
    function N(an, ao) {
        an.find(".dock-container").html("");
        $.getJSON(D(ao), function (ap) {
            $.each(ap, function (aq, ar) {
                an.find(".dock-container").append("<a class='dock-item' href='#rageface'  data-pack='" + ao + "' data-icon='" + aq + "' ><img onmouseover=\" openPV('" + z(aq, "") + '\') "onmouseout=" closePV()" src=\'/' + z(aq, "") + "' title='" + ar + "' />")
            });
            $(".dock-container .dock-item").click(function (aq) {
                aq.preventDefault();
                var at = $(this).data("pack"),
                    ar = $(this).data("icon");
                a(at, ar)
            });
            $(".dock-container").find("a > img").batchImageLoad({
                loadingCompleteCallback: ak
            })
        })
    }
    function D(an) {
        return S + "&pack=" + an
    }
    function z(ao, an, ap, aq) {
        if (ao != "") {
            return "RageContain/Ragecomic/" + ao + an
        } else {
            return an
        }
    }
    function c(ax) {
        o += ax;
        var aw = o;
        var an = document.createElement("div");
        an.id = "DivEditLineContain" + aw;
        an.style.cssText = "top:" + (240 * parseInt(aw) - 50) + "px; position: absolute;";
        var ao = document.createElement("div");
        ao.style.cssText = "text-align: center";
        var ap = document.createElement("div");
        ap.id = "DivEditUnder" + aw;
        an.appendChild(ao);
        an.appendChild(ap);
        if (aw > 1) {
            var ay = "DivEditUnder" + (parseInt(aw) - 1);
            var aq = document.getElementById(ay);
            var aA = document.createElement("span");
            aA.title = "Xóa dòng kẻ ngang bên trái";
            aA.Typ = "UL";
            aA.onclick = function () {
                b(this)
            };
            aA.id = "spUnderLeft" + (parseInt(aw) - 1);
            aA.mod = "1";
            aA.style.cssText = "cursor: pointer;";
            aq.appendChild(aA);
            var au = document.createElement("img");
            au.src = "/RageContain/Controller/underLine.png";
            aA.appendChild(au);
            var aB = document.createElement("span");
            aB.title = "Xóa dòng kẻ ngang bên phải";
            aB.Typ = "UR";
            aB.onclick = function () {
                b(this)
            };
            aB.id = "spUnderRight" + (parseInt(aw) - 1);
            aB.mod = "1";
            aB.style.cssText = "cursor: pointer;";
            aq.appendChild(aB);
            var av = document.createElement("img");
            av.src = "/RageContain/Controller/underLine.png";
            aB.appendChild(av)
        }
        var az = document.createElement("span");
        az.title = "Xóa dòng kẻ giữa";
        az.Typ = "MD";
        az.onclick = function () {
            b(this)
        };
        az.id = "spMiddle" + aw;
        az.mod = "1";
        az.style.cssText = "cursor: pointer;";
        ao.appendChild(az);
        var at = document.createElement("img");
        at.src = "/RageContain/Controller/middleLine.png";
        az.appendChild(at);
        var ar = document.getElementById("DivEditLine");
        ar.appendChild(an);
        i();
        d(ax);
        f(ax)
    }
    function b(an) {
        aa();
        n.context.save();
        n.context.translate(0.5, 0.5);
        n.context.lineWidth = 1;
        n.context.beginPath();
        if (an.mod == 1) {
            n.context.strokeStyle = "white";
            an.mod = 0
        } else {
            an.mod = 1;
            n.context.strokeStyle = "black"
        }
        var ao;
        var aq;
        var ap = an.Typ;
        switch (ap) {
            case "UR":
                an.title = an.mod == 1 ? "Xóa dòng kẻ ngang bên phải" : "Thêm dòng kẻ ngang bên phải";
                ao = an.id.replace("spUnderRight", "");
                aq = parseInt(ao) * 240 - 1;
                n.context.moveTo(321, aq);
                n.context.lineTo(640 - 1, aq);
                break;
            case "UL":
                an.title = an.mod == 1 ? "Xóa dòng kẻ ngang bên trái" : "Thêm dòng kẻ ngang bên trái";
                ao = an.id.replace("spUnderLeft", "");
                aq = parseInt(ao) * 240 - 1;
                n.context.moveTo(0 + 1, aq);
                n.context.lineTo(319, aq);
                break;
            case "MD":
                an.title = an.mod == 1 ? "Xóa dòng kẻ giữa" : "Thêm dòng kẻ giữa";
                ao = an.id.replace("spMiddle", "");
                aq = (parseInt(ao) - 1) * 240;
                n.context.moveTo(320, aq);
                n.context.lineTo(320, aq + 240 - 1);
                break;
            default:
        }
        n.context.stroke();
        n.context.restore();
        e(o)
    }
    function d(an) {
        var ao = n.context.canvas.height / o;
        n.context.save();
        n.context.fillStyle = "#fff";
        n.context.fillRect(0, (o - an) * ao, n.context.canvas.width, n.context.canvas.height);
        n.context.restore()
    }
    function e(an) {
        var ao = n.context.canvas.height / o;
        n.context.save();
        n.context.translate(0.5, 0.5);
        n.context.strokeStyle = "black";
        n.context.lineWidth = 1;
        n.context.beginPath();
        var ap = ((o) * ao) - 1;
        n.context.moveTo(0, ap);
        n.context.lineTo(n.context.canvas.width, ap);
        n.context.moveTo(0, 0);
        n.context.lineTo(n.context.canvas.width, 0);
        n.context.moveTo(0, (o - an) * ao);
        n.context.lineTo(0, n.context.canvas.height);
        n.context.moveTo(n.context.canvas.width - 1, (o - an) * ao);
        n.context.lineTo(n.context.canvas.width - 1, n.context.canvas.height);
        n.context.stroke();
        n.context.restore()
    }
    function f(an) {
        var ao = n.context.canvas.height / o;
        n.context.save();
        n.context.translate(0.5, 0.5);
        n.context.strokeStyle = "black";
        n.context.lineWidth = 1;
        n.context.beginPath();
        n.context.moveTo(n.context.canvas.width / 2, (o - 1) * ao);
        n.context.lineTo(n.context.canvas.width / 2, n.context.canvas.height);
        n.context.closePath();
        n.context.stroke();
        n.context.restore();
        e(an)
    }
    function W() {
        if (o > 1) {
            document.getElementById("DivEditLine").removeChild(document.getElementById("DivEditLineContain" + o));
            o -= 1;
            i();
            if (o >= 1) {
                var an = document.getElementById("DivEditUnder" + o);
                var ao = document.getElementById("spUnderLeft" + o);
                var ap = document.getElementById("spUnderRight" + o);
                an.removeChild(ao);
                an.removeChild(ap)
            }
        }
    }
    function i() {
        if (n) {
            var ao = $("#drawingCanvas").width() / 2;
            var an = ao / 1.3333;
            n.resize(ao * 2, an * o)
        }
        n.setLineWidth($("#brushSizeSlider").slider("option", "value"))
    }
    function aa() {
        var an = $("#drawingCanvas")[0];
        var ao = Canvas2Image.saveAsPNG(an, true);
        Y.push(ao.src)
    }
    function X() {
        if (Y.length > 0) {
            var an = new Image();
            an.onload = function () {
                n.context.drawImage(this, 0, 0, this.width, this.height)
            };
            an.src = Y.pop()
        }
    }
    function C(an) {
        var ao = new Image();
        ao.src = (an.getAttribute ? an.getAttribute("src") : false) || an.src;
        return ao.width
    }
    function B(an) {
        var ao = new Image();
        ao.src = (an.getAttribute ? an.getAttribute("src") : false) || an.src;
        return ao.height
    }
    function u(az, ay) {
        var aw = $(".face").get();
        aw.sort(function (aA, aB) {
            var aC = $(aA).css("z-index");
            var aD = $(aB).css("z-index");
            return (aC < aD) ? -1 : (aC > aD) ? 1 : 0
        });
        $.each(aw, function () {
            if ($(this).find(".faceImage").length > 0) {
                s($(this))
            } else {
                if ($(this).find(".speechTextBox").length > 0) {
                    t($(this))
                }
            }
        });
        var at = $("#drawingCanvas")[0];
        var au = Canvas2Image.saveAsJPEG(at, true);
        au.id = "canvasImage";
        Y = [];
        $("#blank_content").empty().addClass("completedComicSubmission");
        $("body").unbind("dragover dragenter drop");
        var ap = "<ul><li><b>Để lưu ảnh về máy:</b> Click chuột phải vào ảnh và chọn Save as</li><li><b>Để đăng ảnh lên gocthugian.vn:</b> Nhập mô tả cho ảnh và click Đăng ảnh</li></ul><b>Mô tả:</b>";
        var an = Agr.$("blank_content");
        an.innerHTML = ap;
        var aq = Agr.$C("input");
        aq.type = "text";
        aq.style.width = "300px";
        an.appendChild(aq);
        var av = Agr.$C("a", an, "button button-green", '<span class="hIcon save">Đăng ảnh</span>');
        an.appendChild(au);
        var ax = Agr.$C("span", an);
        var ao = $("#canvasImage").attr("src").split(",", 2)[1];
        av.onclick = function () {
            if (Agr.State.IP) {
                return
            }
            if (aq.value == "") {
                Agr.Dlg("Lỗi", "Chưa nhập mô tả cho ảnh!!!");
                return
            }
            Agr.State.IP = true;
            Agr.ShowLoading(ax);
            Agr.RNTK(function () {
                var aB = Agr.Crypto.IS.Encrypt(Agr.State.GTK()) + ":PostPhoto:" + encodeURIComponent(Agr.HtmlEncode(aq.value)) + ":" + ao;
                var aA = function (aC) {
                    if (aC.Status == "OK") {
                        an.innerHTML = aC.Message
                    } else {
                        if (aC.Message) {
                            Agr.Dlg(aC.Status, aC.Message)
                        }
                    }
                    Agr.State.IP = false
                };
                Agr.CallBack.Call(R.UID, aB, aA)
            })
        };
        var ar = "Kick chuột phải vào ảnh và nhấn save as để lưu và máy tính!"
    }
    var l;
    var ag = function (an) {
        var ap = "FWatermark";
        $("<img class='draggableFace faceImage' id='FWatermark' src='" + an.image + "' title='Watermark sẽ được thêm ở vị trí mặc định nếu di chuyển watermark ra khỏi khung hình'></img>").appendTo("#drawingCanvasContainer").hover(function () {
            $("#" + ap + " > div.objectControllerContainer").show()
        }, function () {
            $("#" + ap + " > div.objectControllerContainer").hide()
        }).bind("dragstart", function (aq) {
            aq.preventDefault()
        }).draggable({
            cursor: "move"
        });
        var ao = document.getElementById(ap);
        ao.style.cursor = "move";
        ao.style.top = "0px";
        ao.style.right = "0px";
        ao.style.position = "absolute";
        ao.style.zIndex = "1001";
        return $("#" + ap)
    };

    function h() {
        var an = document.getElementById("FWatermark");
        var aq = 0;
        var ao = (n.context.canvas.width - l.width);
        if (an != null) {
            var ar = o * 240 - l.height;
            var ap = 640 - l.width;
            if ((an.offsetTop - n.context.canvas.offsetTop) < ar) {
                if ((an.offsetLeft - n.context.canvas.offsetLeft) < ap) {
                    if (an.offsetTop > n.context.canvas.offsetTop) {
                        if (an.offsetLeft > n.context.canvas.offsetLeft) {
                            ao = an.offsetLeft;
                            aq = an.offsetTop
                        }
                    }
                }
            }
        }
        document.getElementById("drawingCanvasContainer").removeChild(an);
        n.context.drawImage(l, ao, aq, l.width, l.height)
    }
    function G() {
        l = l || document.getElementById("watermark");
        l.src = x(l);
        ag({
            image: l.src
        })
    }
    function A(ap) {
        var an = parseInt(ap.alt) || 0;
        var ao = $(ap).offset();
        if (an != 0) {
            var aq = $(ap).css("transform");
            if ($.browser.mozilla) {
                aq = ap.style.transform
            }
            if ($.browser.mozilla) {
                ap.style.transform = "rotate(0deg)"
            } else {
                $(ap).css("transform", $(ap).css("transform").replace("rotate(" + an + "deg)", "rotate(0deg)"))
            }
            ao = $(ap).offset();
            if ($.browser.mozilla) {
                ap.style.transform = aq
            } else {
                $(ap).css("transform", aq)
            }
        }
        return ao
    }
    function F(au) {
        au = $(au);
        var aq = au.css("transform");
        if (aq != "none" && aq.indexOf("rotate") >= 0) {
            var av = aq.indexOf("rotate(") + "rotate(".length;
            var ar = aq.indexOf("deg)");
            return parseInt(aq.substring(av, ar))
        }
        var at = au.css("transform") || au.css("-webkit-transform") || au.css("-moz-transform") || au.css("-ms-transform") || au.css("-o-transform");
        var ao;
        if (at !== "none") {
            var aw = at.split("(")[1].split(")")[0].split(",");
            var an = aw[0];
            var ap = aw[1];
            ao = Math.round(Math.atan2(ap, an) * (180 / Math.PI))
        } else {
            ao = 0
        }
        return ao
    }
    function s(ap) {
        n.context.save();
        var ao = $("#drawingCanvas").offset();
        var av = ap.find(".faceImage")[0];
        var ar = A(av);
        ar.left -= ao.left;
        ar.top -= ao.top;
        var an = av.getBoundingClientRect();
        var at = $(av).width();
        var aq = $(av).height();
        var aw = parseInt(av.alt) || 0;
        var au = $(av).css("transform").indexOf("scaleX") >= 0;
        if ($.browser.mozilla) {
            au = av.style.transform.indexOf("scaleX") >= 0
        }
        if (aw != 0) {
            if (aw < 0) {
                aw = 360 + aw
            }
            n.context.translate(ar.left + (at / 2), ar.top + (aq / 2));
            n.context.rotate(aw * (Math.PI / 180));
            if (au) {
                n.context.scale(-1, 1)
            }
            n.context.drawImage(av, 0, 0, C(av), B(av), (-1 * at) / 2, (-1 * aq) / 2, at, aq)
        } else {
            var ax = ar.left;
            var ay = ar.top;
            if (au) {
                n.context.scale(-1, 1);
                ax = (ax * -1) - at
            }
            n.context.drawImage(av, ax, ay, at, aq)
        }
        n.context.restore();
        ap.remove()
    }
    function t(ar) {
        n.context.save();
        var ao = $("#drawingCanvas").offset();
        var au = ar.find(".speechTextBox");
        var av = parseInt(au.css("font-size").replace("px", ""));
        var an = au.css("font-weight") == "bold" ? "bold " : "";
        var at = au.css("font-style") == "italic" ? "italic " : "";
        var ax = au.css("font-family");
        n.context.textBaseline = "top";
        n.context.fillStyle = au.css("color");
        n.context.font = an + at + av + "px " + ax;
        var aw = au.val().split("\n");
        var aq = $(au).offset();
        aq.left -= ao.left;
        aq.top -= ao.top;
        for (var ap = 0; ap < aw.length; ap++) {
            n.context.fillText(aw[ap], aq.left, aq.top + ap * av)
        }
        n.context.restore();
        ar.remove()
    }
    function E(ao, an) {
        return Math.floor(Math.random() * (an - ao + 1)) + ao
    }
    function ac(an) {
        $("#customWidget").css("color", "#" + an);
        if (n) {
            n.setColor("#" + an)
        }
    }
    function j(an) {
        $.blockUI({
            css: {
                border: "none",
                padding: "15px",
                backgroundColor: "#000",
                "-webkit-border-radius": "10px",
                "-moz-border-radius": "10px",
                opacity: 1,
                color: "#fff"
            },
            baseZ: 9999,
            message: an
        })
    }
    function ak() {}
    $.browser = {
        version: (al.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [0, "0"])[1],
        safari: /webkit/.test(al),
        opera: /opera/.test(al),
        msie: /msie/.test(al) && !/opera/.test(al),
        mozilla: /mozilla/.test(al) && !/(compatible|webkit)/.test(al)
    };
    $.fn.makeAbsolute = function (an) {
        return this.each(function () {
            var ao = $(this);
            pos = ao.position();
            ao.css({
                position: "absolute",
                marginLeft: 0,
                marginTop: 0,
                top: pos.top,
                left: pos.left
            });
            if (an) {
                ao.remove().appendTo("body")
            }
        })
    };
    $(window).load(function () {
        $("canvas").width($("#canvasContainer").width());
        document.getElementById("erase").onclick = function () {
            $("#brushSizeSlider").slideToggle();
            n.setColor("white");
            document.getElementById("drawingCanvasInterface").style.cursor = ('url("/RageContain/Controller/eraser.png"), crosshair')
        };
        n = new CanvasPainter("drawingCanvas", "drawingCanvasInterface", {
            x: $("#drawingCanvas").offset().left,
            y: $("#drawingCanvas").offset().top
        }, aa);
        if (o == 0) {
            c(1);
            c(1)
        }
        n.setDrawAction(1);
        n.setLineWidth(L);
        P($("#drpType"));
        ak();
        if (w(ah())) {
            var an = "";
            var ao = "";
            if ($.browser.msie) {
                ao = "Ứng dụng này không chạy tốt trên Internet Explorer! Chạy tốt trên Firefox hoặc Chrome!";
                an = "images/messageLol.png"
            }
            if (ao != "") {
                ao = "<div><div style='float: left'><img src='" + an + "'></div><div>" + ao + "</div><div style='clear: both'></div></div>";
                $("#startUpMessageContainer").html(ao);
                $("#startUpMessageContainer").dialog({
                    modal: true,
                    resizable: false,
                    buttons: {
                        Ok: function () {
                            $(this).html("");
                            $(this).dialog("close")
                        }
                    }
                })
            }
        }
    });
    $(document).ready(function () {
        $("#promptContainer").dialog({
            modal: true,
            resizable: false,
            autoOpen: false,
            buttons: {
                Import: function () {
                    var an = $("#txtImportUrl").val();
                    $dlg = $(this);
                    $dlg.find(".errorText").html("");
                    if (an != "") {
                        a("", an, function () {
                            $dlg.dialog("close")
                        }, function () {
                            $dlg.find(".errorText").html("Hix, Dường như bạn nhập vào không phải là đường dẫn ảnh.")
                        })
                    } else {
                        $dlg.find(".errorText").html("Nhập Url đến ảnh!")
                    }
                },
                Close: function () {
                    $(this).dialog("close")
                }
            },
            close: function (an, ao) {
                $("#txtImportUrl").val("");
                $("#promptContainer").find(".errorText").html("")
            }
        });
        $("#addFrameCtrl").click(function () {
            c(1)
        });
        $("#removeFrameCtrl").click(W);
        $("#clearCanvas").click(function () {
            if (confirm("Are you sure you want to clear the canvas?")) {
                $(".face").remove();
                n.clearCanvas();
                if (o > m) {
                    while (o > m) {
                        W()
                    }
                } else {
                    if (o < m) {
                        c(m - o)
                    }
                }
                d(o);
                f(o);
                Y = []
            }
        });
        $("#addTextCtrl").click(g);
        $("#exportCanvas").click(function () {
            $("#exportContainer").dialog("open");
            var an = '<div style="float: left; margin: 10px"><img alt="" src="/RageContain/Controller/happy.png" style="width: 80px;"></div><div>Bạn chắc Chắn? Bước tiếp theo bạn không thể chỉnh sửa ảnh của mình.<br /><br /><span style="color: green">Bạn có thể đặt tiêu đề và đăng ảnh của mình.</span></div>';
            Agr.Dlg("Lưu ảnh", an, function () {
                u("memebase")
            })
        });
        $("#importImage").click(function () {
            var ao = '<p style="color: green">Nhập URL hoặc chọn từ máy tính ảnh mà bạn muốn đưa vào.</p>';
            ao += '<h4>Url</h4><input type="text" id="txtImportUrl" style="width:200px" value="" />';
            ao += '<h4>File</h4><input type="file" id="fileToUpload" />';
            var an = Agr.Dlg("Thêm ảnh", ao, function () {
                var at = $("#txtImportUrl").val();
                if (at != "") {
                    a("", at, function () {}, function () {
                        Agr.Dlg("Lỗi", "Hix, Dường như bạn nhập vào không phải là đường dẫn ảnh.")
                    })
                } else {
                    var ap = Agr.$("fileToUpload");
                    if (!ap.files || ap.files.length == 0) {
                        return
                    }
                    var aq = ap.files[0];
                    var ar = new FileReader();
                    ar.onload = function (au) {
                        a("", au.target.result)
                    };
                    ar.readAsDataURL(aq)
                }
            })
        }, null, null, false);
        if ($.browser.mozilla || $.browser.safari) {
            $("body").imgDrop({
                beforeDrop: function () {},
                afterDrop: function (an) {
                    a("", an.attr("src"))
                },
                afterAllDrop: function () {}
            })
        }
        $("#undoBrush").click(function () {
            X()
        });
        $("#customWidget").ColorPicker({
            color: K,
            onShow: function (an) {
                $(an).fadeIn(500);
                return false
            },
            onSubmit: function (ap, ao, aq, an) {
                $(an).ColorPickerHide()
            },
            onChange: function (ao, an, ap) {
                document.getElementById("drawingCanvasInterface").style.cursor = "";
                ac(an)
            }
        });
        $("#brushSizeSlider").slider({
            min: 1,
            max: 50,
            orientation: "vertical",
            value: L,
            change: function (an, ao) {
                n.setLineWidth(ao.value)
            },
            stop: function (an, ao) {
                $("#brushSizeSlider").slideToggle()
            }
        });
        $("#brushSize").click(function () {
            document.getElementById("drawingCanvasInterface").style.cursor = "";
            n.setColor(document.getElementById("customWidget").style.color);
            var an = $(this).offset();
            an.left += 10;
            an.top += 20;
            $("#brushSizeSlider").slideToggle()
        });
        ac(K)
    })
};