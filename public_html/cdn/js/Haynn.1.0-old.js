var Appvl = {};
Appvl.Util = {};
Appvl.Util.getParameter = function (a, b)
{
	if (b == undefined || b == null) b = window.location.search;
	a = a.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var c = "[\\?&]" + a + "=([^&#]*)";
	var d = new RegExp(c);
	var e = d.exec(b);
	if (e == null) return "";
	else return decodeURIComponent(e[1].replace(/\+/g, " "))
};
$(document).ready(function ()
{
	Haivl.Framework.preventDuplicateSubmits();
	Haivl.Framework.registerMessageCloseButton();
	Haivl.Framework.fixWhenScroll()
});
var Haivl = {};
Haivl.GA = {};
var _gaq;
Haivl.GA.init = function (b)
{
	_gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-29994824-4']);
	_gaq.push(['_trackPageview']);
	_gaq.push(['all._setAccount', 'UA-29994824-24']);
	_gaq.push(['all._trackPageview']);
	(function ()
	{
		var a = document.createElement('script');
		a.type = 'text/javascript';
		a.async = true;
		a.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(a, s)
	})()
};
Haivl.Facebook = {
	appId: null,
	preInit: null,
	postInit: null
};
Haivl.Facebook.init = function (c)
{
	Haivl.Facebook.appId = c;
	(function (d, s, a)
	{
		var b, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(a)) return;
		b = d.createElement(s);
		b.id = a;
		b.async = true;
		b.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=" + c;
		fjs.parentNode.insertBefore(b, fjs)
	}(document, 'script', 'facebook-jssdk'))
};
Haivl.Framework = {};
Haivl.Framework.fixWhenScroll = function ()
{
	$(window).scroll(function ()
	{
		var s = $(window).scrollTop();
		$(".fixedScrollDetector").each(function ()
		{
			var a = $(this);
			var b = a.next();
			var c = 51;
			if (a.attr("data-fixedTop") !== undefined) c = a.attr("data-fixedTop");
			if (a.offset().top - c <= s)
			{
				b.css(
				{
					position: "fixed",
					top: c + "px"
				})
			}
			else
			{
				b.css(
				{
					position: "relative",
					top: ""
				})
			}
		})
	})
};
Haivl.Framework.preventDuplicateSubmits = function ()
{
	$(".submitForm").submit(function ()
	{
		if ($(this).valid())
		{
			$('.submitButton').attr("disabled", "disabled")
		}
	})
};
Haivl.Framework.registerMessageCloseButton = function ()
{
	$(".close").click(function ()
	{
		$(this).parent().fadeTo(200, 0, function ()
		{
			$(this).slideUp(300)
		});
		return false
	})
};
Haivl.ListPhoto = {
	isLoading: false,
	page: 1,
	initPage: 1,
	hasNoMore: false,
	sort: null,
	topContributorSort: null
};
Haivl.ListPhoto.init = function (b, c)
{
	Haivl.ListPhoto.sort = b;
	Haivl.ListPhoto.page = c;
	Haivl.ListPhoto.initPage = c;
	$(document).ready(function ()
	{
		Haivl.ListPhoto.topContributorSort = $(".topUsersSortHome a.selected").attr("data-sort");
		Haivl.ListPhoto.showTip();
		Haivl.ListPhoto.registerVoteButtonClick();
		Haivl.ListPhoto.registerTopContributorSortClick();
		Haivl.ListPhoto.registerHotkeys();
		Haivl.ListPhoto.registerHideClick();
		$(window).scroll(function ()
		{
			Haivl.ListPhoto.loadMore();
			Haivl.ListPhoto.fixInfoPanel()
		});
		var a = {
			maxWidth: '400px',
			autoSize: true,
			closeEffect: 'none',
			closeBtn: true,
			topRatio: 0.3,
			helpers:
			{
				overlay:
				{
					closeClick: false
				}
			}
		};
		if ($("#installExtContainer").length > 0)
		{
			setTimeout(function ()
			{
				$.fancybox.open("#installExtContainer", a)
			}, 2000)
		}
	});
	window.fbAsyncInit = function ()
	{
		FB.Event.subscribe('edge.create', Haivl.ListPhoto.recount);
		FB.Event.subscribe('edge.remove', Haivl.ListPhoto.recount)
	}
};
Haivl.ListPhoto.showTip = function ()
{
	var a = ["Hãy giúp <b>hàivl <a href='/vote'>bình chọn ảnh</a></b> để nhiều ảnh hay xuất hiện ở đây hơn", "<b>Bình luận</b> có thể gây cười hơn chính bức ảnh đó!", "Gặp bài hay hãy <b>\"like\"</b> động viên tác giả nhé!", "Bổ sung <b>website</b> vào <b><a href='/account/edit'>thông tin cá nhân</a></b> để PR cho website của bạn!", "<b>Ấn hotkey</b> ← và → hoặc Z và X để duyệt ảnh nhanh hơn", "Hãy giúp haivl báo cáo vi phạm những hình ảnh phản cảm", "<a href='/cach-vao-facebook-bi-chan-mang-vnpt-viettel-fpt'>Đăng nhập vào Facebook</a> để duyệt haivl sướng hơn!", "Ai cũng có thể đăng ảnh lên haivl.com. Thử <a href='/upload'>upload ngay!</a>"];
	var i = Math.floor((Math.random() * a.length));
	$(".tips").html("<b>Mẹo: </b>" + a[i])
};
Haivl.ListPhoto.registerVoteButtonClick = function ()
{
	$(".voteButton").live("click", function (e)
	{
		e.preventDefault();
		var a = $(this).attr("data-upvote");
		var b = $(this).attr("data-id");
		if ($(this).hasClass("upVoted") || $(this).hasClass("downVoted"))
		{
			$(this).removeClass("upVoted downVoted");
			Haivl.ListPhoto.vote(b, a, true)
		}
		else
		{
			$(".voteButton", $(this).parents(".vote")).removeClass("upVoted downVoted");
			if (a == "true") $(this).addClass("upVoted");
			else $(this).addClass("downVoted");
			Haivl.ListPhoto.vote(b, a, false)
		}
	})
};
Haivl.ListPhoto.vote = function (b, c, d)
{
	$.post("/photos/vote",
	{
		photoId: b,
		isUpVote: c,
		isUnvote: d
	}, function (a)
	{
		if (a == "Unauthenticated")
		{
			window.location.href = "/account/login?returnUrl=" + document.location.pathname + document.location.search
		}
		else
		{}
	})
};
Haivl.ListPhoto.recount = function (a)
{
	if (a.toString().indexOf('/photo/') > 0) $.post("/photos/countbyurl?url=" + a)
};
Haivl.ListPhoto.loadMore = function ()
{
	if ($(window).scrollTop() + $(window).height() >= $(document).height() - 300)
	{
		if (Haivl.ListPhoto.isLoading || Haivl.ListPhoto.hasNoMore) return;
		Haivl.ListPhoto.isLoading = true;
		$(".loading").show();
		$.ajax(
		{
			url: Haivl.ListPhoto.sort == "vote" ? "/photos/listvotemore?page=" + (Haivl.ListPhoto.page + 1) : "/photos/more?sort=" + Haivl.ListPhoto.sort + "&page=" + (Haivl.ListPhoto.page + 1),
			dataType: "html",
			success: function (b)
			{
				if (b == "No more") Haivl.ListPhoto.hasNoMore = true;
				else
				{
					var c = $("<div>" + b + "</div>");
					$(".photoListItem", c).each(function ()
					{
						if ($(".photoListItem[data-id=" + $(this).attr("data-id") + "]").size() > 0)
						{
							console.log("removed");
							$(this).remove()
						}
						else if (Haivl.ListPhoto.sort != "vote")
						{
							var a = $(".thumbnail img.thumbImg", $(this)).attr("data-src");
							if ($(this).data("nsfw") != "True") a = a.replace(/\!/g, '9').replace(/\@/g, '1').replace(/\#/g, '6');
							$(".thumbnail img.thumbImg", $(this)).attr("src", a)
						}
					});
					$("#listEnd").before(c.html());
					try
					{
						FB.XFBML.parse()
					}
					catch (e)
					{}
					Haivl.ListPhoto.page++;
					if (Haivl.ListPhoto.page - Haivl.ListPhoto.initPage == 2)
					{
						Haivl.ListPhoto.hasNoMore = true;
						$(".nextListPage").show()
					}
				}
			},
			error: function () {},
			complete: function ()
			{
				Haivl.ListPhoto.isLoading = false;
				$(".loading").hide()
			}
		})
	}
};
Haivl.ListPhoto.fixInfoPanel = function ()
{
	$(".photoListItem").each(function ()
	{
		var s = $(window).scrollTop();
		var a = $(this);
		var b = $(".info", this);
		if (a.offset().top - 60 < s && s < a.offset().top - 60 + a.outerHeight())
		{
			if (s + b.outerHeight() < a.offset().top + a.outerHeight() - 70)
			{
				b.css(
				{
					position: "fixed",
					top: "60px"
				});
				if (a.data("newly-listed") == "True")
				{
					Haivl.Unread.addReadPhoto(parseInt(a.data("id")))
				}
			}
			else
			{
				b.css(
				{
					position: "relative",
					top: ""
				})
			}
		}
		else
		{
			b.css(
			{
				position: "relative",
				top: ""
			})
		}
	})
};
Haivl.ListPhoto.registerTopContributorSortClick = function ()
{
	$(".topUsersSortHome a").click(function (e)
	{
		e.preventDefault();
		var a = $(this).attr("data-sort");
		if (a == Haivl.ListPhoto.topContributorSort) return;
		Haivl.ListPhoto.topContributorSort = a;
		$(".topUsersSortHome a").removeClass("selected");
		$(this).addClass("selected");
		$("#topUserContent").load("/home/TopContributors?sort=" + a)
	})
};
Haivl.ListPhoto.registerHotkeys = function ()
{
	$(document).keydown(function (e)
	{
		var a = e.keyCode;
		if (a == 37 || a == 90)
		{
			var b = false;
			$($(".listItemSeparator").get().reverse()).each(function ()
			{
				if ($(this).offset().top - 60 < $(window).scrollTop())
				{
					scrollTo(0, $(this).offset().top - 60);
					b = true;
					return false
				}
			});
			if (!b) scrollTo(0, 0)
		}
		else if (a == 39 || a == 88)
		{
			$(".listItemSeparator").each(function ()
			{
				if ($(this).offset().top - 62 > $(window).scrollTop())
				{
					scrollTo(0, $(this).offset().top - 60);
					return false
				}
			})
		}
	})
};
Haivl.ListPhoto.registerHideClick = function ()
{
	$("a.hide").live('click', function (e)
	{
		e.preventDefault();
		var a = $(this);
		$.post('/photos/hide',
		{
			id: a.attr('data-id')
		}, function (b)
		{
			if (b.Success)
			{
				a.html("Hidden successfully")
			}
			else
			{
				a.html(b.Message)
			}
			a.addClass("disabled")
		})
	})
};
Haivl.PhotoDetails = {
	photoId: null
};
Haivl.PhotoDetails.init = function (b)
{
	Haivl.PhotoDetails.photoId = b;
	$(document).ready(function ()
	{
		Haivl.PhotoDetails.registerHotKeys();
		Haivl.ListPhoto.registerVoteButtonClick();
		Haivl.PhotoDetails.registerReportClick();
		Haivl.PhotoDetails.registerDeleteClick();
		Haivl.PhotoDetails.registerHideClick();
		Haivl.PhotoDetails.addRef();
		var a = {
			maxWidth: 800,
			maxHeight: 600,
			width: '500px',
			height: '280px',
			autoSize: false,
			openEffect: 'none',
			closeEffect: 'none',
			closeBtn: false
		};
		$(".commentContainer .report").fancybox(a);
		if (Appvl.Util.getParameter("report") == "1") $.fancybox.open("#reportPhotoContainer", a)
	});
	window.fbAsyncInit = function ()
	{
		if (typeof (Haivl.Facebook.preInit) == 'function')
		{
			Haivl.Facebook.preInit()
		}
		FB.Event.subscribe('comment.create', Haivl.PhotoDetails.recount);
		FB.Event.subscribe('comment.remove', Haivl.PhotoDetails.recount);
		FB.Event.subscribe('edge.create', Haivl.PhotoDetails.recount);
		FB.Event.subscribe('edge.remove', Haivl.PhotoDetails.recount);
		if (typeof (Haivl.Facebook.postInit) == 'function')
		{
			Haivl.Facebook.postInit()
		}
	}
};
Haivl.PhotoDetails.recount = function ()
{
	$.post("/photos/count/" + Haivl.PhotoDetails.photoId)
};
Haivl.PhotoDetails.registerHotKeys = function ()
{
	$(document).keydown(function (e)
	{
		var a = e.keyCode;
		if (a == 37 || a == 90)
		{
			if ($(".navButtons a.prev").size() > 0) window.location.href = $(".navButtons a.prev").attr("href")
		}
		else if (a == 39 || a == 88)
		{
			if ($(".navButtons a.next").size() > 0) window.location.href = $(".navButtons a.next").attr("href")
		}
	})
};
Haivl.PhotoDetails.registerReportClick = function ()
{
	$("#reportPhotoContainer .submitButton").click(function ()
	{
		var a = $.trim($("#reportFreeText").val());
		if (a == "") a = $("#reportDdl").val();
		$.fancybox.close();
		$.post("/photos/report",
		{
			photoId: Haivl.PhotoDetails.photoId,
			reason: a
		});
		alert("Cảm ơn bạn đã báo cáo hình ảnh này. BQT sẽ xử lý trong thời gian sớm nhất.")
	})
};
Haivl.PhotoDetails.registerDeleteClick = function ()
{
	$("#deletePhoto").click(function (e)
	{
		e.preventDefault();
		if (confirm("Bạn có chắc chắn muốn xóa?") == true)
		{
			$("#deleteForm").submit()
		}
	})
};
Haivl.PhotoDetails.registerHideClick = function ()
{
	$("#hidePhoto").click(function (e)
	{
		e.preventDefault();
		if (confirm("Sure?") == false)
		{
			return false
		}
		var a = $(this);
		$.post('/photos/hide',
		{
			id: a.attr('data-id')
		}, function (b)
		{
			if (b.Success)
			{
				a.html("Hidden successfully")
			}
			else
			{
				a.html(b.Message)
			}
			a.addClass("disabled")
		})
	})
};
Haivl.PhotoDetails.addRef = function ()
{
	var i = 1;
	$(".randomBox .photoListItemSmall a").each(function ()
	{
		$(this).attr("href", $(this).attr("href") + "?ref=r" + i);
		i++
	});
	i = 1;
	$(".newestBox .photoListItemSmall a").each(function ()
	{
		$(this).attr("href", $(this).attr("href") + "?ref=n" + i);
		i++
	})
};
Haivl.Uploader = {
	isLoading: false,
	page: 1,
	initPage: 1,
	hasNoMore: false,
	uploaderId: null
};
Haivl.Uploader.init = function (a, b)
{
	Haivl.Uploader.uploaderId = a;
	Haivl.Uploader.page = b;
	Haivl.Uploader.initPage = b;
	$(document).ready(function ()
	{
		Haivl.ListPhoto.registerHotkeys();
		$(window).scroll(function ()
		{
			Haivl.Uploader.loadMore();
			Haivl.ListPhoto.fixInfoPanel()
		})
	})
};
Haivl.Uploader.loadMore = function ()
{
	if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50)
	{
		if (Haivl.Uploader.isLoading || Haivl.Uploader.hasNoMore) return;
		Haivl.Uploader.isLoading = true;
		$(".loading").show();
		$.ajax(
		{
			url: "/photos/uploadermore/" + Haivl.Uploader.uploaderId + "?page=" + (Haivl.Uploader.page + 1),
			dataType: "html",
			success: function (a)
			{
				if (a == "No more") Haivl.Uploader.hasNoMore = true;
				else
				{
					var b = $("<div>" + a + "</div>");
					$(".photoListItem", b).each(function ()
					{
						if ($(".photoListItem[data-id=" + $(this).attr("data-id") + "]").size() > 0)
						{
							console.log("removed");
							$(this).remove()
						}
					});
					$("#listEnd").before(b.html());
					try
					{
						FB.XFBML.parse()
					}
					catch (e)
					{}
					Haivl.Uploader.page++;
					if (Haivl.Uploader.page - Haivl.Uploader.initPage == 2)
					{
						Haivl.Uploader.hasNoMore = true;
						$(".nextListPage").show()
					}
				}
			},
			error: function () {},
			complete: function ()
			{
				Haivl.Uploader.isLoading = false;
				$(".loading").hide()
			}
		})
	}
};
Haivl.Upload = {};
Haivl.Upload.init = function (a, b)
{
	$(document).ready(function ()
	{
		if (a) $(".inputForm input, .inputForm textarea, .inputForm .buttons").attr("disabled", "disabled");
		if (b) $("#Source").attr("disabled", "disabled");
		Haivl.Upload.registerSelfMadeChange();
		Haivl.Upload.checkLikeBeggar()
	})
};
Haivl.UploadVideo = {};
Haivl.UploadVideo.init = function (a)
{
	$(document).ready(function ()
	{
		if (a) $(".inputForm input, .inputForm textarea, .inputForm .buttons").attr("disabled", "disabled");
		Haivl.Upload.checkLikeBeggar();
		Haivl.UploadVideo.handleYoutubeUrl()
	})
};
Haivl.UploadVideo.handleYoutubeUrl = function ()
{
	$("#YoutubeUrl").change(function ()
	{
		var a = Appvl.Util.getParameter("v", $("#YoutubeUrl").val());
		if (a == "")
		{
			alert("Đường dẫn Youtube không đúng.");
			return
		}
		$("#youtubeEmbed").html("<iframe width='518' height='300' src='http://www.youtube.com/embed/" + a + "' frameborder='0'></iframe>");
		$("#youtubeEmbed").show()
	})
};
Haivl.Edit = {};
Haivl.Edit.init = function (a)
{
	$(document).ready(function ()
	{
		if (a) $("#Source").attr("disabled", "disabled");
		Haivl.Upload.registerSelfMadeChange();
		Haivl.Upload.checkLikeBeggar()
	})
};
Haivl.Upload.registerSelfMadeChange = function ()
{
	$("#IsSelfMade").click(function ()
	{
		$("#Source").val('');
		$("#Source").attr("disabled", "disabled");
		if (!$("#IsSelfMade").is(':checked'))
		{
			$("#Source").prop("disabled", false)
		}
	})
};
Haivl.Upload.checkLikeBeggar = function ()
{
	$("#Caption").change(function ()
	{
		if ($(this).val().toLowerCase().indexOf("like") >= 0) alert("Chú ý: ảnh có tiêu đề câu like có thể bị xóa (xem nội quy bên phải).")
	})
};
Haivl.LocalStorage = {
	Keys:
	{
		ReadPhotos: "ReadPhotos"
	}
};
Haivl.LocalStorage.isSupported = function ()
{
	try
	{
		localStorage.setItem("isSupported", true);
		localStorage.removeItem("isSupported");
		return true
	}
	catch (e)
	{
		return false
	}
};
Haivl.Unread = {
	justRead:
	{},
	submitted:
	{},
	isLoading: false,
	hasNoMore: false,
	page: 0,
	initPage: 0,
	shouldTrack: false
};
Haivl.Unread.addReadPhoto = function (a)
{
	if (!Haivl.Unread.shouldTrack) return;
	if (!Haivl.Unread.justRead[a])
	{
		Haivl.Unread.justRead[a] = true
	}
};
Haivl.Unread.getJustReadPhotoIds = function ()
{
	var a = "";
	for (var b in Haivl.Unread.justRead)
	{
		if (a == "") a += b;
		else a += "," + b
	}
	return a
};
Haivl.Unread.submit = function (a)
{
	var b = [];
	var c = "";
	for (var d in Haivl.Unread.justRead)
	{
		if (!Haivl.Unread.submitted[d])
		{
			Haivl.Unread.submitted[d] = true;
			b.push(d);
			if (c == "") c += d;
			else c += "," + d
		}
	}
	if (c == "") return;
	$.ajax(
	{
		url: "/photos/markread",
		dataType: "html",
		type: "POST",
		async: a,
		data:
		{
			photoIds: c
		},
		success: function () {},
		error: function ()
		{
			for (var i in b) delete Haivl.Unread.submitted[i]
		},
		complete: function () {}
	})
};
Haivl.Unread.startTracking = function ()
{
	$(document).ready(function ()
	{
		Haivl.Unread.shouldTrack = true;
		setInterval(function ()
		{
			Haivl.Unread.submit(true)
		}, 30000);
		$(window).unload(function ()
		{
			Haivl.Unread.submit(false)
		})
	})
};
Haivl.Unread.init = function ()
{
	$(document).ready(function ()
	{
		Haivl.Unread.startTracking();
		Haivl.ListPhoto.registerHotkeys();
		Haivl.Unread.loadSinglePage();
		$(window).scroll(function ()
		{
			Haivl.Unread.loadMore();
			Haivl.ListPhoto.fixInfoPanel()
		})
	})
};
Haivl.Unread.loadSinglePage = function ()
{
	Haivl.Unread.isLoading = true;
	$(".loading").show();
	$.ajax(
	{
		url: "/photos/listunreadajax",
		dataType: "html",
		type: "POST",
		data:
		{
			excludePhotoIds: Haivl.Unread.getJustReadPhotoIds()
		},
		success: function (a)
		{
			if (a == "No more")
			{
				Haivl.Unread.hasNoMore = true;
				if (Haivl.Unread.page == 0)
				{
					$(".noUnread").show()
				}
			}
			else
			{
				var b = $("<div>" + a + "</div>");
				$(".photoListItem", b).each(function ()
				{
					if ($(".photoListItem[data-id=" + $(this).attr("data-id") + "]").size() > 0)
					{
						$(this).remove();
						console.log("Duplicate removed")
					}
				});
				$("#listEnd").before(b.html());
				try
				{
					FB.XFBML.parse()
				}
				catch (e)
				{}
				Haivl.Unread.page++;
				if (Haivl.Unread.page - Haivl.Unread.initPage == 3)
				{
					Haivl.Unread.hasNoMore = true;
					$(".nextListPage").show()
				}
			}
		},
		error: function () {},
		complete: function ()
		{
			Haivl.Unread.isLoading = false;
			$(".loading").hide()
		}
	})
};
Haivl.Unread.loadMore = function ()
{
	if ($(window).scrollTop() + $(window).height() >= $(document).height() - 300)
	{
		if (Haivl.Unread.isLoading || Haivl.Unread.hasNoMore) return;
		Haivl.Unread.loadSinglePage()
	}
};