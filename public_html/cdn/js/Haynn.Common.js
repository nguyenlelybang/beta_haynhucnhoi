/*!
 * imagesLoaded PACKAGED v3.2.0
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

(function(){"use strict";function e(){}function t(e,t){for(var n=e.length;n--;)if(e[n].listener===t)return n;return-1}function n(e){return function(){return this[e].apply(this,arguments)}}var i=e.prototype,r=this,s=r.EventEmitter;i.getListeners=function(e){var t,n,i=this._getEvents();if("object"==typeof e){t={};for(n in i)i.hasOwnProperty(n)&&e.test(n)&&(t[n]=i[n])}else t=i[e]||(i[e]=[]);return t},i.flattenListeners=function(e){var t,n=[];for(t=0;t<e.length;t+=1)n.push(e[t].listener);return n},i.getListenersAsObject=function(e){var t,n=this.getListeners(e);return n instanceof Array&&(t={},t[e]=n),t||n},i.addListener=function(e,n){var i,r=this.getListenersAsObject(e),s="object"==typeof n;for(i in r)r.hasOwnProperty(i)&&-1===t(r[i],n)&&r[i].push(s?n:{listener:n,once:!1});return this},i.on=n("addListener"),i.addOnceListener=function(e,t){return this.addListener(e,{listener:t,once:!0})},i.once=n("addOnceListener"),i.defineEvent=function(e){return this.getListeners(e),this},i.defineEvents=function(e){for(var t=0;t<e.length;t+=1)this.defineEvent(e[t]);return this},i.removeListener=function(e,n){var i,r,s=this.getListenersAsObject(e);for(r in s)s.hasOwnProperty(r)&&(i=t(s[r],n),-1!==i&&s[r].splice(i,1));return this},i.off=n("removeListener"),i.addListeners=function(e,t){return this.manipulateListeners(!1,e,t)},i.removeListeners=function(e,t){return this.manipulateListeners(!0,e,t)},i.manipulateListeners=function(e,t,n){var i,r,s=e?this.removeListener:this.addListener,o=e?this.removeListeners:this.addListeners;if("object"!=typeof t||t instanceof RegExp)for(i=n.length;i--;)s.call(this,t,n[i]);else for(i in t)t.hasOwnProperty(i)&&(r=t[i])&&("function"==typeof r?s.call(this,i,r):o.call(this,i,r));return this},i.removeEvent=function(e){var t,n=typeof e,i=this._getEvents();if("string"===n)delete i[e];else if("object"===n)for(t in i)i.hasOwnProperty(t)&&e.test(t)&&delete i[t];else delete this._events;return this},i.removeAllListeners=n("removeEvent"),i.emitEvent=function(e,t){var n,i,r,s,o=this.getListenersAsObject(e);for(r in o)if(o.hasOwnProperty(r))for(i=o[r].length;i--;)n=o[r][i],n.once===!0&&this.removeListener(e,n.listener),s=n.listener.apply(this,t||[]),s===this._getOnceReturnValue()&&this.removeListener(e,n.listener);return this},i.trigger=n("emitEvent"),i.emit=function(e){var t=Array.prototype.slice.call(arguments,1);return this.emitEvent(e,t)},i.setOnceReturnValue=function(e){return this._onceReturnValue=e,this},i._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},i._getEvents=function(){return this._events||(this._events={})},e.noConflict=function(){return r.EventEmitter=s,e},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return e}):"object"==typeof module&&module.exports?module.exports=e:this.EventEmitter=e}).call(this),function(e){function t(t){var n=e.event;return n.target=n.target||n.srcElement||t,n}var n=document.documentElement,i=function(){};n.addEventListener?i=function(e,t,n){e.addEventListener(t,n,!1)}:n.attachEvent&&(i=function(e,n,i){e[n+i]=i.handleEvent?function(){var n=t(e);i.handleEvent.call(i,n)}:function(){var n=t(e);i.call(e,n)},e.attachEvent("on"+n,e[n+i])});var r=function(){};n.removeEventListener?r=function(e,t,n){e.removeEventListener(t,n,!1)}:n.detachEvent&&(r=function(e,t,n){e.detachEvent("on"+t,e[t+n]);try{delete e[t+n]}catch(i){e[t+n]=void 0}});var s={bind:i,unbind:r};"function"==typeof define&&define.amd?define("eventie/eventie",s):e.eventie=s}(this),function(e,t){"use strict";"function"==typeof define&&define.amd?define(["eventEmitter/EventEmitter","eventie/eventie"],function(n,i){return t(e,n,i)}):"object"==typeof module&&module.exports?module.exports=t(e,require("wolfy87-eventemitter"),require("eventie")):e.imagesLoaded=t(e,e.EventEmitter,e.eventie)}(window,function(e,t,n){function i(e,t){for(var n in t)e[n]=t[n];return e}function r(e){return"[object Array]"==f.call(e)}function s(e){var t=[];if(r(e))t=e;else if("number"==typeof e.length)for(var n=0;n<e.length;n++)t.push(e[n]);else t.push(e);return t}function o(e,t,n){if(!(this instanceof o))return new o(e,t,n);"string"==typeof e&&(e=document.querySelectorAll(e)),this.elements=s(e),this.options=i({},this.options),"function"==typeof t?n=t:i(this.options,t),n&&this.on("always",n),this.getImages(),u&&(this.jqDeferred=new u.Deferred);var r=this;setTimeout(function(){r.check()})}function h(e){this.img=e}function a(e,t){this.url=e,this.element=t,this.img=new Image}var u=e.jQuery,c=e.console,f=Object.prototype.toString;o.prototype=new t,o.prototype.options={},o.prototype.getImages=function(){this.images=[];for(var e=0;e<this.elements.length;e++){var t=this.elements[e];this.addElementImages(t)}},o.prototype.addElementImages=function(e){"IMG"==e.nodeName&&this.addImage(e),this.options.background===!0&&this.addElementBackgroundImages(e);var t=e.nodeType;if(t&&d[t]){for(var n=e.querySelectorAll("img"),i=0;i<n.length;i++){var r=n[i];this.addImage(r)}if("string"==typeof this.options.background){var s=e.querySelectorAll(this.options.background);for(i=0;i<s.length;i++){var o=s[i];this.addElementBackgroundImages(o)}}}};var d={1:!0,9:!0,11:!0};o.prototype.addElementBackgroundImages=function(e){for(var t=m(e),n=/url\(['"]*([^'"\)]+)['"]*\)/gi,i=n.exec(t.backgroundImage);null!==i;){var r=i&&i[1];r&&this.addBackground(r,e),i=n.exec(t.backgroundImage)}};var m=e.getComputedStyle||function(e){return e.currentStyle};return o.prototype.addImage=function(e){var t=new h(e);this.images.push(t)},o.prototype.addBackground=function(e,t){var n=new a(e,t);this.images.push(n)},o.prototype.check=function(){function e(e,n,i){setTimeout(function(){t.progress(e,n,i)})}var t=this;if(this.progressedCount=0,this.hasAnyBroken=!1,!this.images.length)return void this.complete();for(var n=0;n<this.images.length;n++){var i=this.images[n];i.once("progress",e),i.check()}},o.prototype.progress=function(e,t,n){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded,this.emit("progress",this,e,t),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,e),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&c&&c.log("progress: "+n,e,t)},o.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emit(e,this),this.emit("always",this),this.jqDeferred){var t=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[t](this)}},h.prototype=new t,h.prototype.check=function(){var e=this.getIsImageComplete();return e?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,n.bind(this.proxyImage,"load",this),n.bind(this.proxyImage,"error",this),n.bind(this.img,"load",this),n.bind(this.img,"error",this),void(this.proxyImage.src=this.img.src))},h.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth},h.prototype.confirm=function(e,t){this.isLoaded=e,this.emit("progress",this,this.img,t)},h.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},h.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},h.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},h.prototype.unbindEvents=function(){n.unbind(this.proxyImage,"load",this),n.unbind(this.proxyImage,"error",this),n.unbind(this.img,"load",this),n.unbind(this.img,"error",this)},a.prototype=new h,a.prototype.check=function(){n.bind(this.img,"load",this),n.bind(this.img,"error",this),this.img.src=this.url;var e=this.getIsImageComplete();e&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},a.prototype.unbindEvents=function(){n.unbind(this.img,"load",this),n.unbind(this.img,"error",this)},a.prototype.confirm=function(e,t){this.isLoaded=e,this.emit("progress",this,this.element,t)},o.makeJQueryPlugin=function(t){t=t||e.jQuery,t&&(u=t,u.fn.imagesLoaded=function(e,t){var n=new o(this,e,t);return n.jqDeferred.promise(u(this))})},o.makeJQueryPlugin(),o});

var Haynn = {};

Haynn.Following = {};

Haynn.Following.changeButtonState = function (obj, recursive) {
    if ($(obj).hasClass("not-follow")) {
        $(obj).removeClass("not-follow");
        $(obj).addClass("following");
        $(obj).html("<span class='follow-icon'></span>Đang Hóng");
        $(obj).attr("title", "Nhấn để không hóng nữa");
    }
    else {
        $(obj).removeClass("following");
        $(obj).addClass("not-follow");
        $(obj).html("<span class='follow-icon'></span>Hóng");
        $(obj).attr("title", "Nhấn để hóng");
    }

    if (recursive == true) {
        var followedID = $(obj).attr("data-followed");
        var id = $(obj).attr("id");
        $("[data-followed=" + followedID + "]:not('#" + id + "')").each(function () {
            if ($(this) != $(obj)) Haynn.Following.changeButtonState(this, false);
        });
    }
};

Haynn.Following.init = function () {
    var isProcessing = false;
    $(document).on("click", "[id|='follow']", function (event) {
        event.preventDefault();
        var action = "follow";
        var followed = $(this).attr("data-followed");

        if ($(this).hasClass("following")) action = "unfollow";
        if (isProcessing == false) {
            $(this).find(".follow-icon").addClass("loading");
            $.ajax
            ({
                url: Settings.baseurl + "/ajax/follow?followed=" + followed,
                context: this,
                dataType: 'json',
                beforeSend: function () {
                    isProcessing = true;
                },
                success: function (result) {
                    if (result.error_code == 0) {
                        $(this).find(".follow-icon").removeClass("loading");
                        Haynn.Following.changeButtonState(this, true);
                        isProcessing = false;
                    }
                    else if (result.error_code == 2) {
                        isProcessing = false;
                        $(this).find(".follow-icon").removeClass("loading");
                        displayLoginIsRequiredDialog();
                    }
                    else {
                        $(this).find(".follow-icon").removeClass("loading");
                        isProcessing = false;
                    }
                }
            });
        }
    });
};

Haynn.common = {};


Haynn.common.registerVoteClick = function () {
    $(document).on("click", '.voteButton', function (event) {
        event.preventDefault();
        var postID = $(this).attr("data-id");
        var isUpVote = $(this).attr("data-upvote");
        if ($(this).hasClass("voted")) {
            changeVoteButtonState($(this));
            Haynn.common.votePost(postID, isUpVote, true, $(this));

        }
        else {
            $(this).parent().find(".voteButton").removeClass("voted");
            changeVoteButtonState($(this));
            Haynn.common.votePost(postID, isUpVote, false, $(this));
        }
        ;
    });
    changeVoteButtonState = function (btn) {
        if (btn.hasClass("voted")) {
            btn.removeClass("voted");
        }
        else {
            btn.addClass("voted");
        }
    }
};

Haynn.common.votePost = function (postID, isUpVote, isUnvote, btn) {
    var varResponse;
    $.post(Settings.baseurl + "ajax/vote.php",
        {
            postID: postID,
            isUpVote: isUpVote,
            isUnvote: isUnvote
        }, function (response) {
            if (response == "Unauthenticated") {
                {
                    displayLoginIsRequiredDialog();
                }
            }
            else if (response == "OK") {
                //varResponse = response;
            }
        });
    //return varResponse;
};

Haynn.common.fixedHeader = function () {
    var masthead = $('.masthead');
    $(window).scroll(function () {
        if ($(document).scrollTop() < (masthead.height() / 2)) {
            $('.masthead').removeClass('condensed');
            $('#fixedHeaderAnchor').height(0);
        } else {
            $('#fixedHeaderAnchor').height(110);
            $('.masthead').addClass('condensed');
        }
    });
    /*var fixedHeader = $(".subhead");
     var fixedHeaderHeight = fixedHeader.outerHeight();
     var fixedHeaderAnchor = $("#fixedHeaderAnchor");
     var fixedHeaderAnchorTop = fixedHeaderAnchor.offset().top;
     var headerHeight = $(".head").outerHeight();
     $(window).scroll(function(){
     if ($(this).scrollTop() > fixedHeaderAnchorTop)
     {
     fixedHeaderAnchor.height(fixedHeaderHeight);
     fixedHeader.css({
     "position": 'fixed',
     "top": 0
     });
     }
     else
     {
     fixedHeaderAnchor.height(0);
     fixedHeader.css({
     "position": 'static',
     "top": headerHeight
     });
     }
     })*/
};
Haynn.common.fixWhenScroll = function () {
    var s = $(window).scrollTop();
    $(".fixedScrollDetector").each(function () {
        var a = $(this);
        var b = a.next();
        var c = 10;
        if (a.attr("data-fixedTop") !== undefined) c = a.attr("data-fixedTop");
        if (a.offset().top - c <= s) {
            b.css({
                position: "fixed",
                top: c + "px"
            })
        } else {
            b.css({
                position: "relative",
                top: ""
            })
        }
    })
};

Haynn.common.log = function ($url, $action, $location) {
    $.post(
        Settings.baseurl + "/ajax/log", {
            url: $url,
            location: $location,
            user_id: Account.UID,
            type: $action,
            platform: 'desktop'
        }
    )
};

Haynn.blogIndexPage = {
    isLoading: false,
    page: 1,
    ajaxUri: 1,
    initPage: "blogIndex",
    initPageNumber: 1,
    hasNoMore: false,
    sort: null,
    showLoadMore: true,
    quickUploadType: null,
    players: {}
};

Haynn.blogIndexPage.loadMorePost = function (page, ajaxUri) {
    if (Haynn.blogIndexPage.initPage != "unread") {
        if (categorySlug) {
            slug = "/" + categorySlug;
        }
        else {
            slug = "";
        }

        if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 300)) {
            Haynn.blogIndexPage.showLoadMore = false;
            if (Haynn.indexPage.isLoading || Haynn.blogIndexPage.hasNoMore) return;
            Haynn.blogIndexPage.isLoading = true;
            $('#load_image').show();

            $.get(Settings.baseurl + "/" + ajaxUri + "/" + (page + 1), function (data) {
                if (data == "NoMorePostToShow") {
                    $("#btnLoadMoreContainer").css("height", "");
                    Haynn.blogIndexPage.hasNoMore = true;
                    Haynn.blogIndexPage.showLoadMore = false;
                    $('#load_image').hide();
                    $("#nextPageLink").remove();
                    $("#noMorePost").show();
                }
                else {
                    contentToCheck = $("<div>" + data + "</div>");
                    $(".pin-container", contentToCheck).each(function () {
                        if ($(".pin-container[data-id=" + $(this).attr("data-id") + "]").size() > 0) {
                            $(this).remove();
                        }
                    });
                    dataToInsert = $(contentToCheck.html());
                    if (Haynn.blogIndexPage.theme == 1) {
                        $("#listEnd").before(dataToInsert);
                        $('#load_image').hide();
                        Haynn.blogIndexPage.isLoading = false;
                        Haynn.blogIndexPage.page++;
                        Haynn.blogIndexPage.showLoadMore = true;
                        $("#btnLoadMoreContainer").css("height", "");

                        dataToInsert.find("[id|=likebutton]").each(function () {
                            FB.XFBML.parse($(this)[0]);
                        });
                    }
                    else {
                        var container = $('#post-container');
                        container.append(dataToInsert);
                        $('#load_image').hide();
                        Haynn.blogIndexPage.isLoading = false;
                        Haynn.blogIndexPage.page++;
                        Haynn.blogIndexPage.showLoadMore = true;
                        $("#btnLoadMoreContainer").css("height", "");
                        FB.XFBML.parse();
                        /*
                         var dataToParseLike = $('<div>' + contentToCheck.html() + '</div>');
                         dataToParseLike.find("[id|=entry]").each(function(){

                         });*/
                        //container.isotope('appended', dataToInsert);
                        /*dataToInsert.imagesLoaded(function() {
                         //Haynn.indexPage.setExpand();
                         //Haynn.indexPage.reLayout();

                         });*/
                    }


                    $("#nextPageLink").attr('href', Settings.baseurl + "/" + ajaxUri + "/" + (Haynn.blogIndexPage.page + 2));

                    if (Haynn.blogIndexPage.page - Haynn.blogIndexPage == 2) {
                        $("#btnLoadMore").show();
                        Haynn.blogIndexPage.hasNoMore = true;
                    }
                }
            });
        }
    }
};

Haynn.indexPage = {
    isLoading: false,
    page: 1,
    ajaxUri: 1,
    initPage: "index",
    initPageNumber: 1,
    hasNoMore: false,
    sort: null,
    showLoadMore: true,
    quickUploadType: null,
    players: {}
};

Haynn.indexPage.recount = function ($url, $html_element) {
    if ($url.toString().indexOf('/photo/') > 0) {
        $.post(Settings.baseurl + "/ajax/updatestats/?url=" + $url);
    }
};


Haynn.indexPage.getTopComment = function (a) {
    if (a.commentID !== undefined) $.post(Settings.baseurl + "ajax/count/?do=getTopComment&url=" + a.href)
};

Haynn.indexPage.layout = function () {
    var container = $("#post-container");
    container.imagesLoaded(function () {
        Haynn.indexPage.setExpand();
        container.isotope({
            itemSelector: '.pin-container',
            //animationEngine: 'jquery',
            transformsEnabled: false,
            masonry: {
                columnWidth: 240
            },
            onLayout: function (elems, instance) {
                elems.each(function (e) {
                    $(this).css({"z-index": (elems.length - e)});
                });
            }
        });
    });
};

Haynn.indexPage.reLayout = function () {
    var container = $('#post-container');
    container.isotope('reLayout');
};

Haynn.indexPage.setExpand = function () {
    $(document).ready(function () {
        $(".pin-container").each(function () {
            if ($(this).height() > 1200) {
                $(this).addClass("expandable");
                $(this).find(".pic").css('height', "600px");
            }
            ;
        });
    });
};

Haynn.indexPage.scrollToClip = function (postID) {
    setTimeout(function () {
        $('html,body').animate(
            {
                scrollTop: $("#anchor" + postID).offset().top - 60
            }, 500)
    }, 1000);
};

Haynn.indexPage.scrollToEntry = function (postID) {
    setTimeout(function () {
        $('html,body').animate(
            {
                scrollTop: $("#entry-" + postID).offset().top - 45
            }, 500)
    }, 1000);
};

/*Haynn.indexPage.loadMoreAtBottom = function ()
 {
 if (Haynn.indexPage.initPage != "unread")
 {
 if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 400))
 {
 /*if ((Haynn.indexPage.page % 4) === 0)
 {
 if (Haynn.indexPage.showLoadMore === true)
 {
 $("#btnLoadMore").show();
 Haynn.indexPage.showLoadMore = false;
 }
 }*/
/*if (Haynn.indexPage.page - Haynn.indexPage.initPage == 2)
 {
 $("#btnLoadMore").show();
 Haynn.indexPage.hasNoMore = true;
 }
 else
 {
 Haynn.indexPage.loadMorePost(Haynn.indexPage.page, Haynn.indexPage.theme);
 }
 }
 }
 };*/

Haynn.indexPage.registerLoadMorePost = function () {
    $("#btnLoadMore").click(function () {
        Haynn.indexPage.showLoadMore = false;
        $("#btnLoadMoreContainer").css("height", "42px");
        $(this).fadeOut(200);
        if (Haynn.indexPage.initPage != "unread")
            Haynn.indexPage.loadMorePost(Haynn.indexPage.page);
        else
            Haynn.Unread.loadSinglePage(false);
    });
};

Haynn.indexPage.registerTopClick = function () {
    /*$("#topDay, #topWeek, #topMonth").click(function(){
     var id = $(this).attr("id");
     var sort = $(this).attr("data-sort");
     $(this).parent().find("div").removeClass("active");
     $(this).addClass("active");
     if ($("#" + id + "Content").html() == "")
     {
     $("#topLoadingImg").show();
     $.get(Settings.baseurl + "ajax/top.php?type=" + sort, function(data){
     $("#topDayContent, #topWeekContent, #topMonthContent").hide();
     $("#" + id + "Content").html(data);
     $("#" + id + "Content").show();
     $("#topLoadingImg").hide();
     })
     }
     else
     {
     $("#topDayContent, #topWeekContent, #topMonthContent").hide();
     $("#" + id + "Content").show();
     }
     });*/

    moveTopList = function () {
        var headerHeight = $(".head").outerHeight();
        var sidebarWidth = $(".sidebar").width();
        var rListAnchor = $("#topContentAnchor");
        var rList = $(".fixedWhenScroll");
        var rListHeight = rList.height();
        var rListAnchorTop = rListAnchor.offset().top - headerHeight;
        if ($(this).scrollTop() >= rListAnchorTop && rList.css('position') != 'fixed') {
            rListAnchor.css('height', rListHeight);
            rList.css({
                'top': headerHeight + "px",
                'position': 'fixed',
                width: sidebarWidth
            });
            $(".blockTitle", rList).addClass('grayBlockTitle');
            /*$("#topDay").html("Top Ngày");
             $("#topWeek").html("Top Tuần");
             $("#topMonth").html("Top Tất Cả");*/
        }
        else if ($(this).scrollTop() < rListAnchorTop && rList.css('position') != 'relative') {
            rListAnchor.css('height', '0px');
            rList.css({
                'top': '0px',
                'position': 'relative',
            });
            $(".blockTitle", rList).removeClass('grayBlockTitle');
            /*          $("#topDay").html("Ngày");
             $("#topWeek").html("Tuần");
             $("#topMonth").html("Tất cả");*/
        }
        ;
    };
    if (Haynn.indexPage.theme == 1) {
        $(window).scroll(function () {
            moveTopList();
        });
    }
    ;
};

Haynn.indexPage.registerModToolsClick = function () {
    $(document).on('click', '.modTools', function (event) {
        event.preventDefault();
        var token = $(this).attr("data-token");
        var postID = $(this).attr("data-pid");
        var action = $(this).attr("data-action");
        var cid = $(this).attr("data-cid");
        var thisOne = $(this);
        $.post(
            Settings.baseurl + 'ajax/moderation.php',
            {
                "do": action,
                "token": token,
                "pid": postID,
                "cid": cid
            },
            function (data, textStatus, xhr) {
                if (data == "OK" && (action == "delete" || action == "movetohome")) {
                    $("#entry-" + postID).fadeTo('250', 0.0, function () {
                        $(this).slideUp("250", function () {
                            if (Haynn.indexPage.theme == 2) {
                                $(this).css("margin-bottom", "0px").css("margin-top", "0px");
                                $(this).remove();
                                Haynn.indexPage.reLayout();
                            }
                            else {
                                $(this).remove();
                            }
                            if ($("[id|=entry]").length == 1) {
                                Haynn.indexPage.loadMorePost();
                            }
                        });
                    });
                }
                else if (data == "OK" && action == "movetocat") {
                    thisOne.parent().find(".modTools").removeClass('active');
                    thisOne.addClass('active');
                }
            });
    });
};

Haynn.indexPage.registerTopUsersClick = function () {
    $(".topUsers .sort a").click(function (event) {
        event.preventDefault();
        var sort = $(this).attr("data-sort");
        if (sort == Haynn.indexPage.sort) return;
        Haynn.indexPage.sort = sort;
        $(".topUsers .sort a").removeClass('selected');
        $(this).addClass('selected');
        $(".topUsers .content").load(Settings.baseurl + "/ajax/get-top-member/type/" + sort);
    });
};

Haynn.indexPage.loadMorePost = function (page, ajaxUri) {
    if (Haynn.indexPage.initPage != "unread") {
        if (categorySlug) {
            slug = "/" + categorySlug;
        }
        else {
            slug = "";
        }
        ;
        if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 300)) {
            Haynn.indexPage.showLoadMore = false;
            if (Haynn.indexPage.isLoading || Haynn.indexPage.hasNoMore) return;
            Haynn.indexPage.isLoading = true;
            $('#load_image').show();

            $.get(Settings.baseurl + "/" + ajaxUri + "/" + (page + 1), function (data) {
                if (data == "NoMorePostToShow") {
                    $("#btnLoadMoreContainer").css("height", "");
                    Haynn.indexPage.hasNoMore = true;
                    Haynn.indexPage.showLoadMore = false;
                    $('#load_image').hide();
                    $("#nextPageLink").remove();
                    $("#noMorePost").show();
                }
                else {
                    contentToCheck = $("<div>" + data + "</div>");
                    $(".pin-container", contentToCheck).each(function () {
                        if ($(".pin-container[data-id=" + $(this).attr("data-id") + "]").size() > 0) {
                            $(this).remove();
                        }
                    });
                    dataToInsert = $(contentToCheck.html());
                    if (Haynn.indexPage.theme == 1) {
                        $("#listEnd").before(dataToInsert);
                        $('#load_image').hide();
                        Haynn.indexPage.isLoading = false;
                        Haynn.indexPage.page++;
                        Haynn.indexPage.showLoadMore = true;
                        $("#btnLoadMoreContainer").css("height", "");

                        dataToInsert.find("[id|=likebutton]").each(function () {
                            FB.XFBML.parse($(this)[0]);
                        });
                    }
                    else {
                        var container = $('#post-container');
                        container.append(dataToInsert);
                        $('#load_image').hide();
                        Haynn.indexPage.isLoading = false;
                        Haynn.indexPage.page++;
                        Haynn.indexPage.showLoadMore = true;
                        $("#btnLoadMoreContainer").css("height", "");
                        FB.XFBML.parse();
                        /*
                         var dataToParseLike = $('<div>' + contentToCheck.html() + '</div>');
                         dataToParseLike.find("[id|=entry]").each(function(){

                         });*/
                        //container.isotope('appended', dataToInsert);
                        /*dataToInsert.imagesLoaded(function() {
                         //Haynn.indexPage.setExpand();
                         //Haynn.indexPage.reLayout();

                         });*/
                    }
                    ;

                    $("#nextPageLink").attr('href', Settings.baseurl + "/" + ajaxUri + "/" + (Haynn.indexPage.page + 2));

                    if (Haynn.indexPage.page - Haynn.indexPageNumber == 2) {
                        $("#btnLoadMore").show();
                        Haynn.indexPage.hasNoMore = true;
                    }
                }
            });
        }
    }
};

Haynn.indexPage.fixVoteGroup = function () {
    var headerHeight = $(".head").outerHeight();
    $(".pin-container").each(function () {
        var windowScrollTop = $(window).scrollTop();
        var pin = $(this);
        var upperInfo = $(".upperInfo", this);
        var upperInfoHeight = upperInfo.outerHeight();
        var voteGroup = $(".userInfo", this);
        var placeHolder = $(".upperInfoHeightHolder", this);

        if ((pin.offset().top - upperInfoHeight < windowScrollTop + 5)) {
            if (pin.attr("data-new") == "true") {
                Haynn.Unread.addReadPhoto(parseInt(pin.attr("data-pid")));
            }
            if (windowScrollTop + voteGroup.outerHeight() < pin.offset().top + pin.outerHeight() - headerHeight + 5) {
                voteGroup.css(
                    {
                        position: "fixed",
                        top: headerHeight + "px"
                    });
                upperInfo.css({
                    position: 'fixed',
                    top: headerHeight + "px",
                    "z-index": "10",
                    "background": "#FFFFFF",
                    "width": "398px"
                });
                //upperInfo.addClass('upperInfoFixed');
                placeHolder.height(upperInfoHeight);
            }
            else {
                voteGroup.css(
                    {
                        position: "absolute",
                        top: pin.height() - voteGroup.height() - parseFloat(voteGroup.css("padding-top")) - 3
                    });
                upperInfo.css(
                    {
                        position: "absolute",
                        top: pin.height() - voteGroup.height() - parseFloat(voteGroup.css("padding-top")) - 3,
                        "z-index": ""
                    });
            }
        }
        else {
            voteGroup.css(
                {
                    position: "relative",
                    top: "",
                    "z-index": ""
                });
            upperInfo.css(
                {
                    position: "relative",
                    top: ""
                });
            placeHolder.height(0);
        }
    })
};

Haynn.indexPage.fixInfoPanel = function () {
    var count = 18;
    $(".pin-container").each(function () {
        var scrollTop = $(window).scrollTop();
        var pin = $(this);
        var rightCol = $(".rightCol", this);

        if (pin.offset().top - 10 < scrollTop && scrollTop < pin.offset().top - 10 + pin.outerHeight()) {
            if (scrollTop + rightCol.outerHeight() < pin.offset().top + pin.outerHeight() - 10) {
                rightCol.css({
                    position: "fixed",
                    top: "10px",
                    zIndex: 20
                });
            } else {
                rightCol.css({
                    position: "relative",
                    top: "",
                    zIndex: 15

                })
            }
        } else {
            rightCol.css({
                position: "relative",
                top: "",
                zIndex: count
            })
        }
        count--;

    })
};

Haynn.indexPage.quickUploadInit = function () {
    var postPhotoForm = $("#postPhotoForm");
    var postVideoForm = $("#postVideoForm");
    var postMusicForm = $("#postMusicForm");
    var boxAnchor = $(".quickSubmitForm").find(".boxAnchor");
    var photoTips = $("#photoTips");
    var videoTips = $("#videoTips");
    var postTitle = $("#post_title");
    var postEditor = $('.emoji-wysiwyg-editor');
    var videoLink = $("#photo_post_url");
    var musicLink = $("#music_post_url");
    var videoInfo = $(".videoInfo");

    $("[id^='clickSubmit']").click(function (event) {
        event.preventDefault();
        if ($(this).hasClass('loginRequired')) {
            displayLoginIsRequiredDialog();
            return;
        }

        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
        } else {
            $(this).parent().find("[id^='clickSubmit']").removeClass("active");
            $(this).addClass("active");
        }

        if ($(this).attr("id") == "clickSubmitPhoto") {
            if (!(postPhotoForm).is(":visible")) {
                $("#photo_file_upload").trigger("click");
            }
            if (postVideoForm.is(":visible")) {
                videoTips.hide();
                postVideoForm.hide();
                videoInfo.hide();
            }
            if (postMusicForm.is(":visible")) {
                postMusicForm.hide();
                videoInfo.hide();
            }
            postPhotoForm.fadeToggle(350, function () {
                if (Haynn.indexPage.theme == 2) {
                    Haynn.indexPage.reLayout();
                }
                setQuickUploadType();
            });
            photoTips.fadeToggle(350);
            boxAnchor.css("margin-left", "");
        }
        if ($(this).attr("id") == "clickSubmitVideo") {
            if (postPhotoForm.is(":visible")) {
                photoTips.hide();
                postPhotoForm.hide();
            }
            if (postMusicForm.is(":visible")) {
                videoInfo.hide();
                postMusicForm.hide();
            }
            postVideoForm.fadeToggle(350, function () {
                if (Haynn.indexPage.theme == 2) {
                    Haynn.indexPage.reLayout();
                }
                setQuickUploadType();
            });
            videoTips.fadeToggle(350);
            videoLink = $("#photo_post_url");
            if ($(".videoInfoThumbnail").html() != "") videoInfo.fadeToggle(350);
            boxAnchor.css("margin-left", "40px");
        }

        if ($(this).attr("id") == "clickSubmitMusic") {
            if (postPhotoForm.is(":visible")) {
                photoTips.hide();
                postPhotoForm.hide();
            }
            if (postVideoForm.is(":visible")) {
                videoTips.hide();
                postVideoForm.hide();
                videoInfo.hide();
            }
            postMusicForm.fadeToggle(350, function () {
                if (Haynn.indexPage.theme == 2) {
                    Haynn.indexPage.reLayout();
                }
                setQuickUploadType();
            });
            videoLink = $("#music_post_url");
            if ($(".videoInfoThumbnail").html() != "") videoInfo.fadeToggle(350);
            boxAnchor.css("margin-left", "43px");
        }
    });

    videoLink.on("input", function () {
        value = $(this).val();
        if (value != "") {
            if (isValidYouTubeURL(value) || isValidVimeoURL(value) || isValidZingTVUrl(value) || isValidNCT(value) || isValidMeCloud(value)) {
                $("#videoLoadingImg").show();
                $.getJSON(Settings.baseurl + "/ajax/get-media-info?url=" + encodeURIComponent(value))
                    .success(function (data) {
                        $(".videoInfo").show();
                        $(".videoInfoTitle").html("<p class='videoTitle'>" + data.title + "</p><p class='metadata'>Đăng bởi <span class='name'>" + data.uploader + "</span></p>");
                        $(".videoInfoThumbnail").html("<img src='" + data.thumbnail + "' alt=''/>");
                        $("#videoLoadingImg").hide();
                        $("#noData").hide();
                        $("#btnSubmit").removeAttr('disabled');
                    })
                    .error(function () {
                        $(".videoInfo").hide();
                        $("#noData").show();
                        $("#videoLoadingImg").hide();
                        $("#btnSubmit").attr('disabled', 'disabled');
                    });
            }
            else {
                displayMessageDialog("#invalidVideoURL");
                return false;
            }
            ;
        }
        ;
    });
    musicLink.on("input", function () {
        value = $(this).val();
        if (value != "") {
            if (isValidZingMp3URL(value)) {
                $("#videoLoadingImg").show();
                $.getJSON(Settings.baseurl + "ajax/get-media-info?url=" + value)
                    .done(function (data) {
                        $(".videoInfo").show();
                        $(".videoInfoTitle").html("<p class='videoTitle'>" + data.title + "</p><p class='metadata'><span class='name' style='font-weight: normal'>" + data.uploader + "</span></p>");
                        $(".videoInfoThumbnail").html("<img class='musicThumb' src='" + data.thumbnail + "' alt=''/>");
                        $("#videoLoadingImg").hide();
                        if (Haynn.indexPage.theme == 2) {
                            Haynn.indexPage.reLayout();
                        }
                    });
            }
            else {
                alert('ddd');
                displayMessageDialog("#invalidVideoURL");
                return false;
            }
        }
    });

    $("#btnSubmit").click(function (event) {
        event.preventDefault();
        checkAndSubmit();
    });

    function checkAndSubmit() {
        if ((postTitle.val().match(/:/g) || []).length >= 5) {
            displayMessageDialog('#MaxEmoticons');
            postEditor.css('border-color', 'red');
            return;
        }

        if (postTitle.val() == "") {
            displayMessageDialog("#titleEmptyAlert");
            postEditor.css('border-color', 'red');
            return;
        } else {
            postTitle.css('border-color', '');
        }
        if (postTitle.val().match(/([\<])([^\>]{1,})*([\>])/i) != null) {
            displayMessageDialog("#titleScriptAlert");
            postEditor.focus();
            return;
        }

        if (postTitle.val().trim().length > 150) {
            displayMessageDialog("#titleTooLong");
            postEditor.css('border-color', 'red');
            return;
        }
        if (Haynn.indexPage.quickUploadType == null) {
            displayMessageDialog("#mediaIsRequired");
            return;
        }
        if (Haynn.indexPage.quickUploadType == "photo") {
            if ($("#photo_file_upload").val() == "") {
                displayMessageDialog("#fileEmptyAlert");
                return;
            }
            var size = document.getElementById("photo_file_upload").files[0].size;
            var maxsize = Math.ceil(size / 1024 / 1024);
            if (maxsize > 4) {
                displayMessageDialog("#MaxFileSizeHome");
                return;
            }

            var ext = $('#photo_file_upload').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg', 'jpeg', 'gif']) == -1) {
                displayMessageDialog("#wrongExtAlert");
                return;
            }
        } else if (Haynn.indexPage.quickUploadType == "video") {
            if (videoLink.val() == "") {
                displayMessageDialog("#urlEmptyAlert");
                return;
            }

            if (!isValidYouTubeURL(videoLink.val()) && !isValidVimeoURL(videoLink.val()) && !isValidZingTVUrl(videoLink.val()) && !isValidNCT(videoLink.val()) && !isValidMeCloud(videoLink.val())) {
                displayMessageDialog("#invalidVideoURL");
                return;
            }
        } else if (Haynn.indexPage.quickUploadType == "music") {
            if (musicLink.val() == "") {
                displayMessageDialog("#urlEmptyAlert");
                return;
            }

            if (!isValidZingMp3URL(musicLink.val())) {
                displayMessageDialog("#invalidVideoURL");
                return;
            }
        }
        $("#btnSubmit").attr("disabled", "disabled");
        if ($("#isNSFW").is(":checked")) {
            var isNSFW = 1;
        } else var isNSFW = 0;

        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "isNSFW").val(isNSFW);
        $("#form-post-image").append(input).submit();
    }

    function isValidYouTubeURL(url) {
        var p = /^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/;
        return (url.match(p)) ? true : false;
    }

    function isValidVimeoURL(url) {
        var p = /^(?:http(?:s)?:\/\/)?(www\.)?vimeo\.com\/(\w*\/)*(([a-z]{0,2}-)?\d+)/;
        return (url.match(p)) ? true : false;
    }

    function isValidZingTVUrl(url) {
        var p = /^(?:http(?:s)?:\/\/)?(www\.)?tv\.zing\.vn\/video\/([A-Za-z-\d]+)(\/(\w{8}\.html))?\/(\w{8})(\.html)$/;
        return (url.match(p)) ? true : false;
    }

    function isValidZingMp3URL(url) {
        var p = /^(http:\/\/)?mp3\.zing\.vn\/(bai-hat|album|playlist)\/([A-Za-z-\d]+)\/(\w{8})(\.html)$/;
        return (url.match(p)) ? true : false;
    }

    function isValidNCT(url) {
        var p = /^(http:\/\/www.nhaccuatui.com|http:\/\/nhaccuatui.com)\/(bai-hat|video)\/[a-zA-Z-0-9]+\.(.*)\.html$/;
        return (url.match(p)) ? true : false;
    }

    function isValidMeCloud(url) {
        return (url.indexOf('[mecloud]') > -1 && url.indexOf('[/mecloud]') > -1);
    }

    function setQuickUploadType() {
        Haynn.indexPage.quickUploadType = null;
        if (postPhotoForm.is(":visible")) {
            $("#form-post-image").attr("action", Settings.baseurl + "/upload");
            $("#photo_post_url, #music_post_url").attr("disabled", "disabled");
            $("#photo_file_upload").removeAttr("disabled");
            $("#post_type").val("Photo");
            Haynn.indexPage.quickUploadType = "photo";
        }
        else if (postVideoForm.is(":visible")) {
            $("#form-post-image").attr("action", Settings.baseurl + "/upload/video");
            $("#photo_file_upload, #music_post_url").attr("disabled", "disabled");
            $("#photo_post_url").removeAttr("disabled");
            $("#post_type").val("Video");
            Haynn.indexPage.quickUploadType = "video";
        }
        else if (postMusicForm.is(":visible")) {
            $("#form-post-image").attr("action", Settings.baseurl + "/upload/music");
            $("#photo_file_upload, #photo_post_url").attr("disabled", "disabled");
            $("#music_post_url").removeAttr("disabled");
            $("#post_type").val("Video");
            Haynn.indexPage.quickUploadType = "music";
        }
    }
};

Haynn.indexPage.getTopCommentHome = function (a) {
    // if (a.commentID !== undefined) //$.post(Settings.baseurl + "ajax/count/?do=getTopComment&url=" + a.href)
    console.log(a);
};


Haynn.indexPage.init = function (pageName, pageNumber) {
    Haynn.indexPage.ajaxUri = pageName;
    Haynn.indexPage.initPage = pageName;
    Haynn.indexPage.page = pageNumber;
    Haynn.indexPageNumber = pageNumber;
    Haynn.common.registerVoteClick();
    $(document).ready(function () {
        if (Haynn.indexPage.theme == 2) {
            Haynn.indexPage.layout();
        }
        //Haynn.indexPage.registerVideoClick();
        if (Haynn.indexPage.initPage == "unread") {
            // Haynn.indexPage.registerLoadMorePost();
        }
        Haynn.indexPage.registerTopClick();
        Haynn.indexPage.registerModToolsClick();
        Haynn.indexPage.registerTopUsersClick();
        Haynn.indexPage.quickUploadInit();
        $(window).scroll(function () {
            // Haynn.indexPage.loadMorePost(Haynn.indexPage.page, Haynn.indexPage.ajaxUri);
            Haynn.indexPage.fixInfoPanel();
            Haynn.common.fixWhenScroll();
        });

        $('body').on('click', '.vote-btn-wrapper',function () {
            $.ajax({
                type: 'POST',
                url: Settings.baseurl + '/ajax/vote',
                data: {
                    post_id: $(this).attr('data-postid'),
                    user_id: Account.UID,
                    token: $(this).attr('data-token'),
                    time: Settings.now
                },
                beforeSend: function() {
                    $('div', this).addClass('loading');
                },
                context: this,
                cache: false,
                dataType: 'json'
            }).done(function(response) {
                $('div', this).removeClass('loading');
                if (!response.error) {
                    if (response.action == 'unlike') {
                        $('div', this).removeClass('liked');
                    } else {
                        $('div', this).addClass('liked');
                    }
                    $(this).attr('title', response.tooltip).attr('original-title', response.tooltip);
                    $('#like-button-count-' + $(this).attr('data-postid')).html(response.number);
                    $(this).tipsy('hide'); $(this).tipsy('show');
                }
            });
        });
    });
    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create', function ($url) {
            Haynn.indexPage.recount($url);
            Haynn.common.log($url, 'like', Haynn.indexPage.initPage);
        });
        FB.Event.subscribe('edge.remove', function ($url) {
            Haynn.indexPage.recount($url);
            Haynn.common.log($url, 'unlike', Haynn.indexPage.initPage);
        });
        FB.Event.subscribe('comment.remove', function ($url) {
            Haynn.indexPage.recount($url);
            Haynn.common.log($url, 'comment', Haynn.indexPage.initPage);
        });
        FB.Event.subscribe('comment.create', function ($url) {
            Haynn.indexPage.recount($url);
            Haynn.common.log($url, 'uncomment', Haynn.indexPage.initPage);
        });
    }
};

Haynn.Facebook = {
    appId: null
};

Haynn.Facebook.init = function (c) {
    Haynn.Facebook.appId = c;
    (function (d, s, a) {
        var b, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(a)) return;
        b = d.createElement(s);
        b.id = a;
        b.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=" + c;
        fjs.parentNode.insertBefore(b, fjs)
    }(document, 'script', 'facebook-jssdk'))
};

Haynn.itemDetail = {
    itemID: null,
    displayInfoPane: true,
    likeButtonRendered: false
};

Haynn.itemDetail.init = function (c, v) {
    Haynn.itemDetail.itemID = c;
    videoType = v || "none";
    Haynn.common.registerVoteClick();

    window.fbAsyncInit = function () {
        FB.Event.subscribe('edge.create', function (url) {
            Haynn.itemDetail.recount();
            Haynn.common.log(url, 'like', 'detail');
        });
        FB.Event.subscribe('edge.remove', function (url) {
            Haynn.itemDetail.recount();
            Haynn.common.log(url, 'unlike', 'detail');
        });
        FB.Event.subscribe('comment.remove', function (reponse) {
            Haynn.itemDetail.recount();
            Haynn.common.log(reponse.href, 'uncomment', 'detail');
        });
        FB.Event.subscribe('comment.create', function (reponse) {
            Haynn.itemDetail.recount();
            Haynn.common.log(reponse.href, 'comment', 'detail');
        });
    };

    Haynn.itemDetail.registerDeleteClick();
    Haynn.itemDetail.registerReportClick();
    Haynn.itemDetail.fixWhenScroll();
    //Haynn.itemDetail.registerNavigationKeys();
    Haynn.itemDetail.displaySuccessMessage();
};

Haynn.itemDetail.fixWhenScroll = function () {
    var contentWrap = $(".contentWrap");
    var headerHeight = $(".head").outerHeight();
    var moveInfoPanel = function () {
        var scrollAnchor = $(".scroller_anchor");
        var scroller = $(".fixedInfoPanel");
        var scrollAnchorTop = scrollAnchor.offset().top - headerHeight;
        var fixedInfoPanelHeight = scroller.outerHeight();
        if ($(this).scrollTop() >= scrollAnchorTop + 10 /*&& scroller.css('display') != 'block' && Haynn.itemDetail.displayInfoPane*/) {
            scrollAnchor.css('height', fixedInfoPanelHeight + 'px');
            scroller.css({
                'top': '0',
                'position': 'fixed'
            });
            //scroller.fadeIn(250);
            //scroller.show();
            /*if (!Haynn.itemDetail.likeButtonRendered)
             {
             FB.XFBML.parse(document.getElementById('fixedInfoPanelLike'));
             Haynn.itemDetail.likeButtonRendered = true;
             }*/
        }
        else if ($(this).scrollTop() < scrollAnchorTop + 10) {
            scroller.css({
                'position': "relative",
                "top": "0px"
            });
            scrollAnchor.css('height', '0px');
            //scroller.fadeOut(250);
            //scroller.hide();
        };
    };
    var moveRList = function () {
        var rListAnchor = $("#fixedRecommendedListAnchor");
        var rList = $(".featuredClips");
        var rListHeight = rList.height();
        var rListAnchorTop = rListAnchor.offset().top - 45;
        if ($(this).scrollTop() >= rListAnchorTop && rList.css('position') != 'fixed') {
            rListAnchor.css('height', rListHeight);
            rList.css({
                'top': '44px',
                'position': 'fixed'
            });
        }
        else if ($(this).scrollTop() < rListAnchorTop && rList.css('position') != 'relative') {
            rListAnchor.css('height', '0px');
            rList.css({
                'top': '0px',
                'position': 'relative'
            });
        }
    };
    var moveVoteButtons = function () {
        var headerHeight = $(".head").height();
        var rListAnchor = $("#voteGroupAnchor");
        var rList = $(".voteGroup");
        var rListHeight = rList.height();
        var rListAnchorTop = rListAnchor.offset().top - 111;
        if ($(this).scrollTop() >= rListAnchorTop && rList.css('position') != 'fixed') {
            rListAnchor.css('height', rListHeight);
            rList.css({
                'top': '110px',
                'position': 'fixed'
            });
        }
        else if ($(this).scrollTop() < rListAnchorTop && rList.css('position') != 'relative') {
            rListAnchor.css('height', '0px');
            rList.css({
                'top': '0px',
                'position': 'relative'
            });
        }
        if ($(this).scrollTop() > (contentWrap.offset().top + contentWrap.height() - $("#voteGroup").height() - 212)) {
            rList.css({
                'top': contentWrap.height() - contentWrap.offset().top - 79,
                'position': 'absolute'
            });
        }
    };
    $(window).scroll(moveInfoPanel);
    $(window).scroll(moveRList);
    //$(window).scroll(moveVoteButtons);
};

Haynn.itemDetail.recount = function () {
    $.post(Settings.baseurl + "/ajax/updatestats/id/" + Haynn.itemDetail.itemID)
};

Haynn.itemDetail.getTopComment = function () {
    console.log(Haynn.itemDetail.itemID);
    $.post(Settings.baseurl + "/ajax/addeletecomment/?do=add&itemID=" + Haynn.itemDetail.itemID);
};

Haynn.itemDetail.getRemoveComment = function () {
    $.post(Settings.baseurl + "/ajax/addeletecomment/?do=delete&itemID=" + Haynn.itemDetail.itemID);
    console.log(Haynn.itemDetail.itemID);
};

Haynn.itemDetail.displaySuccessMessage = function () {
    $(document).ready(function () {
        if (window.location.href.indexOf("?new=1") > -1) {
            var html = 'Bạn đã đăng bài thành công. Sau khi được duyệt, bài sẽ xuất hiện ở trang <a href="' + Settings.baseurl + '/new">Bài mới</a> và cần được cộng đồng đánh giá trước khi xuất hiện ở <a href="' + Settings.baseurl + '">Trang chủ</a>.';
            html += '<img src="' + Settings.imgurl + '/close.png" class="close-icon" id="closeMessage"/>';
            $("#successfullyPosted").html('');
            $("#successfullyPosted").fadeIn(1000);
        }

        $("#closeMessage").click(function () {
            $("#successfullyPosted").animate({height: 0, opacity: 0}, function () {
                $(this).slideUp();
            });
        });
    });
};

Haynn.itemDetail.registerDeleteClick = function () {
    $("#btnDelete").click(function (e) {
        e.preventDefault();
        var confirmToDelete = $("#confirmToDelete");
        var oldContent = confirmToDelete.html();
        confirmToDelete.dialog({
            resizable: false,
            modal: true,
            height: 120,
            buttons: {
                "Chắc chắn": function () {
                    $(this).html("<center><img src='images/hnnloading.gif' alt='' /> Đang xóa</center>");
                    $.ajax({
                        url: Settings.baseurl + "ajax/deletepost.php?pid=" + Haynn.itemDetail.itemID,
                        context: $(this),
                        success: function (result) {
                            if (result == "OK") {
                                $(this).dialog("close");
                                Haynn.itemDetail.displayDeleteOKDialog();
                            }
                            if (result == "Fail") {
                                $(this).dialog("close");
                                displayMessageDialog("#unsuccessfullyDelete");
                            }
                        }
                    });

                },
                "Nhấn nhầm thôi": function () {
                    $(this).dialog("close");
                },
            },
            show: {
                effect: "fade",
                duration: 200
            },
            beforeClose: function () {
                confirmToDelete.html(oldContent);
            }
        });
    })
};

Haynn.itemDetail.registerReportClick = function () {
    $("#btnReport").click(function (e) {
        e.preventDefault();
        var reportForm = $("#reportPost");
        reportForm.dialog({
            resizable: false,
            modal: true,
            buttons: [
                {
                    text: "Báo cáo",
                    click: function () {
                        var reason = $("input:radio[name=reason]:checked").val();
                        var reasonDetail = $("input[name=reasonDetail]").val();
                        $.ajax({
                            type: 'POST',
                            url: Settings.baseurl + "/ajax/report",
                            data: {
                                itemID: Haynn.itemDetail.itemID,
                                reason: reason,
                                reasonDetail: reasonDetail
                            },
                            context: $(this),
                            success: function (result) {
                                if (result == "OK") {
                                    $(this).dialog("close");
                                    displayMessageDialog("#successfullyReported");
                                }
                                else {
                                    $(this).dialog("close");
                                    displayMessageDialog("#unsuccessfullyReported");
                                }
                            }
                        });
                    }
                },
                {
                    text: "Bỏ qua",
                    class: "cancelButton",
                    click: function () {
                        $(this).dialog("close")
                    }
                }],
            show: {
                effect: "fade",
                duration: 250
            },
            hide: {
                effect: "fade",
                duration: 250
            }
        });
    });

    $("input:radio[name='reason']").change(function () {
        if ($(this).attr("id") != "khac") {
            $("input:text[name='reasonDetail']").attr("disabled", "disabled");
        }
        else {
            $("input:text[name='reasonDetail']").removeAttr("disabled");
        }
    })

};

Haynn.itemDetail.displayDeleteOKDialog = function () {
    $("#successfullyDelete").dialog({
        resizable: false,
        modal: true,
        height: "auto",
        buttons: {
            "OK": function () {
                window.location.href = Settings.baseurl;
            }
        },
        show: {
            effect: "fade",
            duration: 200
        },
        dialogClass: 'no-close'
    });
};

Haynn.itemDetail.registerNavigationKeys = function () {
    $(document).keydown(function (e) {
        var url = false;
        // Left arrow key code
        if (e.which == 37) {
            url = $('#btnPrev_post').attr('href');
        }
        // Right arrow key code
        else if (e.which == 39) {
            url = $('#btnNext_post').attr('href');
        }

        if (url) {
            window.location = url;
        }
    });
};

Haynn.itemDetail.video = {
    postContent: "postContent",
    vmPlayer: "vmPlayer",
    ytPlayer: "ytPlayer",
    clipAnchor: "#clipAnchor"
};

Haynn.itemDetail.video.youTube = function () {
    var postContent = $("." + Haynn.itemDetail.video.postContent);
    var ytPlayer = $("#" + Haynn.itemDetail.video.ytPlayer);
    var cOriginalHeight = postContent.height();
    var cOriginalWidth = postContent.width();
    var yOriginalHeight = ytPlayer.height();
    var yOriginalWidth = ytPlayer.width();
    var clipAnchor = $(Haynn.itemDetail.video.clipAnchor).offset().top - 45;
    var player;


    window.onYouTubeIframeAPIReady = function () {
        player = new YT.Player(Haynn.itemDetail.video.ytPlayer, {
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    };

    function onPlayerReady(event) {
        //event.target.playVideo();
    };
    var done = false;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            Haynn.itemDetail.displayInfoPane = false;
            //$("#voteGroup").hide();
            $("#rightCol").animate({
                'margin-top': '640px'
            }, 500, function () {
                $(".navigation").animate({
                    width: "945px",
                }, 250);
                $(".contentWrap").animate({
                    width: "945px",
                }, 250);
                ytPlayer.animate({
                    width: '940px',
                    height: '530px'
                }, 250);
                player.setSize(940, 529);
                player.setPlaybackQuality('large');
                postContent.animate({
                    width: '1005px',
                    height: '562px',
                    "margin-left": "-25px"
                }, 250, function () {
                    /*$('html,body').animate({
                     scrollTop:clipAnchor
                     }, 500);*/
                    $(".fixedInfoPanel").hide();
                    Haynn.itemDetail.displayInfoPane = false;
                });
            });
        }
        if (event.data == YT.PlayerState.ENDED) {
            $("#voteGroup").show();
            ytPlayer.animate({
                width: yOriginalWidth,
                height: yOriginalHeight
            }, 250);
            $(".navigation").animate({
                width: cOriginalWidth - 50,
            }, 250);
            $(".contentWrap").animate({
                width: cOriginalWidth - 60,
            }, 250);
            postContent.animate({
                width: cOriginalWidth,
                height: cOriginalHeight,
                "margin-left": "0"
            }, 250, function () {
                $("#rightCol").animate({
                    'margin-top': '0'
                }, 500);
                /*$('html,body').animate({
                 scrollTop:0
                 }, 500);*/
                player.setSize(yOriginalWidth, yOriginalHeight);
            });
            Haynn.itemDetail.displayInfoPane = true;
        }
    };

};

Haynn.itemDetail.video.vimeo = function () {
    var postContent = $("." + Haynn.itemDetail.video.postContent);
    var vmPlayer = $("#" + Haynn.itemDetail.video.vmPlayer);
    var cOriginalHeight = postContent.height();
    var cOriginalWidth = postContent.width();
    var mOriginalHeight = vmPlayer.height();
    var mOriginalWidth = vmPlayer.width();
    var clipAnchor = $(Haynn.itemDetail.video.clipAnchor).offset().top - 45;

    var iframe = vmPlayer[0],
        player = $f(iframe);

    // When the player is ready, add listeners for pause, finish, and playProgress
    player.addEvent('ready', function () {
        player.addEvent('pause', onPause);
        player.addEvent('finish', onFinish);
        player.addEvent('playProgress', onPlayProgress);
        player.addEvent('play', onPlay);
    });

    function onPause(id) {
        vmPlayer.animate({
            width: mOriginalWidth,
            height: mOriginalHeight
        }, 350);
        $(".navigation").animate({
            width: cOriginalWidth - 50,
        }, 250);
        $(".contentWrap").animate({
            width: cOriginalWidth - 60,
        }, 250);
        postContent.animate({
            width: cOriginalWidth,
            height: cOriginalHeight,
            "margin-left": "0"
        }, 350, function () {
            $("#rightCol").animate({
                'margin-top': '0'
            }, 500);
            $('html,body').animate({
                scrollTop: 0
            }, 500);
            Haynn.itemDetail.displayInfoPane = true;
        });
    }

    function onFinish(id) {

    }

    function onPlayProgress(data, id) {

    }

    function onPlay(id) {
        $("#rightCol").animate({
            'margin-top': '640px'
        }, 500, function () {
            $(".navigation").animate({
                width: "945px",
            }, 250);
            $(".contentWrap").animate({
                width: "945px",
            }, 250);
            vmPlayer.animate({
                width: '940px',
                height: '529px'
            }, 300);
            //player.setPlaybackQuality('hd720');
            postContent.animate({
                width: '1005px',
                height: '562px',
                "margin-left": "-25px"
            }, 300, function () {
                $('html,body').animate({
                    scrollTop: clipAnchor
                }, 500);
                Haynn.itemDetail.displayInfoPane = false;
            });
        });
    }
};
Haynn.itemDetail.video.init = function (a) {
    if (a == "vimeo") Haynn.itemDetail.video.vimeo();
    else Haynn.itemDetail.video.youTube();
};
Haynn.settings = {};
Haynn.settings.checkAndSubmit = function () {
    var name = $("[name=fname]");
    var borderColor = name.css("border-color");
    var password = $("[name=current-password]");
    var newPassword = $("[name=new-password]");
    var verifyPassword = $("[name=verify-password]");
    //var error = false;
    verifyName = function () {
        var error;
        var nameError = $("#nameError");
        if (name.val() == "") {
            name.css("border-color", "red");
            nameError.show();
            error = true;
        }
        else {
            name.css("border-color", borderColor);
            nameError.hide();
            error = false;
        }
        return error;
    };

    verifyLength = function () {
        var isOK = true;
        if (newPassword.val() != "" && newPassword.val().length < 8) {
            $("#newPasswordLength").show();
            newPassword.css("border-color", "red");
            isOK = false;
        }
        else {
            isOK = true;
        }

        if (verifyPassword.val() != "" && verifyPassword.val().length < 8) {
            $("#verifyPasswordLength").show();
            verifyPassword.css("border-color", "red");
            isOK = false;
        }
        else {
            isOK = true;
        }

        return isOK;
    };

    verifyEmptyChange = function (error) {
        var isOK = true;
        $.each([newPassword, verifyPassword], function () {
            var thisAttr = $(this).attr("disabled");
            var isDisabled = false;
            if (thisAttr == "disabled") isDisabled = true;
            else isDisabled = false;

            if (isDisabled == false) {
                if ($(this).val() == "") {
                    $(this).css("border-color", "red");
                    isOK = false;
                }
                else {
                    isOK = true;
                }
            }

        });
        return isOK;
    };

    verifyEmptyCreate = function (error) {
        var isOK = true;
        if ((newPassword.val() != "" && verifyPassword.val() == "")) {
            verifyPassword.css("border-color", "red");
            isOK = false;
        }
        else {
            isOK = true;
        }

        if ((newPassword.val() == "" && verifyPassword.val() != "")) {
            newPassword.css("border-color", "red");
            isOK = false;
        }
        else {
            isOK = true;
        }
        return isOK;
    };

    verifyMatchPassword = function () {
        var isOK = true;
        var blankCurrentPassword = $("#blankCurrentPassword");
        if (newPassword.val() != "" && verifyPassword.val() != "") {
            if (newPassword.val() != verifyPassword.val()) {
                $("#passwordNotMatch").show();
                verifyPassword.css("border-color", "red");
                isOK = false;
            }
            else {
                isOK = true;
            }
        }
        return isOK;
    };

    $("#settings_submit").click(function () {
        $("#nameError, #blankCurrentPassword, #newPasswordLength, #passwordNotMatch, #verifyPasswordLength").hide();
        $("[name=new-password], [name=current-password], [name=verify-password]").css("border-color", borderColor);
        if ($("[name=createPassword]").length) {
            if (verifyEmptyCreate()) {
                if (verifyLength()) {
                    if (verifyMatchPassword()) ($("#form-settings").submit());
                }
            }
        }

        if ($("[name=changePassword]").length) {
            if (verifyEmptyChange()) {
                if (verifyLength()) {
                    if (verifyMatchPassword()) ($("#form-settings").submit());
                }
            }
        }
    });

};

Haynn.settings.uiSettingInit = function () {
    var btnSaveSetting = $("#btnSaveSetting");
    $("[id^='settingToggle']").click(function () {
        var id = $(this).attr("id");
        if ($(this).hasClass('off')) {
            $(this).removeClass('off');
            if (id == "settingTogglePhoto") $("input[name='enablePhoto']").attr("value", "1");
            if (id == "settingToggleMusic") $("input[name='enableMusic']").attr("value", "1");
            if (id == "settingToggleClip") $("input[name='enableClip']").attr("value", "1");
        }
        else {
            $(this).addClass('off');
            if (id == "settingTogglePhoto") $("input[name='enablePhoto']").attr("value", "0");
            if (id == "settingToggleMusic") $("input[name='enableMusic']").attr("value", "0");
            if (id == "settingToggleClip") $("input[name='enableClip']").attr("value", "0");
        }
        ;
        btnSaveSetting.html("LƯU");
    });
    $("[id^='themeSettingToggle']").click(function () {
        $(this).find(".switch").toggleClass('off');
        if ($("#1ColThemeSwitch").hasClass('off')) $("input[name='themeStyle']").attr("value", "2");
        if ($("#2ColThemeSwitch").hasClass('off')) $("input[name='themeStyle']").attr("value", "1");
        btnSaveSetting.html("LƯU");
    });
    $("[id^='colorSettingToggle']").click(function () {
        $(this).find(".switch").toggleClass('off');
        if ($("#blueThemeSwitch").hasClass('off')) {
            $("input[name='colorStyle']").attr("value", "black");
        }
        if ($("#blackThemeSwitch").hasClass('off')) {
            $("input[name='colorStyle']").attr("value", "blue");
        }
        btnSaveSetting.html("LƯU");
    });
    btnSaveSetting.click(function () {
        var photoValue = $("input[name='enablePhoto']").attr("value");
        var clipValue = $("input[name='enableClip']").attr("value");
        var musicValue = $("input[name='enableMusic']").attr("value");
        var themeValue = $("input[name='themeStyle']").attr("value");
        var colorValue = $("input[name='colorStyle']").attr("value");
        $.ajax({
            url: Settings.baseurl + '/user/ajaxsavesettings',
            type: 'POST',
            dataType: 'default',
            beforeSend: function () {
                btnSaveSetting.html("Loading...")
            },
            data: {
                "enablePhoto": photoValue,
                "enableClip": clipValue,
                "enableMusic": musicValue,
                "theme": themeValue,
                "color": colorValue
            },
            complete: function (data, textStatus, xhr) {
                if (data == "OK") alert("abc");
                btnSaveSetting.html("ĐÃ LƯU");
            }
        })
    });

    $("#resetSetting").click(function (event) {
        event.preventDefault();
        $("[id^='settingToggle']").removeClass('off');
        $("[name='enablePhoto'], [name='enableMusic'], [name='enableClip'], [name='themeStyle']").attr("value", "1");
        $("#1ColThemeSwitch").removeClass('off');
        $("#2ColThemeSwitch").addClass('off');
        $("#blueThemeSwitch").removeClass('off');
        $("#blackThemeSwitch").addClass('off');
        btnSaveSetting.html("LƯU");
    });
};

Haynn.settings.toggle = function () {
    var borderColor = $("[name=fname]").css("border-color");
    $("[name=current-password]").keyup(function () {
        if ($(this).val() == "") {
            $("[name=new-password]").attr("disabled", "disabled").val("").css("border-color", borderColor);
            $("[name=verify-password]").attr("disabled", "disabled").val("").css("border-color", borderColor);
            $("#passwordNotMatch").hide()
        }
        else {
            $("[name=new-password]").removeAttr("disabled");
            $("[name=verify-password]").removeAttr("disabled");
        }
    });
};

Haynn.settings.init = function () {
    Haynn.settings.checkAndSubmit();
    Haynn.settings.toggle();
    Haynn.settings.uiSettingInit();
};

Haynn.userProfile = {
    userID: null,
    page: 1,
    isLoading: false,
    hasNoMore: false,
    ajaxLoadEnable: false
};

Haynn.userProfile.commentNotification = function (userID, profileID, response) {
    $.ajax({
        url: Settings.baseurl + '/ajax/profilecommented',
        type: 'POST',
        data: {
            "do": 'comment',
            "profileID": profileID,
            "userID": userID,
            "response": response
        }
    });
};
Haynn.userProfile.init = function (a, b, c) {
    Haynn.userProfile.userID = a;
    var viewerID = b;
    Haynn.userProfile.ajaxLoadEnable = c;
    window.fbAsyncInit = function () {
        FB.Event.subscribe('comment.create', function (response) {
            Haynn.userProfile.commentNotification(viewerID, Haynn.userProfile.userID, response);
        });
    };
    if (Haynn.userProfile.ajaxLoadEnable) {
        $(window).scroll(function () {
            Haynn.userProfile.loadMorePost();
        });
    }
    Haynn.userProfile.recount();
};
Haynn.userProfile.loadMorePost = function () {
    if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 200)) {
        if (Haynn.userProfile.isLoading || Haynn.userProfile.hasNoMore) return;
        Haynn.userProfile.isLoading = true;
        $('#load_image').show();
        $.get(Settings.baseurl + "/ajax/profile/loadmore/" + (Haynn.userProfile.page + 1) + "/" + Haynn.userProfile.userID, function (data) {
            if (data == "NoMorePostToShow") {
                Haynn.userProfile.hasNoMore = true;
                Haynn.userProfile.showLoadMore = false;
                $('#load_image').hide();
                $("#noMorePost").show();
            }
            else {
                var contentToCheck = $("<div>" + data + "</div>");
                $(".postWrap", contentToCheck).each(function () {
                    if ($(".postWrap[id=" + $(this).attr("id") + "]").size() > 0) {
                        $(this).remove();
                    }
                });
                var dataToInsert = $(contentToCheck.html());
                $("#listEnd").before(dataToInsert);
                $('#load_image').hide();
                Haynn.userProfile.isLoading = false;
                Haynn.userProfile.page++;
            }
        });
    }
};
Haynn.userProfile.recount = function () {
    $.post(Settings.baseurl + "/ajax/ajxupu/id/" + Haynn.userProfile.userID);
};

Haynn.Unread =
{
    justRead: {},
    submitted: {},
    isLoading: false,
    hasNoMore: false,
    page: 0,
    initPage: "unread",
    shouldTrack: false,
    showLoadMore: true,
    theme: 1
};
Haynn.LocalStorage = {
    Keys: {
        ReadPhotos: "ReadPhotos"
    }
};
Haynn.LocalStorage.isSupported = function () {
    try {
        localStorage.setItem("isSupported", true);
        localStorage.removeItem("isSupported");
        return true
    }
    catch (e) {
        return false
    }
};
Haynn.Unread.addReadPhoto = function (a) {
    if (!Haynn.Unread.shouldTrack) return;
    if (!Haynn.Unread.justRead[a]) {
        Haynn.Unread.justRead[a] = true;
    }
};
Haynn.Unread.getJustReadPhotoIds = function () {
    var justReadPhotoIds = "";
    for (var postID in Haynn.Unread.justRead) {
        if (justReadPhotoIds == "") justReadPhotoIds += postID;
        else justReadPhotoIds += "," + postID
    }
    return justReadPhotoIds;
};
Haynn.Unread.submit = function (a) {
    var b = [];
    var idToSubmit = "";
    for (var postID in Haynn.Unread.justRead) {
        if (!Haynn.Unread.submitted[postID]) {
            Haynn.Unread.submitted[postID] = true;
            b.push(postID);
            if (idToSubmit == "") idToSubmit += postID;
            else idToSubmit += "," + postID
        }
    }
    if (idToSubmit == "") return;
    $.ajax(
        {
            url: Settings.baseurl + "ajax/misc.php?do=markread",
            dataType: "html",
            type: "POST",
            async: a,
            data: {
                postIDs: idToSubmit
            },
            success: function () {
            },
            error: function () {
                for (var i in b) delete Haynn.Unread.submitted[i]
            },
            complete: function () {
            }
        })
};
Haynn.Unread.startTracking = function () {
    $(document).ready(function () {
        Haynn.Unread.shouldTrack = true;
        setInterval(function () {
            Haynn.Unread.submit(true)
        }, 30000);
        window.onbeforeunload = function () {
            Haynn.Unread.submit(false)
        }
    })
};
Haynn.Unread.init = function () {
    $(document).ready(function () {
        Haynn.Unread.startTracking();
        Haynn.Unread.loadSinglePage(true);
        $(window).scroll(function () {
            Haynn.Unread.loadMore();
        })
    })
};
Haynn.Unread.loadSinglePage = function (isFirstLoad) {
    Haynn.Unread.showLoadMore = false;
    if (Haynn.Unread.isLoading || Haynn.Unread.hasNoMore) return;
    Haynn.Unread.isLoading = true;
    $('#load_image').show();
    $.post(
        Settings.baseurl + "ajax/unread.php",
        {
            excludePhotoIds: Haynn.Unread.getJustReadPhotoIds(),
            isFirstLoad: isFirstLoad
        },
        function (data) {
            if (data == "NoMoreUnread") {
                $("#btnLoadMoreContainer").css("height", "");
                Haynn.Unread.hasNoMore = true;
                Haynn.Unread.showLoadMore = false;
                $('#load_image').hide();
                $("#noMorePost").show();
            }
            else {
                var contentToCheck = $("<div>" + data + "</div>");
                $(".pin-container", contentToCheck).each(function () {
                    if ($(".pin-container[data-id=" + $(this).attr("data-id") + "]").size() > 0) {
                        $(this).remove();
                    }
                });
                var dataToInsert = $(contentToCheck.html());
                if (Haynn.indexPage.theme == 1) {
                    $("#listEnd").before(dataToInsert);
                    $('#load_image').hide();
                    Haynn.Unread.isLoading = false;
                    Haynn.Unread.page++;
                    Haynn.Unread.showLoadMore = true;
                    $("#btnLoadMoreContainer").css("height", "");
                    dataToInsert.find("[id|=likebutton]").each(function () {
                        FB.XFBML.parse($(this)[0]);
                    });
                }
                else {
                    var container = $('#post-container');
                    container.append(dataToInsert);
                    container.isotope('appended', dataToInsert);
                    dataToInsert.imagesLoaded(function () {
                        //Haynn.indexPage.setExpand();
                        Haynn.indexPage.reLayout();
                        $('#load_image').hide();
                        Haynn.Unread.isLoading = false;
                        Haynn.Unread.page++;
                        Haynn.Unread.showLoadMore = true;
                        $("#btnLoadMoreContainer").css("height", "");
                        dataToInsert.find("[id|=likebutton]").each(function () {
                            FB.XFBML.parse($(this)[0]);
                        });
                    });
                }
            }
        });
};
Haynn.Unread.loadMore = function () {
    if ($(window).scrollTop() + $(window).height() >= ($(document).height() - 400)) {
        if ((Haynn.Unread.page % 4) === 0) {
            if (Haynn.Unread.showLoadMore === true) {
                $("#btnLoadMore").show();
                Haynn.Unread.showLoadMore = false;
            }
        }
        else {
            Haynn.Unread.loadSinglePage(false);
        }
    }
};

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var players = {};

$(document).ready(function () {
    Haynn.Following.init();
    //Haynn.common.fixedHeader();
});

function displayMessageDialog(dialogID) {
    $(dialogID).dialog({
        show: {
            effect: 'fade',
            duration: 250
        },
        hide: {
            effect: 'fade',
            duration: 250
        },
        resizeable: false,
        modal: true,
        height: 140,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}

function displayLoginIsRequiredDialog(returnURL) {
    var loginRequired = $("#loginIsRequired");
    loginRequired.dialog({
        resizable: false,
        modal: true,
        height: 200,
        buttons: [
            {
                text: "Đăng nhập",
                click: function () {
                    if (typeof returnURL === "undefined") {
                        var url = document.URL;
                        window.location.href = Settings.baseurl + "/user/login?url=/" + url.replace(Settings.baseurl, "");
                    }
                    else {
                        url = returnURL;
                        window.location.href = Settings.baseurl + "/user/login?url=/" + url;
                    }

                }
            },
            {
                text: "Bỏ qua",
                class: "cancelButton",
                click: function () {
                    $(this).dialog("close");
                    changeVoteButtonState(btn);
                }
            }],
        show: {
            effect: "fade",
            duration: 200
        },
        hide: {
            effect: "fade",
            duration: 200
        }
    });
}

$.fn.scrollTo = function (target, options, callback) {
    if (typeof options == 'function' && arguments.length == 2) {
        callback = options;
        options = target;
    }
    var settings = $.extend({
        scrollTarget: target,
        offsetTop: 50,
        duration: 500,
        easing: 'swing'
    }, options);
    return this.each(function () {
        var scrollPane = $(this);
        var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
        var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
        scrollPane.animate({scrollTop: scrollY}, parseInt(settings.duration), settings.easing, function () {
            if (typeof callback == 'function') {
                callback.call(this);
            }
        });
    });
};

$.fn.shuffle = function () {
    var allElems = this.get(),
        getRandom = function (max) {
            return Math.floor(Math.random() * max);
        },
        shuffled = $.map(allElems, function () {
            var random = getRandom(allElems.length),
                randEl = $(allElems[random]).clone(true)[0];
            allElems.splice(random, 1);
            return randEl;
        });

    this.each(function (i) {
        $(this).replaceWith($(shuffled[i]));
    });

    return $(shuffled);
};

Haynn.render = {};
Haynn.render.emotions = function () {
    a = document.getElementById('post-container');
    if (a) {
        b = a.getElementsByClassName("field-items");
        for (i = 0; i < b.length; i++) {
            c = b.item(i).innerHTML.replace(/:\)\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/21.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:yaoming:/gi, "<img src='http://i1247.photobucket.com/albums/gg621/jonyht/troll/100.jpg' alt='' class='w2bsmly'/>");
            c = c.replace(/:dapxe:/gi, "<img src='http://img.qqtouxiangzq.com/6/985/19.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:facepalm:/gi, "<img src='http://images2.wikia.nocookie.net/__cb20111107003558/runescape/images/1/1f/Emoticon-Facepalm.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:dapdau:/gi, "<img src='http://kawaii.ucoz.net/newemo/tusat.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\)\)\)\)/gi, "<img src='http://smiles.vinaget.us/0_002.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:mon:/gi, "<img src='https://lh4.googleusercontent.com/-nlahvjhNM-w/Ua_cnWwShsI/AAAAAAAAAaI/e4R86lUkPPE/s100-no/cats.jpg' alt='' class='w2bsmly'/>");
            c = c.replace(/:neptune:/gi, "<img src='http://lh3.googleusercontent.com/-OUgtWM8KIbE/UamX2xApasI/AAAAAAAAAYo/tEx_yCIyVv8/s506-o/brick.png' alt='' class='w2bsmly'/>");
            c = c.replace(/tem/gi, "<img src='https://lh6.googleusercontent.com/-4s1_D0LpDF8/UcqTphNrnPI/AAAAAAAAAbw/Riw6WKC-a9A/w150-h67-no/tem..gif' alt='' class='w2bsmly'/>");
            c = c.replace(/O:-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/25.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-bd/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/113.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/7:\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/19.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/2\):\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/48.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\)\]/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/100.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\(\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/20.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/2.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\;\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/3.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:D/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/4.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\;\;-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/5.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/7:\P/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/47.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/~\X\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/102.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-\//gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/7.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\/:\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/23.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:x/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/8.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\P/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/10.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-\*/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/11.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\(\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/12.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-SS/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/42.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-\O/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/13.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\X\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/14.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/B-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/16.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\#:-\S/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/18.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-S/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/17.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:7/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/15.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/1.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\(:\|/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/37.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\|/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/22.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\)\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/24.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-B/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/26.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\;/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/27.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-c/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/101.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-h/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/103.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-t/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/104.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/8-7/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/105.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\I-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/28.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/8-\|/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/29.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\L-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/29.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-a/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/31.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-\$/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/32.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\[-\(/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/33.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\O\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/34.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/8-\}/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/35.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/2:-\P/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/36.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\P~/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/38.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-\?/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/39.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/#-o/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/40.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/=\D7/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/41.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\@-\)/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/43.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\^o/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/44.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-w/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/45.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\X\_\X/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/109.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:\!\!/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/110.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\\m\//gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/111.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:-q/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/112.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/\^\#\(\^/gi, "<img src='http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/114.gif' alt='' class='w2bsmly'/>");
            c = c.replace(/:ar\!/ig, "<img src='http://l.yimg.com/a/i/us/msg/emoticons/pirate_2.gif' alt='' class='w2bsmly'/>");
            b.item(i).innerHTML = c;
        }
    }
};

