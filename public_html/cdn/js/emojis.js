$.emojiarea.path = 'http://st.test.haynhucnhoi.local/images/emoticons/basic/';
$.emojiarea.icons = {
    ':smile:': 'smile.png',
    ':laughing:': 'laughing.png',
    ':trollface:': 'trollface.png',
    ':scream:': 'scream.png',
    ':heart:': 'heart.png',
    ':cry:': 'cry.png',
    ':sexy:': 'sexy.png',
    ':sogood:': 'sogood.png',
    ':beauty:': 'beauty.png',
    ':gach:': 'gach.png'
};