var znn = new function ()
{
	var totalNoti5Main, divNotifi, btnNoti5;
	var countUnread = 0;
	var firstTimeShowed = false;
	var nCounter = 10;
	var gettingMorePopup = false;
	var scrollableUi = null;
	var scrollTimeout, scrolling = false;
	var updateUnread = function (unread)
	{
		countUnread = unread;
		var title = document.title;
		var match = title.match(/^\((\d+\+?)\) /);
		if (match && match.length > 0)
		{
			title = title.substring(match[0].length - 1, title.length);
		}
		if (countUnread == 0)
		{
			totalNoti5Main.html("").hide();
			document.title = title;
		}
		else
		{
			var t = countUnread > 99 ? "99+" : countUnread;
			totalNoti5Main.html(t).show();
			document.title = "(" + t + ") " + title;
		}
	}
	var init = function ()
	{
		totalNoti5Main = zm("#totalNoti5Main");
		divNotifi = zm("#divNotifi");
		btnNoti5 = zm("#btnNoti5");
		btnNoti5.click(showPopup);
		zm("html").click(function (e)
			{
				divNotifi.hide();
			});
		divNotifi.click(function (e)
			{
				e.stopPropagation();
			});
		zm.post("/noti5/ajax/counter",
			{
				"f": "get",
				"ownerId": ziniConfig.ownerId,
				"viewerId": ziniConfig.viewerId,
				"timestamp": ziniConfig.timestamp,
				"zajaxtoken": ziniConfig.zajaxtoken
			},
			{
				"dataType": "json"
			}, function (data)
			{
				if (data.error >= 0)
				{
					if (data.data.main != countUnread)
					{
						updateUnread(data.data.main);
					}
				}
			});
	};
	
	var resetCounter = function (type)
	{
		if (type != "mention" && type != "main")
		{
			return;
		}
		var redirect = function (type)
		{
			if (type == "mention")
			{
				window.location.href = "/?tweet=mention";
			}
		}
		if (type == "main" && countUnread == 0)
		{
			return;
		}
		else if (type == "metion" && countMention == 0)
		{
			redirect(type);
		}
		zm.post("/noti5/ajax/counter",
			{
				"f": "reset",
				"type": type,
				"ownerId": ziniConfig.ownerId,
				"viewerId": ziniConfig.viewerId,
				"timestamp": ziniConfig.timestamp,
				"zajaxtoken": ziniConfig.zajaxtoken
			},
			{
				"dataType": "json"
			}, function (data)
			{
				redirect(type);
			}, function ()
			{
				redirect(type);
			});
	}
	this.resetCounter = resetCounter;
	var updatePopupContent = function (content)
	{
		if (scrollableUi == null)
		{
			scrollableUi = new zm.ScrollableUI(
				{
					content: '',
					width: 350,
					uiClass: "",
					height: 350,
					onscroll: function (ui, event, isBottom)
					{
						scrolling = true;
						if (scrollTimeout)
						{
							clearTimeout(scrollTimeout);
						}
						if (isBottom && nCounter <= 30)
						{
							showMorePopup();
						}
						scrollTimeout = setTimeout(function ()
							{
								scrolling = false;
							}, 200);
					}
				});
		}
		nCounter = 10;
		gettingMorePopup = false;
		scrollableUi.scrollToTop();
		scrollableUi.setContent(content);
		zm("#notifiContent .blank-notif").remove();
		zm("#notifiContent").append(scrollableUi.getElement());
	}
	var updateTimestamp = function (ids)
	{
		if (typeof ids == "undefined" || ids == null || ids.length == 0)
		{
			return;
		}
		var timeIds = [];
		for (var i = 0; i < ids.length; i++)
		{
			timeIds.push("ntt_" + ids[i]);
		}
		zmDateTime.add(timeIds, true);
	}
	var showPopup = function (e)
	{
		e.stopPropagation();
		if (divNotifi.is(":visible"))
		{
			divNotifi.hide();
		}
		else
		{
			divNotifi.show();
			if (countUnread > 0 || !firstTimeShowed)
			{
				var viewAll = zm("#divNotifi .view-all");
				var onEmpty = function ()
				{
					zm("#notifiContent").html('<li class="notif-item blank-notif"><i class="icon icon-blank-notif"></i>Hiện tại bạn chưa có thông báo nào</li>');
					viewAll.hide();
				}
				var onError = function ()
				{
					zm("#divLoadMoreNotifi").remove();
					zm("#notifiContent").html('<li class="notif-item blank-notif no-data"><i class="icon icon-face-dead-xxl"></i>Có lỗi xảy ra</li>');
					viewAll.hide();
				}
				zm.post("/noti5/ajax/popup",
					{
						"ownerId": ziniConfig.ownerId,
						"viewerId": ziniConfig.viewerId,
						"timestamp": ziniConfig.timestamp,
						"zajaxtoken": ziniConfig.zajaxtoken
					},
					{
						"dataType": "json"
					}, function (resp)
					{
						zm("#divLoadMoreNotifi").remove();
						if (resp.error >= 0)
						{
							if (resp.data.length == 0)
							{
								onEmpty();
							}
							else
							{
								//zm("#notifiContent").html(resp.data.data);
								viewAll.css('display', 'block');
								updatePopupContent(resp.data.data);
								updateTimestamp(resp.data.notifyIds);
							}
							resetCounter("main");
							updateUnread(0);
						}
						else if (resp.error == -10)
						{
							onEmpty();
						}
						else
						{
							onError();
						}
						firstTimeShowed = true;
					}, function ()
					{
						onError();
						firstTimeShowed = true;
					});
			}
		}
		return false;
	}
	this.showPopup = showPopup;
	var showMorePopup = function ()
	{
		if (gettingMorePopup)
		{
			return;
		}
		gettingMorePopup = true;
		zm.post("/noti5/ajax/popupmore",
			{
				"nCounter": nCounter,
				"ownerId": ziniConfig.ownerId,
				"viewerId": ziniConfig.viewerId,
				"timestamp": ziniConfig.timestamp,
				"zajaxtoken": ziniConfig.zajaxtoken
			},
			{
				"dataType": "json"
			}, function (resp)
			{
				if (resp.error >= 0)
				{
					nCounter += 10;
					var data = resp.data;
					zm("#notifiContent .zmscrollableui_content .last").remove();
					zm("#notifiContent .zmscrollableui_content").append(data.data);
					if (nCounter > 30)
					{
						zm("#notifiContent .zmscrollableui_content .last").remove();
					}
					updateTimestamp(data.notifyIds);
				}
				gettingMorePopup = false;
			}, function ()
			{
				gettingMorePopup = false;
			});
	}
	this.showMoreDetail = function (callback)
	{
		var divContent = zm(".main-content");
		divContent.show();
		zm.post("/noti5/ajax/detailmore",
			{
				"ownerId": ziniConfig.ownerId,
				"nDCounter": nDCounter,
				"viewerId": ziniConfig.viewerId,
				"timestamp": ziniConfig.timestamp,
				"zajaxtoken": ziniConfig.zajaxtoken
			},
			{
				"dataType": "json"
			}, function (resp)
			{
				if (resp.error >= 0)
				{
					nDCounter += 10;
					var data = resp.data;
					if (data.data.length == 0)
					{
						zm(".btn.btn-showmore").remove();
					}
					else
					{
						zm(".btn.btn-showmore").remove();
						zm(".main-content").append(data.data);
					}
				}
				callback && callback();
			}, function ()
			{
				zm(".main-content").html("Co loi xay ra");
			});
		return false;
	}
	var notify = function ()
	{
		zm.post("/notify",
			{
				"ownerId": ziniConfig.ownerId,
				"viewerId": ziniConfig.viewerId,
				"timestamp": ziniConfig.timestamp,
				"zajaxtoken": ziniConfig.zajaxtoken
			},
			{
				"dataType": "json"
			}, function (data)
			{
				if (data.error < 0 || data.data.length == 0)
				{
					notify();
					return;
				}
				var jso = JSON.parse(data.data);
				if (jso.msgType == 1)
				{ // increase counter main
					updateUnread(countUnread + 1);
				}
				else if (jso.msgType == 2)
				{ // increase counter main + mention
					updateUnread(countUnread + 1);
				}
				else if (jso.msgType == 3)
				{
					if (zm("#tweet-stream-0").hasClass("active"))
					{
						if (zm(".notify.noti-info.noti-newtweet").is(":visible"))
						{
							zm(".num-newfeed").html(parseInt(zm(".num-newfeed").html()) + 1);
						}
						else
						{
							zm(".notify.noti-info.noti-newtweet").show();
						}
					}
					else if (zm("#tweet-stream-" + jso.tweetType).hasClass("active"))
					{
						if (jso.tweetType == '1')
						{
							if (zm(".notify.noti-info.noti-newtweet").is(":visible"))
							{
								zm(".num-newfeed").html(parseInt(zm(".num-newfeed").html()) + 1);
							}
							else
							{
								zm(".notify.noti-info.noti-newtweet").show();
							}
						}
					}
				}
				else if (jso.msgType == 4)
				{
					if (jso.type == "main")
					{
						updateUnread(0);
					}
				}
				notify();
			}, function ()
			{
				notify();
			});
		return false;
	}
	this.notify = notify;
	zm.ready(function ()
		{
			if (ziniConfig.viewerId > 0)
			{
				init();
				notify();
			}
		});
};