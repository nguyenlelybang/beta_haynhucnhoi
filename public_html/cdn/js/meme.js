﻿function Meme(memeID, u, g, k)
{
    var l = 500,
        width, height, scale, memeId = memeID,
        buildUrl = k,
        url = u,
        imageUrl = g,
        canvas, topText, bottomText, isBusy = false,
        outputData, watermark = null,
        watermarkData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAAANCAYAAAD/qmFtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo5QTBFMkVERkYzNEUxMUUyQjY0Q0NBRDU3Q0QyNDM1OCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo2NkI1QjYyRUUyNEQxMUU0QjU2NEIwODc0MkMyOUZGRSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo2NkI1QjYyREUyNEQxMUU0QjU2NEIwODc0MkMyOUZGRSIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjIyRUI3MzhGNzU3NjExRTQ5RDcwOEU3MDZBRDZEQjI2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjIyRUI3MzkwNzU3NjExRTQ5RDcwOEU3MDZBRDZEQjI2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+VznSqgAABV9JREFUeNrsWU1IXFcUnqQqcVzImIyIxB+0igOOLiaiG6XqIqPoQt2IZKek2ShWsGLqQkUXWoxFaaAtumlxZ2ahi1Ewgm78G8GxdBJHReMPxvFnXDiKCu39JKdcbt+8eTO+pI2dA4d59+ecc+/5f2/uaDSapxoVoK6u7kF+fn7C0dGRp7S09HF0dPQPmiAE4T8OIWowMZvN2paWlicnJycHZ2dnnqDzB+FzgS8Ymrxl9OzsbO3s7KyHxjqdTrO6unpJY1orLi6OnJqass/Nza0uLi5u8vsoQLCH9ov8SBahOC/S8Wfi6fh9Uuvi+cVzSs1JyfV1H3E/P5bb52vNl3yl9lJyVlGfUjLV6hxuYm9ftqB1OT95yiMjqD89PXXt7+//gV+73W7FPMYTExO/0r4/GdAz1txu9wZ+gQMDA708T9BhPzvYc5q7vLw8dTqdU3L0dAaisVgsP4EPreOZaIC8TMgivpAlnh9zdDf+HnQmEfn7go8oT+SHdRp3dXV1Yp3meVpRj950zOuBly9nr0Dk4P48P14mPyYcHR0dOD8/c0vhy5c/fk/73r/f+x17RXrwhH4Ctbcv38I62Z+3CeFdMSK7u7vNrI/fRBtTUVHRaTAYcpVEss1mmwYNsLq62iGuX11deaqqqq6rDbuQISQkRKuEPiwsTIsoxnN6erpB5Es0YtvV19d3gDnwPT4+vr5PYWHhNMm/uLjwxMfHp6mRxaT4jYyMjJHu2L0fb2xs2GhNr9cbQKOGbDl7+SsHmTMxMdEUERHxHfiBL3OYcjmakpKS2Xv3wr8dHBz8BWM8A7e2tuyPHmWlYO7Zs6/vR0ZGPnzxoscuysNvU1PT5k3s7cu3yP7AjIyMV/zaPwIgPDxce35+fl0irFarJzQ09Bs1DIVDJCUlXV8oKyvL4HK5HErpGhsbTR8MGq+UTg7y8vJMy8vLU8zQelK22vwQxMhmLAPlxsTEGGpra/9W/Pb2to05a5kaepWzl79yMjMz9QhkGoMv+Adyrp2dne2UlC+NeK6srDQiIF6/njzj98BpEWQ3tXegviUZAHKQk5NjZkbtBEo4QTkrNb1Ab/SIbvwiU66vrzuU0O/t7b1LTU01eHNUomEKVvw1KzY21jA0NGRjvB2kbCVAd4celPCbnJwcKygoeIJ5OCfNr6ysOChb+aNjf+FTyZGC9vb2WRY899va2h4mJyenvH274hT3pKWlmRYWFmxq2FvOt1AJiQ4Jya8AoDIFmJmZsaI8AsV97CX4FbKPXMVgkfmODsAU71FCz2hcUVFRCa2trblv3ryxiTyJJi4u7melZV6r1T7AVyvwNRqNigOA7g49KOFXVlZm83g8B+Pj49Mir+HhYYtUdpbTsdL7fQo5vgDZnpnOWVRUlMEcMEVsfz7oUw8dqWFvOd9CRSA6aoO9BsDh4aGL9WvXSmQvJwk9PT2qKYg5uQ2ZXupicrC7u+swmUxmppTpm56hpqYmD61BR0fHb83Nzb06nS7h3+An9Z4kBwgyyoosyyXgk7MSe/kjZ2lpyUWZFAC+4B+obtbW1pwZGcavEAhi+wNnRWVUy96B+tZdidI4hsh0u93PGxoa6pGZFfbB5d7KjGgMtAv+0ONy6E35NkIsiXKtFw8okSi7eEkGok+3WCymQI0sxc/b/aWqgFI57B1jjL1Yd8IuaLn6+/utSu2lVA7Oj5d16BL8EAzgL7V3f3+/XkkbRIEgrqHNoRZNypn9tbecb/EtkNg63dF4+ScY2UbqALcF4PTMAAf8F4jPAbzZRW17fWz7wxGVtq0fE7wGwG0HfN6bn593+NuKBOF2wf82AIIQBMBfAgwAEGGWKQ/0wVEAAAAASUVORK5CYII=",
        dataWithoutWatermark = null,
        initialTopText, initialBottomText,
        getImage = function (b)
        {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: {
                    imageUrl: imageUrl
                },
                success: function (a) {
                    b(a.image)
                },
                fail: function () {
                    alert('Lỗi: Tải ảnh meme thất bại, bạn vui lòng F5 để thử lại')
                }
            })
        },
        getTextObj = function (a, y)
        {
            return new fabric.Text('',
                {
                    originY: a,
                    left: width / 2,
                    top: y,
                    fill: 'white',
                    stroke: 'black',
                    strokeWidth: 1,
                    lineHeight: 1.3,
                    fontFamily: 'Tahoma, Arial, sans-serif',
                    fontWeight: 700,
                    textAlign: 'center',
                    shadow: 'rgba(0,0,0,0) 0 0 0 ',
                    hasControls: false
                })
        },
        getFontSize = function (a)
        {
            var b = 0;
            var h = height;
            if (h < 350) b = -4;
            else if (h < 500) b = -2;
            var c = 32;
            if (a.length <= 50)
            {
                c = 38
            }
            else if (a.length <= 80)
            {
                c = 36
            }
            return c + b
        },
        updateText = function (a, b)
        {
            a.set('fontSize', getFontSize(b));
            a.set('left', width / 2);
            if (a === topText)
            {
                a.set('top', 10)
            }
            else
            {
                a.top = height - 12
            }
            a.set('text', '');
            var c = fabric.util.object.clone(a);
            var d = b.split(" ");
            var e = '';
            var f = "";
            for (var j = 0; j < d.length; j++)
            {
                c.set('text', f + ' ' + d[j]);
                if (c.width < width)
                {
                    f += d[j] + ' ';
                    e += d[j] + ' '
                }
                else
                {
                    e = e.substring(0, e.length - 1);
                    e += '\n' + d[j] + ' ';
                    f = d[j] + ' '
                }
            }
            a.set('text', e);
            canvas.renderAll()
        },
        updateAllText = function () {
            updateText(topText, $('#topText').val());
            updateText(bottomText, $('#bottomText').val())
        },
        handleEvents = function () {
            $('#topText,#bottomText').bind('keyup', function () {
                updateAllText()
            });
            $('#publish').bind('click', function () {
                publish()
            });
            $('#create').bind('click', function () {
                if (!confirm("Bạn đã xong thật chưa? Nếu xác nhận bạn sẽ không thể tiếp tục sửa ảnh này. Sau đó bạn có thể đăng lên Hay Nhức Nhói hoặc lưu ảnh về máy.")) return;
                if (isBusy)
                {
                    return
                }
                isBusy = true;
                dataWithoutWatermark = canvas.toDataURL();
                addWatermark(function ()
                {
                    var a = canvas.toDataURL(
                        {
                            format: 'jpeg'
                        });
                    isBusy = false;
                    preview(a)
                })
            })
        },
        addWatermark = function (a) {
            canvas.deactivateAll().renderAll();
            if (watermark == null) {
                var b = new Image();
                b.onload = function ()
                {
                    watermark = new fabric.Image(b,
                        {
                            left: 0 + b.width / 2,
                            top: height - b.height / 2
                        });
                    canvas.add(watermark);
                    a();
                };
                b.src = watermarkData;
            }
            else
            {
                a()
            }
        },
        preview = function (a) {
            outputData = a.split(',', 2)[1];
            $('#meme-builder').css('display', 'none');
            $('#meme-preview').css('display', 'block');
            $('#meme-output')[0].src = a;
            $('#title').focus()
        },
        publish = function () {
            var b = $('#title').val();
            if (b == null || b.trim().length == 0) {
                $('#titlememe').text('Đặt tiêu đề cho ảnh đi đã chứ =.=');
                displayMessageDialog('#titleMemeAlert');
                return false;
            }
            if(b.match(/([\<])([^\>]{1,})*([\>])/i)!=null) {
                $('#titlememe').text('Tiêu đề không chứa các script!');
                displayMessageDialog('#titleMemeAlert');
                return false;
            }

            if (isBusy) {
                return false;
            }
            isBusy = true;
            dataWithoutWatermarkAfterSplit = dataWithoutWatermark.split(',',2)[1];
            $.ajax({
                url: buildUrl,
                type: 'POST',
                dataType: 'json',
                data: {
                    title: b,
                    imageData: dataWithoutWatermarkAfterSplit,
                    memeId: memeId,
                    post_type: "Photo",
                    isSelfMade: 1
                },
                success: function (a) {
                    if (a.Success) {
                        window.onbeforeunload = null;
                        top.location.replace(a.Redirect)
                    }
                    else {
                        alert(a.Message)
                    }
                },
                error: function (a) {
                    $('#titlememe').text(a.responseText);
                    displayMessageDialog('#titleMemeAlert');
                },
                complete: function () {
                    isBusy = false;
                }
            })
        },
        drawCanvas = function (a)
        {
            canvas = new fabric.Canvas('canvas',
                {
                    backgroundImage: a.src,
                    backgroundImageStretch: true,
                    selection: false
                });
            canvas.setDimensions(
                {
                    width: width,
                    height: height
                });
            $('#canvas').css('display', 'block');
            topText = getTextObj('top', 0);
            bottomText = getTextObj('bottom', height - 22);
            canvas.add(topText);
            canvas.add(bottomText);
            updateAllText();
            handleEvents()
        };
    return {
        initialize: function ()
        {
            initialTopText = $('#topText').val();
            initialBottomText = $('#bottomText').val();
            getImage(function (a)
            {
                var b = new Image();
                b.onload = function ()
                {
                    width = b.width;
                    height = b.height;
                    scale = 1;
                    if (width > l)
                    {
                        height = Math.round(l * height / width);
                        scale = l / width;
                        width = l
                    }
                    drawCanvas(b)
                };
                b.src = a
            });
            return this;
        },
        changed: function ()
        {
            return $('#topText').val() !== initialTopText || $('#bottomText').val() !== initialBottomText
        }
    }
};