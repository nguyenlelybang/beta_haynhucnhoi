var ImageUpload = {
    boxy: undefined
};

ImageUpload.showPopup = function()
{
    var html = '<div id="popup" style="width:400px;margin:0 auto">' +
    '<div class="title">' +
    '<div class="l_title"></div>' +
    '<div class="r_title"></div>' +
    '<h2>Chèn ảnh</h2>' +
    '<a class="close b_close"><img src="'+ imgurl +'/icon_close.jpg" alt="123mua" /></a>' +
    '</div>' +
    '<ul class="tab_upload">' +
    '<li><a href="#up1" class="selected">Upload mới</a></li>' +
    '<li><a href="#up2">Chèn URL</a></li>' +
    '</ul>' +
    '<form class="clear">' +
    '<div id="up1" class="row_field" style="padding:5px 0">' +
    '<div id="tinyMceUploadImage"></div>' +
    '</div>' +
    '<div id="up2" class="row_field" style="padding:5px 0"><span class="label">Chọn ảnh mới:</span>' +
    '<input type="text" id="tinyMceUrlImage" class="field">' +
    '</div>' +
    '</form>' +
    '<div class="footer">' +
    '<div class="l_footer"></div>' +
    '<input class="b_insert" type="button" value="Chèn ảnh" />' +
    '<div class="r_footer"></div>' +
    '</div>' +
    '</div>';

    ImageUpload.boxy = new Boxy(html, {
        modal: true,
        afterShow: function()
        {
            //$(".boxy-wrapper .tab_upload").idTabs();
            $(".boxy-wrapper .tab_upload").idTabs(function(id)
            {
                switch (id)
                {
                    case '#up1':
                        $('.boxy-wrapper input.b_insert').hide();
                        break;
                    case '#up2':
                        $('.boxy-wrapper input.b_insert').show();
                        break;
                }
                return true;
            });
            
            $('#tinyMceUploadImage').photoUpload(
            {
                prefix: 'quick_',
                maxNumID: 1,
                ajaxFile: urlUpload,
                file_types: "jpg,gif,png",
                callback: ImageUpload.callback
            });
            
            $(".boxy-wrapper .b_insert").click(function(){
                var photoSrc = $('.boxy-wrapper #tinyMceUrlImage').val().trim();
                if (photoSrc)
                {
                    $('textarea.box_html').tinymce().execCommand('mceInsertContent',false, '<img src="'+ photoSrc  +'" />');
                }
                Boxy.get($("#popup")).hide();
            });
        }
    });
    
};

ImageUpload.callback = function(resp)
{
    switch(resp.error_code){
        case 0:
            currentID++;
            var photoSrc = resp.data.photo_src_d;
            $('textarea.box_html').tinymce().execCommand('mceInsertContent',false, '<img src="'+ photoSrc  +'" />');
            break;
    }

    Boxy.get($("#popup")).hide();
};

var Product = {};

Product.initEdit = function() {
	$("#btn_submit").click(function(){

            $("#frm_create").validate({
                rules: {
                    inp_title: {
                        required: true,
                        maxlength: 40
                    },
                    inp_summary: {                        
                        maxlength: 200
                    },
                    slt_region : "required",
                    slt_category : "required"
                },
                messages: {
                    inp_title: "",
                    inp_summary: "",
                    slt_region : "",
                    slt_category : ""
                },
                errorLabelContainer: "#warning-box",
                wrapper: "li",

                submitHandler: function(form) {
					$("#mce_0").html(myTopic.cleanupContent($("#mce_0").html()));
					form.submit();
					$("#btn_submit").attr('disabled', 'disabled');
				}
            });
        });
}