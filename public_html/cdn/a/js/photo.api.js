var ImageUpload = {
    boxy: undefined
};

ImageUpload.showPopup = function()
{
    var html = '<div id="popup" style="width:400px;margin:0 auto">' +
    '<div class="title">' +
    '<div class="l_title"></div>' +
    '<div class="r_title"></div>' +
    '<h2>Chèn ảnh</h2>' +
    '<a class="close b_close"><img src="'+ imgurl +'/icon_close.jpg" alt="123mua" /></a>' +
    '</div>' +
    '<ul class="tab_upload">' +
    '<li><a href="#up1" class="selected">Upload mới</a></li>' +
    '<li><a href="#up2">Chèn URL</a></li>' +
    '</ul>' +
    '<form class="clear">' +
    '<div id="up1" class="row_field" style="padding:5px 0">' +
    '<div id="tinyMceUploadImage"></div>' +
    '</div>' +
    '<div id="up2" class="row_field" style="padding:5px 0"><span class="label">Chọn ảnh mới:</span>' +
    '<input type="text" id="tinyMceUrlImage" class="field">' +
    '</div>' +
    '</form>' +
    '<div class="footer">' +
    '<div class="l_footer"></div>' +
    '<input class="b_insert" type="button" value="Chèn ảnh" />' +
    '<div class="r_footer"></div>' +
    '</div>' +
    '</div>';

    ImageUpload.boxy = new Boxy(html, {
        modal: true,
        afterShow: function()
        {
            //$(".boxy-wrapper .tab_upload").idTabs();
            $(".boxy-wrapper .tab_upload").idTabs(function(id)
            {
                switch (id)
                {
                    case '#up1':
                        $('.boxy-wrapper input.b_insert').hide();
                        break;
                    case '#up2':
                        $('.boxy-wrapper input.b_insert').show();
                        break;
                }
                return true;
            });
            
            $('#tinyMceUploadImage').photoUpload(
            {
                prefix: 'quick_',
                maxNumID: 1,
                ajaxFile: urlUpload,
                file_types: "jpg,gif,png",
                callback: ImageUpload.callback
            });
            
            $(".boxy-wrapper .b_insert").click(function(){
                var photoSrc = $('.boxy-wrapper #tinyMceUrlImage').val().trim();
                if (photoSrc)
                {
                    $('textarea.box_html').tinymce().execCommand('mceInsertContent',false, '<img src="'+ photoSrc  +'" />');
                }
                Boxy.get($("#popup")).hide();
            });
        }
    });
    
};

ImageUpload.callback = function(resp)
{
    switch(resp.error_code){
        case 0:
            currentID++;
            var photoSrc = resp.data.photo_src_d;
            $('textarea.box_html').tinymce().execCommand('mceInsertContent',false, '<img src="'+ photoSrc  +'" />');
            break;
    }

    Boxy.get($("#popup")).hide();
};

var maxNumID = 40;
(function($) {
    $.fn.extend({
        photoUpload: function(options) {    		
            var opt = $.extend({}, $.uploadSetUp.defaults, options);
            if (opt.file_types.match('jpg') && !opt.file_types.match('jpeg')) {
                opt.file_types += ',jpeg';
            }
            $this = $(this);
            new $.uploadSetUp(opt);
        },
        photoDel: function(id)
        {
            //$('#li_'+id).remove();            
        }
    });

    $.uploadSetUp = function(opt) {
        var elm_input;        
        opt.maxNumID = parseInt(opt.maxNumID);       
                
        /*Add global variable*/       
        maxNumID = opt.maxNumID;
                
        $('body').append($('<div style="display: none;"></div>').append($('<iframe width="0" height="0" src="about:blank" id="'+ opt.prefix +'myFrame" name="'+ opt.prefix +'myFrame"></iframe>')));
        elm_input = createElmInput(opt);
        $this.append(elm_input);
        $("#"+ opt.prefix +"myFrame").after($('<form target="'+ opt.prefix +'myFrame" enctype="multipart/form-data" action="' + opt.ajaxFile + '" method="POST" name="'+ opt.prefix +'myUploadForm" id="'+ opt.prefix +'myUploadForm" style="display:none"></form>'));
      
        //Init load file	
        initFile(opt);
        init(opt);
    };

    $.uploadSetUp.defaults = {
        // image types allowed
        file_types: "jpg,gif,png",
        // php script
        ajaxFile: "",
        maxNumID: 40,
        prefix: "",
        callback: null
    };

    function createElmInput(opt)
    {
        return '<div class="post_label_container"><label class="postLabel">Hình ảnh:</label></div><input rel="1" id="'+ opt.prefix  +'picture" name="'+ opt.prefix  +'picture" type="file" value="" />&nbsp;<span id="'+ opt.prefix +'spanLoading"></span>';
    };
	
    //Init load file
    function initFile(opt)
    {
        //Bind
        $('#'+ opt.prefix +'picture').bind('change', function(e)
        {
            if (checkFileType(opt, this.value))
			{
				var oldElement = this;
				var newElement = $(oldElement).clone();
				$(oldElement).attr('id', opt.prefix +'Filedata');
				$(oldElement).attr('name', 'Filedata');
				$(oldElement).attr('class', 'clone');
				$(oldElement).before(newElement);
				$('#'+ opt.prefix +'myUploadForm').empty();
				$(oldElement).appendTo('#'+ opt.prefix +'myUploadForm');
				$('#'+ opt.prefix +'spanLoading').html('<img width="16" height="11" src="'+imgurl+'/loading_small.gif" />');
				$('#'+ opt.prefix +'myUploadForm').submit();				
			}
        });
    }
			
    //check if file extension is allowed
    function checkFileType(opt, file_) {
        var ext_ = file_.toLowerCase().substr(file_.toLowerCase().lastIndexOf('.') + 1);
        if (!opt.file_types.match(ext_)) {
            alert('File ảnh không hợp lệ');
            return false;
        } 
        else return true;
    };
    
    function init(opt) {
        // execute event.submit when form is submitted
        $('#'+ opt.prefix +'myUploadForm').submit(function(){
            var bool = event.submit(this);
            initFile(opt);
            return bool;
        });        
                
        // function to handle form submission using iframe
        var event = {
            // setup iframe
            frame: function(_form) {
                $("#"+ opt.prefix +"myFrame")
                .empty()
                .one('load',  function() {
                    event.loaded(this, _form)
                });
            },
            // call event.submit after submit
            submit: function(_form) {
                event.frame(_form);
            },
            // display results from submit after loades into iframe
            loaded: function(id, _form) {            	
                var d = frametype(id);
                var data = d.body.innerHTML.replace(/^\s+|\s+$/g, '');                
                try
                {
                    try
                    {
                        var resp = eval('(' + data + ')');
						
                        if (opt.callback != null)
                        {
                            opt.callback(resp);
                        }
                    //callbackUpload(resp);
                    }
                    catch (ex){
                        alert(ex);
                    }
                    if(typeof rs == 'undefined')
                    {
                        var rs = null;
                    }
                }
                catch(ex)
                {
                    alert("Có lỗi xảy ra trong quá trình đăng ảnh. \nVui lòng thử lại lần nữa.");
                }
            },
            onerror: function(){
                try
                {
                }
                catch(ex)
                {
                    alert(ex);
                }
            }
        };		
        
        // check type of iframe
        function frametype(fid) {
            return (fid.contentDocument) ? fid.contentDocument: (fid.contentWindow) ? fid.contentWindow.document: window.frames[fid].document;
        };       
    };	
})(jQuery);

function callbackUpload(resp)
{		
    switch(resp.error_code){
        case 0:
            currentID++;
            var photo_src = resp.data.photo_src_d.replace('_574_574', '_182_182');
            photo_src = photo_src.replace('d.f', 't.f');
            var photo_src_110 = photo_src.replace('_182_182', '_130_130');
            var photoId = resp.data.user_id + '.' + resp.data.album_id + '.' + resp.data.photo_id+ '<<>>' + photo_src;
            $("#photo_thumb").attr('src', photo_src_110);
            $("#photo_data").val(photoId);
            //$('.pic').html('<img height="110px" width="110px" src="'+photo_src_110+'" alt="123mua" />'+'<input id="photo_data" type="hidden" value="'+photoId+'" />');
            $('#spanLoading').html('&nbsp;');
            break;
        case 1:
            $('#msg_error').show();
            $('#msg_error').html(resp.error_message);
            $('#spanLoading').html('&nbsp;');
            break;
        default:
            break;
    }
};