/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'en';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/ckfinder.html';

    config.filebrowserImageBrowseUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = 'http://haynhucnhoi.tv/cdn/a/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};




