<?php
error_reporting(E_ALL);
date_default_timezone_set('Asia/Ho_Chi_Minh');
define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', realpath(DOCUMENT_ROOT . '/../application'));
define('DATA_PATH', realpath(DOCUMENT_ROOT . '/../data'));
define('LOGGERS_PATH', realpath(DOCUMENT_ROOT . '/../logs'));
define('LIBS_PATH', realpath(DOCUMENT_ROOT . '/../library'));
define('PAGES_PATH', realpath(DOCUMENT_ROOT . '/static_pages'));

require_once (APPLICATION_PATH . '/configs/defines.php');
require_once (APPLICATION_PATH . '/configs/emoticons.php');
require_once (APPLICATION_PATH . '/configs/debug.php');

if ($_SERVER['Site_ENV'] == 'production') {
    ini_set('session.gc_maxlifetime', 604800);
    ini_set('session.save_handler', 'memcache');
    ini_set('session.save_path', 'tcp://localhost:11211?persistent=1&weight=1&timeout=1&retry_interval=15');
    define('STATIC_PATH', realpath(DOCUMENT_ROOT . '/../public_html/cdn'));
    define('UPLOAD_PATH', realpath(DOCUMENT_ROOT . '/../public_html/cdn/uploads'));
} else {
    define('STATIC_PATH', realpath(DOCUMENT_ROOT . '/../public_html/cdn'));
    define('UPLOAD_PATH', realpath(DOCUMENT_ROOT . '/../public_html/cdn/uploads'));
}

if (isset($_GET['gensitemap'])) {
    $path = realpath(DOCUMENT_ROOT . '/../script/cron/sitemap_generator.php');
    $command = 'php ' . $path;
    echo exec($command) . "\n<br/>";
    echo "done. path: " . $path;
    exit;
}

if (!isset($_COOKIE['is_hnn_mobile'])) {
    include_once LIBS_PATH . '/My/Helper/MobileDetect.php';
    $mobileDetect = new My_Helper_MobileDetect();
    $isMobile = $mobileDetect->isMobile() ? 1 : 0;
    $_COOKIE['is_hnn_mobile'] = $isMobile;
    setcookie("is_hnn_mobile", $isMobile, time() + 3600, '/');
}

$isMobile = $_COOKIE['is_hnn_mobile'];

set_include_path(implode(PATH_SEPARATOR, array(LIBS_PATH, get_include_path())));

////cache html
//require_once 'Zend/Application.php';
//$application = new Zend_Application(APPLICATION_ENVIRONMENT);
//// Zend_Cache_Frontend_Page
//$pageCache = Zend_Cache::factory(
//    'Page', 'File',
//    array(
//        'caching' => true,
//        'lifetime'=>300,
//        'ignore_user_abort' => true,
//        'debug_header' => true,
//        'default_options' => array(
//            'cache' => true,
//            'cache_with_cookie_variables' => true,
//            'cache_with_get_variables' => true,
//            'cache_with_session_variables' => true,
//            'cache_with_files_variables' => true,
//            'cache_with_post_variables' => true
//        ),
//        'regexps' => array(
//            '^/admhnn01/adm_index/' => array('cache' => false),
//        ),
//    ),
//    array(
//        'cache_dir' => APPLICATION_PATH.'/storage/cache',
//        'file_name_prefix'=>'pagecache'
//    )
//);
//$uri      = explode('/', $_SERVER['REQUEST_URI']);
//
//$cacheKey = array();
//foreach ($uri as $value) {
//    if (empty($value)) {
//        continue 1;
//    }
//    $cacheKey[] = str_replace('.', '', str_replace('-', '', $value));
//}

try {
    $options = array(
        'bootstrap' => array(
            'path' => APPLICATION_PATH . '/Bootstrap.php',
            'class' => 'Bootstrap'
        ),
        'phpSettings' => array(
            'display_startup_errors' => (APPLICATION_ENVIRONMENT != 'production') ? 1 : 0,
            'display_errors' => (APPLICATION_ENVIRONMENT != 'production') ? 1 : 0
        )
    );
    require_once 'Zend/Application.php';
    $application = new Zend_Application(APPLICATION_ENVIRONMENT, $options);
    //Display
    $application->bootstrap()->run();
} catch (Zend_Exception $exception) {

    if ('production' == APPLICATION_ENVIRONMENT) {
        if (My_Zend_Globals::getAltIp() == '115.77.136.7') {
            print_r($exception->getMessage());
        } else {
            header( 'Location: '.BASE_URL.'/error/pagenotfound');
        }
    }
    else {
        print_r($exception->getMessage());
    }
}
