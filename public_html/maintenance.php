<?php
header("HTTP/1.1 503 Service Temporarily Unavailable");
header("Status: 503 Service Temporarily Unavailable");
header("Retry-After: 3600");
?>
<html>
<head>
    <meta charset="utf-8" />
    <title>Hay Nhức Nhói đang bảo trì</title>
    <meta name=”robots” content=”none” />
</head>
<body>
<p style="text-align: center"><img src="cdn/images/maintenance.png" alt="Server đang được mang đi rửa. Mời bạn quay lại sau..." /></p>
</body>
</html>